
require "redcarpet"
set :markdown_engine, :redcarpet
set :markdown, no_intra_emphasis: true,
               tables: true,
               fenced_code_blocks: true,
               autolink: true,
               strikethrough: true,
               space_after_headers: true,
               superscript: true,
               lax_spacing: true,
               with_toc_data: true

page "články/index.html", :layout => :articles
page "články/*/index.html", :layout => :articles
page "články/*", :layout => :article
page "admin/*", :layout => false
page '/*.xml', layout: false

require "blog"
activate :blog, latest: '2024-2025', previous: '2023-2024'

# Build-specific configuration
set :css_dir, 'public/themes/morasice/css'
set :js_dir, 'public/scripts'
set :images_dir, 'public/images'

configure :build do
  activate :minify_html
end
after_build do
  FileUtils.remove_dir('build/admin/data')
end
