FROM ruby:2.5.9

RUN curl -sL https://deb.nodesource.com/setup_13.x -o nodesource_setup.sh  && \
    bash nodesource_setup.sh && \
    apt install nodejs

ADD ./data /app/sokolmorasice/data
ADD ./source /app/sokolmorasice/source
COPY ./blog.rb ./config.rb ./Gemfile ./Gemfile.lock ./netlify.toml /app/sokolmorasice/

WORKDIR /app/sokolmorasice

RUN bundle install --without debug
EXPOSE 4567
