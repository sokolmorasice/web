
## 1) generate json

**! depends on local scortes installation**

| Category | Command                                                                                                                  |
| -------- |--------------------------------------------------------------------------------------------------------------------------|
| Muži - podzim | `DATE="2024-08-01" COMPETITION="533A1A" CATEGORY="muzi" bin/news/generate-json`                                          |
| Pohár - podzim | `DATE="2024-08-01" COMPETITION="533Z1A" CATEGORY="pohar" bin/news/generate-json`                                         |
| Žáci st. - podzim | `DATE="2023-08-01" COMPETITION="533E1A" CATEGORY="zaci.st" MY_TEAM="Horní Újezd" IMAGE="novinky/2019-2020/zaci-starsi" bin/news/generate-json` |
| Žáci ml. - podzim | `DATE="2024-08-01" COMPETITION="533F1A" CATEGORY="zaci.ml" IMAGE="novinky/2019-2020/zaci-mladsi" bin/news/generate-json` |
| Muži - jaro | `DATE="2023-12-31" COMPETITION="533A1A" CATEGORY="muzi" OFFSET="13" bin/news/generate-json`                              |

## 2) generate articles

```bash
DATE="2018-08-01" COMPETITION="533A1A" CATEGORY="muzi" bin/news/generate-json
# JSON: bin/news/muzi-2018-07-05.json
# Generate articles:

bin/news/generate-articles "bin/news/2018-07-05.json"
Missing images:
/public/images/zapasy/mladejov-morasice.png
/public/images/zapasy/horni-ujezd-morasice.png

# Show hidden articles:
$ nano blog.rb
   :minSize, 0
   is_visible: true

# Check articles:
bundle exec middleman
```