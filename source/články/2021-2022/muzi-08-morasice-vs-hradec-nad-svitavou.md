---
title: Konečně radost
date: 2021-09-26
category: muzi
image: /public/images/zapasy/morasice-hradec-nad-svitavou.png
h1: Morašice vs Hradec n. S
sestava: J. Špinar - V. Tměj, M. Ropek (M. Frank), D. Štancl, M. Černohorský -
  J. Vít (Z. Skala), M. Chmelík, T. Nádvorník, D. Ondráček (B. Válek) - V.
  Štancl, F. Bureš (J. Kubeš)
goly: F. Bureš 2 (na obě V. Štancl), M. Chmelík (F. Bureš), V. Štancl (J. Vít),
  J. Kubeš (D. Ondráček)
zlute: J. Vít
cervene: null
multimedia: https://drive.google.com/drive/folders/1Uhh_faqYVXaCBb0UYnA_venyp8dvTINN
scortes: https://scortes.rozpisyzapasu.cz/api/533A1A/2021/Morašice/3b230e3e-1cb6-47e9-a18b-46780d2b45cb.html
---
V nedělním zápase proti Hradci nad Svitavou dosáhl významného milníku náš hrající trenér Martin Ropek, který nastoupil k 1000. zápasu v morašickém dresu. 91 jich odehrál za žáky, 96 za dorost a zbytek za muže. Se všemi kategoriemi dokázal vyhrát soutěžní ročník, byl přímým aktérem nejlepší éry našeho áčka, v sezóně, kdy muži dosáhli historického třetího místa v A třídě, byl nejlepším střelcem mužstva. Na danou metu dosáhl jako druhý nejmladší v naší historii, rychleji se to povedlo pouze Josefu Kopeckému, pomaleji nikomu. V těch 1000 zápasech nastřílel celkem 136 gólů, víc nedokázal vsítit žádný z 9 Ropků, kteří se mihli v našich dresech. Snad mu zdraví umožní jeho statistiky pěkně zaokrouhlit -  4 góly do mužské stovky a 183 zápasů do mužské tisícovky.

Tolik pro ty, kteří propásli nebo neslyšeli náš předzápasový rozhlas. Ti, kteří nestihli dostat pivo před výkopem, zase propásli první náznak naší šance, když hned v první minutě dostal Ondráček míč za obranu, ale šlápl si na něj. Po 8 minutách rychle hodil Tměj dlouhý aut za obranu, na hrot posunutý V. Śtancl našel ve vápně Bureše a s jeho zblokovaným pokusem si mezi nedůraznými obránci protančil až ke gólu Chmelík. Nově složená útočná dvojice si svou spolupráci zopakovala o pár minut později, když byl Bureš s přispěním nevystoupivšího beka vyslán do sólového úniku, který k úlevě publika přesně zakončil skrytou placírou na přední tyč. Po čtvrthodině už to bylo 3:0, když si na přímák Víta ze strany naskočil V. Štancl a na jeho koníček byl hostující gólman krátký. Vidina snadných průniků za obranu nás trochu vybláznila k několika ukvapeným rozehrávkám, náš střelecký půst tak trval více než čtvrt hodiny, kdy jednoduchý odkop Špinara a prodloužení V. Štancla nabídl další nájezd Burešovi, který chladnokrevně přeloboval vystoupeného gólmana přímo do vinglu. Rozmnožit náš účet mohl po přímáku na zadní tyč i oslavenec Ropek, ale po nepřesné hlavičce byl hlavně rád, že mu absolvovaný střet vydržela žebra. Hosté nás ohrozili jedinkrát, když po Vítově zbytečném faulu před vápnem nestříleli z nařízeného volného kopu vůbec zle, ale nerozchytaný Špinar se k tyči za zdí dokázal složit včas. V samém závěru jsme mohli zaokrouhlit poločasového bůra, jenže Ondráček si za obranou znovu nerozuměl s přímočarostí, jeho přičiněním vzniklá Burešova šance na hattrick zblízka skončila na hostujícím gólmanovi a na následující odraz nezvládl před prázdnou bránou správně zareagovat V. Štancl.

Naše často zbrklé řešení přechodové fáze pokračovalo i po změně stran, ale přeci jenom z toho vyplynuly dvě po gólu volající pozice za obranou, ale V. Štancl dal trestuhodně hodně času hostujícím obráncům k bloku jeho střely a Ondráček svým příliš vysokým projektilem naopak nedal šanci ke zblokování nikomu. Hosté nás pak na chvíli drželi na naší půlce, ale Špinar si připsal maximálně dva lehčí zákroky. Z útlumu nás dostala akce, při níž Nádvorník vyslal průnikovku do Ondráčkova běhu, ten se rozhodl už nepokoušet štěstěnu a nezištnou přihrávkou daroval při sólovém postupu gól střídajícímu Kubešovi. Že to byla v 65. minutě poslední gólová akce zápasu je trochu škoda, ale chvíli trvalo než jsme přizpůsobili kombinaci rozhodnutému výsledku a vystřídavší se trenér, tak z klidu lavičky viděl jen pár zazděných přihrávek z křídelních pozic.