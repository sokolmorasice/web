---
title: Hladká výhra
date: 2021-09-19
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Březová
sestava: Lukáš Mach – Štěpán Řejha, Ondřej Kusý, Jan Kvapil, Josef Rosypal, Vít
  Famfulík, Jan Kopecký, Vojtěch Andrle, Matyáš Bulva, Filip Flídr
goly: Vojtěch Andrle 2x, Jan Kopecký, Josef Rosypal, vlastní
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2021/Horní
  Újezd/Morašice/dd4b697f-15d8-4eb6-8b59-5f04049d6115.html
---
Čtvrté utkání sezony jsme odehráli v neděli dopoledne na domácím hřišti v Horním Újezdě proti Březové. Do zápasu jsme vstoupili aktivněji a začali jsme se dostávat do šancí. V 6. minutě utkání rozehrával Vojta Andrle rohový kop a obránce si nešťastně srazil míč do vlastní branky. Po tomto gólu jsme měli spoustu šancí, které jsme nedokázali proměnit. Soupeř si nedokázal vytvořit téměř žádné šance a když už se mu povedlo vstřelit gól, tak nebyl uznán, jelikož útočící hráč clonil v ofsajdu našemu brankaři. Do dvoubrankového vedení jsme se dostali ve 24. minutě po důrazu Vojty Andrleho ve vápně. Do konce poločasu již žádný gól nepadl. Druhý poločas jsme začali opět aktivně, ale stejně jako v prvním poločase nás trápila koncovka. Do vedení 3:0 jsme se dostali ve 39. minutě po pěkné střele Honzy Kopeckého. I nadále jsme přehrávali soupeře a ve 46. minutě jsme se dostali již do čtyřbrankového vedení po dorážce Vojty Andrleho. Poslední gól utkání vstřelil Josef Rosypal v 50. minutě po pěkné střele na zadní tyč. Příští utkání hrajeme 26. 9. ve 13:30 v Městečku Trnávka.