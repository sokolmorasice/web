---
title: Jednou zlatí, jednou stříbrní
date: 2022-05-16T09:32:34.405Z
category: pripravka
image: /public/images/uploads/logo.png
---
# Starší přípravka

Další díl našich turnajů nás zavedl do malebných kopců Jevíčska. Hřiště krásně připravené a trávník vzorně sestřižen. Sešli jsme se v hojném počtu.

**Sestava:** Martina Famfulíková - Samuel Vomočil, Sára Vomočilová, Vojtěch Huryta, František Flídr, Matěj Kovář, Michal Kovář, David Klusoň, David Boček, Dominik Prokop, Antonín Krejsa

**Morašice / Horní Újezd - Jevíčko / Jaroměřice dívky  3:0 (0:0)**

Hned na začátek nás čekala válka – Dívčí válka. František Ringo Čech by se divil, jak se dá zažít jeho známé dílo přímo na hřišti. Děvčata z Jaroměřic/Jevíčka jsou fyzicky velmi dobře připravené a nebojí se hrát více do těla, na což jsme v prvních minutách neznali odpověď. Postupem času se nám dařilo hrát více náš styl a využívat naše šance.

**Branky:** Vojtěch Huryta, František Flídr, David Boček

**Morašice / Horní Újezd - Jevíčko  5:4 (3:3)**

O pár minut později nás čekal další lítý boj a opět s místním týmem z Jevíčka. Oboustranně velmi rychlý duel, kde útoky obou týmu převyšovaly obrany. Na každý vstřelený gól soupeř našel ihned odpověď a opačně. Takto se to přelévalo až do závěrečného hvizdu, kde šťastnějším týmem jsme byli my. 

**Branky:** Vojtěch Huryta 3, František Flídr, David Boček

**Morašice / Horní Újezd - Čistá  4:2 (1:1)**

Třetí duel se nesl v podobném duchu jako ty dva předchozí. Tým Čisté je velmi dobře organizovaný v obraně s rychlými brejky. Několik našich hráčů se už potýkalo s bolestivými šrámy z předchozích střetů, proto náš herní projev do poločasu znatelně strádal. S nástupem do druhé půle se nám podařilo rychle navýšit vedení a uhájit ho až do závěrečného, pro nás šťastného konce. 

**Branky:** Vojtěch Huryta, František Flídr, Antonín Krejsa, Matěj Kovář

**Další výsledky turnaje:** TJ SK Jevíčko – Čistá 3:2, Jaroměřice/Jevíčko dívky – Čistá  0:7, TJ SK Jevíčko - Jaroměřice/Jevíčko dívky 8:4.

**Tabulka turnaje:**

**1.Horní Újezd / Morašice (9b, 12:6)**

2.TJ / SK Jevíčko (6., 15:11)

3.TJ Sokol čistá (3b., 11:7)

4.Jaroměřice/Jevíčko dívky (0b., 4:18)

autor: Martin Kovář

# Mladší přípravka

Na domácím turnaji se nám sešly všechny děti, které v současnosti hrají turnaje mladší přípravky. V počtu 13 hráčů se jen těžko dají uspokojit představy a přání všech o čase stráveném na hřišti, snad nikdo neodcházel zklamaný. Třetí jarní dějství bylo zatím nejkvalitněji obsazeným turnajem, kterého jsme se v druhé polovině sezony účastnili.

**Sestava:** Anna Vopařilová - Jaroslav Chaun, Adam Bartoš, Julie Vodehnalová, Tereza Hlušičková, Miriam Vomočilová, Jan Langr, Tereza Veselíková, Kryštof Valentín, Štěpán Kovář, Patrik Vurkminovič, Ivo Langr

**Horní Újezd/Morašice – Jevíčko 9:2 (5:0)**

– skvělý vstup do turnaje, na našich dětech byla vidět chuť odčinit nezdar z minulého týdne, postupně jsme se dostali do vedení 9:0, soupeř vstřelil své čestné branky až v závěru

**Branky:** Jaroslav Chaun 3, Patrik Vurkminovič 2, Miriam Vomočilová 2, Tereza Veselíková, vlastní

**Horní Újezd/Morašice – Čistá 2:5 (1:2)**

– v zápase jsme vstřelili první gól, ale ještě do poločasu silný soupeř skóre otočil ve svůj prospěch, tým se snažil, ale na bodový zisk to nestačilo

**Branky:** Jan Langr, vlastní

**Hradec n. S. – Horní Újezd/Morašice 3:3 (1:2)**

– utkání o 2. místo na turnaji, nepodařilo se nám navázat na kvalitní výkony z dvou předchozích zápasů, i tak jsme po většinu času vedli, ale domácí ve druhé půli vyrovnali, škoda dvou našich velkých šancí v závěru, kterými jsme mohli rozhodnout

**Branky:** Jaroslav Chaun 2, Štěpán Kovář

Jsem rád, že jsme se po nevydařeném domácím turnaji zvedli a ukázali, že fotbal hrát umíme. Nejvíce nás potěšila Anička Vopařilová, která na dnešním turnaji musela zaskočit v brance a předvedla skvělý výkon. Svými výkony mužstvo i nadále táhne Jára Chaun. Pochvalu ale zaslouží všichni hráči. Díky lepšímu skóre jsme nakonec obsadili 2. místo.

**Tabulka turnaje:**

1.Čistá (9 bodů)

**2.Horní Újezd/Morašice (4 b.)**

3.Hradec n. S. (4 b.)

4.Jevíčko (0 b.)

autor: Mgr. Zdeněk Beneš