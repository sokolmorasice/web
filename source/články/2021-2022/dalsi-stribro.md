---
title: Další stříbro
date: 2022-06-05T19:11:19.871Z
category: pripravka
image: /public/images/uploads/logo.png
---
# Mladší přípravka

5.6. - Morašice

Sestava: Tereza Hlušičková - Jaroslav Chaun, Martin Boštík, Patrik Vurkminovič, Kryštof Valentín, Štěpán Kovář, Julie Vodehnalová, Miriam Vomočilová, Tereza Veselíková, Jan Langr, Ivo Langr

**Horní Újezd/Morašice – Dlouhá Loučka 8:1 (4:1)**

– do zápasu jsme vstoupili ospale a nevýrazně, až závěr první půle se přiblížil představám trenérů, ve druhém poločase už byla převaha na naší straně

**Branky:** Patrik Vurkminovič 3, Kryštof Valentín 2. Jan Langr, Jaroslav Chaun, Miriam Vomočilová

**Horní Újezd/Morašice – Litomyšl B 5:3 (2:2)**

– vyrovnaný a napínavý zápas, v poločase nerozhodný výsledek, brzy po pauze jsme dostali gól na 2:3, ale díky velkému nasazení jsme dokázali výsledek otočit

**Branky:** Patrik Vurkminovič 2, Jaroslav Chaun 2, Kryštof Valentín

**Horní Újezd/Morašice – Bystré 1:17 (1:7)**

– kdo čekal bitvu o první místo na turnaji, musel být brzy zklamán, Bystré si krátce po začátku vytvořilo pohodlný náskok a svojí pohlednou a nápaditou hrou nenechalo nikoho na pochybách, kdo je nejfotbalovějším týmem letošního ročníku naší soutěže

**Branka:** Jaroslav Chaun

**Tabulka turnaje:**

1.Bystré (9 b.)

**2.Horní Újezd/Morašice (6 b.)**

3.Litomyšl B (3 b.)

4.Dlouhá Loučka (0 b.)

I přes nelichotivý výsledek posledního zápasu musí panovat s turnajem spokojenost. Našim hráčům je třeba poděkovat za nasazení, což je hlavní důvod úspěchu. Již počtvrté za sebou jsme obsadili 2. místo na turnaji. Posledním mistrovským turnajem sezóny bude Pohár OFS Svitavy v sobotu 16.6. v Čisté.

autor: Mgr. Zdeněk Beneš