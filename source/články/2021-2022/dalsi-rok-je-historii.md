---
title: Další rok je historií
date: 2021-12-27T20:13:09.257Z
category: obecne
image: /public/images/uploads/logo.png
---
Pokud patříte mezi pár jedinců, které zajímají historické statistiky našeho fotbalového oddílu, je tento článek určen pro vás. Pokud ne, plácejte se do hlavy v tichosti, ať nerušíte okolí.

Rok 2021 byl ve znamení tisícovek. Josef Kopecký založil klub mužských tisícovkářů a jubilejní [zápas](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2020-2021/pohadka-tisice-a-jednoho-mace) oslavil gólem a asistencí. Martin Ropek se k němu přidal do klubu morašických tisícovkářů a kromě upomínkového daru dostal přídavkem i málo vídanou poločasovou [kanonádu](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2021-2022/muzi-08-morasice-vs-hradec-nad-svitavou). 

Bohumilu Válkovi chybí po podzimu pouze jediný gól, aby se vyrovnal nejlepšímu střelci naší historie Petru Kutovi a zároveň se posunul historickým pořadím v odehraných zápasech (v mužích se přehoupl přes 400 zápasů a celkově už je 7. nejvytíženějším hráčem). 

Milan Chmelík bude na svých umístěních mezi mužskými i celkovými vytrvalci během následujících tří let těžko něco měnit, ale svou zvýšenou kadencí posledních sezón už se prostřílel před Vojtěcha Štěpána do elitní třicítky střelců (když vezmeme v potaz, že nehrál za žáky a v mužích strávil dekádu na stoperu, je to záviděníhodná potence).

Jan Špinar přeskočil Němce Jana na pátém místě co do odchytaných zápasů za áčko a na jaře bude atakovat kótu 100.

Historické tabulky dorostu se bohužel nemají jak hýbat, nahlíženou přísnou statistickou optikou není dobrou zprávou ani to, že tabulky kategorií žáků a elévků se plní dětmi z Horního Újezdu, byť jim neochvějně vládnou příslušníci více či méně legendárních rodů - Famfulíková, Válek, Kopecký, Štancl.

Více, včetně Vás, pokud jste jeden z 647 lidí, co za nás kdy nastoupili, najdete přímým kliknutím [zde](https://docs.google.com/spreadsheets/d/1leVjJu7cU-j48mBYp9hx7PgFwTmH01byvIbeJqPWgPo/edit#gid=1378160355). Přístup ze stránky jinak vede [tudy](https://sokolmorasice.cz/o-klubu/) a těm, co mají nemají rádi excel, ale mrtvá data, nabízíme [toto](https://drive.google.com/drive/folders/1W-URUL12I_eHnrk5WVquAKwv4S6Ef2DW).