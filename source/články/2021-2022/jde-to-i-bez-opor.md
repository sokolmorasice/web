---
title: Jde to i bez opor
date: 2021-09-19T10:36:31.729Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Starší přípravka

Před odjezdem na turnaj do Němčic píplo několik omluvných sms zpráv, ze kterých vyplynulo, že se musíme obejít bez Fandy Flídra, Vojty Huryty a Dominika Prokopa a tak bylo jasné, že oproti poslednímu turnaji na Horním Újezdě budeme oslabeni.

**Sestava:** Martina Famfulíková, Šimon Flídr, Sára Vomočilová, Samuel Vomočil, David Boček, David Klusoň, Matyáš Lněnička, Jarda Chaun a Michal Kovář.

**HÚ / Morašice – Opatov 5 : 2** -utkání mělo vcelku vyrovnaný průběh, když už se zdálo, že jsme si vytvořili uspokojující náskok, tak soupeř vstřelil kontaktní branku a zápas jsme rozhodovali v jeho závěru. 

**Branky:** Samuel Vomočil 2, Šimon Flídr, David Klusoň, Jarda Chaun

**HÚ / Morašice – Němčice 5 : 4** - velmi pěkný, bojovný a vyrovnaný zápas z obou stran se šťastným koncem pro nás. Soupeř šel po našich chybách do vedení, my jsme dokázali výsledek otočit, ale za malou chvíli bylo srovnáno. Oba týmy se snažili strhnout vítězství na svojí stranu ale nakonec jsme se radovali my, jelikož na naší poslední branku již soupeř nestihl odpovědět. 

**Branky:** Šimon Flídr 3, Samuel Vomočil, Jarda Chaun

**HÚ / Morašice – Bystré 2 : 0** - závěrečný zápas jsme sehráli s týmem, se kterým jsme  na Horním Újezdě remizovali a soupeř díky jednomu vstřelenému gólu navíc bral tehdy první místo. Takže motivace na „srovnání účtu“ v týmu byla a zápas tomu i tak odpovídal. Oba týmy jsou vyrovnané a otázkou tak bylo kdo vstřelí první branku. Toto se povedlo Šimonu Flídrovi., na kterého navázal precizně provedeným trestňákem Jarda Chaun a my tak mohli slavit celkové vítězství.

Všem patří poděkování za odvedený výkon, bojovnost a nasazení. Tým si dokázal, že i když není kompletní, může vítězit a právě takovéto vyrovnané zápasy jako na tomto turnaji, jsou pro ně velkým přínosem. Vyzdvihnout musím však tentokrát jistý výkon Martiny Famfulíkové ( chytila pokutový kop) a Šimona Flídra ( kromě vstřelených branek má na svém kontě několik gólových asistencí a 3 nastřelené tyče soupeřových branek).

autor: František Bartoš