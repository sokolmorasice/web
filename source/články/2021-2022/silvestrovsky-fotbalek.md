---
title: Silvestrovský fotbálek
date: 2021-12-21T06:14:56.619Z
category: obecne
image: /public/images/uploads/logo.png
---
TJ Sokol Morašice děkuje všem, kteří se v roce 2021 nachomýtli k chodu klubu. Trenérům a vedoucím za organizaci tréninků a zápasů, sekáčům za přípravu hřiště, rodičům a dětem za spolupráci a docházku, fotografům za obrazovou dokumentaci, obci za zázemí a podporu, sponzorům za každý žluťásek, restauratérům a uzenářův za občerstvení, fanouškům za přízeň i těm, kteří v tomhle výčtu omylem chybí, za to, na co jsme zapomněli. Přejeme jim klidné prožití vánočních svátků a hodně štěstí, zdraví a společných zážitků v novém roce. 

Tím posledním zážitkem v roce 2021 bude tradiční silvestrovský fotbálek a následný nápoj dle vlastního dovozu (i když možná bude i hospoda), vykopáváme v 10:00. Kdo příchod vyloženě plánuje, může vyplnit svou účast nebo doplnit svoje jméno [zde](https://docs.google.com/spreadsheets/d/19JiZ2z0OcaOwEy1E_U_44N_X9VA9zgg_Qm_OB8J1dgU/edit#gid=0), ať víme, na co a koho se těšit.