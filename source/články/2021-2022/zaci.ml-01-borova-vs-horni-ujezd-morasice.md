---
title: Začátek se špatnou koncovkou
date: 2021-09-02
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Borová vs Horní Újezd/Morašice
sestava: Martina Famfulíková – Ondřej Kusý, Jan Kvapil, Jan Kopecký, Lukáš Mach,
  Kryštof Tomšíček, Matouš Zach, Josef Rosypal, Matyáš Bulva, Filip Flídr,
  Štěpán Řejha
goly: Lukáš Mach
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2021/Horní
  Újezd/Morašice/66cdeaf7-6877-4faf-ab1d-cc33d0b35816.html
---
K prvnímu utkání sezony jsme vyrazili stejně jako v minulém roce do Telecího, kde jsme hráli proti týmu Borové. Od úvodních minut zápasu jsme hráli poměrně pěkný kombinační fotbal a mírně jsme přehrávali soupeře. V 5. minutě utkání jsme se dostali do vedení díky krásně střele z dálky od Lukáše Macha. Bohužel už v 7. minutě soupeř vyrovnal po podklouznutí naší brankářky, kdy následně útočník vstřelil gól do prázdné branky. Do konce poločasu jsme měli na hřišti převahu, ale bohužel jsme všechny střely mířili do soupeřova brankaře anebo mimo bránu, proto poločas skončil 1:1. Druhý poločas jsme zahájili v podobném stylu, jako jsme ukončili ten první, takže jsme měli převahu, ale bohužel jsme nedokázali žádnou šanci proměnit. V 58. minutě utkání se soupeř dostal do ojedinělé šance v druhém poločase a skóroval. Do konce utkání už žádný gól nepadl, a tak jsme první zápas smolně prohráli po tom, co nám dala Borová lekci v produktivitě

autor: Tomáš Chadima