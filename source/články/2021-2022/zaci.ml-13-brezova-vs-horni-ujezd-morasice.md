---
title: Poločas napětí
date: 2022-04-17
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Březová vs Horní Újezd/Morašice
sestava: Martina Famfulíková - Ondřej Kusý, Matyáš Bulva, Filip Flídr, Jan
  Kvapil, Jan Kopecký, Vojtěch Andrle, Lukáš Mach, Vojtěch Huryta, Matěj Rejman,
  Šimon Flídr
goly: Filip Flídr 2, Vojtěch Andrle, vlastní
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2021/Horní
  Újezd/Morašice/8bf1e4c0-fce3-4872-8159-f0a25eadb40d.html
---
V nedělním utkání jsem soupeře z Březové dostali od začátku pod tlak, ale za celý poločas se nám nepovedlo vstřelit branku. To naštěstí hned po přestávce zlomil Filip Flídr a po dalších brankách v rychlém sledu jsme zápas ovládli a vrátili se do čela tabulky, které budeme hájit v neděli od 10:00 na hřišti v Morašicích proti spojenému družstvu Litomyšle a Čisté.