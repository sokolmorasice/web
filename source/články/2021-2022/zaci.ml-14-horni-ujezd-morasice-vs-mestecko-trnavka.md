---
title: Nadále v čele
date: 2022-04-23
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs M. Trnávka
sestava: Martina Famfulíková - Matyáš Bulva, Filip Flídr, Jan Kvapil, Šimon
  Flídr, Josef Rosypal, Vojtěch Andrle, Vít Famfulík, Ondřej Kusý, Lukáš Mach,
  Matěj Rejman, Vojtěch Štěpán, Jan Kopecký, František Flídr
goly: Ondřej Kusý 2, Vojtěch Andrle 2
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2021/Horní
  Újezd/Morašice/c58fe3bc-40d6-4e23-ace0-a265957a7bcb.html
---
Sobotní zápas proti tabulkově dotírajícímu družstvu Městečka Trnávky jsme rozehráli rozpačitě. Soupeř nás zaskočil svým napadáním. ale dva slepené góly Vojty Andrleho nás uklidnili. I když nám zpracování balónu a přihrávky občas dělaly problém, podařilo se nám do poločasu vstřelit ještě třetí branku. Když brzy po pauze přidal druhý gól i Ondra Kusý, bylo rozhodnuto. Mohli jsme protočit sestavu, aby si všichni pořádně zahráli, a v klidu kontrolovali výhru, díky které jsme udrželi první místo. To budeme v sobotu v 10:00 hájit na půdě druhých Sebranic.