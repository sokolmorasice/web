---
title: Zimní příprava mužů
date: 2021-12-21T06:38:30.301Z
category: muzi
image: /public/images/uploads/logo.png
---
Muži by za ideální morálky a vnější konstelace měli trénovat každé úterý a čtvrtek od 17:30 v tělocvičně a na přilehlých komunikacích. Předběžný plán přípravy zde:

**ČT 13.1. 17:30** - 1.trénink, poté každé úterý a čtvrtek

**PÁ 11.2. - NE 13.2.** - soustředění v Daňkovicích (zařiďte si dovolenou, očkování nebo prodělání)

**ČT 10.3. 17:30** - trénink na UMT v Litomyšli

**NE 13.3. 15:00 -** přátelák na UMT v Litomyšli

**ČT 17.3. 17:30** - trénink na UMT v Litomyšli

**SO 19.3. 15:00 -** přátelák na UMT v Litomyšli

**SO 26.3. - NE 27.3.** - přátelák na přírodním hřišti

**NE 2.4. 15:30** - mistrák v Jevíčku