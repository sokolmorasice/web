---
title: Kaňka na závěr
date: 2021-10-24T06:03:48.514Z
category: pripravka
image: /public/images/uploads/logo.png
---
K tomuto turnaji jsme nastupovali bez Davida Klusoně a Fandy Flídra a jak se později ukázalo, právě jeho pravidelné gólové příspěvky nám tentokráte chyběly.

**HÚ / Morašice – Opatov 0 : 3 -** odehráli jsme absolutně nejhorší zápas na podzim. Většina týmu podala průměrný až podprůměrný výkon a když k tomu připočteme neproměňování ani těch nejvyloženějších šancí, tak je výsledek zápasu naprosto spravedlivý.

**HÚ / Morašice – Němčice 1 : 0 -** z naší strany se výkon trochu zlepšil, ale o nějakém chválení nemůže být ani řeč, do standartních podzimních výkonů to mělo hodně daleko a zápasu by více slušela konečná remíza. 

Branka: David Boček.

**HÚ / Morašice – Sebranice 1 : 2**, z toho co jsme na tomto turnaji předvedli, to bylo asi to nejlepší, nebýt toho závěru, kdy jsme pár minut před koncem inkasovali vyrovnávací gól a se závěrečným hvizdem i ten co nás nakonec odsunul na konečné 3. místo. 

Branka: Vojta Huryta.

Celý turnaj se nám nepovedl a byl pravým opakem všech podzimních zápasů. Všechny naše střely, i ty z těch nejvyloženějších pozic, směřovaly do soupeřových brankářů, asi 6x jsme nastřelili břevno nebo tyč, prostě vstřelit branku byl pro nás tentokráte velký problém. Ve skutečnosti se však nic tak hrozného nestalo, prohry a slabší výkony k fotbalu prostě patří a je dobré, že i toto náš tým okusil. Takže všem hráčům a hráčkám děkuji jménem trenérů za krásné podzimní výkony, radost z fotbalu a těšíme se na jarní sezónu.

František Bartoš