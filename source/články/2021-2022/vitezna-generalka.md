---
title: Vítězná generálka
date: 2022-05-30T06:25:49.447Z
category: pripravka
image: /public/images/uploads/logo.png
---
# Mladší přípravka

Další dějství okresní soutěže mladší přípravky hostilo Telecí. Turnaj znehodnotily Jaroměřice, které bez omluvy nepřijely.

**Sestava:** Anna Vopařilová – Jaroslav Chaun, Julie Vodehnalová, Martin Boštík, Tereza Hlušičková, Tereza Veselíková, Jan Langr,  Kryštof Valentín, Štěpán Kovář, Ivo Langr

**Horní Újezd/Morašice – Březová 4:6 (3:3)**

– celkem brzy jsme vedli 2:0, jenže soupeř přidal a skóre otočil, v poločase byl stav nerozhodný, ve druhé půli byla Březová lepším týmem a zaslouženě vyhrála.

**Branky:** Jaroslav Chaun 2, Tereza Veselíková, Martin Boštík

Borová – Březová 4:4

**Borová – Horní Újezd/Morašice 2:4 (1:3)**

– znovu brzké vedení, soupeř sice vyrovnal, ale pak jsme se díky zodpovědnému výkonu dostali do tříbrankového vedení, korekce soupeře na 2:4 přišla až v závěru.

**Branky:** Tereza Veselíková 2, Jaroslav Chaun, Kryštof Valentín

**Tabulka turnaje:**

1.Březová (skóre 10:8, 4 body)

**2.Horní Újezd/Morašice (8:8, 3 b.)**

3.Borová (6:8, 1 b.)

Turnaj se odehrával v přátelské atmosféře. Náš tým opět zaslouží pochvalu za bojovnost, je vidět, že chuť do fotbalu našim dětem nechybí. Další turnaj odehrajeme v neděli 5.6. od 9:30 na domácím hřišti v Morašicích.

Mgr. Zdeněk Beneš

# Starší přípravka

Náš další turnaj přivítalo malebné městečko Bystré. Tato naše druhá městská štace po Jevíčku je pro nás poslední zkouškou před závěrečným turnajem u nás na domácím hřišti v Morašicích.  Hráče přišlo podpořit spousta starších i mladších rodin s dětmi, kterým bylo k dispozici občerstvení všeho druhu k překonání volného času před nedělním obědem. Opět v hojném počtu jsme byli odhodláni jim ukázat to nejlepší z našeho umění.

**Sestava:** Martina Famfulíková – Samuel Vomočil, Sára Vomočilová, Vojtěch Huryta, Šimon Flídr, František Flídr, David Klusoň, Antonín Krejsa Krejsa, Matěj Kovář, Michal Kovář.

**Horní Újezd/Morašice – Bystré 2:3 (1:0)**

Podle rozpisu zápasů jsme očekávali velmi těžký průběh turnaje. Hned na úvod jsme slízli místní tým. Od úvodního hvizdu jsme se snažili hrát náš už zajetý kombinační fotbal. Z několika nadějných šancí se nám podařilo využít jen jednu, přesto jsme do druhé půle odcházeli s uklidňujícím vědomím, že na soupeře v poho máme. Omyl. Dvakrát rychle po sobě jsme dáváním si přednosti před naší bránou darovali soupeři dva ušmudlané góly. Tím se soupeř i diváci probudili a zápas dostal úplně jiný náboj, než jsme chtěli. Tuto bitvu jsme ještě zdramatizovali vyrovnávacím gólem, ale následnou hrubou chybou v komunikaci darovali soupeři další nechtěný zásek. I přes náš velký závěrečný tlak jsme už nedokázali skórovat a soupeři tím darovali toto vítězství.

**Branky:** František Flídr, Matěj Kovář

**Horní Újezd/Morašice – Sebranice 9:1 (3:1)**

Před dalším dějstvím jsme si zašli vyříkat zklamání z předchozího nezdaru do soukromí místní kabiny. Přeci jen nás čekalo střetnutí s naším velkým rivalem ze Sebranic. Publikovatelné je jen to ,,hurá jdeme na ně‘‘. Vzájemné opatrné oťukávání nevydržela sledovat naše brankářka a krásnou tvrdou střelou přes půl hřiště prostřelila vše, co jí stálo v cestě. Tímto gólem se jí podařilo, jak další průběh zápasu ukázal, sestřelit sebevědomí soupeřova brankáře i odhodlání celého týmu a rozjet tím nevídaný brankostroj našich kluků. Skóre v poločase 3:1 tomu ještě nenasvědčovalo, ale to závěrečné, 9:1, umlčelo i soupeřova emotivního trenéra.

**Branky:** Vojtěch Huryta 4, Šimon Flídr 2, Martina Famfulíková, Samuel Vomočil, Michal Kovář.

**Horní Újezd/Morašice – Jaroměřice/Jevíčko Dívky  16:0 (8:0)**

Ve třetím utkání na nás čekala děvčata z Jaroměřic. Jejich bojovné odhodlání a nefér kousky jsme si živě pamatovali ještě z turnaje v Jevíčku. Porovnávání bolístek a modřin z tohoto střetnutí jsme rychle na střídačce rozehnali dřepy za lavičkou a kluci na hřišti prvními góly. Nadšení děvčat již bylo otupené předchozími prohranými zápasy. Naše kreativita v rozehrávce jim dělala velké problémy. Narůstající skóre po poločase vyhnalo z brány i naší Marťu do útoku. Vývoj ostatních zápasů nás motivoval hrát na skóre až do závěrečného hvizdu. Což se nakonec vyplatilo, protože přepočítání bodů a skóre v celkové minitabulce nám přiřadilo krásné a sladké vítězství.

**Branky:** František Flídr 5, Antonín Krejsa 3, Martina Famfulíková 2, Šimon Flídr 2, Matěj Kovář, Samuel Vomočil, Vojtěch Huryta, David Klusoň.

**Další výsledky turnaje:** Sebranice – Jaroměřice/Jevíčko Dívky 13:0 (6:0), Bystré  – Jaroměřice/Jevíčko Dívky  9:1 (3:0), Sebranice – Bystré  3:0 (1:0)

**Tabulka turnaje:**

**1.  Horní Újezd/Morašice           27:4    6 bodů**

2.  Bystré                                       12:6    6 bodů

3.  Sebranice                                 17:9    6 bodů

4.  Jaroměřice/Jevíčko Dívky     1:38    0 bodů

autor: Martin Kovář