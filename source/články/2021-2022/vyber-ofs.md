---
title: Výběr OFS
date: 2021-09-16T05:58:13.423Z
category: pripravka
image: /public/images/uploads/logo.png
---
Dne 15.9.2021 se v Českých Heřmanicích uskutečnil turnaj výběrů okresních fotbalových svazů v kategorii přípravek. 

Výběr OFS Svitavy se utkal s výběry OFS Ústí nad Orlicí, OFS Pardubice a OFS Chrudim. Týmy byly složené výhradně z chlapců a dívek hrajících okresní přípravkové soutěže a výběr OFS Svitavy reprezentovali mj. hráči našeho společného přípravkového týmu, a to:

\-Martina Famfulíková a Vojtěch Huryta z Morašic

\-František a Šimon Flídrovi z Horního Újezdu

Náš tým OFS Svitavy skončil na třetím místě se čtyřmi body za jedno vítězství, jednou remízu a závěrečnou porážku. Nicméně hráčky a hráči odvedli velmi bojovný výkon, za což jim děkuje tým trenérů OFS Svitavy:

Jiří Mokrejš

Jiří Procházka

Jiří Famfulík.