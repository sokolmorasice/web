---
title: Rodinný podnik
date: 2021-09-12T19:15:43.725Z
category: pripravka
image: /public/images/uploads/logo.png
---
Je trochu nadnesené říct, že druhý turnaj mladší přípravky pořádaly Morašice. Spíš se sluší říct, že to byla rodina Famfulíkových, která se postarala o vše od hraní po občerstvení, a poděkovat jim za to. 

Na turnaji jsme odehráli tři dobré zápasy. Proti Litomyšli jsme předvedli v prvním poločase výborný výkon, Bystré a Čistá byly urostlejší a fotbalovější, ale i tak od nás nikdo nic nevypustil. Pochvala za bojovnost pro celý tým. Vyzdvihnout bych chtěl Adama Bartoše, který statečně zaskočil v brance za nemocnou Terezu Hlušičkovou, Julii Vodehnalovou, která zalepila díru v obraně, a Patrika Vurkminoviče, který svůj první turnaj ozdobil dvěma góly. Snad nám budou děti dělat radost svými výkony i nadále.

**Sestava:** Bartoš A. – Chaun J., Vodehnalová J., Valentín K., Vurkminovič P., Vomočilová M., Kovář Š., Veselíková T., Langr J., Langr I.

**Výsledky:**

**Horní Újezd/Morašice – Litomyšl 8:1 (5:0)**

**branky:** Chaun J. 2x, Veselíková T. 2x, Valentín K., Kovář Š., Vurkminovič P., Vodehnalová J.

**Horní Újezd/Morašice – Čistá 3:10 (2:6)**

**branky:** Chaun J. 2x, Vurkminovič P.

**Horní Újezd/Morašice – Bystré 2:10 (1:6)**

**branky:** Valentín K., Chaun J.

**Tabulka turnaje:**

1.Bystré

2.Čistá

**3.Horní Újezd/Morašice**

4.Litomyšl „B“

\
Mgr. Zdeněk Beneš