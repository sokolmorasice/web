---
title: Ještě není konec
date: 2022-05-08
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Jaroměřice
sestava: Martina Famfulíková - Matyáš Bulva, Filip Flídr, Jan Kvapil, Ondřej
  Kusý, Josef Rosypal, Vojtěch Andrle, Jan Kopecký, Vít Famfulík, Lukáš Mach,
  Vojtěch Štěpán
goly: Vojtěch Andrle 3, Ondřej Kusý
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2021/Horní
  Újezd/Morašice/39586c36-3c90-4f47-aac3-c978daeea405.html
---
Do nedělního posledního zápasu základní části jsme vstoupili skvěle naladěni a hned v první minutě se ujali vedení. Hráli jsme pěkný fotbal plný krásných přihrávek s jedinou chybou - opět jsme neproměňovali šance. Až v závěru poločasu jsme díky dokonanému hattricku Vojty Andrleho soupeři nadvakrát odskočili na rozdíl dvou gólů a mohli si druhý poločas zahrát pěkný bojovný zápas plný šancí a krásných akcí. Ujala se z nich už jen jedna, což nemělo vliv na to, že jsme po základní části skončili na druhém místě. Do finálové skupiny spolu s námi postoupili Sebranice, Městečko Trnávka a Březová. S každým z nich se utkáme doma a venku, a tyto zápasy určí okresního přeborníka pro tento ročník.