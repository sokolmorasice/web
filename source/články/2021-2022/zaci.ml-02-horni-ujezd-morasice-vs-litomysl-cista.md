---
title: Čestné vítězství
date: 2021-09-05
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Litomyšl/Čistá
sestava: Lukáš Mach – Štěpán Řejha, Ondřej Kusý, Jan Kvapil, Josef Rosypal,
  Vojtěch Andrle, Kryštof Tomšíček, Matouš Zach, Matyáš Bulva, Filip Flídr
goly: Vojtěch Andrle 2, Filip Flídr, Ondřej Kusý
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2021/Horní
  Újezd/Morašice/a7e909d1-a847-407e-892d-9dea26a7d1bb.html
---
Druhý zápas sezony jsme odehráli v neděli dopoledne na domácím hřišti v Morašicích proti týmu Litomyšl/Čista. Hostující tým přijel pouze se sedmi hráči, takže se zápas odehrál v rámci fair play v počtu hráčů 6+1. Zápas jsme začali opět poměrně hezkým kombinačním fotbale, ale byla znát absence jednoho hráče na hřišti, s čímž jsme se poměrně dlouhou dobu vyrovnávali. Od začátku utkání jsme byli lepším týmem, ale nedokázali jsme se dostat do žádné vyložené šance a pokud jsme se do nějaké šance dostali, tak jsme ji nedokázali proměnit. Do vedení jsme se dostali v 17. minutě utkání, kdy se dokázal prosadit Filip Flídr po pěkně přihrávce od Vojty Andrleho. Ve 20. minutě jsme se dostali do dvougólového vedeni po krásné křižné střele Vojty Andrleho, kterému přihrával Ondra Kusý. O další čtyři minuty déle využil Ondra Kusý špatného postavení brankaře a střelou z dálky zvýšil už na 3:0. Více důležitého se už v prvním poločase nestalo. Druhý poločas začal postejně jako první, takže jsme se snažili kombinovat a byli jsme lepším týmem. V 37. minutě Vojta Andrle zvyšoval pěknou křižnou střelou už na 4:0. V té době už to vypadalo, že se zápas nemůže nijak zdramatizovat. O pár minut později se ale zranil hostující hráč a zápas se dohrával v počtu hráčů 5+1, v té době nás začal soupeř přehrávat, i když jsme si mysleli, že už musí fyzicky odejit, jelikož celý zápas nemohl střídat. V 50. minutě hostující tým snížil na 4:1 po špatné rozehrávce našeho brankaře. Do konce zápasu se už nic zásadního nestalo a utkání jsme vyhráli 4:1. Další zápas sezony odehrajeme 11. 9. 2021 ve 13:00 v Opatově.

autor: Tomáš Chadima