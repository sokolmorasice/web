---
title: První derby? Check!
date: 2022-04-10
category: muzi
image: /public/images/zapasy/morasice-horni-ujezd.png
h1: Morašice vs Horní Újezd
sestava: J. Špinar - D. Štancl, V. Štancl, J. Štěpán, K. Kutzer - B. Válek, M.
  Chmelík, T. Nádvorník (P. Lněnička), J. Vít (D. Ondráček) - M. Frank (Z.
  Skala, F. Halsbach), V. Novák
goly: J. Štěpán (M. Chmelík)
zlute: T. Nádvorník
cervene: null
multimedia: https://drive.google.com/drive/folders/1oGn9-G9mZhGncTaxzMvjlBtGxoxS-_nr
scortes: https://scortes.rozpisyzapasu.cz/api/533A1A/2021/Morašice/d715be46-d750-4f10-a6ef-060d632858ed.html
---
Na první domácí zápas se počasí s diváky ani hráči nemazlilo a polední sněhová přeháňka vsáknutá do hřiště předurčila, že derby s Horňákem bude víc o bojovnosti než fotbalovosti. A protože bylo té fotbalovosti opravdu málo a bojovnosti míň, než by se slušelo, není za první půlku o čem psát. My dostali jednu žlutou, hosté dvě, kopali jsme víc rohů, ale nejnebezpečnější na nich byly snahy hostů o rychlý kontr, míč jsme taky drželi víc, ale bránu jsme netrefili ani jednou, jen při Chmelíkově dalekonosném pokusu jsme se vešli do alespoň dvoumetrové tolerance. Náš brankář Špinar si v závěru poločasu jeden zákrok připsal, ale střela Opletala z vápna naštěstí mířila slabě doprostřed. Takže pro diváky dost bída, ale aspoň bylo hezky.

Do druhého poločasu jsme vstoupili dvěma šancemi z pravé strany, ale po akcích vystřídavšího Ondráčka nebo Nováka neuspěli v zakončení ani zablokovaný Skala ani nepřipravený Válek, který zblízka promáchl šanci osamostatnit se na prvním místě našich střeleckých tabulek před domácím publikem. Bohužel ne naposled. Pak na sebe strhlo pozornost počasí, a kdyby sněžilo o pár minut déle, asi by se ani nedohrálo. Výrazné oživení do hry přinesl Lněnička, který po hodině hry nahradil Nádvorníka a záhy se podařilo i změnit skóre. Chmelík se bratru 10 vteřin motal na středu mezi třemi protihráči, než vyslyšel pobídku J. Štěpána k vysvobození. Ten naopak nedal na volání Kutzera, z dobrých třiceti metrů napřáhl proti větru a míč krásně zapadl pod víko mimo dosah plachtícího gólmana. Nutno dodat, že ovace hřmějící po vyhlášení jeho jména naší legendou u mikrofonu, na našem stadionu neslýcháme často. Ale zase se na jeho gól v Morašicích čekalo bez jednoho měsíce 14 let, které strávil v divizi a na odpočinku, tak se těžko divit. Sic to pak kvůli zvýšenému tlaku hostí, silnému protivětru i těžkému terénu nebylo z naší strany moc ke koukání a na hřišti jsme působili až neklidně bránícím dojmem, šance jsme si vypracovávali i nadále. Lněnička postup z levé strany napálil do gólmana, střídající Halsbach zblízka hlavičkoval do tyče, Novák z dobrých pozic a zbytku sil pálil třikrát přesně do stejného místa, někam k lanku na krajním sloupku sítí za bránou. Hosté si připsali zvýšený počet standartek, ale Špinara už jinak než centrem neohrozili. Šplíchal po jednom z přímáku na zadní tyči hlavou balón jenom lízl a minutu před koncem jeho střel ofoukla šibenici zvenčí. Po delší době tři body pro diváky, ale za týden v Cerekvici to bude chtít přidat.