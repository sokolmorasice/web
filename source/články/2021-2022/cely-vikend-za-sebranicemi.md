---
title: Celý víkend za Sebranicemi
date: 2022-05-23T09:29:02.694Z
category: pripravka
image: /public/images/uploads/logo.png
---
# **Mladší přípravka**

**Sestava:** Anna Vopařilová – Jaroslav Chaun, Adam Bartoš, Julie Vodehnalová, Tereza Hlušičková, Miriam Vomočilová, Patrik Vurkminovič, Jan Langr, Martin Boštík, Kryštof Valentín, Štěpán Kovář, Ivo Langr

**Horní Újezd/Morašice – Pomezí 5:2 (1:1)**

– první půle byla vyrovnaná, ale v druhém dějství jsme byli nebezpečnější vepředu a v rozmezí pár minut jsme rozhodli o výsledku, pochvala pro soupeře, který se snažil o pohlednou kombinační hru.

**Branky:** Miriam Vomočilová 2, Jaroslav Chaun 2, Jan Langr

**Horní Újezd/Morašice – Březová 1:3 (0:2)**

– do zápasu jsme vstoupili aktivně, jenže první gól dal soupeř a pak jsme měli hluchou pasáž, v druhé půli jsme snížili, ale Březová si už vítězství vzít nenechala.

**Branka:** Jaroslav Chaun

**Sebranice – Horní Újezd/Morašice 4:3 (2:3)**

– v první půli jsme i díky dvěma gólům Járy Chauna z přímých kopů chvíli vedli 3:0, jenže domácí tým přidal, do pauzy stáhl naše vedení na jeden gól a v druhé půli skóre otočil.

**Branky:** Jaroslav Chaun 2, Patrik Vurkminovič

Na turnaji jsme opět odehráli tři vyrovnané zápasy. I když jsme vyhráli pouze první z nich, znovu se ukázalo, že při maximálním úsilí jsme schopni konkurovat všem soupeřům. Odměnou za bojovnost nám je další 2. místo na turnaji, protože kromě stoprocentního domácího týmu získal zbytek účastníků turnaje 3 body, a tak rozhodovalo skóre, které jsme měli z tříbodových týmů nejlepší.

**Tabulka turnaje:**

1.Sebranice (9 bodů)

**2.Horní Újezd/Morašice (3 b.)**

3.Březová (3 b.)

4.Pomezí (3 b.)

autor: Mgr. Zdeněk Beneš

# Starší přípravka

Tuto neděli nás čekal další z našich domácích turnajů a to na Horním Újezdě. Areál v údolí a krásné ideální počasí vybízelo ke sportování. Podle chuti někdo nohejbal, dětské hřiště, někdo jen pivko a fandění z tribuny. My jsme se sešli v hojném počtu na fotbálek.

**Sestava:** Martina Famfulíková – Sára Vomočilová, Samuel Vomočil, Vojtěch Huryta, Šimon Flídr, František Flídr, David Boček, David Klusoň, Dominik Prokop, Antonín Krejsa, Matěj Kovář, Michal Kovář

**Horní Újezd/Morašice – Čistá 11:0 (7:0)**

Jako první jsme si naservírovali hráče Čisté. Ještě po ránu spící soupeř nás nestíhal vůbec v ničem. Dařilo se nám hrát kombinačně s dobrým napadáním. Co se nepodařilo proměnit na poprvé velice aktivním útočníkům, ihned dorazila obrana. Pohodové vítězství nás až moc uklidnilo, což ukázal další průběh turnaje.

**Branky:** Vojtěch Huryta 2,Dominik Prokop 2, David Boček 2, František Flídr 2, Šimon Flídr, David Klusoň, Matěj Kovář

**Horní Újezd/Morašice – Sebranice  1:2 (0:2)**

Ve druhém zápase na nás čekal velmi těžký soupeř ze Sebranic. Tento tým nás v této sezóně pokaždé porazil a dokáže se proti nám zdravě vyhecovat k ještě lepším výkonům. To nám se od první minuty nedařilo. Zbytečnými ztrátami míče jsme vždy namazali soupeři do velkých šancí, ze kterých naštěstí překonal naší vynikající brankářku jen dvakrát. Do druhé půle jsme se oklepali z předešlých nezdarů a vytvořili si spoustu slibných šancí. Bohužel se nám, k velké radosti soupeře, podařilo využít jen jednu, což na vítězství nestačilo.

**Branka:** Vojtěch Huryta

**Horní Újezd/Morašice – Němčice  9:3 (6:2)**

Třetí zápas jsme odehráli s týmem z Němčic. Soupeř se do nás zakousl hned od úvodního hvizdu. Jeho několik vyložených šancí jsme přečkali s ohromným vypětím sil a ještě větší porcí štěstí. Co se nepodařilo soupeři, to se podařilo nám -  za gólmana popadalo skoro vše, čeho jsme se dotkli. Pohodový poločasový výsledek nám dal klid do poločasu druhého, ve kterém jsme drželi převážně balon na našich kopačkách a soupeře již do žádných větších šancí nepustili.

**Branky:** Šimon Flídr 3, Vojtěch Huryta 2, Matěj Kovář 2, Dominik Prokop, František Flídr.

**Další výsledky turnaje:** Sebranice – Němčice 8:1 (1:1), Němčice – Čistá 4:4 (2:1), Sebranice – Čistá 13:2 (5:1)

**Tabulka turnaje:**

1.Sebranice                        23:4    9 bodů

**2.Horní Újezd/Morašice   21:5    6 bodů**

3.Němčice                           8:21    1 bod

4.Čistá                                  6:28    1 bod

autor: Martin Kovář