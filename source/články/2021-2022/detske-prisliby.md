---
title: Dětské přísliby
date: 2021-09-06T14:11:54.993Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Mladší přípravka

**Sestava:** Hlušičková T. – Chaun J., Bartoš A., Vodehnalová J., Boštík M., Vomočilová M., Valentín K., Mach L., Langr J., Langr I.

**Horní Újezd/Morašice – Hradec n. S. 5:1 (2:1)**

– první poločas byl vyrovnaný, několika výbornými zákroky nás podržela naše brankařka, ve druhé půli jsme byli lepším týmem, branky: Chaun J. 3x, Vomočilová M., Boštík M.

**Horní Újezd/Morašice – Jevíčko 3:9 (1:7)**

– zápas rozhodly výkony šikovných jevíčských holek, branky: Chaun J. 2x, Vodehnalová J.

**Bystré – Horní Újezd/Morašice 10:1 (5:0)**

– domácí mají letos výborný tým, výsledek je obrazem průběhu utkání, branka: Vodehnalová J.

Z našich deseti hráčů na turnaji měli do této doby nějaký odehraný zápas pouze dva, je jasné, že každý turnaj bude cenná zkušenost. Všechny naše zástupce je třeba pochválit. Nejvíce mě potěšil výkon Terezy Hlušičkové v brance, k pilířům mužstva budou patřit Jára Chaun, Miri Vomočilová a Adam Bartoš. Turnaj vyhrálo Bystré, které všechny zápasy opanovalo, zbývající účastníci byli vyhlášeni na 2. místě, protože si shodně připsali 1 vítězství a 2 prohry. Další turnaj odehrajeme v neděli 12.9. od půl desáté v Morašicích.

autor: Zdeněk Beneš ml.

## Starší přípravka

Po odchodu silného ročníku 2010 do kategorie mladších žáků a téměř roční pauze od soutěžních zápasů jsme byli zvědaví, jak se povede týmu poskládaného z ročníků 2011 a 2012, zejména zda se povede navázat na dobré výsledky z loňského podzimu. 

**HÚ / Morašice – Březová 7 : 0** – v prvním poločase byla na všech hráčích vidět herní přestávka, avšak s přibývajícím časem jsme se dokázali prosazovat, hráči pochopili, co je po nich žádáno a výsledkem bylo herní zlepšení ve druhém poločase a jasné vítězství. Branky: Šimon Flídr 2x, Michal Kovář 2x, Jarda Chaun 1x, David Klusoň 1x a Fanda Flídr 1x.

**HÚ / Morašice – Bystré 3 : 3** – nepovedlo se nám zachytit úvod zápasu, neproměnili jsme pár šancí ,a tak šel soupeř do vedení 2 : 0. Kluci a holky se však nevzdali a zlepšeným výkonem otočili na 3 : 2. Když se už zdálo, že utkání dovedeme do vítězného konce, tak soupeř vyrovnal a zápas skončil zaslouženou remízou. Branky: Šimon Flídr 2x, Jarda Chaun 1x.

**HÚ / Morašice – Opatov 7 : 2** – povedlo se nám poměrně brzy vstřelit dva góly, což určilo ráz celého zápasu, ve kterém jsme zaslouženě zvítězili a to jsme ještě neproměnili  minimálně dalších 5 vyložených gólových příležitostí po skvělých nahrávkách Šimona Flídra. Nakonec nás to může trochu mrzet, protože právě jedna branka nám chyběla k vítězství v celém turnaji při shodném bodovém zisku s Bystrým. Branky: Šimon Flídr 2x, Fanda Flídr 2x, Vojta Huryta 1x, Dominik Prokop 1x a Jarda Chaun 1x.

Každopádně jsme se vstupem do podzimní části spokojení, celému týmu patří pochvala a poděkování za předvedený výkon (zde si dovolím vyzdvihnout Jardu Chauna, protože patří ještě do kategorie mladší přípravky) a těšíme se na další turnaje s vírou, že je budeme moci odehrát. Turnaj odehráli: Martina Famfulíková, Sára Vomočilová, Šimon Flídr, David Klusoň, Dominik Prokop, Jarda Chaun, Vojta Huryta, Samuel Vomočil, David Boček, Fanda Flídr, Michal Kovář a Matyáš Lněnička.

autor: František Bartoš