---
title: Výměna stráží
date: 2022-04-30
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Sebranice vs Horní Újezd/Morašice
sestava: Martina Famfulíková - Matyáš Bulva, Filip Flídr, Jan Kvapil, Ondřej
  Kusý, Josef Rosypal, Kryštof Tomšíček, Vojtěch Andrle, Jan Kopecký, Vít
  Famfulík, Lukáš Mach, Matěj Rejman, Vojtěch Štěpán, Vojtěch Huryta
goly: Ondřej Kusý, Vojtěch Andrle
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2021/Horní
  Újezd/Morašice/40ce85ae-cde2-44c3-896d-02e25b0f3922.html
---
Sobotní utkání o první místo v Sebranicích začínalo už v 9 hodin, a možná i tahle nezvykle brzká hodina ovlivnila náš výkon. Do zápasu jsme vstoupili bez energie a elánu, a i když jsme se brankou Ondry Kusého dostali na brzo do vedení, bylo to od nás na dlouho dobu vše. Všude jsme byli o krok pozadu a víceméně sledovali jsme koncert soupeře, který do přestávky třemi góly otočil skóre. Ve druhém poločase už to bylo lepší s naší snahou, podařilo se nám i snížit, ale jinak jsme prostě neměli svůj den a soupeř po zásluze vyhrál a vystřídal nás na prvním místě.