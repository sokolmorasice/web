---
title: Skoro 100%
date: 2021-09-26T06:19:22.325Z
category: pripravka
image: /public/images/uploads/logo.png
---
## **Mladší přípravka**

**Sestava:** Hlušičková T. – Chaun J., Bartoš A., Vomočilová M., Vodehnalová J., Boštík M., Kovář Š., Veselíková T., Langr J., Langr I.

**Horní Újezd/Morašice – Březová A 6:5 (2:3)**

Vyrovnaný zápas, kdy jsme dlouho tahali za kratší konec provazu, jsme pro sebe rozhodli na v druhé půli, pochvala za bojovnost, i když se prohrávalo, pořád byla snaha s výsledkem něco udělat, což se vyplatilo.

**Branky**: Chaun J. 4x, Kovář Š. 2x

**Horní Újezd/Morašice – Březová B 8:3 (2:2)**

V první půli jsme se trápili, až po pauze se nám podařilo strhnout skóre na svoji stranu, pětibrankový rozdíl je pro soupeře až příliš krutý.

**Branky:** Chaun J. 5x, Kovář Š., Vodehnalová J., Vomočilová M.

**Horní Újezd/Morašice – Sebranice 5:6 (3:5)**

Také v našem posledním utkání byl k vidění vyrovnaný boj, soupeř nám v první půli v jednu chvíli utekl o tři góly, i když jsme se snažili, na lepší výsledek to v tomto zápase nebylo.

**Branky:** Chaun J. 2x, Boštík M., Bartoš A., Kovář Š.

Tabulka turnaje:

1.Sebranice (9 b.)

**2.Horní Újezd/Morašice (6 b.)**

3.Březová A (3 b.)

4.Březová B (0 b.)

Mgr. Zdeněk Beneš

## Starší přípravka

Ani k tomuto turnaji jsme neodjížděli kompletní, omluveni byli David Klusoň a Sára Vomočilová. Dlouhodobě zraněný je Matěj Kovář.

**HÚ / Morašice – Čistá 3 : 1**

S tímto soupeřem jsme se utkali letos na podzim poprvé a tak nás čekalo vzájemné seznamování. Brzy jsme šli do vedení a průběh hry nasvědčoval, že si v klidu dojdeme pro vítězství. Opak byl však pravdou, druhý poločas byl zatím nejhorší z toho co jsme na podzim odehráli, vytratila se lehkost, bojovnost a hlavně chuť do hry, soupeř po zásluze snížil a zápas tak zdramatizoval

**Branky**: Šimon Flídr 1x, Fanda Flidr 1x, Jarda Chaun 1x.

**HÚ / Morašice – Sebranice 2 : 1**

Očekávali jsme velice vyrovnaný zápas, což se také potvrdilo. Soupeř měl balón více na svých kopačkách, ale my hrozili z rychlých protiútoků, z nichž jeden proměnil Jarda Chaun. Po přestávce se trefil po rozehrání rohového kopu Michal Kovář, ale soupeř byl dále nebezpečný a díky Martině Famfulíkové jsme však dokázali odolat, kontaktní gól vstřelili domácí se závěrečným hvizdem rozhodčího.

**Branky:** Jarda Chaun

**HÚ / Morašice – Bystré 4 : 2**

do utkání jsme vstoupili vcelku dobře a brzy vstřelili uklidňující branky, nicméně jsme si potom vybrali slabší chvilky a soupeři dovolili našimi chybami snížit a zápas zdramatizovat. O vítězství v zápase  jsme však zabojovali a ze Sebranic si odvezli další turnajové vítězství.

**Branky:** Šimon Flídr 1x, David Boček 1x, Fanda Flídr 1x a Michal Kovář 1x.

Turnaj nás nezastihl v té nejlepší formě, ale i přesto jsme dokázali zápasy dotáhnout do vítězného konce, k vidění byla vyrovnaná utkání, což je to nejlepší pro další fotbalový růst všech zúčastněných hráčů.

František Bartoš