---
title: Co bude s áčkem?
date: 2022-03-07T19:21:54.824Z
category: muzi
image: /public/images/uploads/logo.png
---
Z pravidelného tréninkového programu mužů budou v nejbližší budoucnosti vybočovat tyto události:

**ČT 10.3. 17:30-19:30** - trénink na UMT v Litomyšli (možnost modelového zápasu s Cerekvicí trénující na druhé půlce)

**NE 13.3. 15:00** - přátelský zápas proti Sloupnici na UMT v Litomyšli

**ČT 17.3. 17:30-19:30** - trénink na UMT v Litomyšli (možnost modelového zápasu s Cerekvicí trénující na druhé půlce)

**SO 19.3. 15:00** - přátelský zápas proti Kameničkám na UMT v Litomyšli

**SO 26.3. 8:30-???** - velká brigáda na hřišti (montáž nových sítí za brankou, oprava vjezdové brány, rozměření a úklid hřiště, příprava areálu na zápasy,...), v případě mimořádně nepříznivého počasí náhradní termín v sobotu 9.4.

**NE 27.3.** - přátelský zápas s Horním Újezdem (termín a místo bude upřesněn dle počasí)

**SO 2.4. 15:30** - první mistrák v Jevíčku