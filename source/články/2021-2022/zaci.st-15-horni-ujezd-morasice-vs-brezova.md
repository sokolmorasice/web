---
title: Těžká šichta
date: 2022-04-10
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Horní Újezd/Morašice vs Březová
sestava: Jiří Kučera – Jakub Kvapil, Jonáš Krška, Ondřej Žďára, Vlastimil
  Hladík, Zuzana Famfulíková, Jindřich Mičík, Ondřej kusý, Vojtěch Andrle, Jan
  Kopecký, Matěj Brettinger, Martina Famfulíková, Lukáš Mach, Vojtěch Paťava
goly: Jonáš Krška, Vlastimil Hladík
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2021/Horní
  Újezd/Morašice/89722845-902e-4a7e-923a-b88c18d011c8.html
---
Po úvodní pohodlné výhře proti Němčicím čekal naše starší žáky ve druhém zápase soupeř z nejtěžších - Březová. Proti favoritovi jsme začali dobře a dostali soupeře pod tlak, z kterého jsme vytěžili první branku. Pak jsme začali ztrácet fyzicky, soupeř přebral aktivitu a třemi góly do přestávky otočil zápas. Naši snahu o vrácení se do zápasu v druhém poločase Březová zbrzdila další brankou, která odhodlání týmu hodně srazila. A když se nám přece jen podařilo snížit, soupeř hned znovu odskočil na tři góly a pak už šlo jen o to, dohrát zápas se ctí.

autor: Miloš Kopecký