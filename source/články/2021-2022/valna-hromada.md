---
title: Valná hromada
date: 2022-03-07T19:31:29.774Z
category: obecne
image: /public/images/uploads/logo.png
---
Výbor TJ Sokol Morašice zve všechny členy a příznivce na volební valnou hromadu, která se bude konat v pátek **8.4.** od **19:00** v hospodě na hřišti.