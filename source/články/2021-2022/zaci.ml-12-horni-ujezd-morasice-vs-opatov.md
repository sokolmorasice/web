---
title: Rozpačitý start
date: 2022-04-09
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Opatov
sestava: Ondřej Kusý – Šimon Flídr, Kryštof Tomšíček, Matyáš Bulva, Filip Flídr,
  Josef Rosypal, Jan Kvapil, Vojtěch Andrle, Lukáš Mach, Martina Famfulíková,
  Vojtěch Huryta, Matěj Rejman, Vojtěch Štěpán, Jan Kopecký, Vít Famfulík
goly: Kryštof Tomšíček, Filip Flídr
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2021/Horní
  Újezd/Morašice/af67a49d-23ef-4716-abb1-fcc64254d80d.html
---
V první zápase na UMT měli naši mladší žáci málo kluků na střídání. Po rychlém vedení, které zařídil Vojtěch Andrle, našim docházely síly, domácí vyrovnali do šatny, po půlce otočili a na zvrat už se kluci nezmohli.

V druhém zápase proti Opatovu už bylo dětí dostatek, ale proti poslednímu Opatovu z toho nakonec byla jen hubená výhra.