---
title: Dětský zpravodaj
date: 2022-05-04T13:08:25.823Z
category: pripravka
image: /public/images/uploads/logo.png
---
# Mladší přípravka

## 24.4.2022 - Sebranice

**Sestava:** Tereza Hlušičková  – Jaroslav Chaun, Adam Bartoš, Patrik Vurkminovič, Martin Boštík M, Julie Vodehnalová, Anna Vopařilová, Tereza Veselíková, Jan Langr, Ivo Langr

**Horní Újezd/Morašice – Borová 2:3 (1:0)**

– pravý opak posledního podzimního měření sil těchto soupeřů, v utkání jsme byli po většinu času lepším týmem, ale více gólů dala Borová, mrzí řada neproměněných šancí.

**Branky:** Patrik Vurkminovič, Tereza Veselíková

**Sebranice – Horní Újezd/Morašice 3:3 (3:1)**

– domácí po první půli zaslouženě vedli o dva góly, ve druhém dějství jsme ale zápas nezabalili a dokázali vyrovnat

**Branky:** Martin Boštík, vlastní, Jaroslav Chaun

**Horní Újezd/Morašice – Litomyšl B 3:2 (2:1)**

– vyrovnaný zápas, který jsme rozhodli gólem v poslední minutě

**Branky:** Patrik Vurkminovič, Jaroslav Chaun, Julie Vodehnalová

Úvodní jarní turnaj hodnotím velmi pozitivně. Naši kluci a holky dělali na hřišti, co dostali nakázáno, a když se k tomu přidala stoprocentní bojovnost, museli mít z předvedeného výkonu dobrý pocit všichni přítomní. Turnaji nechybělo (kromě lepšího počasí) nic – nejprve jsme prožili zklamání z těsné porážky, pak radost z vybojované remízy a nakonec nadšení z vítězného gólu chvíli před koncem zápasu. Navíc jsou takovéto vyrovnané zápasy nejlepší pro fotbalový růst. Snad se budeme podobně prezentovat i nadále.

**Tabulka turnaje:**

1.Sebranice (5 bodů)

2.Borová (4 b.)

3.Horní Újezd/Morašice (4 b.)

4.Litomyšl B(3 b.)

## 1.5.2022 - Telecí

**Sestava:** Tereza Hlušičková  – Jaroslav Chaun, Adam Bartoš, Martin Boštík, Miriam Vomočilová, Kryštof Valentín, Julie Vodehnalová, Anna Vopařilová, Tereza Veselíková, Jan Langr, Ivo Langr

**Naše výsledky:**

**Borová – Horní Újezd/Morašice 1:3**

**branky:** Jaroslav Chaun 2, Martin Boštík

**Horní Újezd/Morašice – Bystré 2:4 (1:2)**

**branky:** Ivo Langr, Jaroslav Chaun

**Horní Újezd/Morašice – Jevíčko 3:1**

**branky:** Jaroslav Chaun 2, Miriam Vomočilová

Také ve druhém turnaji jsme předvedli velice solidní výkon. Ve všech zápasech jsme inkasovali jako první a museli dohánět ztrátu, ve dvou ze tří utkání se nám ale podařilo skóre otočit. Opět musím tým pochválit za snahu a bojovnost.\
\
**Tabulka turnaje:**

1.Bystré

2.Horní Újezd/Morašice

3.Jevíčko

4.Borová

Mgr. Zdeněk Beneš