---
title: Samá radost
date: 2021-10-11T19:04:04.031Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Mladší přípravka

**Sestava:** Hlušičková T. – Chaun J., Vodehnalová J., Kovář Š., Valentín K., Vurkminovič P., Veselíková T., Langr J., Langr I.

**Horní Újezd/Morašice – Jevíčko 6:3 (3:0)**

– výborný výkon především v první půli znamenal naše první letošní vítězství nad Jevíčkem

**Branky:** Chaun J. 3x, Valentín K., Langr J., vlastní

**Horní Újezd/Morašice – Dlouhá Loučka 11:1 (3:0)**

– v první půli jsme se trochu hledali, druhý poločas byl lepší, soupeř má mladý, ale talentovaný tým

**Branky:** Chaun J. 5x, Valentín K. 2x, Veselíková T. 2x, Vodehnalová J. 2x

**Litomyšl „B“ – Horní Újezd/Morašice 9:3 (5:2)**

– obdobný zápas jako minulý týden, rozhodovaly individuality domácích

**Branky:** Vurkminovič P., Veselíková T., Valentín K.

Odehráli jsme další vydařený turnaj, znovu jsme si připsali 2. místo. Tým je potřeba pochválit za bojovnost a nasazení. Poslední podzimní turnaj odehrajeme v sobotu v Březové.

**Tabulka turnaje:**

1.Litomyšl „B“

**2.Horní Újezd/Morašice**

3.Jevíčko

4.Dlouhá Loučka

Mgr. Zdeněk Beneš

## Starší přípravka

K turnaji jsme odjížděli společně s týmem mladších žáků, který hrál v sousedních Jaroměřicích a oslabeni o Sáru Vomočilovou a Matyáše Lněničku, avšak s vědomím, že nás čekají týmy se kterými jsme již letos hráli.

**HÚ / Morašice – Jevíčko 8 : 0**

Prvních pár minut to vypadalo na relativně vyrovnané utkání, ale poté se náš tým dostal do té správné provozní teploty a začal předvádět hru, na kterou jsme u nich zvyklí. Výsledkem tak bylo jednoznačné vítězství. 

**Branky:** Dominik Prokop 2x, Fanda Flídr 1x, Vojta Huryta 1x, Michal Kovář 1x, Jarda Chaun 1x, Martina Famfulíková 1x a Šimon Flídr 1x.

**HÚ / Morašice – Březová 15 : 0**

Utkání mělo od samého začátku jednostranný průběh a vývoj skóre, proto nastoupil do druhého poločasu do branky Jarda Chaun a více prostoru ve hře dostávali tentokrát ti mladší hráči. 

**Branky:** Vojta Huryta 3x, Sam Vomočil 3x, Fanda Flídr 3x, Dominik Prokop 2x, Martina Famfulíková 2x, Šimon Flídr 1x a David Boček 1x.

**HÚ / Morašice – Čistá 8 : 0**

K utkání jsme nastupovali s vědomím, že poslední vzájemný zápas na turnaji v Sebranicích byl relativně vyrovnaný. Toto však tentokrát neplatilo, protože náš tým zastihl turnaj ve výborné formě a předvedeným výkonům nelze nic vytknout. K vidění tak byly pěkné akce a góly, které znamenaly konečné prvenství. 

**Branky:** Vojta Huryta 4x, Fanda Flídr 2x, Dominik Prokop 1x a Šimon Flídr 1x.

Děkuji všem hráčům za perfektní turnaj, protože všichni měli chuť do hry, což je pro úspěšný výsledek to základní. Budeme doufat, že v předvedených výkonech budeme pokračovat i nadále, i když patrně se budeme muset příští turnaj obejít bez brankářské jedničky Martiny Famfulíkové, která by měla posílit tým mladších žáků.

František Bartoš