---
title: Na vítězné vlně
date: 2021-10-17T21:17:51.911Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Starší přípravka

Věděli jsme, že se budeme muset tentokrát obejít bez Martiny Famfulíkové, ke které se ještě z důvodu nemoci přidal Šimon Flídr. Před celým týmem tak stála výzva, jak si povedou bez dvou opor základní sestavy??

**HÚ / Morašice – Jaroměřice / Jevíčko dívky 11 : 0**

\-zápas se od samého začátku vyvíjel jednoznačně v náš prospěch a tak prostor dostali zejména mladší hráči ročníku 2012.

**Branky :** Fanda Flídr 3x, Vojta Huryta 2x, Dominik Prokop 2x, David Boček 3x, David Klusoň 1x.

**HÚ / Morašice – Bystré 5 : 4**

\-zápasy s tímto soupeřem jsou letos velmi vyrovnané a tak jsme věděli, že nás nečeká nic lehkého a pokud jsme chtěli uspět, tak jedině s maximálním nasazením a bojovností celého týmu. Nechali jsme si dát laciné góly a  v poločase jsme prohrávali 1 : 3. Povedlo se nám snížit, ale soupeř opět odskočil na rozdíl dvou branek a zdálo se, že tentokrát odejdeme poraženi. Avšak opak byl pravdou. Kluci opravdu makali celý zápas, v závěru se ještě zvedli a vítězství udržel perfektním zákrokem v samém závěru Jarda Chaun, který nahradil v brance pro tento turnaj Martinu Famfulíkovou a patří mu za výkon pochvala a  poděkování. Toto vítězství má pro tým velký význam, dokázali otočit nepříznivý vývoj skóre a potvrdili si, že fotbal je týmový sport a pokud opravdu všichni zabojují, tak mohou nahradit chybějící hráče. 

**Branky:** Vojta Huryta 2x, Fanda Flídr 3x.

**HÚ / Morašice – Čistá 5 : 3**

\-další zajímavé a vyrovnané utkání, s opačným průběhem než to předešlé. Naši šli do dvoubrankového vedení, které si udržovali celý zápas a soupeři vyrovnat nedovolili. Výsledkem tak bylo další celkové prvenství v turnaji, které má asi nejvyšší hodnotu ze všech dosavadních. 

**Branky:** Vojta Huryta 3x, David Boček 1x, Fanda Flídr 1x

Týmu není v podstatě co vytknout, jelikož všichni předvedli svá maxima a všem patří poděkování za předvedený výkon na který se budeme snažit navázat na posledním podzimním turnaji za týden v Morašicích.

autor: František Bartoš