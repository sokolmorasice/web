---
title: Splněná povinnost
date: 2021-09-12
category: muzi
image: /public/images/zapasy/morasice-radimer.png
h1: Morašice vs Radiměř
sestava: J. Kubeš - V. Tměj, M. Ropek (Z. Skala), V. Štancl, M. Černohorský - B.
  Válek (J. Vedral), D. Štancl, M. Chmelík, J. Vít (J. Špinar) - V. Novák, F.
  Bureš (M. Frank)
goly: B. Válek 2 (V. Novák 2), Z. Skala (J. Vedral)
zlute: V. Novák
cervene: null
multimedia: https://drive.google.com/drive/folders/1IFAsbSEKejq9swU8o1EXB6iUSRVtVr6f
scortes: https://scortes.rozpisyzapasu.cz/api/533A1A/2021/Morašice/cf926350-e247-4022-9bcb-00934f460865.html
---
Na úvod domácího trojboje jsme přivítali Radiměř, a přestože do brány musel na záskok Kubeš a nemoc k utkání nepustila ani Nádvorníka, o ničem jiném než třech bodech nebyla řeč. Tedy byla, konkrétně o nasazení, ukázání, kdo je doma pánem a takové ta silácká slova, ale ty se nám nějak nepodařilo uvést v život. A protože hosté neměli úplně dispozice, aby našeho přístupu využili ke Kubešově ohrožení, nebylo toho věru moc k vidění. O nějakou střelu se pokusili Tměj a Chmelík, ale oba, zejména Chmelík, měli v danou chvíli na výběr lepší možnosti, joako třeba otevřenou pravou stranu s Vítem v náběhu. A tak to s naším rozhodováním chodilo často. Až po půlce prvního dějství se nám podařilo udržet míč na zemi a přes střed si nahrát až k malému vápnu, kde dal Novák svým šermováním proti hostujícím obráncům čas Válkovi, který do souboje vběhl a bodlem nebo nártem poslal míč k přední tyči. O pět minut později zabojoval v běžeckém souboji Vít, nehleděl odpadnuvšího obránce reklamujícího šlápnutí a nacentroval do vápna, Novák si někde za malým vápnem míč zpracoval, přiťukl pod sebe Válkovi a ten z velkého vápna mířil přesně k stejné tyči jako v prvním případě. Z pohody nás rychle vyvedlo špatné přebírání hráčů, díky kterému měli hosté čas si připravit pozici pro nevinný centr z hloubi naší půlky, a špatný odhad Černohorskému, kterému míč skočil na ruku. Opět po týdnu jsme tedy darovali soupeři penaltu, a přestože do té doby prakticky nezaměstnaný Kubeš vystihl stranu, na přesné provedení hostujícího exekutora nedosáhl. Do půli se už nepřekvapivě nic zvláštního nestalo.

Během první čtvrthodiny druhého dějství měl dvě možnosti k vylepšení svých statistik Bureš, ale při jednom brejku po jeho pasu přestřelil vysunuvší se Černohorský, při druhém chybělo našemu útočníkovi pár centimetrů výšky, aby mohl Chmelíkův centr hlavičkovat komfortněji. Naše hra s postupujícím časem postrádala jistotu, hosté se ale naštěstí dostali jenom k několika přímákům a rohům, po odchodu Ropka a vysunutí hostujícího kapitána na hrot nás třikrát v rychlém sledu uchránily offsajdy, které se hostům nelíbily stejně jako Tmějovo hraní na zemi ve vápně, dost podobné tomu, které minule předl V. Štancl. Ten odmítl přidat pojistku špatně vyřešeným přečíslením ve vápně, na padající list Franka hostující gólman dosáhl, Tmějův pokus rozvlnil síť pouze zvrchu, a tak jsme přidali pojistku až v úplném, a už docela nervózním závěru. Odražený nebo propadlý míč na brankové čáře doběhl Vedral, předložil na malé vápno Skalovi a ten s klidem vymetl pavouky v protějším rohu. Hosté už nedostali ani možnost rozehrát, my si oddychli a můžeme zase zkusit sbírat odhodlání na příště.