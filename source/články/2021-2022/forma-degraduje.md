---
title: Forma degraduje
date: 2022-03-27T19:03:31.313Z
category: muzi
image: /public/images/uploads/logo.png
h1: Morašice - Horní Újezd 1:1 (0:0)
sestava: J. Špinar (V. Štancl) – K. Kutzer, M. Černohorský, D. Štancl, V. Novák,
  F. Halsbach, J. Vít, M. Chmelík, Z. Skala, D. Ondráček, M. Frank, P. Lněnička,
  F. Bureš, J. Kopecký
goly: P. Lněnička (V. Novák)
multimedia: https://drive.google.com/drive/folders/1dwujeS9Rqa_I6-tjJUyR6D6j2GrogaUp
---
Až na poslední přátelské utkání jsme se utkali s okresním soupeřem, tentokráte v Morašicích proti Hornímu Újezdu. Debut v našem dresu si odbyl nováček Filip Halsbach. I když počasí přálo a přilákalo nemalé množství diváků, na naši hru už tak hezká podívaná nebyla. Často ji brzdilo zpracování balónu nebo také dlouhé rozmýšlení, co s míčem provést. Z naší útočné snahy stojí za zmínku snad jen nevýrazná střela Franka, Vítův průnik po levé straně a následný centr, který však jen prolétl malým vápnem a střela Nováka (podle jeho slov by skončila v síti), kterou ale neúmyslně zblokoval Frank. Hostujícím jsme dovolili pouze dva rohy a dvě střely – jednu vedle, druhou jistil Špinar.

Herní projev ve druhém poločase se výrazně nezlepšil, naopak jsme se ještě více potýkali s nepřesnou rozehrávkou a malou nabídkou. Alespoň se podařilo rozvlnit síť. Bohužel nejprve té naší brány, když se nám nepodařilo u rohového praporku odkopnout míč a přihrávku pod sebe si našel „Klacek“, který zamířil přesně k tyči. O vyrovnání se zasloužil po docela rychlé akci Lněnička, když z vápna zakroutil míč přesně za zadní tyč. Z výraznějších šancí utkvěly v paměti už jen únik hostujícího Opletala, který ale z úhlu netrefil bránu a na druhé straně Ondráčkovo nastřelené břevno v poslední minutě, když se pokusil z levé strany napodobit předchozí úspěch Lněničky. Skóre už se nezměnilo, a tak jsme přípravu po předchozích dvou vítězstvích zakončili remízou. Příští sobotu nás čeká první mistrovské utkání na hřišti Jevíčka. 

autor: Dominik Štancl