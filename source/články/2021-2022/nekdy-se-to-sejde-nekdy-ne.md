---
title: Někdy se to sejde, někdy ne
date: 2022-05-09T13:17:24.021Z
category: pripravka
image: /public/images/uploads/logo.png
---
# Starší přípravka

Po velmi vydařených turnajích v Čisté a na domácím hřišti v Morašicích, jsme k tomu třetímu cestovali v neděli do Němčic. Předchozí víkendy jsme měli hojnou účast, tentokrát jsme se museli popasovat s několika omluvenými i neomluvenými posty. 

**Sestava:** Matyáš Lněnička - Samuel Vomočil, Sára Vomočilová, Vojtěch Huryta, František Flídr, Matěj Kovář, Michal Kovář, David Klusoň.

**Morašice / Horní Újezd - Němčice   3:6 (1:2)**

V prvním zápase nás čekal jeden z nejlepších týmů dnešního dne, domácí Němčice. Po většinu první půle jsme byli lepším týmem, ale protože jsme proměnili jen jednu z mnoha šancí, prohrávali jsme v poločase 1:2. Ve druhém se nám už zas tak nedařilo a prohráli celkově 3:6. 

**Branky:** Vojtěch Huryta 3

**Morašice / Horní Újezd - Březová    9:1 (5:1)**

Na druhý duel s Březovou už uměleckém vystoupení pro maminky dorazila i Sára Vomočilová, která výrazně zlepšila naši obranu. Soupeře jsme více napadali a začalo se nám dařit i vepředu. Velmi aktivní Fanda Flídr nás zásoboval krásnými šancemi, nahrávkami a samozřejmě góly.  Což dokazuje poločasové skóre 5:1. Ve druhé půli jsme pečlivě drželi obranný val a k našemu spolehlivému brankáři Matymu Lněničkovi jsme snad propustili jediný balon. Za to dopředu se valil jeden útok za druhým, což se nakonec projevilo i na výsledku. 

**Branky:** Vojtěch Huryta 4, Matěj Kovář 2, František Flídr 2, Samuel Vomočil

**Morašice / Horní Újezd - Opatov   3:1 (1:1)** 

Třetí duel byl s technicky i fyzicky velmi vyspělým týmem z Opatova. Před začátkem utkání jsme přišli o Davida Klusoně, který byl do té doby v obraně i útoku vynikající, ale odjel plnit hasičské povinnosti. I přes nevysoký vzrůst ho však v obraně bravurně zastoupil Sam Vomočil. Brzy jsme smolně inkasovali, ale manko jsme do poločasu srovnali. Ve druhé půli se nám dařilo více držet balón na našich kopačkách, a převahu jsme díky bratrům Kovářovým přetavili v další dvě branky a překlopili tím vítězství na naší stranu.

**Branky:** Vojtěch Huryta, Matěj Kovář, Michal Kovář 

**Střelci celkově:**  Vojtěch Huryta 8, Matěj Kovář 3, František Flídr 2, Samuel Vomočil 1, Michal Kovář 1

**Další výsledky turnaje:** Opatov – Březová 5:1, Němčice – Opatov 1:0, Němčice – Březová 10:1

**Tabulka turnaje:**

1. Němčice (9b.)
2. **Morašice / Horní Újezd (6b.)**
3. Opatov (3b.)
4. Březová (0b.)

autor: Martin Kovář

# Mladší přípravka

Na domácím turnaji se nám sešly všechny děti, které v současnosti hrají turnaje mladší přípravky. V počtu 13 hráčů se jen těžko dají uspokojit představy a přání všech o čase stráveném na hřišti, snad nikdo neodcházel zklamaný. Třetí jarní dějství bylo zatím nejkvalitněji obsazeným turnajem, kterého jsme se v druhé polovině sezony účastnili.

**Sestava:** Tereza Hlušičková - Jaroslav Chaun, Adam Bartoš, Anna Vopařilová, Julie Vodehnalová, Miriam Vomočilová, Martin Boštík, Jan Langr, Tereza Veselíková, Kryštof Valentín, Štěpán Kovář, Patrik Vurkminovič, Ivo Langr

**Horní Újezd/Morašice – Bystré 0:9 (0:4)**

Do turnaje jsme vstoupili levou nohou, soupeř, se kterým jsme minulý týden prohráli o dva góly, byl všude dřív, dělali jsme spoustu chyb a nedařilo se vůbec nic.

**Horní Újezd/Morašice – Čistá 2:3 (1:3)**

Výrazně zlepšený výkon, sice jsme po 10 vteřinách prohrávali, ale na týmu byla vidět vůle s vývojem zápasu něco udělat, k vyrovnávací brance jsme se ale nedopracovali.

**Branky:** Jaroslav Chaun 2

**Horní Újezd/Morašice – Hradec n. S. 1:3 (1:2)**

Poslední zápas jen podtrhl zmar z domácího vystoupení, nechyběla snaha, vedli jsme 1:0, ale soupeř skóre otočil, s čímž už jsme nedokázali nic udělat

**Branka:** Jaroslav Chaun

**Tabulka turnaje:**

1.Bystré (9 bodů)

2.Čistá (6 b.)

3.Hradec n. S. (3 b.)

**4.Horní Újezd/Morašice (0 b.)**

autor: Mgr. Zdeněk Beneš