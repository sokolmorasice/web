---
title: Tři, dva (jedna, start!)
date: 2022-03-13T20:24:44.257Z
category: muzi
image: /public/images/uploads/logo.png
h1: Morašice - Sloupnice  3:2 (3:1)
sestava: J. Špinar - V. Novák, V. Štancl, D. Štancl, M. Černohorský, B. Válek,
  M. Chmelík, T. Nádvorník, J. Vít, F. Bureš, M. Frank, J. Štěpán, J. Kopecký,
  K. Kutzer, P. Lněnička
goly: F. Bureš (T. Nádvorník), M. Chmelík (J. Vít), V. Novák
---
V prvním přátelském zápase jsme se na litomyšlské umělce utkali s bétřídní Sloupnicí. V kabině jsme přivítali až nečekaný počet 15 lidí, na čemž měly podíl i překvapivé návraty Jana Štěpána, Patrika Lněničky a Karla Kutzera (dříve Vopařila - první a poslední vysvětlení pro čtenáře). Navrátilce určitě potěší zpráva, že pro jejich zápisné je v kabině nově místa dost. Ani počet diváků na tribuně nebyl úplně v řádu malých jednotek. Začátek jsme měli snový, Nádvorník vystihl rozehrávku soupeře, vyslal Bureše do navlas stejné pozice jako v poslední minutě čtvrtečního modelové zápasu s Cerekvicí a výsledek nájezdu našeho útočníka byl také stejný - míč v síti u pravé gólmanovy tyče. Dařilo se nám až překvapivě kombinovat a držet míč, ale soupeř nás záhy ztrestal po rohovém kopu, když jsme ho nechali zahrát si nerušený hlavičkový biliár se snadným zakončením. Vedení nám po centru Víta vrátil zadní tyč zavírající důrazný Chmelík a po Novákově parádním sólu po pravé straně s překvapivě úspěšným zakončením podél gólmana to bylo dokonce o dva góly. To vydrželo až do poločasu, protože přes několik slušných kombinací jsme soupeřova gólmana už napřímo prakticky neohrozili. Ale náš brankář se taky nudil.

Do druhého poločasu Sloupnice vysunula presink a přitlačila na důrazu, nám rapidně ubylo držení míče, ale zatímco sobě vypracovala jen jednu šanci, když jejich útočník přestřelil volej z penalty, našemu útoku poskytla několik okýnek v obraně. Bohužel jsme si nadějné i vyložené pozice sami kazili nepřesnými nahrávkami nebo špatným dotekem a branku, která by definitivně vzala soupeři myšlenky na vyrovnání a třeba zchladila tu a tam projevené emoce nehodné přátelského zápasu, se nám vstřelit nepodařilo. Pět minut před koncem nás soupeř podruhé nachytal po standartní situaci, když jsme špatné nabrali náběh na zadní tyč, kde se sloupenský hráč zblízka nemýlil. Nijak přeceňovanou, ale přesto cennou výhru jsme si už ohlídali, stejně jako narychlo vypůjčené balóny, a sami sobě ukázali, že i když naše komunikace v zimní přípravě připomínala spíše chráněnou dílnu, na hřišti to s námi až tak špatné není.