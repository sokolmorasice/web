---
title: Mladí v dáli
date: 2021-09-18T20:47:46.724Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Mladší přípravka

**Sestava:** Hlušičková T., Chaun J., Vodehnalová J., Vurkminovič P., Valentín K., Vomočilová S., Kovář Š., Veselíková T., Langr J., Langr I.

**Horní Újezd/Morašice – Hradec n. S. 4:5 (2:4)**

Na turnaj jsme kvůli dopravním komplikacím kolem Jevíčka dorazili na poslední chvíli, do zápasu jsme vstoupili téměř bez rozcvičky, což způsobilo, že jsme skoro celou dobu byli o jeden až dva góly pozadu.

**Branky:** Chaun J. 3x, Veselíková T.

**Jevíčko – Horní Újezd/Morašice 7:5 (2:3)**

Náš nejlepší výkon na turnaji, bojovalo se, ale stejně jako před 14 dny zápas rozhodly individuální výkony šikovných domácích děvčat.

**Branky:** Vomočilová M. 2x, Veselíková T., Vodehnalová J., Valentín K.

**Horní Újezd/Morašice – Jaroměřice 9:0 (5:0)** 

Proti mladšímu soupeři jsme si s chutí zastříleli a poprvé udrželi nulu vzadu, i když výkon nebyl stoprocentní, 

**Branky:** Valentín K. 4x, Veselíková T. 2x, Chaun J., Vodehnalová J., Hlušičková T.

**Tabulka turnaje:**

1.Jevíčko

2.Hradec n. S.

**3.Horní Újezd/Morašice**

4.Jaroměřice

Mgr. Zdeněk Beneš