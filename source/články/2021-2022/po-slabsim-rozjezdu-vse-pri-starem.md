---
title: Po slabším rozjezdu vše při starém
date: 2021-09-12T19:16:07.586Z
category: zaci.st
image: /public/images/uploads/logo.png
---
**28.8.2021**

**Litomyšl / Čistá - Horní Újezd / Morašice 2:1 (2:1)**

Naši starší žáci si na úvod sezóny připsaly po dvou stoprocentních nedokončených podzimech první porážku, když doplatili na mizernou produktivitu a špatné řešení šancí, protože celý zápas se hrál za jejich vytrvalé převahy. Pochvalu za záskok v bráně zaslouží přes inkasované góly Martina Famfulíková

**Sestava:** Martina Famfulíková - Daniel Hanyk, Jonáš Krška, Jakub Kvapil, Jindřich Mičík, Zuzana Famfulíková, Ondřej Žďára, Ondřej Kusý, Anežka Břeňová, Vojtěch Andrle, Jan Kvapil, Jan Kopecký, Kryštof Tomšíček, Jiří Kučera

**Góly:** Jonáš Krška

**1.9.2021**

**Horní Újezd / Morašice - Němčice  3:0 (0:0)**

I v druhém zápase mělo naše družstvo navrch, ale pokračovalo jejich střelecké trápení, které by zápas dříve rozhodlo. Chvíli před koncem mohli hosté snížit na 1:2 z penalty, ale místo zdramatizování přišla vzápětí definitivní pojistka.

**Sestava:** Lukáš Němec - Daniel Hanyk, Ondřej Žďára, Ondřej Kusý, Jakub Kvapil, Jindřich Mičík, Jonáš Krška, Vojtěch Paťava, Matěj Brettinger, Anežka Břeňová, Martina Famfulíková, Jan Kopecký, Jan Kvapil, Štěpán Řejha

**Góly:** Jonáš Krška, Matěj Brettinger, Anežka Břeňová

**5.9.2021**

**Opatov - Horní Újezd / Morašice 1:7 (0:4)**

Na rozdíl od mužstva mužů zvládli o den později naši žáci z opatovského hřiště odjet s plným bodovým ziskem, o kterém tentokrát rozhodli už v prvním poločase.

**Sestava:** Lukáš Němec - Daniel Hanyk, Vlastimil Hladík, Jindřich Mičík, Jakub Kvapil, Anežka Břeňová, Jonáš Krška, Vojtěch Paťava, Jan Kopecký, Jiří Kučera, Jakub Bartoš

**Góly:** Jonáš Krška 2, Jakub Kvapil 2, Anežka Břeňová, Vojtěch Paťava, vlastní

**12.9.2021**

**Horní Újezd / Morašice - Dlouhá Loučka / Křenov 11:0 (3:0)**

Zápas proti poslednímu týmu tabulky byl celou dobu v naší režii, ale gólově se to podařilo vyjádřit až po přestávce, hattrickem ke kanonádě přispěl Jakub Kvapil.

**Sestava:** Lukáš Němec - Daniel Hanyk, Vlastimil Hladík, Matěj Brettinger, Jakub Kvapil, Anežka Břeňová, Jonáš Krška, Vojtěch Paťava, Ondřej Kusý, Jiří Kučera, Jakub Bartoš, Jan Kopecký, Lukáš Mach, Jan Kvapil

**Góly:** Jakub Kvapil 3, Vojtěch Paťava 2, Lukáš Mach 2, Daniel Hanyk, Anežka Břeňová, Jiří Kučera, Jan Kopecký