---
title: Férovka v Opatově
date: 2021-09-11
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Opatov vs Horní Újezd/Morašice
sestava: Martina Famfulíková – Ondřej Kusý, Jan Kvapil, Josef Rosypal, Lukáš
  Mach, Kryštof Tomšíček, Jan Kopecký, Vojtěch Andrle, Matyáš Bulva, Filip Flídr
goly: Vojtěch Andrle 2x
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2021/Horní
  Újezd/Morašice/acefcbca-6d39-45a8-a968-5547c19ff8c0.html
---
Třetí zápas aktuální sezony jsme odehráli v sobotu odpoledne na pěkném hřišti v Opatově. Hned od začátku se začal hrát důrazný, fyzicky náročný, ale férový fotbal. Již v 6. minutě jsme dostali gól po střele zpoza vápna, která zapadla pěkně pod břevno. Poté následovala část zápasu, která se odehrávala mezi vápny s minimem gólových šancí. Ve 24. minutě jsme vyrovnali na 1:1 po gólu Vojty Andrleho, který dostal pěknou přihrávku a následně předvedl krásnou individuální akci a zakončil do šibenice. Do konce prvního poločasu se již nic důležitého nestalo. Druhý poločas začal podobně jako první, hra byla vyrovnaná a hrálo se především mezi vápny. V 56. minutě jsme se dostali do vedení po přímém kopu z dálky, když Vojta Andrle zavěsil svoji střelu pod břevno a brankář byl na ni krátký. Po tomto gólu měl soupeř několik slibných šancí, ale naštěstí nic neproměnil, takže jsme dokráčeli k vítězství 2:1. Chtěl bych určitě pochválit hráče obou týmů za předvedený výkon a férový boj.