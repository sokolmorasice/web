---
title: Opravdu to mohlo být horší?
date: 2021-10-10
category: muzi
image: /public/images/zapasy/morasice-linhartice.png
h1: Morašice vs Linhartice
sestava: J. Špinar - V. Tměj, M. Ropek, D. Štancl, M. Černohorský (Z. Skala) -
  J. Vít (D. Ondráček, J. Vedral), M. Chmelík, T. Nádvorník (M. Frank) - V.
  Štancl, F. Bureš (V. Štěpán)
goly: null
zlute: null
cervene: null
multimedia: https://drive.google.com/drive/folders/14Gr9rkY6OsWKzZ0G8hfYj1B6CaaiaDhu
scortes: https://scortes.rozpisyzapasu.cz/api/533A1A/2021/Morašice/f4ca06d7-ba28-4362-9a04-27c87a413bba.html
---
Byl to hnus. Jak pro nezaujatého fanouška fotbalu, tak pro ty, kteří by se spokojili i s ošklivým vítězstvím. Proti poslednímu týmu tabulky jsme možná jedinkrát vystřelili na bránu, první poločas jsme byli horším týmem. Kdyby nám v druhém poločase vyšlo alespoň jedno z pár nepřesných zakončení po standartkách, možná by to zbytečně vyvolalo pocit, že to tam vždycky nějak doklepem. Nedoklepem. S takovým chvilkovým nasazením ne. Dalo by se napsat podrobněji o moravské předehře zápasu, o strastech se službou, o těch pár šancích, o pomezním, který přes veškerý čas strávený "řešením situací na lajně" nedokázal napsat zápis přesně, ale ten výkon si snad úsilí spojené s napsáním článku ani nezaslouží.