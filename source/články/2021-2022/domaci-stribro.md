---
title: Domácí stříbro
date: 2022-06-12T20:58:53.268Z
category: pripravka
image: /public/images/uploads/logo.png
multimedia: https://drive.google.com/drive/folders/1T99xi7rEim6akOKuN3_nqN3LLZoOkaiW
---
Probudili jsme se do krásného slunného rána. Pohled na oblohu sliboval velmi teplý den s teplotami ke 30 stupňům. Sbalit kopačky, hodně pití a hurá na už tak moc očekávaný domácí závěrečný turnaj.  Již dopředu jsme věděli, že nepřijedou týmy z Opatova a dívek z Jaroměřic, takže jsme přivítali šest dalších nadšených týmů, které chtěli ukázat, co v nich je a čemu se za uplynulý fotbalový rok naučili a v čem se zlepšili. Počet jen sedmi týmů umožnil velmi spravedlivý systém ‚,každý s každým‘‘. To pro každého z nás to znamenalo porci šesti  zápasů, a proto jsme velmi uvítali dobrou zprávu, že na naší soupisce máme jen jednu omluvenku Sáry Vomočilové.

**Sestava:** Martina Famfulíková –   Vojtěch Huryta, Šimon Flídr, František Flídr, David Klusoň, David Boček, Antonín Krejsa, Matěj Kovář, Michal Kovář, Dominik Prokop, Samuel Vomočil, Jaroslav Chaun, Matyáš Lněnička

**Horní Újezd/Morašice – Čistá 2:1 (2:1)**

Turnaj jsme zahájili s týmem z Čisté.  Než jsme se rozkoukali, už jsme díky smolné teči prohrávali. Až poté se naše hra uklidnila. Přestali se dělat chyby v rozehrávce a přineslo to náš první gól. Do poločasu jsme skóre ještě otočili a ve druhém trpělivou hrou od zabezpečené obrany výsledek udrželi.

**Branky:** František Flídr, Jaroslav Chaun

**Horní Újezd/Morašice – Jevíčko 4.1 (2:0)**

Celý zápas se odehrával převážně ve středu pole a před soupeřovou bránou. Soupeř velmi dobře bránil před brankový prostor, který jsme překonali v první půli jen dvakrát. Střelou z vápna a jednou šťastnou dorážkou. Druhý poločas nepřinesl nic nového. Náš stálý tlak s dalšími dvěma góly jen drobně pokažený jedním neuhlídaným brejkem.

**Branky:** Šimon Flídr, Jaroslav Chaun, Michal Kovář, David Klusoň

**Horní Újezd/Morašice – Němčice 5:0 (1:0)**

Od první chvíle vyrovnané utkání. My měli lehkou územní převahu, soupeř hrozil velmi nebezpečnými brejky. Do těsného vedení jsme se dostali těsně před poločasem. Ve druhém se nám dařilo postupem času čím dál tím víc soupeře zavírat na jeho polovině. Z tohoto tlaku začaly padat i góly do soupeřovi sítě. Ale objektivně řečeno, konečný výsledek neodpovídá soupeřově kvalitě.

**Branky:** František Flídr 2, Šimon Flídr, Dominik Prokop, Vojtěch Huryta

**Horní Újezd/Morašice – Bystré 1:0 (1:0)**

Hned od úvodního hvizdu na nás soupeř zatlačil.  Od inkasování nás zachránila třikrát tyč a výborné zákroky naší brankařky. Z ojedinělé šance se nám přeci jen podařilo trefit bránu a skórovat. Do druhé půle jsme vstoupili mnohem zodpovědněji. A až na pár malých chybiček jsme již nedovolili soupeři vůbec nic a hubené vydřené vítězství ubránili až do konce.

**Branka:**  Dominik Prokop

**Horní Újezd/Morašice – Sebranice 2:2 (2:1)**

Již před zápasem jsme tušili, že půjde o klíčové utkání, které bude napovídat o vývoji konečného pořadí celého turnaje.  Hráče se nám podařilo správně zdravě namotivovat. Začátek se nám podařil skvostně a brzy jsme vedli o dva góly. V hlavách hráčů nastalo uspokojení a nepodařilo se nám dále skóre navýšit.  Bohužel jsme si nechali dát gól  ,,do šaten“. Což soupeře nakoplo zpět do zápasu, který přes zákroky Marti Famfulíkové dokázal vyrovnat.

**Branky:** Vojtěch Huryta, Šimon Flídr

**Horní Újezd/Morašice – Březová nad Svitavou 9:2 (5:1)**

Do posledního utkání v turnaji jsme šli s tím, že si musíme spravit chuť a reputaci z předešlé remízy, díky které Sebranice udržely v tabulce turnaje průběžné vedení. Soupeř výborně bránil a hrozil nebezpečnými brejky. I přes nasčítanou únavu se naši hráči moc snažili a gólový přírůstek postupně navyšovali, ale protože v souběžně hraném utkání Sebranice dokázali stejně jako proti nám smazat dvougólové manko a otočit zápas s Němčicemi, zbylo na nás o skóre druhé místo.

**Branky:** Vojtěch Huryta 3,  František Flídr 2, Martina Famfulíková 2,  Šimon Flídr, David Klusoň

**Další výsledky turnaje:** 

Čistá – Jevíčko 5:2, Čistá – Němčice 1:1, Čistá – Bystré 0:4, Čistá – Sebranice 0:6, Čistá - Březová nad Svitavou 1:1, Jevíčko – Němčice 1:5,  Jevíčko - Bystré  2:4, Jevíčko – Sebranice 1:7,  Jevíčko - Březová nad Svitavou 0:2, Němčice – Bystré 1:2, Němčice - Březová nad Svitavou 1:3, Bystré – Sebranice 2:5, Bystré – Březová nad Svitavou 3:1, Sebranice - Březová nad Svitavou 6:1, Němčice – Sebranice 4:5

V posledním kole jsme se na chvíli virtuálně posunuli do čela, ale Sebranice proti Němčicím dokázali stejně jako proti nám smazat dvougólové manko a otočit zápas, a tak na nás nakonec nás zbylo o skóre druhé místo. Samozřejmě to mrzí, ale jinak jsme ukázali, že fotbal hrát umíme. A že máme v Martě Famfulíkové nejlepší brankářku, jak potvrdilo předávání individuálních ocenění.

**Tabulka turnaje:**

1.Sebranice                          31:10    16 bodů

**2.Horní Újezd/Morašice     23:6      16 bodů**

3.Bystré                                 15:10    12 bodů

4.Březová nad Svitavou      10:20      7 bodů

5.Čistá                                      8:16      5 bodů

6.Němčice                              12:17     4 body

7.Jevíčko                                   7:27     0 bodů

autor: Martin Kovář