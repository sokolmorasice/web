---
title: Teď náš přejde HÚ / MOR
date: 2022-06-11T20:52:46.017Z
category: pripravka
image: /public/images/uploads/logo.png
---
# Mladší přípravka

Posledního turnaje mladších přípravek se jako obvykle účastnily všechny týmy hrající okresní soutěž, až na omluvené Jevíčko a Jaroměřice. Mužstva byla rozdělena do dvou základních skupin, ve kterých se hrálo systémem každý s každým, na závěr následovaly boje o konečné umístění. Každý zápas trval 15 minut.

**Sestava:** Jan Langr - Jaroslav Chaun, Julie Vodehnalová, Miriam Vomočilová, Štěpán Kovář, Tereza Veselíková, Ivo Langr, Patrik Vurkminovič

Základní skupina:

**Horní Újezd/Morašice – Litomyšl B 1:1**

**Branka**: vlastní

**Horní Újezd/Morašice – Dlouhá Loučka 6:2**

**Branky:** Tereza Veselíková 3, Jaroslav Chaun 3

**Horní Újezd/Morašice – Bystré 2:7**

**Branky:** Jaroslav Chaun, Tereza Veselíková

**Horní Újezd/Morašice – Pomezí 0:5**

O 7. místo:

**Horní Újezd/Morašice – Hradec n. S. 1:5**

**Branka:** Jaroslav Chaun

První dva zápasy ve skupině byly herně vydařené. S Bystrým jsme bojovali, ale kvalita byla na straně soupeře. Pomezí bylo výrazně fotbalovější. Zápas o 7. místo byl v poli vyrovnaný, ale moc šancí ke skórování jsme si nevytvořili. Na turnaji nám citelně chyběly obě obvyklé brankařky.

**Tabulka turnaje:**

1.Čistá

2.Bystré

3.Sebranice

4.Pomezí

5.Březová

6.Litomyšl B

7.Hradec n. S.

**8.Horní Újezd/Morašice**

9.Borová

10.Dlouhá Loučka

Turnaj byl minimálně na rok poslední společnou akcí v této kategorii v souklubí s Horním Újezdem, do nového ročníku přihlásíme mladší přípravky samostatně. 

autor: Mgr. Zdeněk Beneš