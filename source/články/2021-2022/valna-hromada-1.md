---
title: Valná hromada
date: 2022-04-08T21:13:44.850Z
category: obecne
image: /public/images/uploads/logo.png
---
8.4. se konala v občerstvení na hřišti volební valná hromada TJ Sokol Morašice. Přibližně 25 členů si po odbytí formalit vyslechlo výroční zprávu za loňský rok, zprávy trenérů jednotlivých kategorií, zprávu o činnosti výkonného výboru za uplynulé období, zprávu o výsledcích hospodaření a zprávu Kontrolní a revizní komise.

Valná hromada pak schválila změnu stanov o prodloužení funkčního období výkonného výboru i předsedy na 4 roky a změnu výše členských příspěvků. 

Poté byl s výjimkou dvou hlasů zvolen výkonný výbor ve složení:

Krejsová Dana - předsedkyně

Chmelík Milan - místopředseda

Štancl Václav - sekretář

Andrle Vojtěch

Famfulík Jiří

Klement Milan

Kopecký Michal st.

Kopecký Miloš

Ropek František

Štancl Dominik

Štancl Martin

Následovalo zvolení Kotrolní a revizní komise ve složení:

Famfulíková Zuzana - předsedkyně

Kuřová Lenka

Novák Václav

Diskuse se protáhla hlavně debatou ohledně zajištění sekání hřiště a možností, jak dotáhnout na hřiště více dětí.