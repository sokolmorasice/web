---
title: Zápisy do historie
date: 2022-06-30T20:07:05.074Z
category: obecne
image: /public/images/uploads/logo.png
---
A jakých milníků a posunů kdo dosáhl v historických tabulkách? Při pohledu na tabulky [celkové](https://docs.google.com/spreadsheets/d/1leVjJu7cU-j48mBYp9hx7PgFwTmH01byvIbeJqPWgPo/edit#gid=1613289360), zahrnující žáky, dorost a muže, musíme začít Bohumilem Válkem, který 7 jarními góly přeskočil Petra Kutu na čele střelců. M. Ropek a Chmelík se jedním gólem příblížili 11. resp.28. pozici, J. Štěpánovi jeden gól k posunu na sdílené 22.místo stačil. V zápasech se Válek přiblížil 600 zápasům, J. Štěpán dorovnal Holomka na 14. místě s 519 zápasy, V. Štancl pokořil hranici 500 zápasů a dorovnal bratrance Martina na 18. pozici.

V tabulkách čistě [mužských](https://docs.google.com/spreadsheets/d/1leVjJu7cU-j48mBYp9hx7PgFwTmH01byvIbeJqPWgPo/edit#gid=606575394) se také činil Válek, když dotáhl na 3. místě střelců Františka Ropka a k dalším posunům mu tak pomůže "jen" 33, resp.58 gólů. M. Ropek se o něco přiblížil 9. Jiřímu Jánovi a hranici100 gólů (aktuálně 97), na chvost TOP 30 se prostřílel V. Štancl. V zápasech se o dvě příčky na 15. posunul Válek se 419 zápasy. Skala přeskočil co do zápasů Holomka a M. Štancla, ale zároveň ho o zápas předběhl V. Štancl, takže jim s 330, resp. 329 zápasy patří 21. a 22. příčka. A posun na 25. místo před A. Buriána zaznamenal navrátilec J. Štěpán (318 zápasů).

Co se týče [odchytaných](https://docs.google.com/spreadsheets/d/1leVjJu7cU-j48mBYp9hx7PgFwTmH01byvIbeJqPWgPo/edit#gid=323301644) zápasů za áčko, V. Štancl se posunul o tři příčky na 14. pozici a Špinar zůstal jeden zápas od toho, aby se jako pátý gólman postavil mezi morašické tyče stokrát.

Aktivní hráči se na svá čísla mohou podívat na [soupisce](https://sokolmorasice.cz/o-klubu/hr%C3%A1%C4%8Di), bývalí hráči nefigurující v TOP 30 se mohou najít [zde](https://docs.google.com/spreadsheets/d/1leVjJu7cU-j48mBYp9hx7PgFwTmH01byvIbeJqPWgPo/edit#gid=1378160355), zájemcům o historii připomínáme [tuto](https://sokolmorasice.cz/o-klubu/hr%C3%A1%C4%8Di) záložku na našich stránkách.