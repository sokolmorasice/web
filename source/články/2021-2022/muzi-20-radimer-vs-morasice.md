---
title: Léčba venkem
date: 2022-05-07
category: muzi
image: /public/images/zapasy/radimer-morasice.png
h1: Radiměř vs Morašice
sestava: V. Štancl - V. Tměj, J. Kopecký (M. Ropek), M. Černohorský, V. Novák -
  J. Vít (T. Nádvorník), D. Štancl, B. Válek, D. Ondráček - F. Bureš (M. Frank),
  Z. Skala
goly: V. Novák 2 (Z. Skala, B. Válek), F. Bureš (B. Válek), M. Ropek (Z. Skala),
  M. Frank (B. Válek)
zlute: J. Vít
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533A1A/2021/Morašice/fd2c0313-dd39-4b44-a0e4-a68ece00f814.html
---
Po dvou domácích ztrátách, z nichž byla každá jiným způsobem laciná, jsme třízápasovou sérii venkovních zápasů začali na hřišti tonoucí Radiměře. Ale protože naše fotbalové gentlemanství nezná meze, hodili jsme jim první čtvrthodinu pořádnou udičku, aby se chytli. Naštěstí z našeho šíleně laxního bránění a častých pádů jejich kapitána vytěžili pouze několik standartek a hlavičku mimo. I během té čtvrt hodiny jsme přes časté ztráty více hrozili my, ale domácí gólman kryl jak střelu Ondráčka, tak průnik Nováka, který si to z beka, kam byl umístěn abychom z něho setřásli tíhu očekávaných vstřelených gólů, namířil přes narážečky až do soupeřova vápna, ale z ostrého úhlu nepochodil. Po našem prvním rohu hlavičkoval neobsazený Bureš pouze do břevna a v přebírání otěží hry nás nevyrušilo ani vynucené omlazení obrany Ropkem. Hodně prostoru měl napravo Ondráček, který dvakrát předložil míč na malé vápno. Poprvé shluklé trojici Válek, Bureš, Skala, ale jeden to měl na dlouhou, druhý na paty a třetí nečekal, že to nikdo mít nebude. Podruhé si náš záložník jako cíl vybral Válka, který se ale osamocené pozice před gólmanem asi lekl, nebo mu zasvítilo slunce do zad, a zakončil slabounce přesně do náruče. Před přestávkou ještě jednou potvrdil dobrý výběr místa a špatné hlavičkářské schopnosti Bureš, ale poločasové vedení nám v poslední možné chvíli zajistil Novák, který do sítě protečoval Skalův přímák z levé strany.

Vstup do druhého dějství rozhodl zápas. Nejprve se po třech minutách prokličkoval k centru Tměj, a po Válkově přizvednutí do míče plácl na penaltě levačkou Bureš. Nebyla to zrovna střela nechytatelné rychlosti, ale do brány si cestu našla. O dvě minuty později zakončil obléhání vápna Ondráček průnikovkou na Válka, ten nezazmatkoval, potáhl míč až do lajny, nádherným obloučkem na zadní tyč vyšachoval obranu i gólmana a do útoku opět vysunutý Novák se jednoduchou hlavičkou do prázdné brány ocitl 40 minut od hattricku. Tempo pak opadlo, do vyložených pozic jsme se přes většinovou kontrolu míče vinou horších řešení ve finální fázi už moc nedostávali. Ty největší měli Frank a Skala, kterým ale hlavičku do břevna a samostatný nájezd pro strkání a ofsajd z říše snů stornovali rozhodčí. Ti se k lepšímu pískání nedali přesvědčit ani Vítem. Pět minut před koncem zahrával Skala přímák podobný tomu předpoločasovému a k němu se před stojícím gólmanem dostal Ropek, který míč hlavou takřka donesl do sítě a na tři góly se přiblížil metě sta vstřelených gólů za muže. V úplném závěru ještě Skala vyslal do nájezdu Válka, ale ten se rozhodl, že Skalovi zhatí bilanci 0+3 a že on bude mít raději třetí nahrávku než 1+2. Takže odrazem o gólmana napálil hlavu dobíhajícího Franka, od které míč doputoval do sítě. Povinná výhra tedy splněna, ale nedělejme z toho aféru.