---
title: Zase (jenom) jedna prohra
date: 2021-10-03T06:26:26.283Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Mladší přípravka

**Sestava:** Hlušičková T., Chaun J., Vodehnalová J., Valentín K., Vurkminovič P., Bartoš A., Vomočilová M., Kovář Š., Veselíková T., Langr J., Langr I.

**Horní Újezd/Morašice – Litomyšl „B“ 1:4 (1:2)**

Z města přijel do Jaroměřic jiný tým, než který jsme v Morašicích porazili 8:1, kvalita soupeře nás donutila předvést velice dobrý výkon, i přes prohru se jednalo o náš nejlepší sobotní zápas.

**Branka:** Valentín K.

**Horní Újezd/Morašice – Borová 5:3 (3:3)**

Nepovedl se nám začátek a konec prvního dějství, navíc na druhou půli musel místo Terezy Hlušičkové do branky Adam Bartoš, i tak jsme díky velice zodpovědnému výkonu po pauze dovedli zápas do zdárného konce.

**Branky:** Chaun J. 2x, Valentín K., Bartoš A., Vurkminovič P.

**Jaroměřice – Horní Újezd/Morašice 0:7 (0:3)**

Výrazně mladší tým domácích bojoval statečně, ale věkový rozdíl byl znát.

**Branky:** Chaun J. 5x, Bartoš A., Vodehnalová J.

**Tabulka turnaje:**

1.Litomyšl „B“

**2.Horní Újezd/Morašice**

3.Borová

4.Jaroměřice

Mgr. Zdeněk Beneš

## Starší přípravka

Do turnaje v Březové, který provázelo relativně studené počasí, jsme nastoupili kompletní, ale s vědomím, že nás čekají dva soupeři, se kterými jsme ještě na podzim nehráli.

**HÚ / Morašice – Sebranice 8 : 0**

Soupeř, se kterým jsme minulý týden odehráli vyrovnané utkání nebyl tentokrát kompletní a to určilo i  vývoj celého zápasu, kdy jsme si celkem bez problémů došli pro vítězství.

**Branky:** Fanda Flídr 3x, Šimon Flídr 2x, Samuel Vomočil 1x, Jarda Chaun 1x a Vojta Huryta 1x.

**HÚ / Morašice – Březová 4 : 1**

Tento zápas byl o poznání vyrovnanější než ten předešlý, a to i naší zásluhou, kdy hra postrádala lehkost, chyběly nahrávky a pohyb. Šancí jsme si vytvořili na dva zápasy, ale bohužel se nám nedařilo je proměňovat. Naštěstí se na to už nemohla dívat Martina Famfulíková a svým gólem nám zajistila dostatečný náskok, který jsme již udrželi.

**Branky:** Jarda Chaun 1x, Fanda Flídr 1x, Martina Famfulíková 1x a Vojta Huryta 1x.

**HÚ / Morašice -  Jevíčko 6 : 0**

V posledním zápase  se náš výkon již začal přibližovat tomu co umíme a bylo to pochopitelně znát i na výsledku a předvedené hře. Toto vítězství nám tak zajistilo i celkové prvenství v celém turnaji.

**Branky:** Fanda Flídr 2x, Vojta Huryta 1x, Martina Famfulíková 1x, Šimon Flídr 1x a Samuel Vomočil 1x.

František Bartoš