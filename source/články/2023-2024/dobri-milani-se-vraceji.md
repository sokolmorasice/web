---
title: Dobří Milani se vracejí?
date: 2024-03-09T21:06:32.595Z
category: muzi
image: /public/images/uploads/logo.png
h1: Morašice – Pomezí 	4:2 (3:0)
sestava: J. Špinar – L. Rusnák, M. Chmelík, V. Novák, J. Vít, D. Štancl, T.
  Nádvorník, J. Štancl, P. Lněnička, Z. Skala, F. Bureš, M. Frank, J. Kopecký,
  M. Klement
goly: F. Bureš (D. Štancl), M. Frank (L. Rusnák), P. Lněnička (D. Štancl), D.
  Štancl (L. Rusnák)
---
Do třetice na umělce, tentokrát s Pomezím. Naši úzkou sestavu (doufejme, že dočasně) jsme pro změnu doplnili hráči, řadící se již do kategorie staré gardy, Chmelíkem a Klementem. Nutno ale říci, že i bez nich bychom na tom nebyli hůře než soupeř, který neměl jediný kus na střídání. První poločas patřil nám, Špinar v bráně byl prakticky bez práce. Jediným vykřičníkem byli dva nebo tři rohy, kdy soupeř téměř nikým neobsazen hlavičkoval, ale naštěstí pro nás nemířil vůbec přesně. Do vedení jsme se dostali poměrně brzo, když obránci pod nohou prošla přihrávka ze středu hřiště a Bureš samostatný nájezd proměnil. Dva góly během chvíle obstarali nejprve Frank, který si po Rusnákově přízemním centru narazil s gólmanem, aby měl přeci jen větší jistotu, že na jeho dorážku už nedosáhne, a Lněnička, který svojí slabší pravačkou umístil vyraženou střelu D. Štancla pod břevno. První poločas byl ukončen předčasně, neboť pana rozhodčího volala příroda, ale o tento čas jsme ochuzeni nebyli.

Rotace v sestavě a pozicích měla zásadní vliv na naší nabídce, obsazování hráčů a klidu na míči. Tím jsme dali „bétřídnímu“ soupeři dostatek prostoru na klidnější kombinaci na středu hřiště a rozdávání míče na svá rychlá křídla. Kontaktní gól přišel po chybě D. Štancla, který u lajny neodkopl ani neudržel balón a útočníci akci dva na nikoho bezpečně proměnili. Pomezí snížilo na rozdíl jediného gólu, když Špinar neudržel centr z přímého kopu v náruči a nikdo z našich hráčů nedobíhal dorážejícího útočníka. Mezitím nás však Špinar podržel jedním vychytaným samostatným nájezdem. Šance ale byly i na druhé straně, ale Lněnička nejprve střílel do tyče, podruhé při rodícím se úniku byl včas zakřižován, Bureš při závaru ve vápně střílel z polootočky těsně vedle, stejně tak Nádvorník z hranice vápna. Další kanadské body v zápase si připsali Rusnák s D. Štanclem, když prvně jmenovanému se „nepovedl“ centr, ale přihrávkou po zemi našel mezi obránci svého spoluhráče, který zakončil mimo dosah gólmana. Konečně výhra, konečně povedený výkon, škoda výpadku. Jen ještě ta docházka.