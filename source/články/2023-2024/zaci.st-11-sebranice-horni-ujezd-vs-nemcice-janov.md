---
title: Úspěšná odveta
date: 2024-04-14
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Sebranice/Horní Újezd vs Němčice/Janov
sestava: Petr Kovář - Jan Kopecký, Lukáš Mach, Pavel Šprojcar, Josef Rosypal,
  Štěpán Němec, Patrik Novotný, Jan Kvapil, Josef Peška, Matyáš Bulva, Filip
  Flídr, Matyáš Vomáčka, Daniel Branda, Jakub Dvořák
goly: Filip Flídr, Štěpán Němec 2
zlute: null
cervene: null
multimedia: https://www.tjhorniujezd.cz/?p=17420
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2023/Sebranice/Horní
  Újezd/6b95fb54-8022-4f38-8fe8-da96a443400a.html
---
V neděli jsme odehráli důležité utkání proti týmu z Němčic a Janova, kterému jsme chtěli odskočit v tabulce a zároveň oplatit podzimní porážku. Zápas se hrál na Horním Újezdě před skvělou domácí kulisou. Zápas jsme rozehráli dobře a hned od začátku si vytvářeli gólové šance. První jsme proměnili v 10. minutě střelou Filipa Flídra. Potom se šance přelévaly z jedné strany na druhou. Naštěstí pro nás soupeř zahodil mnoho gólovek. Když už jsme si pomalu mysleli, že půjdeme do kabiny s jednogólovým náskokem, soupeř udeřil a ve 29. minutě vyrovnal.

Druhý poločas jsme rozehráli stejně dobře jako první. Hned ve 36. minutě využil krásný pas Štěpán Němec a vrátil nám vedení. Potom se poločas odehrával ve stejném duchu jako ten první. Až se nám podařilo v 67. minutě vstřelit uklidňující třetí branku. Závěr jsme si již pohlídali a zápas jsme dovedli do vítězného konce, čímž jsme si upevnili první místo v tabulce.