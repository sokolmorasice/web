---
title: Sem tam výhra, pořád radost
date: 2023-09-28T20:03:06.821Z
category: pripravka
image: /public/images/uploads/logo.png
---
# S﻿tarší přípravka

### 9﻿.9.2023 - turnaj v Němčicích

**s﻿estava:** Max Čechal, Matouš Jetmar, Jiří Kubík, Jan Langr, David Soukup, Viktor Špás, Matyáš Tóth, Matěj Vrbica, Šimon Vrbica, Patrik Vukmirovič, Josef Žatečka, Ivo Langr

**M﻿orašice - Sebranice 2:6**

**b﻿ranky:** Lukáš Tichý 2

**M﻿orašice - Němčice 5:9**

**b﻿ranky:** Šimon Vrbica 2, Jan Langr 2, Matouš Jetmar

### 1﻿6.9. 2023 - turnaj v Hradci nad Svitavou

**s﻿estava:** Max Čechal, Matouš Jetmar, Jiří Kubík, Jan Langr, David Soukup, Viktor Špás, Matyáš Tóth, Matěj Vrbica, Šimon Vrbica, Patrik Vukmirovič, Josef Žatečka,  Lukáš Poslušný

**M﻿orašice - Hradec nad Svitavou 0:5**

**M﻿orašice - Borová 0:5**

**M﻿orašice - Radiměř 12:1**

**b﻿ranky:** Šimon Vrbica 4, Matyáš Tóth 2, Lukáš Tichý 2, Matouš Jetmar 2, Jan Langr 2

### **2﻿4.9.2023 - turnaj v Březové**

**s﻿estava:** Max Čechal, Matouš Jetmar, Jiří Kubík, Jan Langr, David Soukup, Viktor Špás, Matyáš Tóth, Matěj Vrbica, Šimon Vrbica, Patrik Vukmirovič, Josef Žatečka,  Lukáš Poslušný

**M﻿orašice - Březová 3:8 (2:4)**

**b﻿ranky:** Šimon Vrbica, Viktor Špás, Patrik Vukmirovič

**M﻿orašice - Sebranice 0:2 (0:2)**

**M﻿orašice - Horní Újezd 0:4 (0:2)**

# **M﻿ladší přípravka**

### 2﻿3.9.2023 - turnaj v Morašicích

**s﻿estava:** Jiří Famfulík, Lukáš Chmelík, Jan Klement, Kateřina Kovářová, Pavel Kubík, Ivo Langr, Filip Šplíchal,  Daniel Hlušička, Jiří Voženílek

**M﻿orašice - Borová 4:3 (2:1)**

**b﻿ranky:** Jan Klement 2, Lukáš Chmelík 2

**M﻿orašice - Sebranice 3:9 (1:4)**

**b﻿ranky:** Jan Klement 2, Ivo Langr

**M﻿orašice - Čistá 3:9 (1:5)**

**b﻿ranky:** Jan Klement, Lukáš Chmelík, Ivo Langr