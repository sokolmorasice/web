---
title: Nová doba
date: 2023-08-26
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Borová vs Sebranice/Horní Újezd
sestava: Petr Kovář - Barbora Kovářová, Štěpán Němec, Pavel Šprojcar, Sára
  Vomočilová, Lukáš Mach, Jan Kvapil, Vojtěch Štěpán, Šimon Flídr, David Klusoň,
  Jan Kopecký, Jakub Dvořák, Josef Peška
goly: Lukáš Mach, Jan Kvapil
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2023/Sebranice/Horní
  Újezd/58575ce2-31c8-47d6-a724-8095e28a8f0f.html
---
S posledním prázdninovým víkendem se rozjela nová žákovská sezóna, která nám přináší jednu zásadní změnu. Starší žáci se sloučili do trojklubí Morašice, Horní Újezd a Sebranice. Protože formálně trojklubí není možné, v tabulce nás proto najdete pouze jako Sebranice / Horní Újezd. Na první mistrovský zápas jsme odjeli do Telecího, kde jsme se utkali s týmem Borové. Zápas byl ještě poznamenán prázdninami, jednak nás bylo pouze 11, jednak hlavně ze začátku vázla souhra. Postupem času jsme si ale začali vytvářet šance, jenom je ne a ne proměnit. Až do podařené střely Lukáše Macha, který levou nohou uklidil balón do branky. Naše převaha pokračovala, některé šance byly opravdu pěkně vypracované, ale ujal se až ve 43.minutě rohový kop, ze kterého Lukáš Mach krásně našel Honzu Kvapila, který se nemýlil. Ještě několik naších šancí zůstalo bohužel nevyužito. Do příštího zápasu snad dáme dohromady lepší počet a pojedeme dál na vítězné vlně.