---
title: One man show
date: 2023-09-17
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Jevíčko/Jaroměřice
sestava: Jaroslav Chaun - Sára Vomočilová, David Klusoň, Terezie Čejková, Martin
  Čejka, Matěj Kovář, David Boček, Michal Kovář, Samuel Vomočil, František
  Flídr, Šimon Flídr, Dominik Prokop, Jonáš Rovner, Antonín Krejsa
goly: Šimon Flídr 3, František Flídr, Jonáš Rovner
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2023/Horní
  Újezd/Morašice/51568637-254a-4ff4-9576-077b10867980.html
---
Bylo znát, že jsme narazili na soupeře, který by měl patřit do popředí tabulky, stejně jako my. Ve vyrovnaném prvním poločase jsme inkasovali jako první, ale šikovností Šimona Flídra, který dva góly a dal a jeden svou akcí připravil, se nám podařilo soupeři odskočit na dvě branky. Z naší převahy po přestávce vytěžil Šimon Flídr dokonaný hattrick, na soupeřvů gól jsme dokázali ještě jednou odpovědět a závěr už si i díky přežité penaltě pohlídali.