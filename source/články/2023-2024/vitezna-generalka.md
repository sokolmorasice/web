---
title: Vítězná generálka
date: 2024-03-16T16:22:19.762Z
category: muzi
image: /public/images/uploads/logo.png
h1: Morašice – Sebranice 	5:0 (2:0)
sestava: L. Rusnák (J. Špinar) – M. Frank, J. Kopecký, M. Ropek, V. Novák, D.
  Štancl, T. Nádvorník, J. Štancl, J. Vít, F. Bureš, F. Halsbach, M. Kopecký, M.
  Černohorský, V. Štancl
goly: T. Nádvorník (D. Štancl), M. Kopecký (V. Novák), V. Novák (F. Bureš), M.
  Kopecký (T. Nádvorník), J. Štancl (D. Štancl)
---
V posledním přípravném utkání před začátkem jarní části okresního přeboru jsme poměřili síly s mužstvem ze Sebranic. První poločas naznačil, že to nebude jednoduchý zápas, neboť naší nedůsledností a místy laxním obsazováním jsme soupeři dovolili více šancí, než bylo zdrávo. Naštěstí napříč celým poločasem žádnou nevyužil, ale jedna střela do tyče, druhá těsně vedle a samostatný nájezd lapený Rusnákem byly jistým vykřičníkem. V tomto směru jsme byli úspěšnější. Když se nepodařilo využít žádný z dobře kopnutých Vítových rohů, proměnil svůj nájezd Nádvorník, který si na půlce obhodil posledního hráče a po obstřelení gólmana nám zajistil vedení. Vedení navýšil M. Kopecký, který zužitkoval přihrávku pod sebe od Nováka po kombinaci na pravé straně. Na míči jsme sice byli lepší, ale některá zbrklá rozhodnutí nám zkazila vytvoření si více šancí, a tak se již nic nezměnilo.

Jaký to paradox, že jsme soupeři nechávali více prostoru a umožnili mu více kombinovat, ale Špinar, který se postavil do brány na druhý poločas, nemusel vytáhnout jediný zákrok. Naše útočné snahy byly velmi podobné těm v prvním poločase, kdy občas stačilo zachovat chladnou hlavu, ale nějakou tu fotbalovost jsme také ukázali. A k tomu přidali ještě tři góly. Nejprve se po dobře sehraném autovém vhazování protlačil do vápna Novák a zakončil přesně na zadní tyč. Poté vyslal Nádvorník mezi obránci do samostatného úniku M. Kopeckého a ten se nemýlil. A náš třetí nájezd v zápase proměnil J. Štancl po přihrávce od D. Štancla. Zimní přípravu jsme tak zakončili vysokou výhrou. Příští týden přivítáme na domácím hřišti od 15:00 Dlouhou Loučku, na kterou ztrácíme dva body a je jasné, že nás nečeká nic lehkého.