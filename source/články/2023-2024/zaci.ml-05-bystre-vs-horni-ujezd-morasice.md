---
title: Pořád na kontakt
date: 2023-09-24
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Bystré vs Horní Újezd/Morašice
sestava: Jaroslav Chaun - Sára Vomočilová, David Klusoň, Martin Čejka, David
  Boček, Michal Kovář, Samuel Vomočil, František Flídr, Šimon Flídr, Dominik
  Prokop, Miriam Vomočilová
goly: František Flídr, Šimon Flídr
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2023/Horní
  Újezd/Morašice/00381f20-eb03-4de7-9c19-80edb71545de.html
---
Do našeho dalšího zápasu, tentokrát v Bystrém, vstoupili lépe domácí, kteří byli v první půli aktivnější, lépe tvořili hru a byli za to odměněni brankou. Až poté jsme se začali probouzet a soupeři vyrovnávat i herně. Aktivněji jsme vstoupili i do druhé půle, ale nechali jsme si dát druhý gól. To nás vybudilo k ještě vyšší aktivitě, za což jsme byli odměněni dvěma góly v krátkém čase. Od té doby jsme byli těmi lepšími na place, jen jsme nedokázali zužitkovat žádnou z mnoha šancí. Dělba bodů je spravedlivá pro oba týmy – my jsme malinko šťastnější za bod ze hřiště soupeře, který nám tak v tabulce neodskočil.