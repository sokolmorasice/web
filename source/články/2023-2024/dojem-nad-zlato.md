---
title: Dojem nad zlato
date: 2023-10-21T21:52:54.545Z
category: pripravka
image: /public/images/uploads/logo.png
---
## S﻿tarší přípravka

##### **7﻿.10.2023 Pomezí**

T﻿urnaj nás nezastihl v nejlepší formě. Chyběl brankář a typický útočník, který by vstřelil branku. Po protočení brankářů se ukázalo, že Lukáš Poslušný přeci jen zdědil nějaké geny po dědovi ;-)

###### P﻿omezí - **Morašice** 11:0 (4:0)

###### **M﻿orašice** - Kunčina 0:8 (0:6)

**S﻿estava:** Max Čechal, Matouš Jetmar, Jiří Kubík, Jan Langr, Lukáš Poslušný, David Soukup, Viktor Špás, Matyáš Tóth, Matěj Vrbica, Šimon Vrbica, Patrik Vukmirovič, Josef Žatečka

##### **14﻿.10.2023 Morašice**

N﻿a turnaj nedorazila Radiměř. Odehráli jsme dva zápasy s Městečkem Trnávka. První (oficiální) zápas jsme prohráli 7:2, kdy nám jeden výjimečný borec z Trnávky dal 5 kusů. 

###### **M﻿orašice** - Městečko Trnávka 2:7

**S﻿estava:** Max Čechal, Matouš Jetmar, Jiří Kubík, Jan Langr, Lukáš Poslušný, David Soukup, Viktor Špás, Matyáš Tóth, Matěj Vrbica, Šimon Vrbica, Patrik Vukmirovič, Josef Žatečka

## Mladší přípravka

##### **8﻿.10.2023 Telecí - zrušeno pro podmáčený terén**

##### **1﻿5.10.2023 Březová nad Svitavou**

###### D﻿louhá Loučka - **Morašice** 7:4 (4:2)

###### **M﻿orašice** - Borová 6:1 (1:1)

###### B﻿řezová nad Svitavou - **Morašice** 10:3 (6:1)

**S﻿estava:** Jiří Famfulík, Lukáš Chmelík, Jan Klement, Pavel Kubík, Ivo Langr, Jiří Voženílek, Jiří Kubík

**B﻿ranky:** 3 - Lukáš Chmelík, 5 - Jan Klement, 5 - Ivo Langr