---
title: V neděli s rodinou
date: 2024-03-04T19:03:44.199Z
category: muzi
image: /public/images/uploads/logo.png
h1: Morašice - Libchavy 1:2 (0:1)
sestava: L. Rusnák (J. Špinar) - D.Ondráček, M. Černohorský, M. Ropek st., J.
  Vít, D. Štancl, J. Štancl, V. Novák, Z. Skala, M. Kopecký, J. Kopecký, P.
  Lněnička, M. Ropek ml.
goly: V. Novák
---
Druhé zimní přátelské utkání na UMT v Litomyšli jsme sehráli s prvním týmem sousední okresní soutěže Libchavami. Naše sestava se během 24 hodin před výkopem opět povážlivě ztenčila, tak jsme opět sáhli do rodinných zdrojů, tentokrát nás doplnil Martin Ropek mladší. A jakkoli máme naše půlstoleté dějiny podrobně zmapované, společný start otce se synem v nich dosud chyběl. Početně jsme bohužel po kotníkové eskapádě M. Kopeckého byli zase brzo na původním stavu. Po většinu zápasu měl více ze hry pohyblivější soupeř, hlavně díky středním záložníkům. Naši ojedinělou, a tudíž největší šanci prvního poločasu zaznamenal Ropek starší, ale odražený míč po vlastní hlavičce napálili pouze do břevna. Na konci první půle jsme prohrávali 1:0, když útočník prostřelil po úniku ze strany Rusnáka.

Hned zkraje druhé půle jsme mohli nabrat vyšší ztrátu, ale buď nás zachránilo břevno nebo Špinar. Libchavy se nakonec druhého gólu dočkaly, když jsme při autovém vhazování na půlce špatně rozebrali protihráče a následnou akci nedokázali zastavit. Výsledek korigoval Novák, který při presování zachytil špatnou rozehru brankáře a zakončil do odkryté brány. Na skóre se už nic nezměnilo, neboť D. Štancl po rohu hlavou netrefil šibenici, Rusnák přehodil vše, co mu stálo v cestě, a ani soupeř žádnou ze svých útočných akcí nedotáhl do zdárného konce. Ze zimní přípravy tak máme stále pomyslných nula bodů a stále je co zlepšovat, nejprve ze všeho docházku.