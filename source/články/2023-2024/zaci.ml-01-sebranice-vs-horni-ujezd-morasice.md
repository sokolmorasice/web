---
title: Vydařený start
date: 2023-08-29
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Sebranice vs Horní Újezd/Morašice
sestava: Matyáš Lněnička – Sára Vomočilová, Martin Čejka, Terezie Čejková, David
  Klusoň, Matěj Kovář, David Boček, Michal Kovář, Samuel Vomočil, František
  Flídr, Šimon Flídr, Dominik Prokop, Jonáš Rovner
goly: František Flídr, Šimon Flídr, David Boček, David Klusoň, vlastní
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2023/Horní
  Újezd/Morašice/9d2637bc-0592-48d8-9d21-6512c6299d3c.html
---
Do nové sezóny jsme mladších žáků jsme vstoupili v zažitém souklubí s Horním Újezdem, ale už bez Marti Famfulíkové a Vojty Huryty, kteří tento rok zkusí fotbalové štěstí ve Vysokém Mýtě. V Sebranicích se potkaly dva velmi vyrovnané týmy, což bylo vidět už od začátku. Hru provázela spousta nepřesností, ale to bylo dané i tím, že někteří hráči se poprvé potkali až v tento den na hřišti. Postupně se hra přesunula převážně na soupeřovu polovinu, ze které nám občas pohrozil rychlým brejkem. Občasné nepřesnosti se nás držely po celé utkání, ale byly dané i tím, že se snažíme zapojit do hry celé naše početné družstvo. Za konečný výsledek i body jsme velice vděčni, ze Sebranic se nevozí zas tak lehce.