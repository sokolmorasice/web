---
title: Telecí poločas
date: 2024-05-11
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Borová vs Sebranice/Horní Újezd
sestava: Petr Kovář - Josef Rosypal, Jan Kopecký, Lukáš Mach, Marek Mokrejš,
  Pavel Šprojcar, Štěpán Němec, Patrik Novotný, Jan Kvapil, Josef Peška, Šimon
  Flídr, Matyáš Bulva, Filip Flídr, Matyáš Vomáčka
goly: Filip Flídr 3, Šimon Flídr, Patrik Novotný
zlute: Jan Kopecký
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2023/Sebranice/Horní
  Újezd/ff40f8a0-705a-4bdf-a2d0-f5ee9a25a28d.html
---
K sobotnímu utkání jsme jeli do Telecího, kde jsme hráli s mužstvem Borové. Do zápasu jsme vstoupili s větším odhodláním a už v první minutě dal po krásné akci dal Šimon Flídr branku. Celý první poločas jsme si pěknými kombinacemi vytvářeli mnoho gólových příležitosti, v 7. a 17.minutě z nich vytěžil další dva góly Filip Flídr. Než stihl v poslední minutě poločasu zkompletovat hattrick, prosadil se ještě Patrik Novotný a pětigólové vedení po poločase s našimi hráči zamávalo. V druhém poločase přestali kombinovat, hráli hodně sami na sebe, z neúspěšných akcí jim přibývala nervozita, která se promítla i do diskusí s rozhodčím. Soupeř naštěstí naší proměnu nijak nepotrestal, přesto by měl být druhý poločas varováním, abychom se pro příští zápas vrátili ke hře z prvního poločasu, a soustředili se sami na sebe a na nikoho jiného.