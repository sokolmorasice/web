---
title: Vedení uhájeno až do konce
date: 2023-10-01
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Březová vs Sebranice/Horní Újezd
sestava: P. Kovář - J. Kopecký, B. Kovářová, V. Štěpán, P. Šprojcar, L. Mach, Š.
  Němec, P. Novotný, J. Kvapil, M. Bulva, D. Branda, J. Rosypal, J. Dvořák, J.
  Peška
goly: J. Rosypal, Š. Němec, M. Bulva
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2023/Sebranice/Horní
  Újezd/364b32c5-292e-4e88-b6fb-ffe53439370b.html
---
<!--StartFragment-->

V neděli jsme zavítali se staršími žáky do Březové.  Počasí  nám přálo, tak to vypadalo na pěkné dopoledne.

První poločas jsme rozehráli dobře.  Měli jsme neustálý tlak, který jsme ne a ne proměnit.  Až ve dvacáté čtvrté minutě se nám podařilo skórovat. Pepa Rosypal uklidil balón tam, kam patřil už dávno: Do soupeřovy brány. A to bylo od nás na dlouho vše. Balón jsme měli i dále pod kontrolou my, ale herně jsme se hledali. A tak jsme dohráli i první poločas.

Do druhého poločasu soupeř nastoupil více namotivován a po pěti minutách srovnal skóre. Nám se potom dvěma brankami podařilo skóre navýšit zásluhou Štěpána Němce (46 min.) a Matyáše Bulvy (52 min.), ale to bylo od nás skoro všechno. Občas jsme lehce zahrozili, ale ke gólů to nevedlo. Naopak soupeř začínal čím dál víc hrozit.  V šedesáté osmé minutě snížil na 2:3. Poté jsme se začali strachovat o vítězství.  Rozhodčí nastavil dvě minuty a domácí spálili velkou šanci na srovnání.  Takže se nám podařilo zápas dotáhnout do vítězného konce.  Do příštího zápasu bychom měli nastoupit s větší bojovností.

<!--EndFragment-->