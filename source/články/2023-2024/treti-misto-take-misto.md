---
title: Třetí místo také místo
date: 2024-05-25T16:06:08.698Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Mladší přípravka

##### **25.5.2024 Březová nad Svitavou**

I když máme pouze bod a skončili jsme třetí, je potřeba kluky pochválit. Byly to nejlepší zápasy sezóny s velmi kvalitními soupeři.

**S﻿estava:** Louis Brigante, Daniel Hlušička, Lukáš Chmelík, Jan Klement, Pavel Kubík, Ivo Langr, Filip Šplíchal, Jiří Kubík

**Z﻿ápasy:**

###### Březová nad Svitavou - **Morašice** 7:4 (2:3)

###### **Morašice** - Kunčina 6:6 (4:3)

**B﻿ranky:** Jan Klement 3, Daniel Hlušička 2, Ivo Langr 2, Pavel Kubík, Filip Šplíchal, Jiří Kubík