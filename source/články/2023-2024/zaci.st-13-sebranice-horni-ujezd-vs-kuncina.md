---
title: Samo to nepůjde
date: 2024-05-05
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Sebranice/Horní Újezd vs Kunčina
sestava: Petr Kovář - Jan Kopecký, Lukáš Mach, Marek Mokrejš, Pavel Šprojcar,
  Štěpán Němec, Patrik Novotný, Jak Kvapil, Josef Peška, Šimon Flídr, Matyáš
  Bulva, Filip Flídr, Matyáš Vomáčka, Vojtěch Štěpán
goly: Štěpán Němec 2
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2023/Sebranice/Horní
  Újezd/6a3554b0-9d4d-4f8a-bf80-a8584871a8a2.html
---
V nedělním zápase jsme přivítali tým z Kunčiny. Začátek zápasu se opozdil, protože rozhodčí byl místo na Horňák vyslán do Sebranic a start zápasu propásli i naši hráči, kteří si zřejmě mysleli, že to proti tabulkově slabšímu týmu půjde samo. Hned na úvod si soupeř vytvořil několik šancí, které po nás dobře dopadly, a teprve poté jsme začali hrát i my, i když uklidit balón do branky se nám nepodařilo. To se povedlo až po začátku druhé půle, ale místo našeho uklidnění dvakrát udeřil soupeř a my se dostali do situace, ve které jsme ještě nebyli - museli jsme zápas otáčet. V 54. minutě nám naštěstí aspoň jeden bod zachránil svoji druhou brankou Štěpán Němec. Snad se z tohoto zápasu hráčů poučí, že fotbal je kolektivní hra a je potřeba, aby o výsledek bojovali všichni.