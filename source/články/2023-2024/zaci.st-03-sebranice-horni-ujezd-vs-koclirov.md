---
title: Třetí uzel do šňůry
date: 2023-09-10
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Sebranice/Horní Újezd vs Koclířov
sestava: Petr Kovář - Jan Kopecký, Barbora Kovářová, Vojtěch Štěpán, Pavel
  Šprojcar, Lukáš Mach, Štěpán Němec, Patrik Novotný, Jan Kvapil, Matyáš Bulva,
  Filip Flídr, Josef Rosypal, Daniel Branda, Jakub Dvořák
goly: Štěpán Němec 4, Pavel Šprojcar, Jan Kvapil, Matyáš Bulva, Filip Flídr,
  Josef Rosypal
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2023/Sebranice/Horní
  Újezd/2848fc50-45ef-4c85-8bc3-8168bb09bb31.html
---
V nedělním utkání nastoupili starší žáci proti týmu z Koclíŕova. Zápas jsme rozehráli dobře, v páté minutě Pepa Rosypal vstřelil úvodní branku. Soupeře jsme k ničemu vážnějšímu nepouštěli, ale zároveň jsme ani my v koncovce nevynikali. Až Pavel Šprojcar rozjel v devatenácté minutě  menší kanonádu, na kterou navázal třemi brankami Štěpán Němec, a o zápase bylo takřka rozhodnuto. Soupeř do poločasu ještě stačil snížit skóre z pokutového kopu. 

Druhý poločas se odehrával ve stejném duchu, jen soupeř více kousal.  Zápas jsme dohráli v klidu a teď už se těšíme na nedělní utkání proti spojenému družstvu Němčic a Janova, ve kterém půjde o první místo. Doufám, že se nám bude dařit jako doteď.