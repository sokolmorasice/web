---
title: Znovu o gól
date: 2023-10-07
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Kunčina vs Sebranice/Horní Újezd
sestava: Petr Kovář - Jan Kopecký, Barbora Kovářová, Vojtěch Štěpán, Pavel
  Šprojcar, Lukáš Mach, Štěpán Němec, Patrik Novotný, Jan Kvapil, Matyáš Bulva,
  Sára Vomočilová, Josef Rosypal, Jakub Dvořák, Daniel Branda
goly: Štěpán Němec
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2023/Sebranice/Horní
  Újezd/24001147-2487-47ab-b903-ffc819957e1e.html
---
Starší žáci zavítali v sobotu na utkání do Kunčiny v ořezaném počtu, ale soupeř na tom nebyl o mnoho lépe. Od začátku to byl boj na obě dvě strany s naším velkým nasazením, ale bezbrankový stav zlomil až v šedesáté sedmé minutě Štěpán Němec, který krásnou střelou z přímého kopu propálil zeď. Konec zápasu už jsme nepustili a přivezli si krásné tři body. Musím všechny pochválit za bojovnost a doufám, že si to přeneseme do dalších zápasů.