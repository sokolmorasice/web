---
title: Nečekaná zápletka
date: 2023-09-03
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Sebranice/Horní Újezd vs M. Trnávka
sestava: Petr Kovář - Jan Kopecký, Barbora Kovářová, Štěpán Němec, Pavel
  Šprojcar, Patrik Novotný, Lukáš Mach, Jan Kvapil, Vojtěch Štěpán, Jakub
  Dvořák, Josef Peška, Daniel Branda, Matyáš Bulva
goly: Jan Kvapil 2, Patrik Novotný
zlute: Jakub Dvořák
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2023/Sebranice/Horní
  Újezd/970e81c7-5a58-473e-9c46-b76b3ba0a5c4.html
---
Starší žáci v neděli odehráli prví domácí zápas v Sebranicích. Proti nim nastoupilo mužstvo z Městečka Trnávka a, se vši úctou, vzhledem k velikosti soupeře se zdálo, že to bude soupeř z řad těch jednodušších. Skutečnost ale byla jiná, i když jsme zápas celkem dobře rozehráli a už ve 2. minutě vstřelili po našem rohu první gól. Jan Kvapil svou střelou trefil soupeře, od kterého se balón odrazil do branky. Potom se střídaly šance na obou stranách, nás  podržel v brance skvělými zákroky Petr Kovář.  Ve 20.minutě se nám podařila další akce, když Jan Kvapil poslal krásný centr na Patrika Novotného a ten po sprintu uklidil pěkně balón do branky.

Do druhého poločasu jsem nastoupili stejně jako do toho prvního. Ve 37. minutě jsme po pěkné střele Jana Kvapila vedli už tři nula. Ale soupeř pořád kousal, až se mu podařilo vstřelit první branku. My jsme naše další šance do konce nedotáhly a v 53. minutě soupeř opět udeřil a zdramatizoval závěr utkání, které se nám podařilo dotáhnout do vítězného konce. Chtěl bych poděkovat všem za převedený výkon a doufám,  že další zápasy se nám podaří zvládnout stejně.