---
title: Výsledky nejsou všechno
date: 2024-04-21T15:58:47.697Z
category: pripravka
image: /public/images/uploads/logo.png
---
## S﻿tarší přípravka

##### **21﻿.4.2024 Čistá**

Úvodní jarní turnaj starší přípravky v okleštěné sestavě přinesl 3 prohry, z toho dvě těsné. Všichni hráči předvedli bojovný výkon. Navzdory výsledkům převládají z turnaje dobré pocity a těšíme se na to, až budeme v plné síle. A soupeři by se měli těšit také.

**S﻿estava:** Dan Ivanov, Ivo Langr, Jan Langr, Lukáš Poslušný, David Soukup, Šimon Vrbica, Matěj Vrbica, Patrik Vukmirovič, Damián Záleský

###### Čistá - **Morašice** 4:3 (4:0)

**B﻿ranky:**  3- Šimon Vrbica

###### Němčice - **Morašice** 8:3 (3:0)

**B﻿ranky:** 2 - Šimon Vrbica, 1 - Patrik Vukmirovič

###### Horní Újezd - **Morašice** 4:3 (1:1)

**B﻿ranky:** 2 - Šimon Vrbica, 1 - Patrik Vukmirovič

## Mladší přípravka

##### **21﻿.4.2024 Bystré - zrušeno**

Jaro měla zahájit rovněž mladší z našich přípravek, ale z důvodu nepříznivého počasí byl turnaj v Bystrém zrušen.