---
title: Obě přípravky po jedné výhře
date: 2023-09-30T19:23:10.294Z
category: pripravka
image: /public/images/uploads/logo.png
multimedia: https://drive.google.com/drive/folders/1wKwAXog6O3SrMvCbOhQGMjldOtX00cV3
---
## **M﻿ladší přípravka (turnaj v Dlouhé Loučce)**

**s﻿estava:** Jiří Famfulík, Daniel Hlušička, Lukáš Chmelík, Jan Klement, Pavel Kubík, Ivo Langr, Filip Šplíchal, Jiří Voženílek, Jiří Kubík

### **M﻿orašice - Dlouhá Loučka 5:2**

**b﻿ranky:** Ivo Langr 3, Jan Klement 2

### **M﻿orašice - Březová nad Svitavou 1:3**

**b﻿ranky:** Jan Klement

## **Starší přípravka (turnaj v Morašicích)**

### **M﻿orašice - Radiměř 6:3 (3:0)**

### **M﻿orašice - Bystré 1:3 (1:0)**

### **M﻿orašice - Čistá 1:8 (0:4)**