---
title: Dvakrát druhé místo
date: 2024-04-27T17:54:35.339Z
category: pripravka
image: /public/images/uploads/logo.png
---
## S﻿tarší přípravka

##### **27﻿.4.2024 Radiměř**

Na turnaj jsme odjeli pouze v 7 lidech. Ranní omluvenky, otřesy mozku a rýmičky nám zúžili kádr. Jedno vítězství nám však stačilo na druhé místo. Klukům se sluší poděkovat, protože někteří odehráli celý turnaj bez střídání, což se projevilo v posledním zápase, na který nám už nezbylo moc sil. Herně byli kluci výborní a předvedli i pěkné kombinace, k lepším výsledkům už chyběla jen lepší koncovka.

**S﻿estava:** Dan Ivanov, Matouš Jetmar, Jan Langr, Lukáš Poslušný, Lukáš Tichý, Šimon Vrbica, Patrik Vukmirovič

###### Radiměř - **Morašice** 2:4

###### Borová - **Morašice** 3:0

###### **Morašice** - Sebranice 2:4

**B﻿ranky:** 2 - Matouš Jetmar, 2 - Šimon Vrbica, 2 - Patrik Vukmirovič

## Mladší přípravka

##### **27﻿.4.2024 Morašice**

I přes mnoho nováčků v týmu se naši nejmladší prali statečně a nakonec z toho bylo krásné druhé místo. O nástupnickou generaci se snad nemusíme obávat.

**S﻿estava:**  FIlip Hájek, Daniel Hlušička, Kateřina Kovářová, Pavel Kubík, Ivo Langr, Filip Šplíchal, Jiří Voženílek, Štěpán Hájek, Gabriela Hlušičková

###### **Morašice -** Čistá 0:20 (0:10)

###### **Morašice -** Opatov 9:6 (3:5)

**B﻿ranky:** 4 - Daniel Hlušička, 3 - Ivo Langr, 1 - Pavel Kubík, 1 . FIlip Šplíchal