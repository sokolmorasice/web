---
title: Zimní příprava
date: 2024-01-02T15:31:39.946Z
category: obecne
image: /public/images/uploads/logo.png
---
**Plán zimní přípravy**

ÚT 16.1. 18:00 - **první trénink** v tělocvičně, poté každé úterý a čtvrtek

P﻿Á 16.2. - NE 18.2. - **soustředění** v Daňkovicích

ČT 22.2. 17:30 – trénink na **UMT v Litomyšli**

PÁ 23.2. 19:00 – **Obecní ples** v Morašicích na rychtě

N﻿E 25.2. 14:00 - **přátelák** s Cerekvicí na UMT v Litomyšli

ČT 29.2. 17:30 – trénink na **UMT v Litomyšli**

N﻿E 3.3. 11:00 - **přátelák** s Libchavami na UMT v Litomyšli

ČT 7.3. 17:30 – trénink na UMT v Litomyšli

SO 9.3. 15:00 - **přátelák** s Pomezím na UMT v Litomyšli

ČT 14.3. 17:30 – trénink na **UMT v Litomyšli**

SO 16.3. 13:00 - **přátelák** se Sebranicemi na UMT v Litomyšli

NE 24.3. 15:00 – první jarní **mistrák** s Dlouhou Loučkou v Morašicích