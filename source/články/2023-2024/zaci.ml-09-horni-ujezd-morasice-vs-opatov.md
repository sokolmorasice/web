---
title: Jasná záležitost
date: 2024-04-07
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Opatov
sestava: Matyáš Lněnička - Sára Vomočilová, David Klusoň, Barbora Kučerová,
  Martin Čejka, Matěj Kovář, David Boček, Samuel Vomočil, František Flídr, Šimon
  Flídr, Dominik Prokop, Jonáš Rovner, Antonín Krejsa
goly: "Šimon Flídr 2, David Klusoň, Samuel Vomočil 2, Sára Vomočilová, Martin
  Čejka, František Flídr 2 "
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2023/Horní
  Újezd/Morašice/f264fd26-3b7c-4bac-981e-07d5e9b57bb9.html
---
Mladší  v neděli proti stejně tabulkově postavenému Opatovu odehráli zápas ve stejném módu jako jejich starší vrstevníci. Nejprve  nervózní začátek a potom jasné přehrání soupeře. I přes jasné vítězství se však dá říci, že mladší umí zahrát daleko lepší fotbal, než nám předvedli.