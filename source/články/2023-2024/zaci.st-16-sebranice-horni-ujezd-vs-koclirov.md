---
title: Naladění na finále
date: 2024-05-26
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Sebranice/Horní Újezd vs Koclířov
sestava: Petr Kovář - Vojtěch Štěpán, Jan Kopecký, Josef Rosypal, Lukáš Mach,
  Pavel Šprojcar, Jan Kvapil, Patrik Novotný, Štěpán Němec, Matyáš Bulva, Filip
  Flídr, Josef Peška
goly: Štěpán Němec 3, Filip Flídr 2, Jan Kvapil, Pavel Šprojcar
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2023/Sebranice/Horní
  Újezd/ec145f96-bd7e-49ed-87f3-80206924089e.html
---
V nedělním zápase na našem domácím morašickém hřišti, jsme přivítali soupeře z Koclířova. Nastoupili jsme v plné sestavě. Zápas jsme měli od začátku ve své režii. Hráli jsme pěkný technický fotbal, až na tu koncovku. Soupeř měl pár rychlých výpadků, které naštěstí neproměnil. Nám se podařilo vstřelit branku až v 17. minutě, když Filip Flídr prostřelil gólmana. V prvním poločase se trefil ještě Štěpán Němec a vnesl na naše kopačky klid. Ve druhém poločase jsme soupeři nasázeli ještě pět branek a došli si pro nakonec jednoznačné vítězství. Tak doufejme, že si to přeneseme i do příštího zápasu. Budeme hrát zřejmě o první místo v sobotu v Němčicich.