---
title: O bod druzí
date: 2024-06-16
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Březová vs Sebranice/Horní Újezd
sestava: Petr Kovář - Barbora Kovářová, Jan Kopecký, Lukáš Mach, Josef Rosypal,
  Patrik Novotný, Jan Kvapil, Josef Peška, Pavel Šprojcar, Filip Flídr, Matyáš
  Vomáčka
goly: Filip Flídr 2
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2023/Sebranice/Horní
  Újezd/f53c305e-ef66-4cf5-8c57-f9dbd2c645ff.html
---
Poslední jarní utkání jsme odehráli v neděli dopoledne na hřišti v Březové. Od začátku jsme soupeře lehce přehrávali, dostávali jsme se před vápno a zkoušeli střelbu která ne a ne tam zapadnout.  V sedmnácté minutě se to konečně podařilo Filipu Flídrovi. Dále jsme hráli stejnou hru, ale další uklidňující gól nepřišel. I soupeř si vytvořil několik pěkných šancí, ale žádnou neproměnil, a tak jsme v poslední minutě upravili skóre na 2:0 opět gólem Filipa Flídra. Sezónu jsme dohráli na druhém místě za Němčicemi o jeden bod. Všem hráčům se sluší poděkovat za celou sezónu a popřát jim mnoho dalších fotbalových úspěchů.