---
title: Jak si vedli muži
date: 2024-06-19T15:32:04.779Z
category: muzi
image: /public/images/uploads/logo.png
---
Áčko má za sebou další rok v okresním přeboru. Rok to byl opravdu turbulentní. Po letním posílení kádru, kdy si trenér Michal Kopecký mohl vybírat až z 20 hráčů, a díky kvalitní docházce, především v úvodní části sezóny, se muži drželi téměř neustále s čelem tabulky. Pak ale přišel zimní spánek, ze kterého se ne všichni probudili. Početná docházka prořídla, ať už z rodinných, pracovních, nebo především zdravotních důvodů, a tak na některé zápasy byly povolány legendy na telefonu. "Uzdravený" Michal Kopecký se vrátil do role hráče a o trenérskou činnost se podělila trojice Václav Štancl, Luboš Rusnák a Martin Ropek. Muži se na konci sezóny umístili na čtvrtém místě tabulky za Třebařovem, Mladějovem a rivalem z Cerekvice, se kterou měli stejně bodů, ale horší vzájemné zápasy i skóre. Čtvrtou pozici v tabulce, i vzhledem k enormní vyrovnanosti letošního ročníku, lze považovat za úspěšnou a odpovídá našemu standardu. Bylo to hlavně díky venkovním zápasům, kde muži získali nejvíce bodů ze všech týmů. Zároveň však je jasné, že vše mohlo dopadnout daleko lépe, neboť rozdíl na první Třebařov činil "pouhých" 6 bodů a nebýt zbytečných domácích ztrát, především s Mladějovem, Horním Újezdem a Hradcem, mluvila by matematika v náš prospěch.

A něco málo ze statistik.

Celkem muži odehráli 33 zápasů (včetně těch přípravných) s bilancí 18 výher, 7 remíz a 8 porážek při skóre 71:46. Nejvíce zápasů, 32, odehrál D. Štancl, který na hřišti strávil i nejvíce minut (2844). Za ním se neblíže seřadili V. Novák (31 z./ 2678 min.), J. Vít (31 z./ 2318 min.) a M. Černohorský (30 z./ 2700 min.), který při každém svém startu odehrál plný počet minut.

Nejlepším střelcem se stal V. Novák s 9 góly, jenž využil jarní absence druhého nejlepšího zakončovatele F. Halsbacha, který stihl svých 8 gólů už za podzim. Za zmínku také stojí obsazení 6. místa L. Rusnákem, byť většinu zápasů odehrál na pozici brankáře, za 5 zápasů a 2 poločasy v poli vsítil 5 branek.

Nejlepším nahrávačem byl D. Ondráček s 8 asistencemi, 7 gólů připravil D. Štancl, po 6 finálních přihrávkách mají T. Nádvorník, Z. Skala a J. Vít.

V celkové produktivitě se pak tři hráči srovnali na 13 bodech, a to V. Novák (9+4), T. Nádvorník (7+6) a D. Štancl (6+7).

Pozici nejtrestanějšího hráče obhájil D. Ondráček, který nasbíral 10 žlutých a jednu červenou, což je stejné skóre jako v sezóně minulé. Dalším v pořadí s největším počtem karet je V. Novák (8 ŽK), následovaný J. Vítem (7 ŽK), který by s největší pravděpodobností ovládl kategorii žlutých karet za nesportovní chování, pokud bychom takovou statistiku vedli. Červenou kartu za celou sezónu obdržel ještě jeden hráč a to M. Kopecký.

Svatyni za druhou nejlepší defenzivou okresního přeboru hájili 3 gólmani, L. Rusnák (19 z.), Jan Špinar (12 z.) a po 2 startech v bráně si připsal i F. Halsbach. Ze 33 zápasů se podařilo 8x udržet čisté konto, z toho 5x s přispěním L. Rusnáka, 2 vychytané nuly jdou na konto J. Špinara a v jednom případě za svá záda nepustil míč ani F. Halsbach.

Pro ty, co mají zájem podrobněji se podívat na své výkony nebo jak se dařilo ostatním, stačí kliknout na odkaz [zde](https://docs.google.com/spreadsheets/d/12RDBybdIfwAnptau3mS_vyLCJLjbg6utglt7vGg450E/edit?pli=1#gid=556056943).