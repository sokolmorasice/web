---
title: Ohlédnutí za mládím
date: 2023-12-10T22:26:15.781Z
category: obecne
image: /public/images/uploads/logo.png
---
**Starší žáci** 

Starší žáci začali sezónu nově v souklubí tří vesnic: Morašice, Horní Újezd a přidaly se Sebranice. Kádr se tím rozšířil natolik, že by se dal hrát i klasický fotbal v jedenácti, ale protože se na okrese nenašlo dost takto početných týmů, zůstali jsme v přeboru 8+1. Na začátku nikdo nevěděl, jak to bude fungovat, ale všechno dobře dopadlo, trénovali jsme jednou týdně společně, jednou zvlášť a zápasy se nám dařily -  sedm vítězství, jedna remíza a jedna prohra nás zatím řadí na první v tabulce. Nejlepším střelcem byl s 8 góly sebranický Štěpán Němec. Do jarní části máme určitě co zlepšovat, ale zdá se, že hráči si udělali dobrou partu, kterou na jaře přenesou na hřiště. 

*Miloš Kopecký*

**Mladší žáci**

Do podzimní části soutěže jsme nastupovali s rozpaky a lehkou nejistotou. Přeci jen vítězství v minulém ročníku bylo příjemné, ale i zavazující. Tahouni týmu z minulé sezóny nám odešli o kategorii výš a dvě velké opory této věkové kategorie nám odešli do SK Vysoké Mýto. Všem těmto hráčům jsme velice vděčni za to, co jsme se mohli od nich naučit. První zápasy nás velice příjemně překvapily. Soupeře jsme herně i výkonově přehrávali. Čtyři duely, čtyři vítězství a skóre 25:6. Až poté jsme ztratili první body s favoritem soutěže z Bystrého díky remíze. Ještě slabší moment jsme si vybrali v následujícím kole na domácím hřišti, hlavně díky marodce klíčových hráčů, a to prohrou se Sebranicemi. Následující dvě kola jsme už bravurně zvládli, a proto přezimujeme na druhém místě tabulky. Náš nejlepší střelec, Šimon Flídr, je pak s 12 góly průběžně třetí v naší soutěži. A co do nového roku? Zlepšit technické dovednosti, orientace a pohyb po hřišti a zapracovat na fyzičce. Ale prdla...Aby to ty naše kluky a holky stále\
bavilo, byli sami se sebou spokojení a veselí, rádi se za námi vraceli, a hlavně ať se nám i Vám vyhýbají zranění a jsme zdraví.

*Martin Kovář*

**Mladší přípravka**

Pro sezónu 2023/2024 nám odešla téměř polovina stabilního kádru z minulého ročníku do starší přípravky a já zde nebudu tajit naše předsezónní obavy, jak s poměrně úzkým a nezkušeným týmem zvládneme podzimní část. Rovnou vám řeknu, že obavy nebyly na místě. Podařilo se nám velmi dobře zapojit děti z fotbalové školičky, které doplnily hráče se zkušeností z minulého ročníku. Na nich bylo znát, že už si pár zápasů odkopali a jsou zase o rok starší, dynamičtější a fotbalovější. Nakonec byl podzim mnohem úspěšnější, než rok přechozí. Ostatním jsme byli vyrovnaným soupeřem, podařilo se nám několik zápasů vyhrát a na turnajích obsazovat třetí či dokonce druhá místa. Co nás ale těší nejvíce, že fotbal kluky baví, mají chuť se dále rozvíjet a tvoří dobrou partu, což dokresluje vznik oslavného pokřiku po vyhraném zápase, který si děti samy vymyslely a po večerech nastudovaly.

*Milan Chmelík*

**Starší přípravka**

Oproti sezónám minulým, kdy jsme měli starší přípravku spojenou s Horním Újezdem, jsme vzhledem k počtu dětí v obou klubech vstoupili do podzimu samostatně pod hlavičkou Sokola Morašice. 14 dětí na soupisce slibovalo bezproblémové obsazení víkendových turnajů (hraje se 5+1) a opravdu tomu tak bylo, minimálně v první polovině, kdy nás nelimitovaly chřipkové epidemie. Dařilo se, vyhraných zápasů určitě nebylo málo. Nejdůležitější ale je, že kluci chtějí hrát, dávat góly a trávit na hřišti co nejvíce času. Hlavním úkolem nás trenérů tedy bylo spravedlivě střídat. Do budoucna chceme dále rozvíjet herní taktiku, připravovat hráče na jednotlivé posty a podporovat týmovou hru. A samozřejmě věříme, že se budeme moci opřít o výborné výkony gólmana, stejně jako celý podzim.

*Milan Chmelík*

Velmi slušně si vedli naši mladí mimo naše týmy. V dorostu v Horním Újezdu máme Tomáše Hurytu, Vaška Jirečka a Lukáše Němce. Tomáš stihl před přestupem dvěma góly pomoct mužům k úvodní výhře v okresním přeboru, v krajské soutěži dorostenců byl pak s 6 góly nejlepším střelcem týmu, který je po podzimu na solidním pátém místě. Lukáš celý podzim odchytal a dočkal se během něho i premiérových startů za horňácké áčko, paradoxně v přípravě i proti áčku našemu. Ve Vysokém Mýtě nás stále reprezentuje Dan Válek, který přerostl svoji kategorii U13 a v divizi C žáků U14 si připsal 5 gólů za 14 zápasů. Do této kategorie nakoukli i naši nováčci v dresu Vysokého Mýta, a to Marťa Famfulíková a Vojta Huryta, kteří ale většinu času odehráli v kategorii U13. Vojta je dokonce s 26 góly za 12 zápasů nejlepším střelcem svého týmu. Všichni tři se pak dočkali i nominace do krajských výběrů.

A výborně funguje i stále plnější fotbalová školička pro nejmenší pod vedením Jirky Famfulíka, která jela od prázdnin až do Mikuláše každý týden.