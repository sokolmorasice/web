---
title: Čistý hattrick
date: 2023-09-12
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Čistá
sestava: Jaroslav Chaun - Sára Vomočilová, David Klusoň, Terezie Čejková, Martin
  Čejka, Matěj Kovář, David Boček, Michal Kovář, Samuel Vomočil, František
  Flídr, Šimon Flídr, Dominik Prokop, Jonáš Rovner, Antonín Krejsa
goly: Samuel Vomočil 2, František Flídr, David Boček, vlastní
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2023/Horní
  Újezd/Morašice/b76756af-e63c-4b5d-9bb2-4e47149cf197.html
---
Zápas se hrál za naší stálé převahy, kterou soupeř narušil až ve druhém poločase, kdy dvěma slepenými góly snížil na 2:4. Jejich třetí gól během deseti minut byl naštěstí do vlastní brány a o vítězi bylo definitivně rozhodnuto.