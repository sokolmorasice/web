---
title: Co by áčko za ty góly dalo
date: 2024-05-11T21:44:58.035Z
category: obecne
image: /public/images/uploads/logo.png
h1: Dolní Újezd "SG" - Morašice "SG" 2:7 (2:3)
sestava: V. Štancl - Jan Štěpán, V. Štěpán, M. Chmelík, M. Klement, L. Vosmek,
  J. Kopecký, V. Beneš, Jindřich Jána, J. Vomočil, M. Janypka, J. Janypka, J.
  Famfulík, M. Burián
goly: J. Kopecký, M. Burián, M. Klement, L. Vosmek, J. Janypka, V. Beneš, V. Štěpán
multimedia: https://drive.google.com/drive/folders/1YZjQaJZcsCGOZf5TXLg4-i7jXZbScMcm
---
Tak jako několik posledních půlroků se v den vzájemného střetu mistrovských týmů sešli k zápasu i staré gardy Morašic a Dolního Újezdu. Počty účastníků tentokrát jasně určily, že hrát se bude pouze 2x30 minut, 7+1, na šířku hřiště. Od začátku zápasu bylo naše mužstvo aktivnější (bodejť by ne, když mělo v sestavě několik "áčkařů"), ale gólově se to nedařilo vyjádřit. Na úvodní gól Kopeckého navázal Burián, ale za újezdské odpověděli přesnými střelami z dálky Heinisch a Svoboda. Poločasové vedení nám povedenou levačkou zajistil Klement.

Velkou část druhého poločasu se hrál v rámci fyzických možností hurá fotbal se spoustou možností navýšit nebo dorovnat skóre. Na újezdské bráně zvonily tyče, na naší straně překopával újezdský Kuta prázdnou bránu. Výsledkově zápas zlomila až trefa Vosmeka, na kterou navázali nováček a benjamínek našeho týmu J. Janypka, po něm dlouho se zastřelující doyen Beneš a konečnou podobu stanovila pěkná střela V. Štěpána.