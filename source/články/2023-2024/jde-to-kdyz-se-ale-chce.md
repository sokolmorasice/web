---
title: Jde to, když se ale chce
date: 2024-05-12T06:49:17.299Z
category: pripravka
image: /public/images/uploads/logo.png
multimedia: https://drive.google.com/drive/folders/1j_pYDdgBvPNI8PxaGDEmecv03kHxE1AE
---
## Mladší přípravka

##### **5.5.2024 Opatov**

I přes teplé počasí a viditelné přemáhání se, nakonec kluci zabojovali a obsadili krásné druhé místo.

**S﻿estava:** Louis Brigante, Lukáš Chmelík, Jan Klement, Pavel Kubík, Ivo Langr, Filip Šplíchal, Jiří Voženílek

**Z﻿ápasy:**

###### Borová - **Morašice** 4:6 (1:2)

###### **Morašice** - Sebranice 0:6 (0:3)

###### Opatov - **Morašice** 1:2 (1:1)

**B﻿ranky:** Ivo Langr 4, Lukáš Chmelík 3, Jan Klement

##### **12﻿.5.2024 Horní Újezd**

Soustředění některých dětí na fotbal bylo na bodu mrazu, což bylo náročné na psychiku trenérů. Možná to byl důsledek ponocování s naším národním hokejovým týmem, možná to bylo krásným počasím. Jedna výhra z toho ale přeci jen byla.

**S﻿estava:** Louis Brigante, Daniel Hlušička, Lukáš Chmelík, Jan Klement, Kateřina Kovářová, Pavel Kubík, Ivo Langr, Filip Šplíchal, Jiří Voženílek, Jiří Kubík

###### Z﻿ápasy:

###### Horní Újezd - **Morašice** 11:0 (7:0)

###### Kunčina - **Morašice** 6:2 (2:1)

###### Borová - **Morašice** 1:9 (1:5)

**B﻿ranky:** Lukáš Chmelík 5, Daniel Hlušička 4, Jan Klement, Ivo Langr