---
title: Pohlídané vítězství
date: 2024-04-28
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Sebranice/Horní Újezd vs Březová
sestava: Petr Kovář - Jan Kopecký, Lukáš Mach, Josef Rosypal, Marek Mokrejš,
  Štěpán Němec, Patrik Novotný, Jak Kvapil, Josef Peška, Matyáš Bulva, Filip
  Flídr, Matyáš Vomáčka, Vojtěch Štěpán
goly: Štěpán Němec 2, Jan Kvapil 2, Filip Flídr
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2023/Sebranice/Horní
  Újezd/da7dbae9-5442-484c-b78a-81ac569ad906.html
---
V neděli jsme se utkali se soupeřem z Březové. Zápas jsme opět rozehráli dobře a soupeře jsme pomalu nepustili za půlku. Jenže nám chyběla bojovnost, kterou jsme měli proti Němčicím a dlouho se nám nedařilo prosadit. To se podařilo až ve 14. minutě Filipu Flídrovi, když prostřelil brankáře a ujali jsme se vedení. Další branku vstřelil Jan Kvapil. Soupeř hrozil jen sporadicky. Ve druhém poločase se nám podařilo vstřelit ještě tři branky a zápas potom v klidu dohrát.