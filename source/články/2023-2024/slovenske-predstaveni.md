---
title: Slovenské představení
date: 2024-02-26T06:58:25.666Z
category: muzi
image: /public/images/uploads/logo.png
h1: Morašice - Cerekvice 2:3 (1:2)
sestava: J. Špinar - M. Ropek, J. Kopecký, L. Rusnák, D. Ondráček, D. Štancl, J.
  Štancl, J. Vít, T. Huryta, F. Halsbach, M. Frank, M. Kopecký, P. Zaujec
goly: F. Halsbach (P. Zaujec), P. Zaujec (D. Ondráček)
---
Na první přátelské utkání zimní přípravy na UMT v Litomyšli se nám vlivem nemocí ze soustředění a plesu tak rozpadla sestava, že jsme sháněli hráče téměř od Šumavy k Tatrám. Naše slepená jedenáctka brzo nabrala dvougólové manko, které se kolem přestávky podařilo umazat. Nejdřív navázal Halsbach na své podzimní střelecké manévry, když zblízka dokončil práci slovenského hosta Petera "Pelého" Zaujece, aby po přestávce novic v našem dresu krásnou ranou zpoza vápna srovnal. Jen houšť takových bratranců. V poslední čtvrthodině nás pak viditelně sehranější i nesmyslně nervóznější soupeř připravil nejprve o Rusnáka, snad pouze dočasně, a poté i o "bod", když využil jednu ze svých šancí. Tak trochu přátelák za trest, který ukázal především to, že je na čem pracovat.