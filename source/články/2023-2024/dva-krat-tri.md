---
title: Dva krát tři
date: 2024-05-12T12:02:52.397Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Starší přípravka

Z﻿e dvou uplynulých turnajů, z nichž druhý se konal u nás v Morašicích, jsme si připsali dvě třetí místa. Bojovný výkon od všech hráčů, ale zároveň jsme ani jeden turnaj nebyli kompletní, pak by z toho možná byly lepší umístění.

##### **4.5.2024 Janov**

**S﻿estava:** Matouš Jetmar, Ivo Langr, Jan Langr, Lukáš Poslušný, Lukáš Tichý, Šimon Vrbica, Matěj Vrbica, Patrik Vukmirovič, Damián Záleský

**Z﻿ápasy:**

J﻿anov - **Morašice** 3:7

Č﻿istá - **Morašice** 9:3

N﻿ěmčice - **Morašice** 8:3

**B﻿ranky:** Šimon Vrbica 6, Lukáš Tichý 2, Patrik Vukmirovič 2, Matouš Jetmar 2

**12.5.2024 Morašice**

**S﻿estava:** Matouš Jetmar, David Soukup, Viktor Špás, Šimon Vrbica, Matěj Vrbica, Patrik Vukmirovič, Damián Záleský, Martin Šplíchal

**Z﻿ápasy:**

**Morašice** - Hradec nad Svitavou 8:0

**M﻿orašice** - Kunčina 4:6

**M﻿orašice** - Horní Újezd 1:2

B﻿ranky: Šimon Vrbica 7, Patrik Vukmirovič 2, Matouš Jetmar, Martin Šplíchal, Damián Záleský