---
title: Sbírání bronzu pokračuje
date: 2024-06-01T16:18:15.199Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Mladší přípravka

##### **1.6.2024 Kunčina**

Turnaj v Kunčině jsme museli absolvovat bez dvou kluků, kteří nám bohužel odpadli během cesty kvůli nevolnosti. Soupeři z Čisté a Kunčiny předvedli na turnaji velmi kvalitní výkony a nám se nakonec podařilo obsadit opět 3. místo.

**S﻿estava:** Lukáš Chmelík, Jan Klement, Pavel Kubík, Ivo Langr, Filip Šplíchal, Jiří Voženílek, Jiří Kubík

**Z﻿ápasy:**

###### Čistá - **Morašice** 8:3 (3:3)

###### **Morašice** - Jevíčko 11:4 (9:1)

###### Kunčina - **Morašice** 7:3 (3:2)

**B﻿ranky:** Ivo Langr 6, Lukáš Chmelík 5, Jan Klement 3, Jiří Voženílek 3