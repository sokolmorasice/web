---
title: Pomalu se přípravkujeme na konec podzimu
date: 2023-10-21T12:55:16.691Z
category: pripravka
image: /public/images/uploads/logo.png
---
### M﻿ladší přípravka (21. 10. 2023 Morašice)

##### **M﻿orašice** - Sebranice 5:4 (4:0)

##### **M﻿orašice** - Březová nad Svitavou 4:12 (1:6)

**S﻿estava**: Jiří Famfulík, Daniel Hlušička, Lukáš Chmelík, Jan Klement, Pavel Kubík, Filip Šplíchal, Jiří Voženílek, Jiří Kubík

**S﻿třelci**: 1 - Jiří Voženílek, 4 - Jan Klement, 4 - Lukáš Chmelík

### S﻿tarší přípravka (22. 10. 2023)

##### B﻿řezová nad Svitavou - **Morašice** 7:1 (6:1)

##### H﻿radec nad Svitavou - **Morašice** 6:0 (4:0)

##### S﻿vitavy B - **Morašice** 6:1 (1:0)