---
title: Skóre až na druhém místě
date: 2023-10-01T19:09:44.893Z
category: obecne
image: /public/images/uploads/logo.png
h1: Morašice SG - Dolní Újezd SG 2:0
sestava: Vojtěch Andrle, Martin Burián - František Ropek, Jaroslav Růžek,
  Jindřich Jána, Milan Chmelík, Milan Klement, Zbyněk Sopoušek, Marek Valla, Jan
  Štěpán, Josef Štěpán, Jiří Vosmek, Luboš Vosmek, Josef Kopecký, Jiří Famfulík
goly: Luboš Vosmek, Marek Valla (pen.)
multimedia: https://drive.google.com/drive/folders/1W2sZG1kB_YpXYvPg4bSF_Z_KrllZqRif
---


Stejně jako vloni, takže třeba brzy budeme moci říkat tradičně, se s mistrovským derby Morašic s Dolňákem i spojil zápas starých gard. Přestože v našem týmu z různých důvodu (dovolená, nominace k následnému mistráku) chyběly i některé legendy z kategorie nejlegendovatějších, odehrál se první poločas v naší režii. Zastřeloval se např. L. Vosmek, nejednou zvonila konstrukce soupeřovy brány a "benjamínek" našeho týmu Burián, který musel do brány, si pouze staticky užíval krásného počasí. 

Do druhého poločasu ho nahradil Andrle, a ten už svoje "nezapomínání" brankářského řemesla musel ukázat, stejně jako Burián nezapomněl na svoje akrobatická zakončení. Z šancí na obou stranách v bráně skončil pouze nepovedený centr L. Vosmeka a penalta Vally, které znamenaly naše první vítězství v novodobé historii těchto klání. Na listině vážně zraněných snad neskončil nikdo ani dodatečně, což je hlavní vítězství těchto klání a hlavní předpoklad k tomu, aby se v založené tradici chtělo na jaře pokračovat.