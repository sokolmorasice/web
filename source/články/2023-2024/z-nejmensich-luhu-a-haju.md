---
title: Z nejmenších luhů a hájů
date: 2023-09-19T05:30:23.198Z
category: pripravka
image: /public/images/uploads/logo.png
multimedia: https://drive.google.com/drive/folders/1qSUpNBXgNr7MgzRgcQunz5760n3SDvkc
---
Pro letošní sezónu jsme přihlásili obě družstva přípravek samostatně. Mladší přípravka je letos pro ročníky 2015 a mladší, náš tým je tvořen hlavně ročníkem 2016, ale snad mu loňské nabrané zkušenosti pomohou tento handicap překonat.

První turnaj v Čisté děti srdnatě odbojovaly a přes dvě těsné prohry odjížděly určitě s lepším pocitem než většiny loňských turnajů.

**Morašice - Čistá 4:6 (1:1)**

**Morašice - Borová 5:7 (3:5)**

**Sestava:** Jiří Famfulík, Lukáš Chmelík, Jan Klement, Kateřina Kovářová, Pavel Kubík, Ivo Langr, Filip Šplíchal

Na druhém turnaji v Dlouhé Loučce už byli děti za výkon odměněny 2 výhrami, z toho jednou vysokou, a celkovým **2. místem!**

**Morašice - Jevíčko 3:7 (1:5)**

**Morašice - Sebranice 4:2 (3:2)**

**Morašice - Dlouhá Loučka 11:1 (8:0)**

**Sestava:** Jiří Famfulík, Lukáš Chmelík, Jan Klement, Kateřina Kovářová, Pavel Kubík, Ivo Langr, Filip Šplíchal, Daniel Hlušička, Jiří Voženílek

N﻿a zprávách o starší přípravce i seznamu našich úspěšných střelců s trenéry **Milanem Chmelíkem, Milanem Klementem a Jirkou Famfulíkem** intenzivně spolupracujeme. Pokud by nám s tím chtěl někdo z rodičů pomoct, budeme moc rádi. Pokud by k nám chtěl někdo přivést svoje děti, budeme ještě radši.