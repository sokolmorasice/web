---
title: Vítězná tečka za podzimem
date: 2023-10-29
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Sebranice/Horní Újezd vs Kunčina
sestava: Kovář Petr, Kopecký Jan, Kovářová Barbora, Štěpán Vojtěch, Šprojcar
  Pavel, Němec Štěpán, Novotný Patrik, Bulva Matyáš, Mach Lukáš, Kvapil Jan,
  Vomáčka Matyáš, Rosypal Josef, Branda Daniel, Peška Josef, Dvořák Jakub
goly: Rosypal Josef, Němec Štěpán, Peška Josef
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2023/Sebranice/Horní
  Újezd/1c8af314-be8f-4222-9c21-4f5c7a4d7e61.html
---
V nedělním utkání jsme hostili na našem domácím Morašickém hřišti soupeře z Kunčiny. Před zápasem se nám otvíráli dvířka podobně jako naším chlapům -  hrát o první místo v tabulce.  Počasí nám přálo, i když  krapet foukalo. Do zápasu jsme vstoupili dobře.  Vytvořili jsme hned na začátku pár šancí, které jsme ale neproměnili.  Soupeř potom začal vystrkovat růžky.  Na štěstí v produktivitě na tom byl stejně jako my. Nám se ve dvacáté třetí minutě podařilo vstřelit první gól po pěkné akci, kdy skóroval Josef Rosypal. Ve třicáté druhé minutě přišla uklidňující druhá branka z kopačky Štěpána Němce. Zápas jsme měli relativně pod kontrolou.  V druhém poločase se nám podařilo skórovat potřetí, kdy jsme se po brance Josefa Peška ujali vedení 3:0. Soupeři se podařilo snížit až v šedesáté šesté minutě na konečný stav 3:1. Do jarní části tak půjdeme z prvního místa.  D﻿oufejme, že se na druhou polovinu sezóny připravíme tak, abychom byli schopni obhájit výsledky z podzimu. Chtěl bych poděkovat všem za podzimní přístup a vzhůru na jaro.