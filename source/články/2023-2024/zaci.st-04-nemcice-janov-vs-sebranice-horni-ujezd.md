---
title: Tuhý a neúspěšný boj
date: 2023-09-17
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Němčice/Janov vs Sebranice/Horní Újezd
sestava: Petr Kovář - Jan Kopecký, Barbora Kovářová, Vojtěch Štěpán, Pavel
  Šprojcar, Lukáš Mach, Štěpán Němec, Patrik Novotný, Jan Kvapil, Filip Flídr,
  Josef Rosypal, Matyáš Vomáčka, Josef Peška, Daniel Branda, Matyáš Bulva
goly: Patrik Novotný, vlastní
zlute: Lukáš Mach, Jan Kvapil
cervene: Jan Kvapil
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2023/Sebranice/Horní
  Újezd/474f60e1-a472-4497-98b8-13a94c5b6108.html
---
V neděli bojovali starší žáci na půdě Němčic o první místo v tabulce. A byl to opravdový boj už od začátku. Většinu času první půle jsme se usadili na soupeřově půlce a naši převahu korunoval ve 23. minutě gólem Patrik Novotný. Chvíli na to si soupeř připsal první žlutou kartu a těsně před poločasem ještě vlastní gól. 

Do druhého poločasu jsme tedy vstoupili s vedením 2:0, ale také jsme viditelně přestali hrát, což Němčice ztrestali nejprve korigujícím a ve 40. minutě vyrovnávacím gólem. Poté jsme se oklepali a vytvořili si několik šancí, které zůstaly bohužel nevyužité. V 60. minutě přišla žlutá karta pro Lukáše Macha, a co hůř, po jeho faulu proměnily Němčice pokutový kop. Pak už se přes pár našich šancí neměnilo skóre gólové, ale pouze karetní. Soupeř obdržel svou druhou žlutou jako první, za nás "kontroval" žlutým faulem v poslední minutě Jan Kvapil, který si to hned vzápětí během nastavení zopakoval a byl vyloučen.