---
title: Těsná výhra
date: 2024-04-28
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Němčice vs Horní Újezd/Morašice
sestava: Matyáš Lněnička - David Klusoň, Barbora Kučerová, Martin Čejka, Matěj
  Kovář, David Boček, Michal Kovář, Samuel Vomočil, František Flídr, Šimon
  Flídr, Dominik Prokop, Jonáš Rovner
goly: František Flídr 2, Šimon Flídr 2, Barbora Kučerová
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2023/Horní
  Újezd/Morašice/d7e93fd7-b9b2-4c82-a8da-3d725e0ac307.html
---
Mladší žáci po dvou prohrách, smolně v Jevíčku a zaslouženě s Bystrým, hráli v Němčicich. Vetšinu zápasu jsme útočili, ale problémy nám dělalo zakončení. Naopak domácí na každý náš gól odpověděli hodně rychle. Nakonec  jsme přeci o jeden gól byli lepší. 

Mladší na jaře stále nehrají v takové pohodě jako na podzim. Hodně chybí nemocná Sára Vomočilová, která si umí srovnat kluky jak na hřišti, tak mimo.