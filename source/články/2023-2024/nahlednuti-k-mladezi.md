---
title: Nahlédnutí k mládeži
date: 2024-06-24T15:29:22.850Z
category: obecne
image: /public/images/uploads/logo.png
---
A﻿ jak se v uplynulé sezóně dařilo naší mládeži?

### Dorost

Naši dorostenci Tomáš Huryta, Lukáš Němec a Václav Jireček byli pro tuto sezónu zapůjčeni do Horního Újezda, který přihlásil krajskou soutěž. Cílem bylo nechat kluky hrát se svými vrstevníky jedenáctkový fotbal, který by je lépe připravil na dospělý fotbal. Tomáš Huryta, pro kterého to byla poslední sezóna v této kategorii, se se 13 góly stal nejlepším střelcem týmu, a doufejme, že mu to bude střílet i v sezóně další v našem "áčku", kde už si pár startů a gólů stihl připsat. Lukáš Němec byl několikrát povolán do brány mužů "Horňáku", kde nasbíral cenné zkušenosti a podepsal se pod ziskem několika bodů.

### Starší žáci

Starší žáci ve spolupráci s Horním Újezdem a Sebranicemi prožili úspěšnou sezónu okresního přeboru 8+1, kde chyběla už jen zlatá tečka. Do jara vstupovali žáci z prvního místa, ale nakonec se museli sklonit před spojeným týmem Němčic a Janova. Po 18 zápasech tak skončili o bod těsně pod vrcholem, s bilancí 13 vítězství, 3 remíz a 2 proher, se skóre 59:20. Nejlepším střelcem byl Štěpán Němec ze Sebranic se 17 góly. Trenér **Miloš Kopecký** hodnotí sezónu takto:

"V jarní části jsme začínali z prvního místa. První tři zápasy se nám podařilo úspěšně dovést do vítězného konce. Pak jsme jednou lehce zaváhali, ale další zápas se nám opět podařilo s přehledem vyhrát. Potom jsme utrpěli první a zatím jedinou jarní porážku, která nás stála první místo v tabulce na konci jara. Následně jsme odehráli asi jedno z nejlepších utkání jara. Do předposledního jarního zápasu jsme nastoupili proti prvním Němčicím. Tam jsme v posledních minutách zachránili alespoň remízu. Doufali jsme, že soupeř v posledním kole klopýtne, ale to se nestalo. I tak celou jarní část hodnotím kladně,  trojklubí si myslím, že fungovalo dobře. Chtěl bych především pochválit kluky za přístup a snahu. Dále bych jim rád popřál mnoho úspěchů do dalších fotbalových let."

### Mladší žáci

Mladší žáci hráli okresní přebor 7+1 v již zažitém spojení s Horním Újezdem. Kluci se pod vedením **Martina Kováře** umístili v soutěži na třetím místě, když z 18 zápasů zaznamenali 11 vítězství, 2 remízy a 5 proher. Nejlepším střelcem s 32 góly byl hornoújezdský Šimon Flídr. 

### Starší přípravka

Starší přípravka letos fungovala samostatně. Co mohla trenéra **Jiřího Famfulíka** určitě těšit, byla účast, na turnaje se pravidelně jezdilo v počtu 8-12 hráčů. Individuální výkony hráčů byly velmi dobré, ale je potřeba ještě více zapracovat na přihrávkách a kombinační hře. Pokud toto zlepšíme, můžeme se směle rovnat s nejlepšími týmy v okrese. 

### Mladší přípravka

Také mladší přípravka se letos přihlásila samostatně. Sezónu hodnotí trenéři **Milan Chmelík** a **Milan Klement** jako úspěšnou. Po celý rok bylo dostatek dětí, přivítáno bylo i pár nováčků. Herně se dařilo, využili se zkušenosti z loňské sezóny a na všech turnajích naši nejmladší vždy jeden zápas vyhráli. Klukům a holkám to určitě dodalo sebevědomí a radost ze hry, což je to nejdůležitější pro jejich další fotbalový rozvoj.

Velký dík za celou mládežnickou sezónu patří nejen našim trenérům, ale i TJ Horní Újezd, rodičům a všem, co se k čemukoli jakkoli pozitivně nachomýtli.