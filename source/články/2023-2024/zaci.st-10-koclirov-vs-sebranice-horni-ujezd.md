---
title: Pokračujeme v čele
date: 2024-04-06
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Koclířov vs Sebranice/Horní Újezd
sestava: Petr Kovář - Jan Kopecký, Lukáš Mach, Pavel Šprojcar, Josef Rosypal,
  Štěpán Němec, Patrik Novotný, Jan Kvapil, Josef Peška,  Matyáš Bulva, Filip
  Flídr, Jakub Dvořák, Daniel Branda, Matyáš Vomáčka
goly: Filip Flídr 2, Matyáš Bulva, vlastní
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2023/Sebranice/Horní
  Újezd/c5eed596-7bc9-4a6c-954b-80c0c84c9cc7.html
---
V sobotu jsme v Opatovci, kde hraje své zápasy Koclířov, rozehráli jarní část sezóny. Navzdory špatnému terénu se nám herně zápas podařil, vytvořili jsme si mnoho gólových šanci a soupeře k ničemu vážnějšímu nepustili. Horší už to bylo se zakončením. V prvním poločase jsme dali pouze jednou branku, ve druhém jsme byli malinko efektivnější, i když nám musel pomoct i soupeř, a nakonec jsme vyhráli 4:0.