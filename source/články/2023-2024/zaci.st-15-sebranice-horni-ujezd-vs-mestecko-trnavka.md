---
title: Každá série jednou končí
date: 2024-05-19
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Sebranice/Horní Újezd vs M. Trnávka
sestava: Petr Kovář - Jan Kopecký, Josef Rosypal, Lukáš Mach, Vojtěch Štěpán,
  Pavel Šprojcar, Patrik Novotný, Štěpán Němec, Jan Kvapil, Matyáš Bulva, Matyáš
  Vomáčka, Jakub Dvořák, Filip Flídr, Josef Peška
goly: Filip Flídr
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2023/Sebranice/Horní
  Újezd/434a162d-509d-4e54-a4de-b8b03947a788.html
---
V nedělní dopoledne jsme přivítali na Horním Újezdě tým z Městečka Trnávky. Se soupeřem jsme měli letos docela vyrovnanou bilanci. Jedna výhra 3:2 a remíza 1:1, takže se dal očekávat těžký zápas. Ani počasí nám moc nepřálo. Před utkáním nám napršelo poměrně dost vody. Soupeř nám hned od začátku dal najevo, že to nebude jednoduché. Do čtvrt hodiny jsme po našich chybách inkasovali hned tři branky. Zbytek poločasu jsme odehráli převážně u soupeře na jeho půlce. Jenže se nám nepodařilo vstřelit kontaktní branku. V druhém poločase se nám podařilo puze korigovat výsledek jednou brankou. A soupeř ještě promarnil několik pěkných šancí. Do příštího zápasu máme co zlepšovat.