---
title: Zápas pod taktovkou domácích ...
date: 2023-10-14
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Sebranice/Horní Újezd vs Borová
sestava: Kovář Petr - Kopecký Jan, Kovářová Barbora, Štěpán Vojtěch, Šprojcar
  Pavel, Němec Štěpán, Novotný Patrik, Bulva Matyáš, Kvapil Jan, Mach Lukáš,
  Flídr Filip, Dvořák Jakub, Branda Daniel, Rosypal Josef, Peška Josef, Vomáčka
  Matyáš
goly: Kovář Petr, Kvapil Jan, Mach Lukáš
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2023/Sebranice/Horní
  Újezd/45e9b94d-8d13-486e-b021-307a2382d42e.html
---
<!--StartFragment-->

V sobotním utkání starší žáci nastoupili proti mužstvu Borové. Celý zápas se odehrával převážně v naší režii na soupeřově půlce. Už v páté minutě Lukáš Mach prostřelil brankáře tvrdou střelou. Soupeře jsme přehrávali po stranách, ale před bránou se nám nedařilo prosadit. Soupeř nám veškeré střely blokoval. Druhou branku jsme zaznamenali až v třicáté druhé minutě s přispěním soupeře. Do druhého poločasu jsme nastoupili stejně jak do toho prvního. V třicáté deváté minutě jsme proměnili pokutový kop zásluhou Petra Kováře.  Potom se druhý poločas odehrával ve stejném duchu jako ten první. Takže jsme čekali na další gól až do šedesáté deváté minuty, kdy skóroval Jan Kvapil. V tomto zápase bylo jenom škoda, že jsme nevstřelii více branek.

<!--EndFragment-->