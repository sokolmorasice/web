---
title: Šikovní, ale malí
date: 2023-09-10
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Opatov vs Horní Újezd/Morašice
sestava: Jaroslav Chaun - Sára Vomočilová, David Klusoň, Terezie Čejková, Martin
  Čejka, Matěj Kovář, David Boček, Michal Kovář, Samuel Vomočil, František
  Flídr, Šimon Flídr, Dominik Prokop, Jonáš Rovner, Antonín Krejsa
goly: Samuel Vomočil 2, David Boček 2, Šimon Flídr 2, Dominik Prokop 2, Sára
  Vomočilová, David Klusoň
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2023/Horní
  Újezd/Morašice/80342326-d9db-424e-8fd5-a7b48dd98e1b.html
---
Zápas rozhodla naše výšková a silová převaha. Soupeřovi děti působily šikovně, za rok je asi čeká dobrá sezóna, ale fyzický handicap byl pro ně tentokrát ještě příliš velký.