---
title: Vánoční radovánky
date: '2018-11-20'
image: ''
category: obecne
---
Vážení přátelé,

děkujeme Vám za, víceméně stálou, přízeň při našich kolísajících výsledcích a pevně věříme, že se budeme potkávat i na jaře.

Zároveň bychom Vám chtěli popřát příjemné a klidné Vánoce, včetně pevného a jistého kroku do roku nového.

Tradiční **Štěpánská zábava** proběhne v **úterý 25.12.2018 na Rychtě v Morašicích**, kde zahraje kapela **Rytmik** a napojí Vás členové místního Sokola.

V **pondělí 31.12.2018** zveme všechny příznivce morašického Sokola na **silvestrovský fotbálek**. Výkop je od **10:00 na hřišti**.
