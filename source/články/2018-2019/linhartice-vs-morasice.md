---
title: Stoprocentně sterilní
date: 2019-04-07T00:00:00.000Z
image: /public/images/zapasy/linhartice-morasice.png
h1: Linhartice vs Morašice
category: muzi
sestava: >-
  M. Prešinský - D. Štancl, M. Ropek, V. Štancl, M. Klement - B. Válek, M.
  Chmelík, Z. Skala, J. Vít (D. Ondráček) - M. Sokol (J. Špinar), F. Bureš (D.
  Vejrych)
zlute: 'M. Ropek, V. Štancl, B. Válek'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533A1A/2018/Morašice/2b8b86c5-476b-439a-aa48-37617546d5a4.html
---
Početní stav se po výkyvu z minulého týdne vrátil pro výjezd od Linhhartic k obvyklejšímu počtu, a tak jsme museli znovu trochu míchat se sestavou. Protože jednou beky přehrabujeme vidlema a podruhé máme jen jednoho, stáhli jsme si z útoku Chmelíka a na beka poslali Klementa. Ještě že tak, protože zrovna na něho vyšel ultra rychlostní typ, se kterým měl i on sám co dělat. Ale popořádku. Hned v první minutě se oba týmy uvedly první střelou, zatímco Skala dostal od domácích první z mnoha volných chvilek a mířil spíše propagačně vedle, domácí úprk po straně rozvlnil vingl, naštěstí zvenku. Bránění našeho soupeře nebylo z nejlepších, ale poskytnutý prostor jsme bohužel nedokázali využít, přitom se hlavně naši střeďáci a útočníci několikrát ocitli v pozicích za vápnem, odkud by se dalo zkusit pozornost domácího gólmana, bohužel se nám přes jakousi poziční převahu dlouho nedařilo ho vůbec zaměstnat. Domácí taktika rychlých brejků a standartních situací byla o poznání nebezpečnější. Po jednom z rohů se za Prešinským rozezvonilo břevno, napříště nás při prudké hlavičce zachránila špatně seřízená mířidla. My jsme si standartkami nebyli schopní jakkoliv pomoct, a tak jsme to zkusili postupnou kombinací. Ta nejdelší z nich navedla koncovku až na do vápna se vysunuvšího Klementa, ale jeho pokus levačkou si domácí gólman spíš zpracoval, než že by ho chytil. Více práce měl po své zpackané rozehrávce, kterou se snažil rychle ztrestat Válek, ale jak jeho střelu, tak dorážku zcela osamoceného Bureše zvládl zlikvidovat. V závěru poločasu jsme ještě naivně doufali, že by hlavní rozhodčí ze své pozice u středového kruhu viděl možná penaltový zákrok na Válka, ale v kabině jsme si ujasnili, že nastolený metr nemá cenu řešit.

Do druhého poločasu jsme vstoupili přece jenom aktivněji a hosty drželi na distanc, ale do kloudných šancí jsme se špatným finálním řešením pořád nemohli dostat. Jednu z nejlepších pozic měl po Sokolově sklepnutí V. Štancl, ale i on byl s muškou hodně na štíru. Aktivně vstoupil do druhé půlky i pomezní rozhodčí, který při každém Válkově náběhu mával ofsajdovým transparentem. A dělal to tak často a nesmyslně, až dosáhl svého a mohl hlavnímu nažalovat, když mu Válek poradil, kam si má ten praporek strčit. Logicky jsme tedy útočili spíše z pravé strany, ale k zakončení se dostal prakticky jenom Sokol, jenže i jemu byla brána malá. V poslední desetiminutovce pak mohli vítězství na svou stranu strhnout domácí, ale přímák z vápna kopli špatně a po těsné střele z otočky to jen tak zašumělo. V úplném závěru ještě prováhal střeleckou pozici ve vápně Vejrych, Špinar si stihl střihnout půl minuty v útoku a dva body byly nenávratně pryč.
