---
title: Zámek na Dunaji za střelce
date: 2019-05-11T00:00:00.000Z
image: /public/images/zapasy/opatov-morasice.png
h1: Opatov vs Morašice
category: muzi
sestava: >-
  O. Vašek - D. Štancl, M. Ropek, M. Chmelík, V. Štancl - B. Válek, Z. Skala, M.
  Sokol (D. Vejrych), V. Tměj - J. Kopecký, J. Vít (M. Hubáček)
zlute: M. Chmelík
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533A1A/2018/Morašice/918d1dd0-2ae2-489e-9e0b-321e1abe26a1.html
---
Před zápasem v Opatově jsme měli pět zraněných, včetně jednoho brankáře a radši ani nepočítajíc dva pravděpodobně definitvní "kolenáře" z podzimu, další dva brankáři dopředu avizovali svou neúčast, jeden útočník zjistil půlhodiny před zápasem, že se nehraje až v neděli, druhý útočník očekával, že na něho kvůli tomu prvnímu nezbude dost místa, tak se jel projet na kole. A tak jsme dorazili do Opatova s brankářem, kterého jsme ještě minulý týden neznali, na střídačce s dvěma lidmi, které jsme ještě na podzim neznali a s dresy putujícími kvůli zapomenuté kopačce tam a zpátky okresem. Tolik k prvnímu třetině zápasu.

V té druhé sice měli domácí fotbalisté územní i střeleckou převahu, ale premiant Vašek v naší bráně dlouho nemusel zasahovat. Standartky končily mimo, stejně jako křížné zakončení z nejlepší domácí šance, kterou Kalánkovi dostatečně znepříjemnil Chmelík. Naše standartky pravidelně rezultovaly v rohy, ty pravidelně sbíral domácí gólman, jen jednou se k odraženému míči dostal Ropek, ale jakoby se na něm při střele zpoza vápna projevilo tréninkové manko. Skalovy pokusy byly spíše pro statistika, Válkova pozice po křížném míči nebyla nepodobné té z minulého týdne, ale výsledek byl stejně žalostný, při ojedinělém závaru ve vápně se Kopecký s Válkem houštinou těl neprodrali k zakončení vůbec. Suma sumárum první poločas musel skončit bez branek, ale snížení domácích šancí na minimum i hratelnost jejich obrany při pozemní kombinaci nám v kontrastu s předzápasovým očekáváním nebraly víru v dobrý výsledek.

Druhý poločas pokračoval v nastoleném trendu, je pravda, že jsme se kolem vápna dostali párkrát do dobrých pozic, ale taky jsme si v nich počínali čím dál hůře, takže domácí gólman si připsal jen pár jednoduchých zákroků "do koše". Náš gólman už se musel častěji projevit při sbírání a vyrážení centrů, a protože si nevedl špatně, publikum si nejvíc zahučelo, když z protějšího rohu hřiště jednoznačně vidělo Válkovo hraní rukou ve vápně při blokování centru. Sudí byl naštěstí ještě blíž a tento požadavek smetl ze stolu, což bylo rozhodně chvályhodnější než žlutá karta pro Chmelíka po souboji, ve kterém byl náš hráč spíš v roli faulovaného. Po jiném faulu z podobné kategorie, se dvacet minut před koncem k přímáku z 25 metrů vzdor několika neúspěšným předchozím pokusům postavil domácí Polák a na jeho plovoucí ránu do středu brány náš gólman zareagoval pozdě. Vzdor nabranému manku pokračovalo naše nesmělé ofenzivní představení stále stejným tempem, jen s otevřenější obranou. Po jednom z brejků opravil domácí Polák férovým gestem rozhodčí, místo rohu jsme tak čelili pouze autu. Ten odvrátil Chmelík bezskurpulózně na roh, u něj byl domácí útočník dříve než náš gólman a bylo prakticky hotovo. Že by karma za dobrý skutek? To, že domácí z dalšího brejku navýšili skóre, není vůbec důležité. To, že si kromě brankáře připsal premiérový start i Hubáček, by se mohlo docenit až jim slušnost poručí přinést zápisné. To, že naši nejnebezpečnější střelu převedl pár minut před koncem předstoper Chmelík béčkovou nohou z dobrých pětatřiceti metrů, to je na celém zápase to nejhorší.
