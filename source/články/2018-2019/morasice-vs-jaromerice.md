---
title: 'Tak jsme krutí, no a co?'
date: 2019-05-19T00:00:00.000Z
image: /public/images/zapasy/morasice-jaromerice.png
h1: Morašice vs Jaroměřice
category: muzi
sestava: >-
  J. Špinar – D. Štancl, M. Ropek, V. Štancl, M. Klement – B. Válek (J. Vít), M.
  Chmelík, Z. Skala, D. Ondráček (M. Sokol) – J. Kopecký (D. Vejrych), M.
  Janypka (F. Bureš)
goly: 'V. Štancl, M. Janypka, B. Válek'
zlute: Z. Skala
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533A1A/2018/Morašice/e72b1b77-3c7c-4adb-9844-7aa684667be3.html
---
Náš mančaft prokazuje čím dál větší psychickou odolnost. Ani když nám pár kol před koncem začíná pomalu hořet do bot, není důvod panikařit s přehnaným trénováním nebo včasným příchodem na zápas. Na místo rychlých útočníků jsme proti vždy ofenzivně laděným Jaroměřicím postavili dopředu Kopeckého s Janypkou, doufajíc v jejich podržení balónu a nějakou tu proměněnou šanci po brejku do otevřené obrany. Začátek patřil hostům, kteří se párkrát zastřelili zpoza vápna, po rozehraném přímáku přízemní střela naštěstí olízla jen boční síť a obávaného Švece vychytal při zakončení z úhlu Špinar nohama. Po patnácti minutách se Skala postavil na půlce k přímáku, jakých jsme za minulé zápasy měli nespočet, ale tentokrát byl V. Štancl u míče o touch down dřív než vybíhající gólman a na rozdíl od něho měl ještě dost sil sledovat, jak jeho hlavička obloučkem zapadla do opuštěné branky. 300 minut možná nezní kdovíjak kriticky, ale nám tahle doba od posledního gólu přišla opravdu dost dlouhá. Prohloubit hostující opaření mohl záhy Janypka, ale jeho křížná střela z otočky mířila těsně mimo. V prostřední pasáži jsme hosty pustili k několika rohům, ale z nich nás nijak neohrozili, bohužel ani naše předfinální fáze nebyla moc k světu a vše podstatné se tak stihlo odehrát těsně před půlkou. Ve 40. minutě si Válek naběhl na Chmelíkovu kolmici, jeho centr propadl k Janypkovi, který si ještě pro jistotu v malém vápně vysvačil gólmana a do prázdné brány zvýšil naše vedení. Brzy nato Skala nasazenými jesličkami oslovil Kopeckého, jeho křižující obránce neomylně našel na vápně Válka, ten se nenechal rozhodit tím, že hosté přestali hrát a tváří v tvář gólmanovi uklidil míč k levé tyči. Přestože zbývalo do šaten jen pár minut, málem jsme stihli o luxusní náskok přijít, ale hlavičkujícího Švece dvakrát vychytal Špinar. A sluší se zmínit hlavně první zákrok, kdy na čáře stihl vyrazit hlavičku z pár metrů alespoň do tyče.

Přes nabádání k opatrnosti začali druhou půli lépe hosté a po čtvrt hodině to bylo už jen o gól. Nejprve se prosadili hlavičkou na zadní tyči a poté zakončili křížnou střelou rychlý brejk po našem rohu, obojí měl na svědomí stejný hráč. Naše psychická odolnost k sra\*\**m byla tatam, s míčem jsme toho moc nenahráli, ale že by si hosté vypracovali další loženku, to se taky říct nedá. Z trvalé převahy vybočila Válkova akce, pár našich rohů zakončených útočnými fauly, nejnepříznivěji to s námi vypadalo, když zblokovaná střela zamířila do Špinarova protipohybu, ale naštěstí i mimo tři tyče. Rozhodnout mohli střídající útočníci, ale Sokola vychytal gólman a Bureš svou dorážku provedl svými slovy nepěkně. Úplný závěr se nám podařilo díky rohům a taktickým střídání uhrát bez nebezpečí a po otřesné druhé půlce jsme tak mohli slavit kýženou výhru i nevídané střelecké probuzení, pro hosty možná až kruté. Na jednu stranu je dobře, že pár lidí nepodlehlo bezprostřední radosti a iluzi z toho, že bylo všechno dobré, na druhou stranu to vůbec dobře není.
