---
title: Běh Terryho Foxe
date: 2019-06-02T00:00:00.000Z
image: /public/images/zapasy/morasice-horni-ujezd.png
h1: Morašice vs Horní Újezd
category: muzi
sestava: >-
  J. Špinar - D. Štancl, M. Chmelík, V. Štancl, V. Tměj (M. Zerzán) - B. Válek,
  M. Klement, T. Nádvorník (M. Sokol), D. Ondráček (J. Vít) - J. Kopecký (K.
  Vopařil), Z. Skala (F. Bureš)
goly: 'T. Nádvorník, B. Válek (pen.)'
zlute: J. Kopecký
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533A1A/2018/Morašice/c15c9572-eca7-478f-bfb3-5f8cb6fd710d.html
---
Poslední nedělní domácí zápas této sezóny se setkal s velkým zájmem jak v hledišti, tak v kabině, nakolik to bylo přitažlivostí derby, hezkým počasím nebo neutuchající zvědavostí, jak jsme mohli dát minulý týden 8 gólů, to ať si každý čtenář přebere sám. První pobyt hostů z Horního Újezda na naší půlce se datoval až do třetí minuty, o to mrzutější byl fakt, že Pechanec svůj přímák kopnul podél zdi a Špinar na něj nedosáhl. Opařující efekt zafungoval stoprocentně a hosté zahrávali v krátkém sledu pár rohů, které z ožehavého prostoru malého vápna odvraceli Špinar nebo Chmelík. Naše první šance přišla rovněž po standartní situaci, když Skala vracel do vápna míč odvrácený z rohového kopu, ale osamocený V. Štancl ve skluzu zpracoval místo vystřelil a poté už zblízka na Havlíka v hostující bráně nevyzrál. K dalšímu pokusu se ze strany prokombinoval Nádvorník, ale jeho první pokus chytil gólman a druhý zblokovala obrana. Svého prvního gólu v našem dresu se ale dočkal vzápětí, když gólman vyboxoval Válkův roh mířící přímo do brány, naše "desítka" si počkala za vápnem na nějakých 25 metrech a odražený balón napálila krásně do víka. Druhou půlku poločasu jsme měli několik možností, jak dokonat obrat a využít hostující hry na čekanou. Kopeckého zblokovaný náběh na přední tyč byl ještě z kategorie překvapivých zakončení, Ondráčkova slabá šajtle po nasazených jeslích působila ukvapeně a Skalovo slabé zakončení z nejvyloženější pozice zůstává záhadou.

Trochu jsme doufali, že v druhém poločase zúročíme naši nabitou lavičku, ale on se házel jeden aut za druhým, po většinu času měl zápas tempo, ale nikoliv zápal, Terryho Foxe, a tak nám tahle karta jaksi netrumfovala. O ojedinělé vzrušení se svým průnikem postaral Klement, ale nebyl z toho ani penaltový faul, ani pořádné zakončení, jen pořádná strkanice Kopeckého s hostujícím stoperem. Asi o třicet osm hozených autů později táhl Válek míč po své straně tak dlouho, až  po kontaktu s protihráčem padl poměrně hluboko ve vápně. Spekulace a vztek hostí, jestli byl ve vápně i ten kontakt, nijak neřešil a stejně jako minulý týden proměnil penaltu po faulu na svou osobu. Místo uklidnění hry pokračovala naše nevýrazná hra, Špinar si proti střelám z dálky připsal první zákroky, ale 10 minut před koncem neměl šanci proti dalšímu trestňáku Pechance, tentokrát mnohem výstavnějšímu. Hned po rozehrávce se dostal Válek až před gólmana, ale ten hostům nohou uchránil nerozhodný stav. Rozhodující akce jakoby vystihla celý zápas - náš odkop odkopli hosté zpátky, naše obrana nebo pomezní sudí zaspali a hostující "Klacek" svůj nájezd proměnil mezi Špinarovy nohy - naše marnost a cosi jako opak zápalu, hostující vyčkávání a střelecká efektivita.
