---
title: Taxis námi vytřeli
date: 2018-10-14T00:00:00.000Z
image: /public/images/zapasy/morasice-kamenna-horka.png
h1: Morašice vs Kamenná Horka
category: muzi
sestava: >-
  J. Špinar - V. Tměj, M. Ropek, D. Štancl, M. Janypka - B. Válek, M. Chmelík,
  Z. Skala - J. Kubeš, P. Pospíšil (J. Vít), F. Bureš (M. Sokol)
goly: F.Bureš
zlute: V.Tměj
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533A1A/2018/Morašice/1d120f84-cafc-44d5-96dc-511fe2eb187a.html
---
Cenný, ale draze vykoupený bod z Jaroměřic jsme doma potřebovali potvrdit i bez tří nových lazarů a Kamenná Horka je soupeř, kterého bychom doma rozhodně porážet měli. Jenže ani sestava se třemi útočníky nám výrazný posun v naší ofenzivní zaostalosti nepřinesla. Pravda, od třetí minuty jsme měli dobývat pouze desetičlenného soupeře, protože Kubešův zmařený samostatný únik si zasloužil od stejného rozhodčího stejné ohodnocení jako Ropkův zákrok na jaře v Trnávce, ale nikde není psáno, že by se nám v tom případě dařilo lépe. S větrem v zádech jsme soupeře přehrávali a dostávali se i nezvykle často k zakončení, bohužel hostující ne zrovna jistotou překypující gólman vyrážel centry a dalekonosné pokusy do prázdna, ve vápně prostě na dorážku vždycky někdo chyběl. V první vypracovanější šanci nemířil o moc vedle Pospíšilův lob. Když z dálky napřáhl Bureš přesně k tyči, bylo s všeobecným podivem a nelibostí přijato, že brankář, do té doby si leda zavazující tkaničky, předvedl výborný zákrok. A do třetice se na zadní tyči protlačil k centru Skala, ale zblízka nedokázal dát míči pořádnou razanci, říkal k tomu něco o dlouhé noze. Pár hostujících ofenzivních výkřiků do té doby brzdily ofsajdy, ale když se jim podařilo si vše ohlídat, byl z toho brejk dva na dva a jejich útočník si v pravý čas odskočil od Tměje, aby usměrnil do prázdné brány lahůdkovou pobídku z křídla. Naše snažení to úplně neotupilo, hostující brankář si připsal zákrok pěstí proti další Burešově střele a roli úspěšného statisty při Válkově pokusu z křídla a kapituloval až poté, co vyrazil Pospíšilovo bodlo pouze před sebe a Bureš nasměroval svou dorážku hlavou mimo jeho dosah. Relativně rychlé vyrovnání nás bohužel nijak nenakoplo, podařilo se nám už jen zazdít pár brejků, ubránit standartky ke konci poločasu a celkově zanechat dojem, že jsme ti míň nažhavení do zápasu.

Do druhé půle musel nuceně odjet hlavní autor zápisků a jeho příjmenovitý náhradník ztratil po pár minutách sílu dále sledovat vývoj utkání. Raději se uchýlil s kolegy do kabiny k lékárně. Po individuálních chybách nasypali hosté do naší branky 4 kusy a šlo se domů. Někteří fanoušci odcházeli dokonce s předstihem a musíme si přiznat, že to asi nebylo jen kvůli Velké pardubické.
