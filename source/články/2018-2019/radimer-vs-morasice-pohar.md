---
title: Jednou a dost
date: 2018-08-04T00:00:00.000Z
image: /public/images/zapasy/radimer-morasice.png
h1: Radiměř vs Morašíce
category: muzi
sestava: >-
  M. Burián – T. Holomek, M. Ropek, V. Štancl, V. Tměj – B. Válek, M. Klement
  (J. Kopecký), M. Chmelík, Z. Skala – M. Zerzán, K. Vopařil
goly: 'B. Válek2, M. Chmelík, K. Vopařil'
zlute: 'M. Chmelík, M. Burián'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533A1B/2018/Morašice/2c5a3927-df19-42c1-8d5a-e7d61cec17ca.html
---
Letní přestávka nepřinesla do našich řad zatím nic a nikoho nového, což znamenalo mimo jiné to, že první kolo okresního poháru ve sluncem zalitém týdnu byla opět o hráče nouze, nota bene když se z různých důvodů nemohli dostavit Martin a Dominik Štanclovi, kteří nechyběli na předchozích 53, resp. 40 zápasech. Jeden přátelák je na náš kádr zkrátka až až. Nakonec jsme jedenáctku dali do kupy snáz než domácí Radiměř, z branky vše jistil Burián a ze střídačky Kopecký. Naštěstí se nám od začátku dařilo diktovat tempo hry, Chmelík s Klementem naprosto ovládli střed pole a díky jejich běhavým výkonům se zbytek naběhal o trochu méně. Když naše kombinace příliš často končily přetaženým centrem ze strany, zkusil to Chmelík průnikovkou na Válka, ten stejně jako hlavní rozhodčí nijak nedbal domácího lajnaře mávajícího praporkem jak na prvního máje, obešel vyběhnuvšího gólmana a po deseti minutách nás uklidnil vedoucím gólem. Oba aktéři si role prohodili při našem prvním rohovém kopu po dvaceti minutách a placírka trestuhodně neobsazeného Chmelíka navýšila náš náskok. Ne že by domácí i díky našim chybám v rozehrávce neměli dostatek nadějných brejků nebo střeleckých pozic, ale Buriána přímou střelou na bránu neohrozili. To my jsme mohli náš náskok pohodlně navýšit, ale Chmelíkova další placírka se svezla po břevně, Vopařilovo zakončení zastavila dobře postavená hlava domácího obránce a i Zerzánův pokus z bezprostřední blízkosti byl po tuhém boji zblokován mimo tři tyče. Když pak z dobré pozice mířil jen do boční sítě jindy se v podobných situacích nemýlící se Holomek, zamířili jsme do kabin s tím nejhorším náskokem a Klement s podebranou patou.

Ve středu hřiště ho nahradil Kopecký, ale klíčovějším faktem bylo, že jsme doslova přestali běhat. Což byl paradox, protože slunko už zapadlo, večerníček dal dávno dobrou noc a vzduch začal být dýchatelnější. Domácí zkoušeli využít naší laxnosti, ale v největší šanci pálil jejich útočník v tísněném samostatném nájezdu nad bránu. Buriánovi se nečinnost asi nezamlouvala, a tak si jednou vyšlápl pro míč těsně za hranici vápna, na což nejvýrazněji upozornil rozhodčího on sám. Domácí ale neproměnili ani tuto standartku. Na druhé straně se pomalu rozjížděl Válek, když nejprve pálil z voleje mimo po Kopeckého pobídce a poté si nenechal dostatečnou ofsajdovou rezervu pro Havránkovy oči. Další náběh na Chmelíkovu kolmici už byl v pořádku a dle trenérových instrukcí si v pohodě navedl balón přes obránce na střed a podél gólmana zaznamenal svou druhou branku. Chvilku na to se situace opakovala, ale místo útoku na hattrick předložil balón před prázdnou bránu Vopařilovi, který svůj snadný gólový příspěvek umně skryl před zraky své začtené přítelkyně. V závěru si ještě už značně nemohoucí Burián s Holomkovou pomocí pohlídal nulu i svou sérii neprůstřelnosti a mohlo by se jet v klidu domů, nebýt Víťova pokusu o oživení jeho chřadnoucího vozu.
