---
title: 'Kapitola čtrnáctá: Konečně jaro'
date: 2019-03-24T00:00:00.000Z
image: /public/images/zapasy/mestecko-trnavka-morasice.png
h1: M. Trnávka vs Morašice
category: muzi
sestava: >-
  J. Špinar - D. Štancl, M. Ropek, V. Štancl, M. Janypka (V. Novák) - B. Válek,
  M. Klement, Z. Skala, J. Vít (J. Kopecký) - M. Chmelík, F. Bureš (M. Sokol))
goly: 'M. Chmelík, M. Ropek, M. Sokol, V. Štancl'
zlute: 'M. Klement, V. Štancl, B. Válek, M. Chmelík'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533A1A/2018/Morašice/66c5cb74-8722-4f29-aaae-7969ed6056c1.html
---
O radostech a hlavně strastech zimní přípravy asi nejvíce vypoví pohled na nabité sestavy při zimních přátelácích. 

Morašice - Horní Újezd 		2:1

Branky: Bureš, Válek (pen).

Hráli: Špinar - Ropek, D. Štancl, V. Štancl, Novák, Janypka, Chmelík, Válek, Vít, Bureš, Skala, Kopecký, Vejrych

Morašice - Čistá		1:1

Branky: Válek

Hráli: Špinar - Ropek, D. Štancl, Novák, Janypka, Chmelík, Válek, Vít, Bureš, Skala, Kopecký, Vejrych, Ondráček



Morašice - Dolní Újezd "B"	4:2

Branky: Ondráček2, Válek, Škoda

Hráli: Špinar - Ropek, D. Štancl, Janypka, V. Štancl, Kopecký, Chmelík, Válek, Skala, Ondráček, Škoda



První ostrý zápas nás nasměroval k předposlednímu celku soutěže. Toto postavení nikdy nic moc neznamená a v našem případě obzvlášť, protože Městečko Trnávka získalo dvě třetiny svých bodů a čtvrtinu svých gólů právě na našem hřišti. Díky jejich chvályhodné péči o webové stránky jsme přikročili k důkladnému videorozboru, abychom odčinili porážku, která jako by předznamenala naše podzimní soužení. Výborný vstup do zápasu nám zajistil Chmelík, který si v přátelácích vytrvale zkoušel jak post útočníka, tak pálení šancí z toho vyplývajících. Naštěstí pro nás si pálil ostrými hned v prvním ostrém zápase. I když slovo pálil je hodně nadnesené, protože když po pěti minutách kličkou přešel přes  posledního obránce a zjistil, že domácí brankář kryje při jeho postupu ze středu pouze pravou čtvrtinu brány, chladnokrevně poslal zpoza vápna balón k volné tyči a ten po dobrých pěti vteřinách slavnostně přešel brankovou čáru. Náš dobrý nástup vyústil ještě v povedenou, ale chycenou střelu Klementa, pak už jsme hrozili hlavně ze Skalových rohů, ale ze hry už jsme se k šancím dostávali hůře. Ale i to nám mohli domácí závidět, protože ti si připsali jen asi čtyři centry do vápna, z nichž jeden hlavičkovali vedle a další si stáhl Špinar. Kolem poloviny poločasu se začalo jiskřit a žlutit ve středu pole, což byl důsledek našeho pomalého dostupování i psychické lability domácího provokatéra a našeho mstitele Válka. Mnohem lepší trest než oplácení si vymyslel Ropek, který byl u Skalova přímáku po faulu dotyčného jedince o vlásek dřív než odvážně až bláznivě vybíhající gólman a pět minut před poločasem nám hlavou zdvojnásobil vedení.



Nejzapamatovatelnějšími momenty druhého poločasu byly dlouho jenom střídající manévry u střídaček, nám se nedařilo dostat k pojistce, domácím ke korekci. Nejblíže třetímu gólu byl Bureš, který si našel odražený balón po Chmelíkově střele, jenže jeho opravdu tvrdá rána mířil opravdu docela dost nad břevno. Kolem 70.minuty nám zatrnulo, když se Ropek pokoušel jít znovu předčasně do sprch, naštěstí stihl sólově postupujícímu útočníkovi vzít skluzem balón. Podle mínění domácích i s nohama, ale že by hlavní rozhodčí upřel penaltu týmu, v němž zná polovinu lidí, to se na naší úrovni moc nestává. Po tvrdé zimní přípravě vtrhl na posledních dvacet minut na hřiště Sokol a stihl toho docela dostat. Nejprve si zběhl na Válkův centr na přední tyč a uklidil míč do sítě, ale z uctivé vzdálenosti vše jistící asistent Havránek ho přistihl v ofsajdu. Poté si na lajně zpracoval balón, obtočil se kolem svého strážce a jeho nepovedený centr prošel gólmanovi u přední tyče do sítě. A brzy na to po pěkné kombinaci ještě rozezvonil břevno. Mezi jeho momenty se ještě vklínil gól V. Štancla, který po rohu zametl odražený balón jen tak trapně levou holení, ale na čáře bránící hráč z toho udělal ještě pěknou trefu pod břevno. V úplném závěru se ještě k samostatnému nájezdu prokličkoval Novák, ale patrně mu blesknul hlavou rozhodnutý stav, jal se vymýšlet lob a byla z toho malá domů. Radost ze splněné povinnosti nám to ale nijak nezkazilo, budeme věřit, že nám výsledek zápasu s Trnávkou opět předurčí ráz nadcházející poloviny sezóny.

Konkurenční, ale nápadně podobný pohled na zápas si můžete dopřát [zde](http://www.sokoltrnavka.cz/).
