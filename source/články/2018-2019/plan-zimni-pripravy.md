---
title: Plán zimní přípravy
date: '2019-01-09'
image: ''
category: obecne
---
Tréninky dle domluvy, individuální plány nežádoucí.

15.-17.2. - soustředění Daňkovice, odjezd v pátek ráno

**SO 23.2.** - pořádání obecního plesu v Morašicích, neúčast bude odpracována v průběhu jara (extra služby, brigády).

**NE 3.3.** - 9:00 přátelák s **Horním Újezdem** (UMT Litomyšl, sraz na místě v 8:15)

**SO 9.3.** - 16:00 přátelák s **Krounou** (UMT Litomyšl, sraz na místě v 15:15)

**SO 16.3.** - 9:00 přátelák s **Dolním Újezdem B** (UMT Litomyšl, sraz na místě v 8:15)

**NE 24.3.** - 15:00 mistrovské utkání v Městečku Trnávka
