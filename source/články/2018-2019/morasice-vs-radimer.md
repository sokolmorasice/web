---
title: Nesnáším loučení
date: 2019-06-15T00:00:00.000Z
image: /public/images/zapasy/morasice-radimer.png
h1: Morašice vs Radiměř
category: muzi
sestava: >-
  Sestava: J. Špinar - V. Novák, M. Ropek (M. Zerzán), V. Štancl, M. Klement -
  B. Válek, Z. Skala, T. Nádvorník (D. Vejrych), D. Ondráček (Z. Škoda) - M.
  Chmelík, F. Bureš (M. Janypka)
goly: 'Góly: V. Štancl, F. Bureš, M. Zerzán, B. Válek'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533A1A/2018/Morašice/c21004da-60c3-4cd8-95a0-5aedc4a4243b.html
---
Poslední zápas této sezóny byl s největší pravděpodobností i posledním zápasem ve starých kabinách a na nějaký čas i posledním zápasem v Morašicích, protože když vše dopadne, jak chceme, čeká nás podzimní vyhnanství. Soupeři z Radiměře se díky povedenému jaru a střelci Stodolovi podařilo zachránit, a tak jsme si mohli poslední zápas všichni užít. Škoda toho dusna a menší návštěvy, nicméně ti co přišli, vydali za metalovou kapelu. Fotbaloví, ale zároveň vzadu slabší hosté nám dovolili poměrně dost, bylo jen na nás, jak se přizpůsobíme. Do vedení nás mohl dostat Bureš, který si vyčkal propadlého míče za obranu, ale jeho dobrou levačku vyrazil gólman neméně dobrým zákrokem na břevno. Hosté se dostali do několika zajímavých pozic kolem vápna a po čtvrt hodině nám předvedli ukázkový brejk. Na finální přihrávku nedosáhl V. Štancl a střelu z ložené pozice ještě lehce lízl do vlastní brány Novák. V. Štancl si naštěstí přichystal brzký odpustek, když k němu ropadl na zadní tyč Skalův přímý kop. Napoprvé ještě nechal vyniknout gólmana, ale dorážce už nezabránil nikdo. Burešův další náběh za obranu sporně zastavil Kuta v roli pomezního, dvakrát se nám povedlo navést koncovku na Válka, kterého ale skvělými zákroky nepotěšil hostující brankář, série tří dobrých rohů nakonec nepřinesla ani přímou střelu na bránu, na Válkův centr si seběhl Ondráček, který se zblízka tak snažil netrefit gólmana, že vůbec netrefil bránu. Až chvíli před pauzou sklepl Chmelík přímák Skaly na Bureše, ten vyhrál hlavičkový souboj s obráncem a poslal nás do vedení.

Druhý poločas byl mizerný na pohyb a nasazení, ale šancí z toho bylo kupodivu dost. Nepřesně pálil Válek, několik jasných situací jsme zazdili špatnou přihrávkou z brankové čáry pod sebe. Střídající Zerzán měl to štěstí, že Ondráčkovu přihrávku vyrazil gólman přímo k němu a uklidil ho pod víko. Z dálky pálil do tyče Nádvorník, Skala na vlastní kůži poznal, jak je těžké si u Famfulíka pohlídat ofsajd, takže jeho gól neplatil a jeho předchozí kázání o tomto problému tak působí trochu nepatřičně. Náladu si spravil pět minut před koncem, když na jeho Hušbauerovský centr Součkovsky naskočil Válek a stanovil konečné skóre, které mohlo být vzhledem k nashromážděným šancím mnohem vyšší. V několika jiných případech nás produktivita stála body a je to jeden z hlavních důvodů, proč jsme sezónu skončili s nejhorším okresním výsledkem za hodně dlouhou dobu. Dnes to ale ultras u klobás nevadilo a poprvé za sezónu jsme byli odměněni bouřlivým skandováním. Ale třeba je jen dojala ta rozlučka s kabinami.
