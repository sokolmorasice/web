---
title: Všechno špatně
date: 2019-04-27T00:00:00.000Z
image: /public/images/zapasy/cerekvice-nad-loucnou-morasice.png
h1: Cerekvice vs Morašice
category: muzi
sestava: >-
  M. Prešinský - D. Štancl, M. Ropek, V. Štancl, V. Novák - B. Válek, M.
  Chmelík, Z. Skala (J. Špinar), D. Ondráček - F. Bureš, J. Vít (M. Sokol)
goly: M. Chmelík
zlute: 'ŽK: B. Válek, D. Ondráček'
cervene: B. Válek
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533A1A/2018/Morašice/cf454a56-6188-44dc-9f0f-174d637c36db.html
---
Kromě bitvy o Zimohrad očekával celý svět v prvním povelikonočním víkendu ještě jednu bitvu - Cerekvice vs. Morašice. Možná ji očekával víc než naši hráči, kteří nedodrželi čas srazu klidně o čtvrt hodiny. Nepovedla se ani rozcvička, ani dodržení trenérova pokynu vyhýbat se faulům kolem vápna. Shodou okolností to byl právě trenér, komu byl hned v první minutě odpískán faul na Malého a Lorenc z výsostné pozice za vápnem hned na začátku určil ráz utkání precizním kopem. Jakkoli byl ten faul z našeho pohledu sporný až nesmyslný, nic to nemění na tom, že setrvávat v zarytém vzteku a podrážděnosti bez chuti hrát a odpovědně bránit techničtější domácí, je špatná cesta. Byli jsme všude pozdě, nepodrželi míč, nedali si přesnou přihrávku, nepokrývali domácí rychlíky. Malý se chvíli zastřeloval vedle nebo přímo do Prešinského, J. Vostrčil po vyškolení Nováka tváří v tvář Prešinskému trefil jen náhle se zjevivší nohu nevzdávajícího se Nováka. Janderův náběh na aut zachytil pouze Uher, který po dvaceti minutách pohodlně uklidil jeho pobídku do prázdné brány. My jsme se zmohli jen na Skalovy přímáky z půlky, ke kterým se ale naši hráči nebyli schopní dostat, Válkovo nepovedené zakončení po dlouhém autu a z převahy domácích, jejichž nadějné pozice ani nestojí za vypisování, vybočil ještě pokus Skaly, který zkoušel vrátit přímo do brány jeden tradičně slabý výkop V. Vostrčila, ale minul.

Naše úsilí udělat něco se zápasem po návratu z kabin utnula série domácích rohů, která vyústila v rozehrávku na Kříže, jenž měl za vápnem moře času na to, aby otřel svou střelu o nešťastníka Ropka a překonal potřetí nešťastníka Prešinského. Po chvilce uklidnění přišla naše hvězdná minuta, do které jsme vměstnali naše jediné, ale hned trojité ohrožení brány. Skala ze střední vzdálenosti trefil břevno, osamoceně dorážejícímu Válkovi na malém vápně se míč svezl po hlavě tak trapně do náruče V. Vostrčila, že by to milovníky absurdních dramat až rozesmálo. K následnému výhozu domácího brankáře se prodral Chmelík a levačkou z uctivé vzdálenosti snížil. Jestli to naše akcie o trochu zvedlo, určitě je mnohem víc srazila následná rozehra domácích, kteří se za deset vteřin dostali ze středového kruhu k penaltě po faulu Nováka na J. Vostrčila. Jandera bezpečně proměnil, po nerozvážném otevření stavidel si ještě poradil se samostatným nájezdem Malý a uzavřel tím bohatý výčet gólů, bohužel výrazně nakloněný v náš neprospěch. Hra na jednu bránu totiž přinesla pouze domácí zastřelování, Novákovo nezaviněné zranění, které demonstrovalo jak nepovedený den jsme u Loučné strávili a Válkovou červenou kartu, která demonstrovala, jak proti nepřízni osudu nebojovat. Příležitost k nápravě a zároveň možnost poprat se o červnový prostor k pomstě budeme mít nezvykle brzy.
