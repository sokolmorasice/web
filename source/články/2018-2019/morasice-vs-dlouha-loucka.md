---
title: Dlouhá není Horní
date: 2018-10-28T00:00:00.000Z
image: /public/images/zapasy/morasice-dlouha-loucka.png
h1: Morašice vs Dlouhá Loučka
category: muzi
sestava: >-
  J. Kubeš - D. Štancl, M. Ropek, M. Chmelík, V. Tměj - B. Válek, M. Klement, Z.
  Skala, J. Vít (V. Novák) - M. Janypka (M. Sokol), F. Bureš (P. Pospíšil)
goly: M.Klement
zlute: 'Z.Skala, M.Klement'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533A1A/2018/Morašice/d1038598-5b96-4593-b7cf-c78b8c7359f8.html
---
Celý týden nám vydržela opojná kocovina z vítězství v Horním Újezdě. Zdánlivě nebyl důvod sahat do nouzové, ale nakonec vítězné sestavy a zdánlivě nebylo objektivních příčin, proč bychom domácí fanoušky po delší době nepotěšily vítězstvím, když je na tom Dlouhá Loučka ještě hůř než my. Ale zdání v okrese klame a trvalo celý deštivý poločas, než jsme vystřízlivěli. Hosté nás přehrávali, všude byli dřív a poctivě se starali o to, aby se hra neodehrávala na fanouškům vzdálené straně hřiště. Nás neposlouchalo ani srdíčko, ani balón. K našemu štěstí ale rozhodčí vyhodnotil Chmelíkův obranný zákrok ve vápně jako dovolený, hostující střely z lepších i horších pozic mířily nad bránu nebo přímo na Kubeše, při nejnebezpečnější standartce Chmelík hlavou ubránil dva nabíhající supy, a tak hostující převaha vyzněla do prázdna. Na druhé straně se něco málo šustlo až ke konci. Válek hodně z úhlu trefil tyč, v největší šanci Klement na dlouhou nohu zakončoval kombinaci ve vápně mimo tři tyče a kdyby Skalovým kombajnům koloval v žilách stejně kvalitní volej, jako předvedl z vápna on, Babiš by se neuživil.

Přestávka poměr sil otočila, a tak si nezaujatí fanoušci nemohli na nic stěžovat, všechno měli na očích. Náš zlepšený výkon a kombinace vyústily ve volného Klementa, který z nějakých dvaceti metrů dvakrát střílel nad. Pravou stranu oživil Novák, který po Janypkově průnikovce pomotal hostujícího obránce, ale gólmana na bližší tyči zachránila po jeho střele kovová ozvěna. Za druhý poločas zaslouženého vedení jsme dočkali až poté, co Skala dostal po rohovém kopu příležitost k reparátu, na jeho centr se ze stínu Ropka vyšrouboval do vzduchu Klement a hlavou trefil odkrytou bránu. Hosté ale na vynulování našeho dlouhého úsilí potřebovali jen několik minut a jednu šanci. Přízemní centr z pravé strany odvrátil Ropek pouze do ruky hostujícího útočníka, a ten poté neměl problém z bezprostřední blízkosti ukončit neprůstřelnost odlétajícího Kubeše. Třeba ta ruka, třeba hostující obranné pokyny „zlom mu nohu“, třeba klasická rozhodcovská starost o urážky místo zdraví, třeba naše frustrace z vlastní neschopnosti závěr utkání notně vyhecovaly. Přesto jsme ještě měli šance strhnout vítězství na naši stranu, v posledních deseti minutách posunul Pospíšil kolmici na Válka do těsného ofsajdu (ale ten to stejně nedal), sám se pak na vápně dostal ke slabé střele a nakonec byl Novákův průnik ve vápně utnut podrážkovým skluzem, který byl v trendu celého zápasu posouzen jako dovolený (čímž vůbec není myšleno, že byl v pořádku). Zatímco Skala pak při vědomí své absence příští týden koketoval se soupeřem a červenou kartou, ostatním zbyly jen dresy ke ždímání. Při ohlédnutí za domácí půlsezónou tak musíme být nesmírně rádi za nečekané výhry nad Cerekvicí a Bystrým, jinak bychom proti nespokojenosti našim fanoušků neměli už vůbec žádný argument.
