---
title: Co bylo
date: 2019-06-28T20:26:32.192Z
image: /public/images/uploads/fotbal.jpg
category: obecne
---
Uplynulý soutěžní ročník se do naší historie rozhodně nezapíše nijak výrazným písmem. Náš tým mužů se touto sezónou od začátku protrápil. Slabá tréninková účast a početná zranění v úzkém kádru se v zápasech projevila především na útočné činnosti, když je slabá produktivita stála výhry nad herně slabšími soupeři, nakonec z toho bylo nelichotivé 9.místo. 

Starší žáci se letos prezentovali dobrým fotbalem, ale žehrali na koncovku. Konečné 6. místo tak příliš nevypovídá o jejich herní kvalitě, která je pro příští ročník velkým příslibem.

Mladší žáci se účastnili soutěže na Chrudimsku a v konkurenci málo známých soupeřů obsadili s bilancí 17 výher z 21 zápasů skvělé 2.místo.

Starší přípravka obsadila na finálovém turnaji v Březové 5.míto z 11 účastníků, což je vzhledem k výsledkům jejich turnajů poměrně odpovídající umístění. K nejlepším něco chybělo, o to hezčí je, že se podařilo vyhrát na jaře zrovna turnaj konaný v Morašicích.

Mladší přípravka patřila celou sezónu k nejlepším tymům soutěže, což se podařilo dokázat na závěrečném poháru v Jevíčku, odkud si přivezli po stoprocentních výsledcích nejcennější trofej.

Poděkování za uplynulou sezónu patří především trenérům mládeže, kterými byli Jirka Famfulík, Honza Špinar, David Severa a Martin Štancl, ale také všem, co pomáhají zabezpečit, že budeme hrát na posekaném hřišti v čistých dresech a budeme si k tomu moct i něco zakousnout. Velký dík za výbornou spolupráci i personální nasazení patří také Tělovýchovné jednotě Horní Újezd, se kterou sdílíme všechny mládežnické kategorie.
