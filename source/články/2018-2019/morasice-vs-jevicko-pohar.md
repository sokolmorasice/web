---
title: Hlavně že on...
date: 2019-05-01T00:00:00.000Z
image: /public/images/zapasy/morasice-jevicko.png
h1: Morašice vs Jevíčko
category: muzi
sestava: >-
  D. Ondráček – D. Štancl, J. Kopecký, M. Ropek, V. Tměj (D. Vejrych) – B.
  Válek, M. Chmelík, Z. Skala, J. Vít (J. Špinar) – F. Bureš (V. Štancl), M.
  Sokol (K. Vopařil)
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533A1B/2018/Morašice/3386d7a7-303d-4784-9f4a-c9f0a9f767e4.html
---
To nejdůležitější z prvního poločasu pohárového semifinále s Jevíčkem se odehrálo bez přítomnosti státního dozoru a notáře, a tak se musíme spolehnout na drby z kuloárů. Premiéru v bráně si odbyl Ondráček, záhy po ukončení letošního ročníku extraligy jsme v kádru přivítaly hokejisty Tměje a Vopařila. První gól padl cca o půl šesté, ale to kvůli předzápasové průtrži běžela zhruba desátá minuta. Prý jednoduchý nákop za obranu a on je ten jejich Linhart hodně rychlej i efektivní a s nájezdem si poradil. Z tance mezi šestnáctkami vybočila akorát Chmelíkova možnost, ale těsně před pauzou netrefil hlavou prý prázdnou bránu. 

I když jsme ve druhém poločase prostřídali útok a hosté šli po hodině do desíti, bylo pro nás neřešitelným úkolem neřešit výroky rozhodčího a dostat se vůbec do šance, natož blízko k vyrovnání. Stejně neřešitelně se jeví psát o tom nějaké tklivé čtení. Válek sice dostal ze strany centr na zadní tyč, ale Vít se k němu nenatlačil, V. Štancl se dostal do pěkné pozice na křídle, ale od brankové čáry přihrál pouze gólmanovi, Vopařil se sice vynořil za obránci, ale v ofsajdu. Celkově vzato jsme se sice snažili útočit, ale hosté to možná ani moc nepocítili, v pohodě si tahali čas, řepácky uklízeli balón, co nejdál to šlo, jeden brejk zazdili, pět minut před koncem si po autovém vhazování za přihlížení několika našich lidí poradili ve dvou k druhému gólu a volná červnová středa byla na světě.
