---
title: Když ani beton nepomůže
date: 2019-05-05T00:00:00.000Z
image: /public/images/zapasy/morasice-brezova-nad-svit.png
h1: Morašice vs Březová nad Svit.
category: muzi
sestava: >-
  M. Prešinský – D. Štancl, M. Ropek, M. Chmelík, V. Štancl – B. Válek, Z.
  Skala, M. Sokol (D. Vejrych), D. Ondráček – J. Vít (K. Vopařil), J. Kopecký
zlute: 'D. Ondráček, K. Vopařil'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533A1A/2018/Morašice/8a64c8a2-4511-4e97-9265-1d1e650a44a0.html
---
Zatímco všem možným prvomájovým průvodům počasí přálo, nedělní zápas se odehrával v kulisách sedících spíše konci podzimní části. Čelní celek přeboru z Březové k nám vypravil autobus, nás z různých důvodů, zejména ne zrovna příznivě nakloněné vyšší moci, zůstalo v kabině jen 13 sirotků, a i proto jsme se vzhledem k našim předchozím nezdarům rozhodli zaparkovat morašický autobus před naším vápnem. Prvních dvacet minut ho ani nebylo třeba, protože jsme překvapivě tahali za delší ofenzivní konec. Hned po pár minutách Válek vystihl kličku hostujícího obránce, vzhledem ke své nulové rychlosti se rozhodl z vápna zakončit ihned, ale mířil vysoko nad. Chvíli na to proletěl Vítův centr malým vápnem, po spolupráci se Skalou hlavičkoval nad V. Štancl, Ondráčka po dobrém náběhu mezi obránce zradil první dotek, po autu se výjimečně Válek trefil mezi tři tyče, ale bohužel přímo do gólmana. Hosté se předvedli pouze nájezdem do Prešinského, o to bolestnější bylo, když se po půl hodině na našem vápně proti sobě rozběhli Ropek s Prešinským bez jasné vize, kdo z nich odehraje zdánlivě bezpečný míč, mezi ně vlétl březovský útočník a prodloužil míč do opuštěné brány. Oba aktéři se připletli i do dalšího momentu smrdícího gólem v naší síti. Ropek byl bez vysvětlení obviněn z faulu zády a následující dobře zahraný přímák Prešinský bravurním skokem vyrazil. Když opadlo naše opaření i jistá zatavenost z fotbalu “nahoru dolů“, dostali jsme se ke konci zase k několika střelám, ale Ondráčkovi chyběla přesnost, vysunutému D. Štanclovi razance a tak byl nejemočnější pokus Válka do boční sítě těsně před pauzou.

Zkraje druhého poločasu hostující „Kobra“ odmítl pojistku z nejnadějněji dohraného brejku a tím Prešinskému prakticky skončil vzruch. My jsme sice prasili jednu rozehrávku za druhou, ale hosté asi chtěli drama nebo je bavilo sáhodlouhé tolerované zdržování, protože útoky do naší rozhozené obrany k nějakému vážnějšímu ohrožení nedotáhli. Míra ubohosti našich přihrávek byla za stálého skandování „BŘE-ZO-VÁ“ opravdu enormní, jako fakt velká, o to víc utkvěly v paměti dvě střely Válka zevnitř vápna. Ale v naší největší šanci mířil nad a druhá střela se zastavila o ruku bránícího hráče, jenže asi jsme té penaltě nešli naproti tak moc, jako Cerekvice v Bystrém. Naše standartky od půlící čáry celý zápas oplývaly neúčinností, a když už hostující obrana v závěru zapomněla na V. Štancla, ten naprosto nerušen demonstroval náš ofenzivní zmar naprosto zkaženou hlavičkou. Takže budeme jako všichni sportovci doufat, že jsme dnes našli něco, od čeho se odrazíme a že příště to už opravdu přijde.
