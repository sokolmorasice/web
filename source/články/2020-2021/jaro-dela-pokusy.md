---
title: Jaro dělá pokusy
date: 2020-12-03T11:05:24.375Z
category: obecne
image: /public/images/uploads/logo.png
---
Tak jsme se uvolnili, třeba ne naposled. Když Pánbu dá a budeme na jaře na správné straně "best in covid", mělo by se v našem oddíle dít zhruba toto:

**Muži** budou hrát od nevidím do nevidím, každý víkend od 13.3. až do 26.6., plus jeden vložený termín plus případné finále okresního poháru (semifinále  - proti Mladějovu, Borové nebo Opatovu - je tentokrát možné sloučit s ligovým zápasem). Začíná se tam, kde se na podzim skončilo, až v průběhu dubna se přejde do odvet.

**Dorost** bude také navazovat bez zásadních změn, v tříčlenné soutěži s tím takový problém není. Začínat by měl 17.-18.4. a končit stejně jako muži, pořadí zápasů se může měnit v návaznosti požadované předzápasy mužů.

**Starší žáci** se taky pokusí odehrát všechno, začnou 10.-11.4. zbývajícími zápasy z podzimu a i s pomocí dvou vložených středečních zápasů by měli dvoukolový systém dohrát 26.6.

**Mladší žáci** přijdou o jednu sadu zápasů s každým soupeřem, původní tříkolový systém je minulostí a bude se hrát jen dvakrát. Což pro ně znamená volnější termínovou listinu, budou hrát od 17.4. do 20.6. Zrovna naši žáci by ale před startem jara měli stihnout dohrát odložený zápas s Jaroměřicemi.

**Přípravky** podzimní resty nahrazovat nebudou, pro jaro dostanou nový rozpis s prvním turnajem plánovaným na 17.4. Starší přípravku čeká 9 víkendů (během každého budou 2 4členné turnaje a 1 tým bude mít volno), finálový turnaj se uskuteční 19.-20.6.. Mladší přípravka bude mít o jeden víkend méně, za každý se stihnou 3 turnaje (jeden 4členný a dva 3členné) a její finále je v plánu 12.-.13.6.

Přijdou Vám ty plány jako moc smělé až pošetilé?

Nevěříte?

Věřte.