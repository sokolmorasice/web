---
title: BORTEL na úvod
date: 2020-09-02
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Borová vs Horní Újezd/Morašice
sestava: Matěj Kopecký – Jakub Bartoš, Filip Flídr, Ondřej Kusý, Jan Kvapil,
  Ondřej Žďára, Lukáš Mach, Jan Flídr, Martin Flídr, Jindřich Mičík, Filip
  Střasák, Martina Famfulíková, Vojtěch Andrle
goly: Ondřej Žďára, Filip Flídr, Lukáš Mach
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2020/Horní
  Újezd/Morašice/5097aef6-9406-48d6-b0fb-f37bbdb9327b.html
---
K prvnímu utkání sezony jsme vyrazili do Telecího, kde jsme hráli proti týmu Borové. V úvodních deseti minutách utkání se na našem týmu projevila nesouhra a díky tomu jsme se téměř nepodívali k soupeři na polovinu. Poté se začala hra postupně vyrovnávat, až jsme začali soupeře přehrávat, bohužel všechny naše střely létaly do soupeřova brankaře, takže to vypadalo, že první poločas skončí bez gólů. V poslední minutě prvního poločasu jsme rozehráli roh na krátko a Ondřej Žďára z ostrého úhlu skóroval. Druhý poločas byl poměrně vyrovnaný a ve 40. minutě soupeř vyrovnal na 1:1. Naštěstí se nám podařilo zápas dovést do vítězného konce, když ve 47. minutě vstřelil svůj první gól za žáky Filip Flídr a pojistka přišla v 53. minutě, kdy Lukáš Mach zahrával rohový kop a obránce si srazil míč do vlastní brány. 

Tomáš Chadima