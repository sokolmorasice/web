---
title: Těch dětí
date: 2020-09-21T05:55:48.623Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Starší přípravka

K turnaji jsme odjížděli s počtem 15ti hráčů, což přesně odpovídalo počtu dresů, které jsme měli k dispozici. Při dvouzápasovém turnaji a hrací době 2 x 15 minut, je docela problém prostřídat všechny hráče, nicméně je to příjemnější starost než mít hráčů málo. 

**Sestava:** Martina Famfulíková, Jan Kvapil, Šimon Flídr, Vojtěch Huryta, Daniel Válek, Matyáš Bulva, František Flídr, Sára Vomočilová, Štěpán Řejha, Kryštof Tomšíček, Josef Rosypal, Matouš Zach, Michal Kovář, Matěj Kovář a Vojtěch Štěpán.

**HÚ/Morašice – Jevíčko 7 : 2**

Začátek zápasu se nám herně moc nepovedl, kluci jakoby nevěděli co mají hrát. Naštěstí s prvními vstřelenými brankami a přibývajícím časem jsme se vrátili k výkonům, které zatím na podzim předvádíme a zápas jsme již kontrolovali a na hřišti se tak prostřídali všichni. 

Branky: Daniel Válek 3, František Flídr 2, Matyáš Bulva, Vojtěch Huryta

**HÚ/Morašice – Březová 10 : 0** 

V tomto utkání jsme dominovali již od samého začátku, a tak jsme mohli ve druhém poločase nasadit do útoku i brankářku Martinu Famfulíkovou a více prostoru dostali ti méně zkušení. 

Branky: Daniel Válek 3, František Flídr 3, Matyáš Bulva 2, Matouš Zach, Jan Kvapil

Autor: František Bartoš

## Mladší přípravka

**Sestava:** Samuel Vomočil, David Klusoň, Jaroslav Chaun, David Boček, Štěpánka Gábrlíková, Štěpán Kovář, Dominik Prokop, Antonín Krejsa

**Horní Újezd/Morašice – Sebranice 3:6 (3:4)**

Vyrovnaný první poločas, gól na 3:4 jsme dostali chvilku před pauzou, v druhé půli Sebranice fotbalovější.

branky: Jaroslav Chaun 3

**Březová – Horní Újezd/Morašice 3:12 (2:5)**

Zasloužené vysoké vítězství, domácí tvoří nový tým, své první góly vstřelili Štěpán Kovář a Domča Prokop.

branky: David Klusoň 4, Štěpán Kovář 3, Samuel Vomočil 2, David Boček, Jaroslav Chaun, Dominik Prokop

**Horní Újezd/Morašice – Bystré 2:1 (0:1)**

Sportovně je třeba uznat, že soupeř byl především v první půli lepší, měl více šancí, ve druhé půli vyrovnal nejprve David Klusoň a poté potrestal David Boček chybu soupeře v rozehrávce, odmakaný zápas a těžce vydřené vítězství.

branky: David Klusoň, David Boček

**Tabulka turnaje:**

1.Sebranice

2.Horní Újezd/Morašice

3.Bystré

4.Březová

Dnes jsme znovu předvedli velice dobrý výkon. V brance se vystřídali Sam Vomočil a Jára Chaun. Pochvala především pro zkušenější hráče, kteří strávili na hřišti více času, jsem rád, že i nováčci se jim snaží svými výkony přiblížit. Další turnaj hostí v neděli 27.9. Telecí.

autor: Mgr. Zdeněk Beneš