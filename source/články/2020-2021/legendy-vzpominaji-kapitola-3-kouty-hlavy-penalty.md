---
title: "Legendy vzpomínají - kapitola 3: Kouty, hlavy, penalty."
date: 2020-12-25T07:43:47.475Z
category: obecne
image: /public/images/uploads/logo.png
---
**Tak po těch místopisných záležitostech pojďme zpátky k fotbalu. Bobši po tvém přestupu do chlapů jste se chvíli plácali okresním přeborem, ale za trenéra Portlíka jste najednou vyskočili k postupu, čím to bylo?**

**JK:** Já si myslím, že se fotbal v Morašicích strašně zvednul za Luboše Adolfa *(předseda oddílu po L. Tobkovi - pozn. redakce)*, který k nám Edu Portlíka dotáhnul.

**Mary ty jsi tou dobou jsi do chlapů naskočil i ty. Vidíš v Portlíkově příchodu taky tu hlavní změnu nebo to třeba bylo tím, že do áčka došla tvoje úspěšná generace z dorostu?**

**MR:** Podle mého názoru Eda Portlík celkově morašický fotbal posunul trošku dál, už to nebylo takové vesnické hraní, dostal do nás fyzičku a secvičili jsme pár akcí, jak se fotbal má hrát a myslím, že on založil základ pro další úspěchy, ať to byla nejúspěšnější sezóna pod Františkem Jandáčkem nebo pak pod Frantou Ropkem a Petrem Kutou.

**Petře, ty jsi byl po postupu do kraje třikrát za sebou nejlepší střelec. Jakou část gólů jsi dával ze standartek a lá [Fernando Couto](**https://c8.alamy.com/comp/HNR16F/fernando-couto-portugal-parma-fc-23-june-1996-HNR16F.jpg**)?**

**PK:** Řekl bych, že převážnou většinu. Ty kouty jsem trefoval opravdu dost pravidelně i přesně, ale občas přišla i doba [Koemanů](<https://www.youtube.com/watch?v=xEdtCk1_WiA>) a [Ronaldinhů](https://www.youtube.com/watch?v=MH-N1kqDKZw). Když dnes vidím české prvoligové exekutory těchto kopů, nechápu jejich odvahu si k tomu stoupnout. To provedení, kdy je patnáctá řada na tribuně dost často to první, co trefí...

**Máš coby nejlepší střelec historie mezi těmi góly nějaký památný? Ať už kvůli kráse nebo důležitosti.**

**PK:** Jeden památný mám, ale ten si pamatuji proto, že v ten den, a byla to neděle 13., se mi narodila první dcera, takže na to se opravdu zapomenout nedá, s tím asi souhlasíte. Ten den jsem dal gól, nebo spíš čudlu, z otočky z malého vápna. Hrálo se v Litomyšli proti Světlé nad Sázavou a byl to gól vítězný, vyhráli jsme 1:0.

**Z jaké největší dálky jsi skóroval?**

**PK:** To si myslím, že byly dva. A to v Kunčině z trestného kopu z půli hřiště. I když jak víte půlka hřiště v Kunčině a půlka hřiště v Barceloně se nedá srovnávat. A potom se to opakovalo ještě ve Srchu nebo Stolanech.

**A měl jsi někdy dlouhou dobu sucha, která tě znervóznila?**

**PK:** Ne, piji velmi pravidelně, proto jsem nervózní nebyl. A jestli ses ptal na gólový půst, tak je odpověď stejná.

**Bobši, ty jsi během té postupové sezóny a prvních krajských taky prolomil svoje dosavadní skromné příspěvky, začal jsi hrát jiný post nebo co bylo příčinou?**

**JK:** Zmiňovaný Eda Portlík měl hodně fyzické tréninky, jak říkal Bufet. A to mně nevadilo, jestli mě na to konto posunul do zálohy, protože viděl, že to s míčem oběhám, to nevím, ale je to možný. Já jsem do té doby hrál pořád stopera, a pak jsem přešel na štíta, když nebyli leváci, tak na levou zálohu, občas napravo. A s tím posunem přišly i góly. Já jsem se v té postupové sezóně gól po gólu přetahoval s Jirkou Vosmekem o nejlepšího střelce. Ale Jirku, nebo Fuču, Portlík nevystřídal ani když měli blbej den. Já vím, že jsem jednou zkazil přihrávku v 5., 10. minutě a šel jsem dolů.

**To tě vystřídal v 10. minutě?**

**JK:** Ne, tak v 5. A myslím, že Jirka otočil ten zápas na 2:1 a o ty góly vyhrál střelce.

**A jakou část těch gólů si dal hlavou?**

**JK:** Většinu, tak dvě třetiny určitě. Já jsem jako žák nikdy nechápal, jak dobře hrajou hlavou Ropkovi, Olda a Fanda, a říkal jsem si, že to bych chtěl taky umět. Tak jsem se učil a učil, i s medicinbalem. Poušťákovi, to už chytal za Cerekvici, jsem dal jednou hlavou jesle. Já mám vždycky rád gól, co brankáře trochu zesměšní. Soupeřova brankáře samozřejmě.  

**Ta zbylá třetina byla předpokládám z penalt. Zahodil si někdy nějakou důležitou?**

**JK:** Důležitou asi ne, nevím o tom. Pamatuju si jednu zahozenou, ještě za Ládi Tobka jsem kopal proti vagónu. Tenkrát jsem viděl na nějakým MS Brazilce, jak to z kroku dal do šíby, na tréninku mi to vyšlo, ale v mistráku to šlo metr nad bránu. Od té doby chodím klidně až za vápno, soupeři do mě rejpou: „si jdi až na půlku ne“, ale to mě je jedno.

**Chodil jsi proto, že sis věřil, nebo se nikomu nechtělo? Třeba na poslední penaltu v rozstřelu.**

**JK:** Věřil jsem si. Vím, kam to chci kopnout, 99% po mý pravý ruce a podle toho, co stojí v bráně nahoru nebo dolů. Když jsem měl jít kopat v rozstřelu, tak vždycky poslední. Vím, že třeba v Žamberku jsme potřebovali vyhrát, dal jsem penaltu v 93.min. Ještě když Vojta Štěpán neuměl levou, tak jsme kopali a střídali se v bráně třeba dvě hodiny.

**Zajímavé, že dneska přenecháváte penalty jiným oba dva. Petře, ty jsi kopal penalty v 90. letech a pak nic. Přestal sis věřit po nějaké neproměněné nebo prostě byli spolehlivější exekutoři jako Luboš Vosmek nebo Bobeš?**

**PK:** Tady sis asi odpověděl sám. Josef byl v tomto směru prakticky neomylný, já nějakou neproměnil, tak jsem si řekl, že moudřejší ustoupí.

**V sezóně 2001/2002 a té historické 2003/2004 si byl nejlepším střelcem ty, Mary. Nebyly to žádné velké hody, ale lepší nikdo nebyl. Jak jsi k tomu přišel?**

**MR:** V té době jsem hrával hlavně štíta. Něco bylo střelou z dálky, něco hlavou. Ale já jsem dal hlavou tak polovinu všech svých gólů, Kuťákovy centry bylo vždycky snadné uklízet. Ale tohle prostě bylo období, kdy mi to padalo samo, já jsem v té sezóně dal gól snad v sedmi zápasech po sobě *(v 6 zápasech ze 7 navazujících - pozn. redakce)*. Ještě teď si přesně vybavuju první gól z té série, který jsem vstřelil doma proti Ústí B střelou z dálky nad vystoupivšího brankáře, myslím, že jsem stvrzoval výsledek na tři nebo čtyři jedna. Tam začala moje nejlepší série a prostě když se daří, tak to tam padá samo, myslím, že v té sérii je i jeden gól v derby v Litomyšli, ještě teď si vybavuju gól v domácím prostředí Chocni B, kdy v závěru za nerozhodného stavu mě Kuťák našel na zadní tyči a já asi z jednoho metru bránu netrefil, ale ještě během posledních minut jsme měli další standardku, a tam jsem už Kuťákův centr proměnil. A vzpomínám si i na gól vstřelený v domácím zápase do sítě na konci sezóny postupující Tesle Pardubice, kdy jsem ve výskoku byl výš než hostující brankář, ten rozhodl o našem vítězství 1:0. No a Pardubice jsou teď až v lize, tenkrát za ně proti nám nastupoval nynější trenér FC Slovácka Martin Svědík.

***A víc toho dnes nestihneme, musíme chystat na Štěpánskou. Tak si ji tu zítra zhodnotíme. A nejen ji.***