---
title: Neprůstřelná Marťa
date: 2020-09-30T12:49:28.890Z
category: pripravka
image: /public/images/uploads/logo.png
---
### Starší přípravka

Na minulém turnaji v Březové jsme si pohrávali s myšlenkou, že na domácím turnaji postavíme dva týmy, nakonec se nám sešlo 6 omluvenek a bylo po myšlenkách. Turnaj byl však nakonec 4 týmový, protože k nám přijeli sousední Sebranice, kterým byl zrušen turnaj v Březové. 

**Sestava:** Martina Famfulíková, Jan Kvapil, Šimon Flídr, Štěpán Řejha, Kryštof Tomšíček, Sára Vomočilová, Daniel Válek, František Flídr, Matyáš Bulva

**HÚ/Morašice – Němčice 8 : 0**

I na domácím hřišti jsme pokračovali v dobrých podzimních výkonech, k vidění byly pěkné nahrávky, akce a pochopitelně i góly.

branky: Daniel Válek 4, František Flídr 2, Šimon Flídr, Kryštof Tomšíček

**HÚ/Morašice – Městečko Trnávka 10 : 0**

S tímto soupeřem jsme letos hráli poprvé a tak nám úplně nebyly známé jejich kvality. Kluci však předvedli standartní výkon a bez problémů jsme dokráčeli k vítězství. 

branky: Daniel Válek 3, Štěpán Řejha 2, Matyáš Bulva, Jan Kvapil, Martina Famfulíková, František Flídr, Šimon Flídr

**HÚ/Morašice – Sebranice 3 : 0** 

V tomto zápase se hrálo o vítězství v turnaji a na kvalitě hry to bylo patrné. Oba týmy hrály s patřičným nasazením a důrazem, šance se střídaly na obou stranách a my můžeme děkovat Martině Famfulíkové, neboť nás v rozhodujících momentech podržela, což je u vyrovnaných zápasů důležité. Kluci postupně ukázali svojí momentální kvalitu a po několika vyložených a neproměněných šancích se prosadili. Bez ohledu na výsledek si pochvalu zaslouží oba týmy a je třeba více takovýchto zápasů. Za zmínku jistě stojí čisté konto Martiny Famfulíkové v celém turnaji. Každopádně chválíme celý tým za kolektivní pojetí a přístup k jednotlivým zápasům.

branky: František Flídr, Šimon Flídr, Daniel Válek

autor: František Bartoš

### Mladší přípravka

**Sestava:** Matyáš Lněnička – David Klusoň, Samuel Vomočil, David Boček, Jaroslav Chaun, Štěpán Kovář, Štěpánka Gábrlíková, Antonín Krejsa

**Borová – Horní Újezd/Morašice 2:6 (0:4)**

Od začátku se hrálo především před brankou domácího týmu, ale v útoku jsme nepodali dobrý výkon. Při zakončení jsme často byli zbrklí a volili špatná řešení. V zápase dostali více prostoru mladší hráči. Obě branky nám vstřelil v druhé půli nejlepší hráč domácích – brankař – po samostatných akcích.

branky: David Boček 3, Jaroslav Chaun, David Klusoň, Samuel Vomočil

**Opatov – Horní Újezd/Morašice 7:0 (4:0)**

Proti nejsilnějšímu soupeři jsme narukovali s náročným běhavým fotbalem, ale ani to na fotbalovost soupeře nestačilo. Po hřišti jsme se proběhli dost, ale převážně bez balónu. Naše hráče je potřeba pochválit za snahu, ale Opatov je letos někde jinde.

**Tabulka turnaje:**

1.Opatov (6 b.)

2.Horní Újezd/Morašice (3 b.)

3.Borová (0 b.)

autor: Mgr. Zdeněk Beneš