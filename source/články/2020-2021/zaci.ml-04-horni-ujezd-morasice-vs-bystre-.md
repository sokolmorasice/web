---
title: Došly síly
date: 2020-09-20
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: "Horní Újezd/Morašice vs Bystré "
sestava: Matěj Kopecký – Ondřej Kusý, Ondřej Žďára, Lukáš Mach, Jindřich Mičík,
  Filip Střasák, Jan Kopecký, Vojtěch Paťava, Filip Flídr
goly: Ondřej Žďára, Lukáš Mach, Filip Flídr
zlute: null
cervene: null
multimedia: http://www.tjhorniujezd.cz/?p=13789
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2020/Horní
  Újezd/Morašice/25af5cb0-882d-48f8-8019-426f9d97f1b1.html
---
Čtvrtý zápas sezony jsme odehráli v domácím prostředí proti zatím neporaženému týmu z Bystrého.

První branka utkání padla až v 11. minutě, kdy Matěj Kopecký odkopával míč z hranice vápna a trefil dobíhajícího hráče soupeře do hlavy, od nějž se odrazil míč do naší brány. Vyrovnání jsme se dočkali o tři minuty později, kdy Ondra Žďára napřáhl téměř z poloviny hřiště a míč proklouzl brankaři mezi nohama do branky. Ve 24. minutě jsme se dostali do vedení, kdy jsme přečíslili soupeře a Lukáš Mach upravil skóre na 2:1. Do konce první půle se již nic zásadního nestalo a odcházeli jsme do kabin se zaslouženým vedením.

Do druhého poločasu jsme vstoupili podobně jako jsme ukončili první a v 39. minutě jsme se dostali do dvou gólového vedení kdy střelu Lukáše Macha dorazil Filip Flídr. Poté nám již docházely síly a soupeř nás začal pomalu přehrávat a ve 49. minutě snížil na 3:2. Od té doby byl více méně nebezpečný jen soupeř, který v 54. minutě vyrovnal. Od té chvíle jsme pouze doufali, že udržíme remízový stav, ale to se nám nepovedlo, když šel v 60. minutě do vedení 4:3 a následně v 61. minutě zvýšil na konečných 5:3.

Autor: Tomáš Chadima