---
title: I to se stane
date: 2020-09-12
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Březová n. S. vs Horní Újezd/Morašice
sestava: Matěj Kopecký – Ondřej Kusý, Ondřej Žďára, Lukáš Mach, Jindřich Mičík,
  Filip Střasák, Jan Kopecký, Martin Flídr, Filip Flídr
goly: null
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2020/Horní
  Újezd/Morašice/9c6013d4-bc98-434e-9ed5-4260d6646033.html
---
K třetímu zápasu sezony jsme vyrazili do Březové na Svitavou. Hned první problémy se ukázaly před odjezdem na zápas, kdy nás místo plánovaných jedenácti hráčů bylo pouze sedm. Naštěstí jsme nakonec k utkání vyrazili v devíti.

Již před zápasem jsme věděli, že náš čeká těžký soupeř, což se ukázalo hned od prvních minut, kdy nás hráči Březové vůbec nepustili na jejich půlku. První gól jsme inkasovali ve 4. minutě a další góly následovaly v 9., 14., 15. a 21. minutě. Teprve v 27. minutě se nám povedlo dostat před soupeřovu branku, kdy jsme rozehrávali několik rohových kopů za sebou, ale bohužel se nám ani jedna šance nepovedla proměnit, takže se šlo o poločase do kabin za stavu 5:0.

Druhý poločas jsme začali aktivně, kdy jsme byli prvních pár minut na půli soupeře, ale bohužel jsme se do žádné vyložené šance nedostali. Poté převzal otěže zápasu do svých rukou opět soupeř a od 34. minuty následovaly další góly do naší brány. My jsme se dostávali k brance domácích jen výjimečně, ale žádné vyložené šance jsme neměli. Skóre se nakonec zastavilo „jen“ na výsledku 12:0, za což můžeme především poděkovat Matěji Kopeckému, který v brance předváděl výborné zákroky a pochytal spoustu vyložených šancí soupeře.

Příští zápas hrajeme v neděli 20. září od 10 hodin v Horním Újezdě proti Bystrému.

autor: Tomáš Chadima