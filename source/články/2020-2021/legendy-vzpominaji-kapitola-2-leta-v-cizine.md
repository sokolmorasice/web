---
title: "Legendy vzpomínají - kapitola 2: Léta v cizině"
date: 2020-12-24T08:14:36.643Z
category: obecne
image: /public/images/uploads/logo.png
---
**Mary, co tě vedlo k tomu, že sis v dorostu ke konci podzimů odskakoval? Jednou do Újezda, jednou do Litomyšle.**

**MR:** Za litomyšlský dorost, kam si mě stáhl bratr trenéra Edy Portlíka, si vybavuji jeden odehraný přípravný zápas. Myslím, že jsme to utkání prohráli asi 1:4 a pískal ho morašický rozhodčí Tomáš Andrle. Ten nařídil penaltu po faulu na mě a já sám jsem ji proměnil. Zápas se mi tehdy hodně povedl a myslím, že byl o mě v Litomyšli velký zájem. V Dolním Újezdě, kam mě to táhlo už tehdy, jsem nastoupil na několik zápasů na konci podzimní části v době, kdy jsme v Morašicích měli už po sezóně, neboť v naší soutěži bylo méně účastníků. A tam si naopak nevybavuji si, že bych tehdy v Újezdě někoho svými výkony oslnil.

**Petře, v Dolňáku, ale už za chlapy, jsi na jaře 95 hostoval i ty, a celý následující ročník jsi strávil v Litomyšli. Co bylo tvým důvodem ke změně působiště?**

**PK:** Na Dolňák jsem šel jednak za bratrem a jednak si zkusit vyšší soutěž, již v té době finančně ohodnocenou. A do Litomyšle mě zlanařili, protože hledali levonohého záložníka. Byla to zase ještě vyšší soutěž, A třída východních Čech. A slíbili mi byt. Na to období vzpomínám jen v dobrém. Na trenéry, na hráče i na servis. Trenér Pavel Daněk mě naučil disciplíně a dostal do mě, že fyzička je základ. Nebyl vůbec problém běhat 4x týdně 15 km a teprve potom jít trénovat s míčem. Byli tam hráči, kteří prvoligové zkušenosti měli nebo později nasbírali. A servisu, který jsme v té době dostávali - osobní stravné, peníze za zápasy, prémie za vítězství, večeře v hotelu, atd. - toho jsem se po zbytek své hráčské kariéry v Morašicích nedočkal.

**A jak se pak stalo, že stavař se srdcem v Morašicích a slíbeným bytem v Litomyšli šel do bytu v Mejtě?**

**PK:** Stavař si našel nevěstu v Mejtě, nebo spíš ona jeho. A protože to zrovna byla podnikatelsky obrovsky hektická doba, 13-14 hodin denně v práci, včetně sobot a někdy i nedělí, neměl stavař vůbec chuť něco stavět ještě doma. A tak koupil v Mejtě byt.

**Lanaření z Mejta neprobíhalo?**

**PK:** Lanaření samozřejmě probíhalo, a to již na stavební průmyslovce ve VM, pak pokračovalo a vlastně pokračuje dodnes.

**Bobši, ty jsi zakotvil pro změnu v Litomyšli, za tím bylo co?**

**JK:** Že v Morašicích nic nebylo a v Litomyšli jsem měl stejně práci.

**A nějakou udičku jsi od místních nedostal?**

**JK:** Jeden čas mě chlapi z Litomyšle oťukávali, i když to nemělo cenu. Vybavuju si, když k nám ještě v dorostu přijelo k přáteláku Mejto a já jim dal dva góly, tak se ptali. Ale jenom než zjistili, že už mi je 18. A stejně bych nešel.

**Když jsme u těch morašických exodů, jak jsi Mary ty přišel k bydlišti u hlavního rivala? Láska k pozemku na první pohled nebo měl tolik praktických výhod, že jsi akceptoval vzdálení se z Morašic?**

**MR:** Tak já jsem nejprve bydlel na Desné, kde jsme měli byt, tam se nám narodilo první dítě. V Morašicích v té době žádné stavební pozemky nebyly, bohužel z toho důvodu mnoho mých vrstevníků postavilo své domy v okolních obcích. Pravda, jeden se usídlil až v Chicagu. A Maruška v té době dostala od rodičů pozemek v Újezdě, takže volba byla jasná.

***Jedním z hlavních témat zítřejší božihodové části budou góly.***