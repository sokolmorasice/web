---
title: Ježiši Kriste, von hraje taky Mates..
date: 2021-05-16T15:33:19.446Z
category: obecne
image: /public/images/uploads/logo.png
h1: Morašice - Jaroměřice 2:0
---
Když pomineme rozhodčí, tak nejhorší věcí na okrese je cestování za Hřebeč. A z cestování za Hřebeč je nejhorší cestování ke kvalitním soupeřům na Malou Hanou. A z cestování ke kvalitním soupeřům na Malou Hanou je nejhorší cestování do Jaroměřic. A nejhorší na tom není, že jsme tam nevyhráli na den nepřesně pět [let](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2015-2016/jarom%C4%9B%C5%99ice-vs-mora%C5%A1ice), ale že si člověk s tamními fanoušky ani nemůže rozumně popovídat. Ne jen o Nietschzem, ale fakt o ničem. O to hřejivější pocit je, že náš nebližší konkurent v tabulce k nám musí jezdit podobně nerad, protože poslední výhru jejich áčka u nás pamatujou jen strejci našeho trenéra. Je pravda, že od nás museli občas odjíždět s lepším herním [pocitem](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2015-2016/mora%C5%A1ice-vs-jarom%C4%9B%C5%99ice) než my od nich, ale o to naštvanější. Takovou frustraci jsme jim tentokrát nehodlali dopřát a udeřili jsme rychlým [gólem](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2016-2017/mora%C5%A1ice-vs-jarom%C4%9B%C5%99ice), když se po Skalově pasu za tradičně děravou soupeřovou obranou zjevil po statistických jubileích toužící navrátilec M. Štancl a nerozhýbaného brankáře obhodil jedním ze svých typických [triků](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/dlouh%C3%A1-lou%C4%8Dka-vs-mora%C5%A1ice) - neumístěnou střelou těsně vedle dřevěné nohy. Hlavní pozornost naší defenzivy byla samozřejmě zaměřena na [pastýře](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/jaromerice_semifinale) hostujícího stáda, okresního střeleckého démona pověstného tím, že udělá z hovna čokoládu - Daniela Švece. Ale protože si ho pravidelně předávají k hlídání naši nejlepší psi obranáři a letos na jaře má málokdo z našeho týmu slabou chvilku, byl Špinar v naší bráně zaměstnán jen jednou a někým jiným, se střelou do horního rohu si ale výborně poradil. To M. Štancl byl o poznání efektivnější, když se před pauzou po rychle sehraném autu chvilku motal s míčem soupeřovým vápnem a skrytou [plašmuškou](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/mora%C5%A1ice-vs-jarom%C4%9B%C5%99ice) zvýšil naše vedení. 

Bohužel dřív než stihl po návratu z kabin přidat třetí gól, stihl si potřetí (minimálně) vyhodit [koleno](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2018-2019/jaromerice-vs-morasice) a kdo ví, jestli ten gól nebyl jeho poslední. Výsledek 2:0 jsme si nějak vnitřně vyhodnotili jako takový ideální pro to, abychom soupeře udrželi v naději na bodový zisk, protože dát víc gólů, tak je ten výsledek ani nezamrzí, a pustit si je na dostřel, to by zas bylo moc o [nervy](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2018-2019/morasice-vs-jaromerice). A tak jsme svědomitě bránili soupeřovy akce, ještě svědomitěji zahazovali naše šance, ale výsledek se nezměnil, a tak je na tabulku i po utkání s nejbližším pronásledovatelem pěkný pohled (když se pomine grafická úprava). A shodou náhod se nově na druhé místo posunul náš příští soupeř Janov. Uvidíme, co s další bitvou o čelo udělá plánované otevření zahrádek (nezapomeňte, že mohou otevřít už v pondělí v 6:00).

**Další výsledky 18.kola:**

Hradec nad Svitavou - Opatov 1:5 (1:3)

Borová - Janov 1:3 (0:1)

Jevíčko - Mladějov 4:0 (2:0)

Radiměř - Boršov 6:2 (3:2)

Cerekvice - Horní Újezd 2:0 (1:0)

Linhartice - Dlouhá Loučka 1:1 (1:1)

**Tabulka okresního přeboru po 18.kole:**



|     |                     |     |        |       |     |
| --- | ------------------- | --- | ------ | ----- | --- |
| 1.  | Morašice            | 18  | 14-2-2 | 52:15 | 44  |
| 2.  | Janov               | 18  | 13-1-4 | 46:30 | 40  |
| 3.  | Jaroměřice          | 18  | 12-2-4 | 45:22 | 38  |
| 4.  | Jevíčko             | 18  | 11-5-2 | 30:18 | 38  |
| 5.  | Opatov              | 18  | 9-1-8  | 60:34 | 28  |
| 6.  | Cerekvice           | 18  | 7-6-5  | 34:37 | 27  |
| 7.  | Mladějov            | 18  | 7-4-7  | 47:38 | 25  |
| 8.  | Radiměř             | 18  | 7-4-7  | 40:39 | 25  |
| 9.  | Dlouhá Loučka       | 18  | 5-5-8  | 31:36 | 20  |
| 10. | Borová              | 18  | 5-4-9  | 37:46 | 19  |
| 11. | Horní Újezd         | 18  | 5-2-11 | 26:48 | 17  |
| 12. | Linhartice          | 18  | 3-7-8  | 29:36 | 16  |
| 13. | Boršov              | 18  | 1-6-11 | 22:52 | 9   |
| 14. | Hradec nad Svitavou | 18  | 2-3-13 | 19:65 | 9   |