---
title: Chcíp pes
date: 2021-02-12T17:21:53.052Z
category: obecne
image: /public/images/uploads/logo.png
---
Novoroční agónie jakoby padla i na stránky našeho fotbalového oddílu, které mají kolikrát o čem psát, i když se nic neděje. Ale prostě jen není tolik fantazie, aby se vydavatelsky pokryla každá další vlna. Takže co je u nás nového?

Skoro nic, dalo by se říct, že u nás chcíp pes.

V mládeži máme neuvěřitelnou tréninkovou morálku, za celý leden naši trenéři nezaznamenali jedinou neomluvenou neúčast. 

Protože děti mají distanční docházky až až a muži chodí do práce (většinou), probíhá distanční formou na revanš zimní příprava áčka. Sice nikdo neví, na co a kdy se připravovat, ale naběhané kilometry i sesbírané fotky hlavně za začátku přípravy předčily očekávání. Minulý víkend se, až na rodinné výjimky, nekonalo tradiční soustředění v Daňkovicích, takže Mary nepřeháněl dávky, Vojta nikomu na silnicích neukázal záda, Kléma nestěhoval postele, kapitán nehulákal a celkově se nikdo společensky neunavil. Opět až na rodinné výjimky.

V mezidobí byl čas požádat o [dotace](https://www.seznamzpravy.cz/clanek/dotace-na-sport-nevyrizuje-hnilickova-agentura-ale-firma-najata-za-miliony-140883) a vzápětí se ujistit, že jejich rozdělování je ve správných [rukou](https://neovlivni.cz/svedek-hnilickova-namestkyne-uz-jeden-tym-rozlozila/).

Našeho pokladníka čeká placení členských příspěvků na účet Fotbalové asociace České republiky, což je sice konzervativní fond s jistým [výnosem](https://www.seznamzpravy.cz/clanek/v-kauze-berbr-se-resi-zpronevera-penez-plzenskeho-krajskeho-svazu-125105) , ale na [dezinfekci](https://www.seznamzpravy.cz/clanek/fotbalova-asociace-platila-miliony-za-dezinfekci-kabin-mnohdy-vylhanou-134151) kabin to vydělá málokomu. Alespoň do doby, než začne [Fevolucí](https://fevoluce.cz/) vzývaná změna odspodu. Stojatým vodám vyhovující odklady těchto "spodních" voleb zkusí OFS Svitavy rozčeřit videovolbou svého nového vedení, snad se do plánovaného termínu 2.3. podaří vyřešit všechna úskalí, která tento způsob komunikace v okresních podmínkách obnáší.

No a protože se neví hodiny, dne ani právního rámce, kdy konečně bude líp, vězte, že naše áčko má pro sichr domluvený následující program:

21.2. 10:00 přátelák s Čistou na UMT v Litomyšli

6.3. 16:30 přátelák se Sebranicemi na UMT v Litomyšli

14.3. 14:00 první mistrovské utkání s Radiměří na UMT v Litomyšli

A pak už se bude hrát každý týden, někdy i dvakrát, až do prázdnin. 

Nebo se spíš koncem května začnou dohánět podzimní resty, aby se dohrálo aspoň jedno kolo vzájemných zápasů a mohlo se postupovat a sestupovat? 

Na to by to chtělo mít křišťálovou kouli. Nebo někoho, kdo ji používá a dá se mu věřit.