---
title: Hattrick sem, hattrick tam
date: 2020-08-30
category: dorost
image: /public/images/novinky/2019-2020/dorost.png
h1: Horní Újezd/Morašice vs Semanín
sestava: Emanuel Hanyk – Marek Vích, Pavel Beneš, Jakub Nádvorník, Radovan
  Flídr, Martin Tmej, Jan Vedral, Jan Pechanec, Šimon Lněnička, Jakub Kvapil
goly: Jan Pechanec 3
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533C1A/2020/Horní
  Újezd/Morašice/3cd7eee0-dd42-41f0-a9b2-065de18a556d.html
---
Po složité, až nijaké přípravě, vstoupilo do sezóny i družstvo dorostu. Kvůli malému počtu jsme nakonec přihlásili jen 8+1, s malými bránami a hokejovým střídáním. Bohužel hned první zápas ukázal, že ani tento formát nebude jednoduché lidsky pokrýt, a tak museli zaskakovat i žáci. První ze šesti vzájemných zápasů proti Semanínu začal rychlým gólem Pechance a přes hostující převahu a zastřelování se podařilo držet vedení celý poločas, na jehož závěru hosté srovnali z nešťastné penalty.

Pechanec bleskově skóroval i ve druhém poločase, po necelé hodině hry dovršil hattrick, ale hosté dvěma rychlými góly srovnali a v závěrečné čtvrthodině se i díky dvěma hattrickářům ve svých řadách střelecky utrhli ze řetězu a nakonec si odvezli přesvědčivou výhru.