---
title: Poločas od výbuchu
date: 2021-04-25T18:47:24.512Z
category: muzi
image: /public/images/uploads/logo.png
h1: Hradec nad Svitavou - Morašice 0:5
---
Píše nám spousta lidí, že by si rádi zahráli fotbal, tak jsme se jim rozhodli vyhovět. Ze sestavy tak byl zajímavý slepenec, což bylo znát. Do brány se po letech přišel provětrat Vojta Andrle. Sledoval, jak domácí [křičí](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2016-2017/hradec-nad-svit-vs-mora%C5%A1ice), jak nás platonicky tlačí, ale protože ho pořádně prověřili jen jednou a na náš podceňující přístup se špatně koukalo, rozhodl se v poločasové přestávce oznámit, že tohle mu vlastně nechybělo a že jede domů. A dnešní oslavenec Petr Kuta, těšiv se na půdě posledního na pohodové špílmachrování, odjel zklamán s ním.

Kromě změn v poli, které nás měli trochu probudit, jsme tedy poslali do brány i Špinara. A účinek se [dostavil](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2019-2020/muzi-05-hradec-nad-svit-vs-morasice). Po pár minutách proměnily sóla další z davu vysloužilých žadatelů Burián a Vopařil a zápas nabral očekávaný směr. Když pak v krátkém sledu Chmelíkovu dalekonosnou střelu na několikrát dorazil Válek, po rohu se prosadil Ropek a odražený míč napálil zpoza vápna Nádvorník, zatoužil po gólu i nudící se Špinar. Ale vytoužený přesun do útoku našemu gólmanovi slávu nepřinesl, vytoužený přesun do brány našemu [kapitánovi](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/hradec-nad-svitavou-vs-mora%C5%A1ice) taky nesplnil očekávání, protože se vlastně už nic zajímavého nestalo. Jen se ukázalo, že ať chytá v Hradci kdokoliv, góla tam prostě nedostaneme.

**Další výsledky 15.kola:**

Cerekvice - Opatov 2:1 (1:1)

Jevíčko - Janov 1:1 (1:0)

Boršov - Mladějov 1:1 (0:0)

Borová - Horní Újezd 4:1 (1:1)

Linhartice - Jaroměřice 1:1 (0:1)

Radiměř - Dlouhá Loučka 4:2 (3:1)

**Tabulka okresního přeboru po 15.kole:**



|     |                     |     |        |       |     |
| --- | ------------------- | --- | ------ | ----- | --- |
| 1.  | Morašice            | 15  | 12-1-2 | 48:15 | 37  |
| 2.  | Janov               | 15  | 11-1-3 | 40:21 | 34  |
| 3.  | Jaroměřice          | 15  | 10-2-3 | 36:18 | 32  |
| 4.  | Jevíčko             | 15  | 9-5-1  | 24:16 | 32  |
| 5.  | Opatov              | 15  | 7-1-7  | 50:30 | 22  |
| 6.  | Mladějov            | 15  | 6-3-6  | 40:34 | 21  |
| 7.  | Radiměř             | 15  | 6-3-6  | 34:35 | 21  |
| 8.  | Cerekvice           | 15  | 4-6-5  | 28:37 | 18  |
| 9.  | Borová              | 15  | 5-3-7  | 34:39 | 18  |
| 10. | Dlouhá Loučka       | 15  | 4-4-7  | 28:32 | 16  |
| 11. | Linhartice          | 15  | 3-6-6  | 27:31 | 15  |
| 12. | Horní Újezd         | 15  | 4-2-9  | 24:44 | 14  |
| 13. | Boršov              | 15  | 1-6-8  | 19:37 | 9   |
| 14. | Hradec nad Svitavou | 15  | 1-3-11 | 16:57 | 6   |