---
title: Létající Václav
date: 2021-04-18T15:31:33.562Z
category: obecne
image: /public/images/uploads/logo.png
h1: Cerekvice - Morašice 1:2 (1:1)
---
S naším nejčastějším soupeřem je to jako na houpačce. Historicky máme s Cerekvicí lepší bilanci, ale naše poslední vítězství na jejich hřišti bylo tehdejší skoro [senzací](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2014-2015/cerekvice-nad-lou%C4%8Dnou-vs-mora%C5%A1ice). Nejnepříjemnější vzpomínky z [posledního](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2018-2019/cerekvice-vs-morasice) klání u Loučné má kromě Válkem uraženého sudího Novák, který dnes začal na lavičce. A z ní sledoval, jak si domácí hrající trenér Vomáčka hlasitým výkřikem vybojoval hned v první minutě přímák, který s bravurou sobě vlastní proměnil Lorenc. Do hry se nás pokusil dostat špatnou rozehrávkou V. Vostrčil, ale Skalovi gól z dálky není přán a již po několikáté v kariéře vrátil soupeřovu gólmanovi míč do břevna. Přestože derby maže spoustu rozdílů, vzhledem k letošnímu rozložení sil jsme si věřili na víc, než první jarní prohru, a k žádoucímu vývoji zápasu nás nasměroval Novák, kterému nuceně přepustil místo na trávníku prozíravě zraněný Vopařil. Ten by se určitě nebál jít do skluzu, ale je možné, že by místo Tmějova centru trefil nějakého protihráče. Novák [doklouzal](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/finale-poharu) po zemi přesně do místa, kde spadl míč a šikovně nebo šťastně ho poslal na vzdálenější tyč.

Náš střelec vlétl i do druhé půle ve velkém stylu. Zatímco jeho [letu](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2020-2021/muzi-06-morasice-vs-janov) hřištěm chybělo klidné zakončení a jeho letu vzduchem kousek štěstíčka, jeho náletu do protihráče nechybělo nic, včetně jeho oblíbené žluté karty. Gólu se paradoxně dočkal takového [ošklivého,](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/mestecko-trnavka-vs-mora%C5%A1ice) soupeřem ztečovaného, ale obdarovaným koňům se zkrátka na zuby koukat nemá, nebo tak nějak. Což platilo i o dalším souboji, po kterém byla našemu zlému muži ukázaná už druhá žlutá, ale protože si to rozhodčí asi zapsal do špatnýho notýsku a červená nepřišla, zůstal Novák tiše v oběhu. Domácí se bodu dotýkali až v závěru, po trmě vrmě ve vápně zakončoval třeba exnáš Částek, ale při Špinarovi stáli všichni svatí s [Václavem](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/cerekvice-nad-loucnou-vs-mora%C5%A1ice) v čele a míč do branky nedoputoval.

**Další zápasy 14.kola:**

Janov - Radiměř 4:1 (2:1)

Opatov - Mladějov 2:3 (0:0)

Dlouhá Loučka - Boršov 1:1 (1:1)

Hradec nad Svitavou - Borová 0:4 (0:2)

Jaroměřice - Jevíčko 0:1 (0:0)

Horní Újezd - Linhartice 3:1 (1:1)

**Tabulka okresního přeboru po 14.kole:**

|     |                     |     |        |       |     |
| --- | ------------------- | --- | ------ | ----- | --- |
| 1.  | Morašice            | 14  | 11-1-2 | 43:15 | 34  |
| 2.  | Janov               | 14  | 11-0-3 | 39:20 | 33  |
| 3.  | Jaroměřice          | 14  | 10-1-3 | 35:17 | 31  |
| 4.  | Jevíčko             | 14  | 9-4-1  | 23:15 | 31  |
| 5.  | Opatov              | 14  | 7-1-6  | 49:28 | 22  |
| 6.  | Mladějov            | 14  | 6-2-6  | 39:33 | 20  |
| 7.  | Radiměř             | 14  | 5-3-6  | 30:33 | 18  |
| 8.  | Dlouhá Loučka       | 14  | 4-4-6  | 26:28 | 16  |
| 9.  | Cerekvice           | 14  | 3-6-5  | 26:36 | 15  |
| 10. | Borová              | 14  | 4-3-7  | 30:38 | 15  |
| 11. | Linhartice          | 14  | 3-5-6  | 26:30 | 14  |
| 12. | Horní Újezd         | 14  | 4-2-8  | 23:40 | 14  |
| 13. | Boršov              | 14  | 1-5-8  | 18:36 | 8   |
| 14. | Hradec nad Svitavou | 14  | 1-3-10 | 16:52 | 6   |