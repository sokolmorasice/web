---
title: Žádný strom neroste do nebe
date: 2020-10-06T06:02:59.587Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Starší přípravka

Sestava: Martina Famfulíková, Jan Kvapil, Vojtěch Huryta, Daniel Válek, František Flídr, Matyáš Bulva, Kryštof Tomšíček, Štěpán Řejha, Matouš Zach, Vojtěch Štěpán, Michal Kovář a Matěj Kovář.

**HÚ/Morašice – Radiměř/ Hradec nad Svitavou 2 : 3**

V desátém zápase podzimní sezóny jsme okusili první porážku, a to zcela zaslouženě. Od všech hráčů to byl podprůměrný výkon, bez bojovnosti a nasazení a výsledkem tak byl poločas 0 : 3. Ve druhém jsme se trochu zlepšili, ale opravdu makat jsme začali až v posledních 5ti minutách, kdy jsme utkání zdramatizovali, ale na vyrovnání už nebyl čas, i když šance byly.

Branky: Jan Kvapil, Daniel Válek.

**HÚ/Morašice – Jevíčko 6 : 2**

Tento zápas už byl o poznání lepší, ale k podzimním výkonům měl ještě daleko, vytvořili jsme si brzy dostatečný náskok, který nakonec stačil na vítězství, které však bylo upracované a některé vstřelené branky i relativně šťastné.

Branky: Daniel Válek 5, Matouš Zach

**HÚ/Morašice – Městečko Trnávka 7 : 0**

Až v tomto zápase s domácím týmem jsme se vrátili k výkonům, na které jsme zvyklí a celý zápas jsme bez problémů ovládli. 

Branky: Daniel Válek 3, Vojtěch Huryta, František Flídr, Kryštof Tomšíček, Matyáš Bulva

V turnaji se hrály zápasy na 2 x 12 minut, což je velice málo, pokud se má prostřídat 12 hráčů, zejména potom ve vyrovnaných utkáních. Každopádně lze brát první porážku pozitivně, kluci si uvědomili, že nic není zadarmo a když nebudou makat, tak se dobré výsledky nedostaví.

autor: František Bartoš

## Mladší přípravka

**Sestava:** Matyáš Lněnička – David Klusoň, David Boček, Jaroslav Chaun, Štěpán Kovář, Štěpánka Gábrlíková, A. Krejsa, Dominik Prokop

**Horní Újezd/Morašice – Březová 12:1 (5:1)**

Zasloužené vysoké vítězství, první gól Tondy Krejsy.

Branky: Jaroslav Chaun 5, David Boček 4, David Klusoň 2, Antonín Krejsa

**Čistá – Horní Újezd/Morašice 6:4 (2:2)**

Bojovný zápas, který se zlomil ve druhé půli, tam nám trochu chyběly síly a soustředěnost, za stavu 3:3 jsme třikrát v krátké době inkasovali a bylo rozhodnuto, i tak pochvala za odmakaný turnaj.

Branky: Jaroslav Chaun 2, David Klusoň 2

**Tabulka turnaje:**

1.Čistá(6 b.)

2.Horní Újezd/Morašice (3 b.)

3.Březová(0 b.)

autor: Zdeněk Beneš