---
title: Nalijme si čistého vína
date: 2021-05-02T14:38:08.863Z
category: muzi
image: /public/images/uploads/logo.png
---
Odložené velikonoční kolo STK Svitavy nejprve automaticky přehodila na květnový svátek, ale nakonec ustoupila kritice ze strany fotbalových novinářů svitavského deníku, že psát o dvou kolech za jeden víkend se prostě nedá, takže nás čekal zápas v Borové, počítaný rovněž jako semifinále okresního poháru. Což divákům statisticky s velkou pravděpodobností mohlo naservírovat atraktivní penaltový rozstřel. Domácí se na telecím plácku oprávněně cítili více jako doma, takže z naší sestavy stojí za to zmínit hlavně Špinara s Ropkem. Náš gólman pomohl laxní obraně nejprve včasným výběhem a poté i zákrokem proti hlavičce V. Štancla. Náš stoper byl jediným v naší laxní jedenáctce, který dokázal mít i při roli favorita správně nastavenou hlavu. Tou se mu podařilo jednak odvrátit několik míčů za obranu a ze standartek, ale taky pomocí biliáru s tyčí a soupeřem otevřít skóre na druhé [straně.](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2019-2020/muzi-13-borova-vs-morasice) Štastný okamžik bohužel zamával i s tak zkušeným chlapem, nastavení hlavy se obrátilo a za chvíli se pokusil o vlastní gól stejně jako jeho kolega ze stoperské dvojice. Bohužel byl úspěšnější a bylo srovnáno.

Z unimobuňky jsme se sice odrazili k popřestávkové ofenzivě, ale ta byla více než půl hodiny jalová. Spasil nás až Ropek, který dokonal hattrick hlavou, když vrátil do sítě gólmanem vyražený [roh](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2015-2016/borov%C3%A1-telec%C3%AD-vs-mora%C5%A1ice). Nabyté vedení bohužel vrátilo do naší mysli mírně ležérní myšlení, které mělo za příčinu několik domácích palebných pozic. Špinar dělal, co mohl - chytal, skákal, nadával - ale všechno neodvratně směřovalo k průseru. Ten dorazil do našeho vinglu po zhruba pětadvacetimetrovém letu vzduchem a, světe div se, penaltový rozstřel byl na světě. Že se nám do tabulky přeboru započítal jenom bod, nepředstavovalo vzhledem k překvapivým výsledkům soupeřů zas takové neštěstí.

Jako první kopali domácí, ale Špinar se vrhl k pravé [tyči](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/k%C5%99enov-vs-mora%C5%A1ice), zalaškoval si s ní i s balónem a nabídl psychologický minibreak Chmelíkovi, který k téže tyči mířil neomylně. I v druhé sérii risknul náš gólman pravou stranu a opět s tím slavil [úspěch](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2020-2021/muzi-06-morasice-vs-janov). Svoje vynikajicí jméno na na stadionu v Telecím nepošpinil Ropek, který navázal navázal na svou proměněnou penaltu proti stejnému soupeři z roku [2015](https://svitavsky.denik.cz/fotbal_region/svatecni-fotbal-v-regionu-ii-20151029.html), o které bohužel kolují jen kusé záznamy a matné vzpomínky. V psychologické válce, že nás gólman přece nebude skákat pořád na stejnou stranu domácí hráč prohrál, protože Špinar se doprava úspěšně složil i [potřetí](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2020-2021/muzi-07-dlouha-loucka-vs-morasice). Náš gólman disponoval střeleckým sebevědomím už před rozstřelem, částečně měl [proč](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2019-2020/chmelik-stoupa-tabulkami) a byl původně hlášen na exekuci našeho třetího kopu. Všichni ví, že na rozhodující penaltu musíme takřka vždy ukecávat Kopeckého, který ji chce stále přenechávat mladším, ale stejně se nakonec na tu poslední nechá zapsat. Jenže v situaci, kdy jednak hrozilo, že nás matador o chvilku slávy přijde a ještě navíc hrozilo, že si ji místo něj užije náš gólman, šla skromnost stranou. Kopecký si vzal míč, odbyl Špinara i rozhodčího, že si pořadí exekutorů špatně zapamatovali, suverénně poslal gólmana na druhou [stranu](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2016-2017/pohar_borova) a nás do finále. Sice nevíme, jestli nás tam čeká Opatov nebo Jaroměřice, zato víme, že s Kopeckým můžeme počítat.

**Další výsledky 16.kola:**

Dlouhá Loučka - Opatov 2:1 (1:1)

Mladějov - Janov 7:0 (3:0)

Boršov - Jaroměřice 0:5 (0:3)

Radiměř - Horní Újezd 0:2 (0:2)

Linhartice - Cerekvice 0:1 (0:1)

Jevíčko - Hradec nad Svitavou 0:2 (0:1)

**Tabulka okresního přeboru po 16.kole:**



|     |                     |     |        |       |     |
| --- | ------------------- | --- | ------ | ----- | --- |
| 1.  | Morašice            | 16  | 12-2-2 | 48:15 | 38  |
| 2.  | Jaroměřice          | 16  | 11-2-3 | 41:18 | 35  |
| 3.  | Janov               | 16  | 11-1-4 | 40:28 | 34  |
| 4.  | Jevíčko             | 16  | 9-5-2  | 24:18 | 32  |
| 5.  | Mladějov            | 16  | 7-3-6  | 47:34 | 24  |
| 6.  | Opatov              | 16  | 7-1-8  | 51:32 | 22  |
| 7.  | Radiměř             | 16  | 6-3-7  | 34:37 | 21  |
| 8.  | Cerekvice           | 16  | 5-6-5  | 29:37 | 21  |
| 9.  | Dlouhá Loučka       | 16  | 5-4-7  | 30:33 | 19  |
| 10. | Borová              | 16  | 5-4-7  | 34:39 | 19  |
| 11. | Horní Újezd         | 16  | 5-2-9  | 26:44 | 17  |
| 12. | Linhartice          | 16  | 3-6-7  | 27:32 | 15  |
| 13. | Boršov              | 16  | 1-6-9  | 19:42 | 9   |
| 14. | Hradec nad Svitavou | 16  | 2-3-11 | 18:57 | 9   |