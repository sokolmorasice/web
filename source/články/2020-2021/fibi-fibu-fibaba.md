---
title: Fíbí, Fíbu, Fíbabá
date: 2021-03-14T17:30:06.320Z
category: muzi
image: /public/images/uploads/logo.png
h1: Morašice - Radiměř  4:0 (3:0)
---
V prvním jarním duelu jsme hostili Radiměř. Je pravdou, že u nich se nehraje [snadno](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/radim%C4%9B%C5%99-vs-mora%C5%A1ice) a že jsme se proti nim jejich první sezónu po postupu do přeboru [střelecky](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/mora%C5%A1ice-vs-radim%C4%9B%C5%99) trápili. A když už jsme u nich pohodlně [uspěli](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2018-2019/radimer-vs-morasice-pohar), uspokojilo nás to tak, že jsme se z toho hrabali až do konce [podzimu](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2018-2019/radimer-vs-morasice). Dnešní zápas byl pozitivně poznamenán naší poctivou, lednovou, tudíž i meziobecní, přípravou, kterou suchý únor nezvládl odbourat. Soupeře jsme předčili ve všem kromě reklamování ofsajdu a naše úsilí zhodnotil F. Bureš, který po samostatné akci z boku napálil míč pod víko na přední tyči a zaznamenal ve třetím zápase proti dnešnímu soupeři už čtvrtý gól. A nezůstalo jen u něj, když si počkal na nákop do domněle ofsajdového postavení. Samozřejmě, že měl špatné postavení zapomenutého soupeřova obránce měl pod kontrolou a ofsajdu se vyhnul zcela zkušeně, nikoli pouze náhodou. Své letité zkušenosti prokázal i při zakončení, proti placírce na dřevěnou nohu neměl gólman šanci. Chvíli před [pauzou](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2018-2019/morasice-vs-radimer) pak hlavou dokonal hattrick, když využil jak Chmelíkova sklepnutí, tak své neuvěřitelné síly v osobních soubojích. 

Po přestávce už to bylo laxnější, až profesorské, ale šancí bylo i tak dost. [Nádvorník](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2018-2019/morasice-vs-radimer) např. trefil z dálky tyč, Famfulíkem odmávaný ofsajd zastavil o stupidním nehlídání si ofsajdů kázajícího Skalu, a tak jediný gól druhé půle zaznamenal [Novák](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2019-2020/muzi-06-radimer-vs-morasice), který si hlavou našel Chmelíkův vracák do ohně a v souhře s ostatními výsledky nám pojistil druhé místo v tabulce.

**Další výsledky 10. kola okresního přeboru:** 

Linhartice - Opatov 0:5 (0:4)

Borová - Jevíčko 0:3 (0:2)

Cerekvice - Boršov 1:0 (0:0)

Hradec nad Svitavou - Mladějov 1:3 (0:2)

Horní Újezd - Dlouhá Loučka 2:0 (0:0)

Jaroměřice - Janov 3:1 (1:0)

Dohrávka 5. kola:

Cerekvice - Hradec n. Svitavou 3:1 (0:1)

**Tabulka okresního přeboru po 10 kolech:**

|     |                     |     |       |       |     |
| --- | ------------------- | --- | ----- | ----- | --- |
| 1.  | Jaroměřice          | 10  | 8-1-1 | 33:14 | 25  |
| 2.  | Morašice            | 10  | 8-0-2 | 32:11 | 24  |
| 3.  | Jevíčko             | 10  | 7-2-1 | 19:13 | 23  |
| 4.  | Janov               | 10  | 7-0-3 | 25:15 | 21  |
| 5.  | Opatov              | 10  | 6-1-3 | 41:18 | 19  |
| 6.  | Mladějov            | 10  | 4-1-5 | 29:27 | 13  |
| 7.  | Cerekvice           | 10  | 3-4-3 | 20:28 | 13  |
| 8.  | Dlouhá Loučka       | 10  | 3-3-4 | 21:18 | 12  |
| 9.  | Radiměř             | 10  | 3-3-4 | 21:26 | 12  |
| 10. | Borová              | 10  | 3-2-5 | 21:25 | 11  |
| 11. | Linhartice          | 10  | 2-4-4 | 16:22 | 10  |
| 12. | Horní Újezd         | 10  | 3-0-7 | 17:35 | 9   |
| 13. | Hradec nad Svitavou | 10  | 1-2-7 | 12:37 | 5   |
| 14. | Boršov              | 10  | 0-3-7 | 12:30 | 3   |