---
title: Těch desítek
date: 2020-09-15T05:52:00.066Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Starší přípravka

I k tomuto turnaji jsme odjížděli v počtu 13ti hráčů, avšak bez nemocného Fandy Flídra, kterého nahradil v útoku Matyáš Bulva a spolu s Dádou Válkem vytvořili velice údernou dvojici. V obou zápasech byly k vidění pěkné fotbalové akce, zakončené ještě hezčími góly a tak doufám, že v tomto trendu budeme pokračovat i v dalších zápasech.

**Sestava:** Martina Famfulíková, Jan Kvapil, Šimon Flídr, Vojta Huryta, Dan Válek, Matyáš Bulva, Štěpán Řejha, Sára Vomočilová, Josef Rosypal, Matouš Zach, Kryštof Tomšíček, Matěj Kovář a Michal Kovář.

<!--EndFragment-->

**HÚ/Morašice – Radiměř/Hradec nad Svitavou 10 : 2**

Tento zápas byl od začátku jednoznačnou záležitostí, a tak více prostoru dostávali Ti mladší hráči. 

Branky: Daniel Válek 3, Matyáš Bulva 2, Vojta Huryta 2, Matouš Zach 2, Šimon Flídr

**HÚ/Morašice – Jevíčko 10 : 2**

I když výsledek je totožný s prvním zápasem, utkání bylo podstatně vyrovnanější, soupeř byl fyzicky vyspělejší než náš tým a tak více prostoru dostávali Ti starší hráči. V rozhodujících momentech tým podržela skvělými zákroky Martina Famfulíková a tak jsme nakonec utkání dohrávali v poklidu.

Branky: Daniel Válek 5, Matyáš Bulva 4, Matouš Zach

autor: František Bartoš

## Mladší přípravka

Druhé kolo okresní soutěže mladší přípravky jsme hostili na domácím hřišti. Utkali jsme se s Němčicemi a Opatovem, oba týmy své úvodní turnaje vyhráli. Turnaje se naopak z důvodu nařízené karantény nezúčastnila Čistá.\
\
**Sestava:** Matyáš Lněnička  - David Klusoň, Samuel Vomočil, Jaroslav Chaun, David Boček, Jan Svoboda, Štěpánka Gábrlíková, Štěpán Kovář

**Horní Újezd/Morašice – Opatov 1:10 (0:4)**

branky: David Boček

**Horní Újezd/Morašice – Němčice 2:8 (1:3)**

branky: David Boček, Jan Svoboda

**Tabulka turnaje:**

1.Opatov (18:2, 6 b.)

2.Němčice (9:10, 3 b.)

3.Horní Újezd/Morašice (3:18, 0 b.)

Ač jsou výsledky jednoznačné, náš výkon byl podstatně lepší než na úvodním turnaji. Největší pochvalu zaslouží Sam Vomočil a David Klusoň, kteří vydrželi na hřišti po celou dobu, a brankař Maty Lněnička, který se lepší zápas od zápasu. Další turnaj nás čeká v sobotu 19.9. od 9:30 v Březové.

Autor: Mgr. Zdeněk Beneš