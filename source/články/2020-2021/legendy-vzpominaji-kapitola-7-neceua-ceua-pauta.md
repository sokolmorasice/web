---
title: "Legendy vzpomínají - kapitola 7: Nezapomenutelní"
date: 2020-12-29T07:00:13.230Z
category: obecne
image: /public/images/uploads/logo.png
---
**Několikrát jste tady jmenovali super partu, bez ní by ten fotbal neměl cenu. Dokázali byste mezi spoluhráči vyjmenovat některé, co čímkoli vyčnívali?**

**MR:** Kdybych měl vypíchnout třeba baviče, tak jednoznačně Luboš Plšek. Ten dokázal na soustředění vyprávět vtipy celý večer celé hospodě, ne jen nám.

**PK:** Ten znal snad 10 000 vtipů. Jeho „a nebo…a nebo“ je legendární. Ale geniální byl i Eda Král, jak při zábavě, tak v bráně.

**JK:** Co se týče zábavy tak jednoznačně Kárčí *(Eda Král – pozn. redakce)* a Bazy (*Josef Temňák – pozn. redakce*). Z brány mi samozřejmě naskočí „jejku“.

**MR:** Mně taky, tím spíš, že je to spolužáka z lavice Vojty Andrleho. *(K Vojtově smůle jedno "jejku" máme i na [Youtube](https://www.youtube.com/watch?v=RXZM-bD4yxM&feature=youtu.be), k Vojtově štěstí je na konci téhož videa i důkaz jeho kvalit - pozn. redakce)*[](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2011-2012/b%C5%99ezov%C3%A1-nad-svitavou-vs-mora%C5%A1ice)

**JK:** Ale jak Cvejn, tak Vojta to „jejku“ dokázali napravit, klidně hned, nějakým zákrokem snů, který nikdo nechápal.  

**MR:** Na bránění byl vynikající Bobeš Jánu. Ten se bránil tréninkům, dodržování životosprávy,…

**PK:** …A přitom přes něj prakticky nešlo přejít. *(A nejde ani po dvaceti letech, stačí si pustit [video](https://www.youtube.com/watch?v=41IvXF747hg&t=5s) z fotbalového dne v čase 0:35 – pozn. redakce)* Ale to mají v Makově tak nějak v krvi.

**Já jsem do chlapů přišel v éře, kdy mě přišlo, že měl celý mančaft bránění tak nějak v krvi, vyberete někoho kdo byl nebezpečný na druhé straně? Zabijáckým instinktem nebo zabíjením šancí?**

**JK:** Šance vždycky pálil celý mančaft, nedat gól je stejná chyba jako „jejku“, akorát je to v bráně víc vidět.

**MR:** Za moji éru jsme podle mě měli jediného klasického koncáka, a tím byl Vašon Benešu, to byl pan fotbalista.

**PK:** Střílení gólů je samostatná kapitola. Zdeněk Fric a jeho klid, Václav Beneš a jeho elegance, Martin Burián a jeho zbrklost. No a Petr Kuta…statistiky nelžou.

**Komu jsi Petře nejradši chystal góly?**

**PK:** Zásobovat útočníky mě bavilo vždycky, ale [Burymu](https://www.youtube.com/watch?v=-gDaxjP2mkY&t=116s) *(Martin Burián - pozn. redakce)* jsem i věřil.

**A s kým se ti ve středu pole hrálo nejlíp? Kdo ti svým čištěním dal nejvíce klidu na tvoření?**

**PK:** Na středu hřiště nebyla špatná souhra "já na bráchu, brácha na mě", nebo s Jirkou Vosmekem. Čističe jsem potřeboval až ve vyšším věku, takže Milan Chmelík a Martin Ropek.

**S Pavlem** *(bratr – pozn. Redakce)* **jste mimořádně soutěživé typy. Soutěžili jste i mezi sebou, měli jste větší motivaci, když druhý odskočil ve střeleckých statistikách nebo se povedl zápas? A mířili jste s touhle soutěživostí i na starší a v áčku taky úspěšné bratry?**

**PK:** S bráchou jsme soutěžili prakticky každý den, a ne jen ve fotbale. Hráli jsme soutěžně stolní tenis za Vidlatou Seč, malý fotbal za Šampony a hokej před domem. A všude se našel důvod ukazovat si, který je v čem lepší. To nás asi posouvalo výkonnostně dále, ale statistiky jsme si mezi sebou, ani se staršími bratry nepoměřovali. Vzpomínám si, že jsme jednou soutěžili v přesnosti hodu kamenem a Pavel vyhrál, trefil mě zezadu do hlavy, když jsem zbaběle utíkal. Jako cenu útěchy za prohru jsem dostal 5 stehů v litomyšlské nemocnici.

**V téhle disciplíně jsem doufal, že porazím Matese** *(bratranec – pozn. Redakce)*, **protože jsem při jakémkoli jiném sportovním měření neměl šanci,** **ale dopadl jsem úplně stejně jako ty. Vy si ještě někoho vybavíte?**

**MR:** Když jsem zmínil ostatní posty, tak v záloze musím jmenovat Kuťáka s Bobšem, to jsou prostě morašičtí matadoři se vším všudy.

**JK:** Já jsem hrál se samýma super lidma, kolikrát mi bylo líto, že někteří furt sedí, nedostanou šanci a chodí na tréninky. A když dostali prostor, tak ho během té krátké chvilky zákonitě nedokázali využít. Nerad bych na někoho zapomněl.

***Zítra budeme bilancovat, snít a přát...***