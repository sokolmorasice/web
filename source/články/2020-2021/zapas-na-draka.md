---
title: Zápas na Draka
date: 2021-05-24T07:13:43.171Z
category: muzi
image: /public/images/uploads/logo.png
h1: Janov - Morašice 1:1
---
Výčet pozitiv ze zápasů v [Janově](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2015-2016/janov-vs-mora%C5%A1ice) skončí u krátké cesty a většinou získaného [bodu](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2016-2017/janov-vs-mora%C5%A1ice). Tamní styl a prostředí voní málokomu, ale naštěstí máme i v našem kádru typy, co si rádi kopnou, šťouchnou, ceknou a tak podobně. A nervy těchto hráčů nečekaly v blocích na startovní výstřel dlouhou dobu. To když [Boháček](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/janov-vs-mora%C5%A1ice) brzy rozvlnil síť umným přímákem přes zeď. Proti tomu by se nedalo moc namítat, spíš závidět. Ale přímáku zákonitě předchází faul a ten se dá ve fotbale vždy posoudit dvojím způsobem a většinou se najdou tací, co jsou přesvědčeni o špatné volbě pana rozhodčího. Válek s Vítem na něj začali sprostě pokřikovat a nafasovali žlutou. Chmelík to zkoušel sice vysvětlit diplomaticky, ale nepřestal dokud si sudí nezapsal i jeho. Samozřejmě, že jsme se mohli hecnout a začít hrát naši hru, soupeře mačkat jako citrón a čekat, až se ujme nějaká šance. Ale to my ne, ono má to hádání a karetní bitva něco do sebe. D. Štancl v tom byl nevinně, ten si předkopl balón a pak u něj byl hold pozdě. V. Štancl taky nechtěl. Hrát balón. Klement to zvládl. Naložit od cesty. Ropek se držel. Dresu rychlejšího útočníka. Novák se na to nemohl dívat. Jak ho doháníme v kartách. Vůbec není divu, že za silného větru a neustálého psaní karet se nic dalšího fotbalového stát nemohlo.

Do druhé půle jsme na místo nejohroženějších kusů poslali čerstvé, emočně nezatížené hráče, jako třeba Ondráčka. A byl to právě on, kdo ze strany oslovil bohužel se zastřelujícího F. Bureše. Byl to on, kdo dostal za vzájemnou potyčku naši první žlutou druhého poločasu. A byl to ten pošťuchující Janovák, kdo probudil Draka. A tak když D. Štancl v čase 5[0:25](https://www.youtube.com/watch?v=41IvXF747hg) narýsoval jakoby nic kolmici do lajny a od Válka vletěl míč do vápna, Drak využil vítr v zádech, [vletěl](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2020-2021/pohar-01-morasice-vs-janov) míči do cesty a nasměřoval ho do domácí kasy. Bohužel, Ondráčkův ajfr jsme nedokázali více využít ani my, ani on, Dráček. Sice si několikrát, jako třeba v čase 7[0:37](https://www.youtube.com/watch?v=41IvXF747hg), dokázal slmsnout na slabších, sice dokázal několikrát poslat míč do ohně, párkrát vystřelil do nebes, ale s remízou už se v zájmu sázek hnout prostě nemohlo.

**Další výsledky 19.kola:**

Opatov - Radiměř 3:0 (1:0)

Dlouhá Loučka - Borová 2:0 (1:0)

Jaroměřice - Cerekvice 5:2 (3:1)

Horní Újezd - Hradec nad Svitavou 2:0 (1:0)

Boršov - Jevíčko 2:5 (2:3)

Mladějov - Linhartice 6:2 (4:1)

**Tabulka okresního přeboru po 19.kole:**



|     |                     |     |        |       |     |
| --- | ------------------- | --- | ------ | ----- | --- |
| 1.  | Morašice            | 19  | 14-3-2 | 53:16 | 45  |
| 2.  | Janov               | 19  | 13-2-4 | 47:31 | 41  |
| 3.  | Jaroměřice          | 19  | 13-2-4 | 50:24 | 41  |
| 4.  | Jevíčko             | 19  | 12-5-2 | 35:20 | 41  |
| 5.  | Opatov              | 19  | 10-1-8 | 63:34 | 31  |
| 6.  | Cerekvice           | 19  | 7-6-6  | 36:42 | 27  |
| 7.  | Mladějov            | 19  | 8-4-7  | 53:40 | 28  |
| 8.  | Radiměř             | 19  | 7-4-8  | 40:42 | 25  |
| 9.  | Dlouhá Loučka       | 19  | 6-5-8  | 33:36 | 23  |
| 10. | Borová              | 19  | 5-4-10 | 37:48 | 19  |
| 11. | Horní Újezd         | 19  | 6-2-11 | 28:48 | 20  |
| 12. | Linhartice          | 19  | 3-7-9  | 31:42 | 16  |
| 13. | Boršov              | 19  | 1-6-12 | 24:57 | 9   |
| 14. | Hradec nad Svitavou | 19  | 2-3-14 | 19:67 | 9   |