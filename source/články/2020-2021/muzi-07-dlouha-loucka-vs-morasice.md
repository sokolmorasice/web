---
title: Snad zas za týden
date: 2020-09-26
category: muzi
image: /public/images/zapasy/dlouha-loucka-morasice.png
h1: Dlouhá Loučka vs Morašice
sestava: J. Špinar – V. Tměj, D. Štancl, M. Ropek, V. Novák – D. Ondráček (J.
  Vít), M. Chmelík, T. Nádvorník, B. Válek – F. Bureš (V. Štěpán), M. Kopecký
  (M. Hubáček)
goly: null
zlute: "D. Ondráček, V. Novák, J. Špinar "
cervene: null
multimedia: https://drive.google.com/drive/folders/1FQHgUQV7i0sKfhx7Hiwcm5lYYvv849ez
scortes: https://scortes.rozpisyzapasu.cz/api/533A1A/2020/Morašice/b7da774c-45c2-45eb-a9e1-16a3bb7e3329.html
---
Do Dlouhé Loučky jsme odjížděli s cílem vrátit se na první místo a s vědomím, že nás čeká nepříjemný a důrazný soupeř. O tom jsme se přesvědčili hned na začátku zápasu, kdy nás soupeř zatlačil na naši polovinu, častým napadáním nás nepouštěl do žádné kombinace a byl velmi důrazný v osobních soubojích. Nejvíce to pocítil Chmelík, který toho v úvodních pěti minutách schytal víc než za zbytek zápasu. Naše akce bohužel končily většinou na půlce, domácí vedli své útoky dlouhými balóny za naši obranu, k přímému ohrožení brány to naštěstí nevedlo. Standardní situace se nám dařilo odvracet, zakončení končila mimo naši bránu a dva „pokusy“ o zahrávání penalty rozhodčí nespolkl. Naše útočné akce končily poměrně brzy, ať už po rychlém dojetí soupeře, zbrklé ztrátě míče, nebo Válkových ofsajdech. Nějaké příležitosti se přeci jen naskytly, když jednou utekl po levé straně Válek, podruhé to byl Ondráček na pravé, ale místo přihrávky pod sebe zvolili centr, který nenašel nikoho v našich barvách. Jindy se zase Nádvorník neodvážil vyzkoušet domácího gólmana zpoza vápna. V závěru poločasu Špinar neodhadl vysoký míč a místo něho trefil pouze hlavu domácího útočníka. K nařízené penaltě se postavil Marván, ten však nevyzrál na našeho gólmana, který tím odčinil svoji chybu a připsal si druhou chycenou penaltu v řadě.

Do druhého poločasu jsme vstoupili aktivněji, což u nás nebývá moc zvykem, ale i tak byly naše útočné snahy spíše bezzubé. Po centrech nám chyběl ve vápně důraz, přímý kop Štěpána a nepřímý kop, po němž střílel Válek, do brány nezapadly, k vypadnutému míči domácího gólmana nestihl Chmelík doklouzat včas a Válkův křížný pas na Ondráčka za obranu si druhý jmenovaný nezpracoval. Na druhé straně zahrozil Marván, který zůstal po standardní situaci u autové čáry osamocený na druhém konci vápna, ale jeho křižná střela prolétla jen těsně kolem Špinarovy tyče. Ten měl po chvíli co dělat, aby vytáhl na roh nepříjemně padající střelu. Po další téměř totožné standardní situaci se míč opět dostal k volnému Marvánovi, kterého sice zastavil Ropek, ale za pomocí faulu, a tak domácí dostali v zápase podruhé možnost zahrávat pokutový kop. K němu se tentokrát postavil Ševčík a ten už Špinara překonal. Navýšení vedení se mohl soupeř dočkat, když se po pár přihrávkách zjevil domácí útočník sám před Špinarem, ten dokázal jeho střelu pouze ztlumit a dobíhající Tměj odvrátil míč do bezpečí. Gól se nakonec podařilo vstřelit i nám, když po odraženém rohu našel Válek na zadní tyči volného Ropka, který ve skluzu dopravil míč do sítě, ale kvůli ofsajdu gól nebyl uznán. V závěru, kdy jsme se už moc nestaraly o defenzivní povinnosti, utíkal domácí hráč po našem odraženém rohu sám na Špinara, který dokázal jeho střelecký pokus vyrazit a dorážející hráč poté netrefil téměř odkrytou bránu. A protože jsme se na víc nezmohli, odjíždíme z Dlouhé Loučky na čtvrtém místě a s obavami vyčkáváme na další plukovníkovy rozkazy.

Autor: Dominik Štancl