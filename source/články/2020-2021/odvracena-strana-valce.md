---
title: Odvrácená strana Válce
date: 2021-03-21T11:07:58.621Z
category: muzi
image: /public/images/uploads/logo.png
h1: Jevíčko - Morašice  1:1 (0:1)
---
Zatímco Proseč máme za rohem, ale nemůžeme tam, protože je to zaprvé jiný okres a za druhé jiný přebor, naše vláčení do Jevíčka nevadí ani epidemionelogickým opatřením, ani domácím, kteří nás ve svém svatostánku za městem pravidelně pouští do předem ztraceného vedení. Nejinak to dopadlo i tentokrát. Po deseti minutách se Štěpán dostatečným obloukem podél lajn vyhl všem a Válek se ve vápně hl tak šťastně, že si od něho míč prakticky [nezaviněně](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2019-2020/muzi-01-jevicko-vs-morasice) našel cestu do sítě. Do poločasu Štěpán ze strany ještě jednou oslovil Válka, který podrobil malohanácké břevno zkoušce jebou. Z vědecky jistě přínosné věci jsme ale fotbalový užitek neměli, spíš naopak, protože zatímco branková konstrukce se během přestávky třást přestala, náš sniper Válek se v každé další šanci klepal víc a víc. 

Když hned po přestávce rozvlnil pouze boční síť, vypadalo to, že vše pokračuje v zajetých kolejích. O vykolejení se postarala domácí Hora s kapitánskou páskou. Ta obvykle na hřišti působí dojmem zralým na dnes populární [coming out](https://sport.aktualne.cz/fotbal/evropska-liga/uplne-jsem-ztratil-kontrolu-slo-mi-jen-o-to-nekoho-zranit-pr/r~672d685088d311eb9f15ac1f6b220ee8/), ale tentokrát se prodrala k hlavičce v našem vápně a bylo [srovnáno](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2018-2019/jevicko-vs-morasice). Zkusili jsme to na sobě nedat znát, ale když se Válkovo další usměrnění střílené přihrávky do brány kvůli nepohlídanému ofsajdu do zápisu o zápasu [nepromítlo](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/jev%C3%AD%C4%8Dko-vs-mora%C5%A1ice), dostala se do jeho jindy spolehlivých nohou křeč. Že netrefil obsazenou svatyni z [velkého](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2016-2017/jev%C3%AD%C4%8Dko-vs-mora%C5%A1ice) vápna, to se stává. Že se nepovedlo z 25 metrů vrátit přesně do opuštěné klece gólmanův mizerný odkop, to se dá odpustit a navrch ocenit pohotovost. Ale netrefit prázdnou ani z [malého](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2015-2016/mora%C5%A1ice-vs-jev%C3%AD%C4%8Dko) vápna, to už bylo na pěší pochod domů. Proti tomuhle soupeři nám prostě i válec bývá skoro k ničemu. Ale jen skoro, protože díky zaváhání Jaroměřic už jsme v nahuštěném čele o skóre první.

**Další výsledky 11.kola okresního přeboru:**

Opatov - Janov 1:2 (1:0)

Dlouhá Loučka - Jaroměřice 2:0 (1:0)

Mladějov - Horní Újezd 2:2 (1:0)

Boršov - Hradec nad Svitavou  2:0 (1:0)

Radiměř - Cerekvice 2:1 (2:0)

Linhartice - Borová 6:1 (2:1)

**Tabulka okresního přeboru po 11 kolech:**

|        |                     |        |        |           |        |           |        |        |
| ------ | ------------------- | ------ | ------ | --------- | ------ | --------- | ------ | ------ |
| **1.** | **Morašice**        | **11** | **\|** | **8-1-2** | **\|** | **33:12** | **\|** | **25** |
| 2.     | Jaroměřice          | 11     | **\|** | 8-1-2     | **\|** | 33:16     | **\|** | 25     |
| 3.     | Jevíčko             | 11     | **\|** | 7-3-1     | **\|** | 20:14     | **\|** | 24     |
| 4.     | Janov               | 11     | **\|** | 8-0-3     | **\|** | 27:16     | **\|** | 24     |
| 5.     | Opatov              | 11     | **\|** | 6-1-4     | **\|** | 42:20     | **\|** | 19     |
| 6.     | Dlouhá Loučka       | 11     | **\|** | 4-3-4     | **\|** | 23:18     | **\|** | 15     |
| 7.     | Radiměř             | 11     | **\|** | 4-3-4     | **\|** | 23:27     | **\|** | 15     |
| 8.     | Mladějov            | 11     | **\|** | 4-2-5     | **\|** | 31:29     | **\|** | 14     |
| 9.     | Linhartice          | 11     | **\|** | 3-4-4     | **\|** | 22:23     | **\|** | 13     |
| 10.    | Cerekvice           | 11     | **\|** | 3-4-4     | **\|** | 21:30     | **\|** | 13     |
| 11.    | Borová              | 11     | **\|** | 3-2-6     | **\|** | 22:31     | **\|** | 11     |
| 12.    | Horní Újezd         | 11     | **\|** | 3-1-7     | **\|** | 19:37     | **\|** | 10     |
| 13.    | Boršov              | 11     | **\|** | 1-3-7     | **\|** | 14:30     | **\|** | 6      |
| 14.    | Hradec nad Svitavou | 11     | **\|** | 1-2-8     | **\|** | 12:39     | **\|** | 5      |

|     |
| --- |