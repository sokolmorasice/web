---
title: Milán válí
date: 2021-04-11T16:57:24.670Z
category: muzi
image: /public/images/uploads/logo.png
---
Je to hrůza být 14 dní bez fotbalu. A aby těch hrůz nebylo málo, tenhle víkend se ženil Štancl, což znamená absenci hned několika střev v naší sestavě. Těch básnických a spisovatelských. Takže se musíme spokojit se zprostředkovanými zprávami, podle nichž to byla one man show v podání Chmelíka. Ten byl vysunutý na hrot, abychom proti tradičně urostlé obraně soupeře z Opatova nebyli za úplné soubojové diliny. Skóre otevřel cca ve 20. minutě, když zakončil rychlou akci umístěnou střelou zpoza vápna a před poločasem navýšil naše vedení hlavou, když předčil chybujícího [gólmana](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2019-2020/muzi-03-morasice-vs-opatov).

Po přestávce se hostům podařilo utkání zdramatizovat, když se proti nám jako téměř vždy dostali k pokutovému kopu a jako [vždy](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2018-2019/morasice-vs-opatov) ho proměnili, ale Chmelíkův lob temenem jejich ambice zase [zchladil](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/mora%C5%A1ice_opatov_pohar). A protože nebyl nikdo další navýšit skóre, musel se Chmelík postarat i o gólovou [tečku](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/mora%C5%A1ice-vs-opatov), když na zadní tyči uklidil do sítě rohový kop. 

**Další výsledky 14. kola okresního přeboru:**

Linhartice - Hradec nad Svitavou 3:3 (2:1)

Mladějov - Dlouhá Loučka 5:1 (2:0)

Borová - Cerekvice 3:3 (1:1)

Jevíčko - Horní Újezd 1:0 (0:0)

Radiměř - Jaroměřice 0:1 (0:0)

Boršov - Janov 2:4 (1:3)

**Tabulka okresního přeboru po 14.kole:**

|     |                     |     |        |       |     |
| --- | ------------------- | --- | ------ | ----- | --- |
| 1.  | Morašice            | 13  | 10-1-2 | 41:14 | 31  |
| 2.  | Jaroměřice          | 13  | 10-1-2 | 35:16 | 31  |
| 3.  | Janov               | 13  | 10-0-3 | 35:19 | 30  |
| 4.  | Jevíčko             | 13  | 8-4-1  | 22:15 | 28  |
| 5.  | Opatov              | 13  | 7-1-5  | 47:25 | 22  |
| 6.  | Radiměř             | 13  | 5-3-5  | 29:29 | 18  |
| 8.  | Mladějov            | 13  | 5-2-6  | 36:31 | 17  |
| 7.  | Dlouhá Loučka       | 13  | 4-3-6  | 25:27 | 15  |
| 9.  | Cerekvice           | 13  | 3-6-4  | 25:34 | 15  |
| 10. | Linhartice          | 13  | 3-5-5  | 25:27 | 14  |
| 11. | Borová              | 13  | 3-3-7  | 26:38 | 12  |
| 12. | Horní Újezd         | 13  | 3-2-8  | 20:39 | 11  |
| 13. | Boršov              | 13  | 1-4-8  | 17:35 | 7   |
| 14. | Hradec nad Svitavou | 13  | 1-3-9  | 16:48 | 6   |