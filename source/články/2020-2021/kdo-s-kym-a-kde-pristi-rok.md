---
title: Kdo s kým a kde příští rok
date: 2021-06-22T05:23:06.132Z
category: obecne
image: /public/images/uploads/logo.png
---
Díky spojení s Horním Újezdem jsme stejně jako loni mohli do okresních soutěží přihlásit kategorie starších i mladších přípravek a starších i mladších žáků. Počet dorostenců by byl i v tomto spojení tak hraniční, že jsme byli nuceni tohle družstvo nepřihlásit. A protože jaro nečekaně zředilo i "lidské zdroje" v A-týmu, výbor s těžkým srdcem odmítl nabídku přihlásit A-tým do I.B třídy.