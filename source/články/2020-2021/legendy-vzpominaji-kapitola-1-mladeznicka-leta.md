---
title: Legendy vzpomínají - kapitola 1:Mládežnická léta
date: 2020-12-23T06:55:27.505Z
category: obecne
image: /public/images/uploads/logo.png
---
Přejeme všem našim hráčům, příznivcům, pomocníkům i dalším našim profesím klidné svátky po boku nejbližších nebo promořených. Abychom Vám zkrátili čas bez fotbalu a odreagovali Vás od zpráv všedních i svátečních dní, nachystali jsme si na vánoční čas druhý seriál našeho občasníku, který vychází zhruba jednou za vlnu. Ten první si můžete připomenout [zde](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2019-2020/fotbal-v-case-korony-dil-1). 

Nebudeme si nic nalhávat o tom, jak by se dal z globálního pohledu hodnotit končící rok 2020, ale ani se zde nebudeme pouštět do obšírných politicko-ekonomických disputací a budeme se držet na našem fotbalovém rybníčku, jehož hlavní událostí bylo dovršení 50-ti let existence. A ať se jim to líbí nebo ne, převážnou část z tohoto období k němu patří i trojice stále aktivních matadorů. Tak si pojďme s **Bobšem Kopeckým, Petrem Kutou** a **Martinem Ropkem** trochu zavzpomínat. Upozorňujeme, že na rozdíl od první vlny, jsou všechny postavy a události v tomto rozhovoru skutečné.

**Petře, tebe všichni známe jako špílmachra a střelce, ale v žácích jsi začínal vzadu, dokonce jsi odchytal polovinu sezóny, na jejímž konci bylo první místo a ty jsi byl vyhlášen nejlepším žákem roku 1985. Jaké máš na žáky vzpomínky? Byl víc potřeba dobrý brankář než dobrý hráč nebo jsi tenkrát tak dobrý hráč ještě nebyl?**

**PK:** K chytání moc vzpomínek nemám, jen to, že jsem byl výborný brankář a nikomu se moc do brány nechtělo.

**JK:** To můžu potvrdit, Petr byl vynikající brankář.

**PK:** A táta už v té době dobře věděl, že žák si má projít všemi posty, aby se zdokonaloval ve všech herních činnostech.

**Táta?**

**PK:** Náš žákovský trenér Honza Klofanda, řečený „táta“. To přesně sedělo k tomu, jak se k nám choval, dal nám dokonalé základy a lásku k fotbalu. Tu potom prohluboval Láďa Tobku *(zakladatel a dlouholetý předseda oddílu – pozn. redakce)*, k němu jsme měli s bráchou velmi osobní vztah, i on k nám. Ještě si ze žáků vzpomínám na opravdu vysokou výhru, myslím 23:1, což byl tenkrát morašický rekord.

**To bylo proti Koclířovu a ten gól jste ani nedostali. Že jsi při té výhře dal 8 gólů si nepamatuješ, skromně o tom mlčíš nebo to někdo blbě zapsal a dal to brácha?**

**PK:** Pamatuju si, že hodně, ale 8 bych rozhodně neřekl.

**Bobši, ty jsi dal za žáky jsi dal jenom jeden gól, to jsi tenkrát nechodil na rohy ani na penalty?**

**JK:** Ten svůj jediný gól jsem dal až někdy ke konci žáků na turnaji v Řetové, místo hlavou jsem trefil míč ramenem, ale byl tam a byl jsem za něj rád.

**Když jsem koukal do zápisků, tak v žácích Vám to klapalo, dokonce jste vyhráli soutěž, ale během prvního roku v dorostu to byla častá kontumace, jednou jste zápas začínali v 7 lidech. Bylo Vás tak málo celou sezónu nebo to bylo nespolehlivostí, protože dorost je prostě vrtkavá kategorie? A neměl si tenkrát chuť se na to vybodnout?**

**JK:** Ty důvody byly různé. Bylo málo lidí, někdy se špatně domluvil termín – dneska je to k smíchu, ale tenkrát musel všechno domlouvat pan Nepraš sám –  někdy se nesehnala doprava. Ale pořád to nebylo tak strašné jako dnes. V dorostu, a teda ne jen v dorostu, se vždycky chlastalo, ale nepamatuju, že by si tenkrát někdo dovolil nepřijít na zápas. Pro mě byl ten přechod dorostu těžký spíš kvůli věkovému skoku. Chodit, když nás bylo málo, mi nedělalo problém. Já jsem nikdy nebyl talent, všechno jsem si musel vydřít na tréninku, a už Klofanda v žácích věděl, že na mě je spoleh, že i kdybychom na tréninku byli jen dva, tak já tam budu. Nikdy jsem nebyl rychlý, ale mohl sem běhat furt, to jsem měl po tátovi, který běhal kilometr za dvě čtyřicet. Dvě čtyřicet.

**Petře, po té bídné sezóně, o které jsme mluvili s Bobšem, jste s dorostem dva roky po sobě postoupili, z III. Třídy jste to půl roku před revolucí dotáhli jako první oddíl morašického fotbalu do kraje. Ty jsi chytal první rok a půl, proč tě pak v bráně nahradil Pepa Rambousku, který ten postup z okresu vystřílel?**

**PK:** Já vychytal postup z pralesa do okresního přeboru, ale Pepika to táhlo do brány, mě do pole, a tak jsme se vyměnili.

**JK:** Já mám dodneška jednu z nejživějších vzpomínek na zápas, který jsem nehrál. V úterý jsem si udělal na volejbale výron a o víkendu se hrálo v Boršově o první místo a postup z pralesa do okresu. A já celý zápas běhal kolem hřiště, že jsem si tu sádru úplně rozdupal. Ale vyhrálo se, myslím 4:1.

**Mary, když se Petr v dorostu přesunul do pole, začínal si hrát v žácích. A jestli mě zápisky nepletou, byl si odmala obránce. Brals to tak, že tam je tvoje místo nebo si doufal, že tě trenéři někdy vysunou?**

**MR:** Já ti do toho ještě skočím, když se mluví o tom postupu dorostu do kraje. V té době tím žily celé Morašice, na první utkání v kraji byly dokonce transparenty, já jsem v hloučku mladých fans celé utkání skandoval a hrkal plechovkou plnou šroubů. Dá se říct, že tihle kluci byli vzorem pro nás mladší. Ale k tvojí otázce, asi jsem vždycky bral, že obránce je moje místo, protože moje fotbalové přednosti byly vždycky v tom někomu vzít balón nebo zabránit akci soupeře, než že bych měl hru tvořit nebo střílet branky. Asi nejlépe jsem se cítil na defenzivním záložníkovi, kde jsem mohl využít i moji fyzičku.

**A svůj gólově nejbohatší zápas si pamatuješ?**

**MR:** Nevybavuji si, jestli se mi někdy podařilo dát hattrick, ale pamatuji si, že jsem k němu byl blízko v dorostu za trenéra Jirky Horáka. V postupové sezóně jsem v Moravské Třebové dal dva góly a poté jsme kopali penaltu, na kterou byl určen Fúča. Já jsem mu ji ale přebral, penaltu neproměnil pak to od trenéra pěkně slíznul!

**No já jsem taky odjakživa obránce a hattrick jsem nikdy nedal, proto jsem myslel, že si budeš pamatovat, že si za žáky v jednom případě skóroval dokonce čtyřikrát. Ale to by bylo o dost sušší než ten nepovedený hattrick, takže díky. Ty ses s žáky pravidelně umisťoval na bedně, jednou jste dokonce vyhráli, s dorostem jste brzo postoupili zpátky do kraje, ze kterého se spadlo po odchodu Kuťáků a spol. Coby obránce jsi byl dvakrát vyhlášen nejlepším hráčem, žákem v roce 1992 a dorostencem hned o rok později. Kapitánoval jsi během mládeže, když si tě trenéři tak vážili?**

**MR:** Určitě jsem kapitánoval, asi většinou ten poslední rok, jak v žácích, tak v dorostu. Nevím, zda to bylo tak, že si mě vážili trenéři, nebo zda to bylo hlasováním hráčů, ale asi nejvíce rozhodlo moje morašické srdíčko a možná také moje povaha, že nerad prohrávám, což také mohlo být oceněno páskou. Já si z toho pamatuji hlavně tu postupovou sezónu. To jsme do posledního kola bojovali na dálku s Horním Újezdem, měli jsme tehdy neskutečnou partu, byla to jízda, v posledním kole jsme doma vyhráli 12:0 *(17:0 proti Třebářovu - pozn. redakce)* a ta oslava patří mezi moje největší zážitky. Vybavuji si, že při vítězném pokřiku ve vagónu jsem rozbil sklo okna, o které jsem se pořezal, a že jsme pak před hospodou na návsi vyhazovali do vzduchu pana Tobka, který byl tehdy ikonou morašického fotbalu.

***V zítřejším pokračování dojde na chvíle, kdy Morašice nebudou na prvním místě. A ne jen v tabulce.***