---
title: "Legendy vzpomínají - kapitola 8: Co dál?"
date: 2020-12-30T07:24:58.965Z
category: obecne
image: /public/images/uploads/logo.png
---
**Co Vám v poslední době na fotbale udělalo radost?**

**PK:** To je jasné, zatčení Romana Berbra ze zločinecké organizace FAČR.

**MR:** Jsem velkým fanouškem Slavie, takže logicky Slavie mi dělá obrovskou radost svou hrou a svými úspěchy v Evropské lize, jejichž přímým účastníkem jsem byl coby fanoušek na utkáních se Sevillou a Chelsea. Zážitek a obrovské emoce ze závěru zápasu se Sevillou mi navždy zůstanou v paměti. Také zápas, na který jsem se coby divák dostal díky svému výkonu ve finále okresního poháru, na mistrovství Evropy U21, ve kterém naše repre porazila v Tychy Itálii 2:1 a po skončení jsme měli možnost se vyfotit s hráči jako jsou Tomáš Souček, Petr Ševčík a Michal Trávník. V poslední době jsem také spokojen s výkony Morašic, kdy se nám naše řady podařilo rozšířit o další kvalitní hráče, kteří zapadli skvěle do kolektivu. Jsem rád součástí kolektivu, který v současné době v Morašicích je.

**JK:** Toho moc nebylo.

**Petře lituješ nějakých rozhodnutí u fotbalu?**

**PK:** Jednoho mého osobního rozhodnutí jsem s odstupem času litoval. To když mě kamarád z Mejta, kde jsem se učil a chodil trénovat, přemlouval abych šel hrát fotbal do Pardubic, které hráli 2. ligu. A já, protože bych musel i přestoupit na novou školu a psal se rok 1987, jsem se rozhodl zůstat v Morašicích.

**Takže slovy Tomáše Rosického tě mrzí, žes nikdy nezjistil, jak dobrý jsi mohl být.**

**PK:** Přesně tak.

**Petře, strávil jsi část kariéry i ve výboru, jeho šedou eminencí, zejména přes investice, jsi dodnes. Jaký počin ti udělal největší radost, co tě zklamalo, co by sis ještě v areálu přál?**

**PK:** Neřekl bych šedou, ale spíš pro někoho černou. Akce, které se podařily za posledních 6-7 let, tedy béčkové hřiště, studna, celoplošná závlaha, světla, rekonstrukce kabin, nová antuka, z toho mám obrovskou radost. A co bych si přál, to ty víš určitě nejlíp, malou zastřešenou tribunku, kde budu mít VIP místo a SKYBOX. Doufám, že všechny, co jsou proti, přesvědčím o správnosti mého záměru.

**A jaké máš plány na další působení v Morašicích? Kam myslíš, že Morašice momentálně patří na fotbalové mapě?**

**PK:** Moje plány na působení v Morašicích jsou následující: jako legenda na telefonu budu hlásit výborné výsledky Sokola a při nerozhodnosti trenéra mu radou pomohu. Morašice dle mého patří na fotbalové mapě do pardubického kraje, a samozřejmě do jeho soutěží, a to již od nejmladších bojovníků.

**Co na to říká současný trenér, jaké jsou jeho sny a ambice?**

**MR:** Plán, sny ani trenérské ambice nemám, spíše takové přání, aby to kluky v Morašicích bavilo, abychom společně dokázali v Morašicích udržet fotbal do budoucna a stále jsme stavěli na skvělé partě, která pro mne byla po celou fotbalovou kariéru nejdůležitější.

**Máš v hlavě nastavený nějaký moment, přes který už pokračovat nebudeš?**

**MR:** Kdyby nebyla v Morašicích dobrá parta, tak by mě to nevábilo pokračovat dál. Herní pokračování může ovlivnit zejména zdraví, ale já jsem takový nezmar, že budu hrát, dokud mě někdo asi nepošle někam… Možná se v budoucnu mou metou ještě stane, zahrát si v jednom mužstvu s mým synem, ale nevím, kdo z nás bude muset přestoupit. A co týče trenérského pokračování, budu to dělat, jen pokud mě to bude bavit, možná jenom pokud budu hrát.

**A nějakou vysněnou statistickou metu máš?**

**MR:** Vím, že jsem posledním zápasem dosáhl 800 zápasů za muže, takže na Bobšovu tisícovku to, bohužel, určitě nevidím, ta jeho čísla jsou skutečně neskutečná, odehrát 1000 zápasů jen za kategorii mužů je prostě bomba. Nevím přesně, jak jsem na tom ve střelených gólech.

**135 celkově, 95 za chlapy.**

**MR:** Těch 150 gólů by bylo hezkých, ale vzhledem vzhledem k mým průměrným 4 brankám ročně, to zkusím jenom zaokrouhlit na stovku mezi chlapama.

**A když ti řeknu, že do celkové tisícovky ti chybí 13 zápasů?**

**MR:** To mě láká a věřím, že to vyjde. I když dneska nikdo neví, kdy budeme moci hrát.

**Petře sleduješ Bojdovo stahování a statistiky celkově? Hřejou tě tvoje čísla nebo tě to tak nebere?**

**PK:** Jasně, že sleduji a přeji mu úspěch. Jen ať mě na výsluní statistik ještě 2-3 roky nechá a potom ať si vedoucí místo vezme. Moje čísla mě ale nechávají opravdu ledově klidným

**Bobši, ty máš ještě nějaké sny?**

**JK:** Já bych si přál víc lidí. Protože když hraješ před 400 lidma, i když je to třeba na Dolňáku a všichni na tebe řvou, tak to chceš pořád. To je přání. A sny mám, ale ty neřeknu, čas nevrátíš..

**Co byste přáli morašickému fotbalu a jeho fanouškům do nového roku?**

**PK:** tak hlavně všem hodně zdraví, štěstí a radosti a celkově hodně obětavých lidi, obětavých rodičů, spoustu zapálených fotbalistů s láskou k tomuto sportu, nadšených fanoušků a ještě minimálně dalších 50 úspěšných let činnosti celého Sokola  

**MR:** Morašickému fotbalu bych přál to, co již jsem několikrát v rozhovoru zmínil, dobrou partu, ve které jeden pro druhého, a to nejen při fotbale, udělá něco navíc. A fanouškům? Těm přeji, ať s morašickým fotbalem můžou zažívat i nadále ty krásné chvíle, na které jsme v našem povídání vzpomínali! Jo a ještě tribunu!!!

***Tak končí naše komedie, nechť virus prohrává a fotbal žije.***