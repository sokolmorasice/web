---
title: Neúspěšná baráž
date: 2021-06-20T04:45:50.182Z
category: muzi
image: /public/images/uploads/logo.png
h1: Morašice - Dolní Újezd "B" 1:2
sestava: J. Špinar – M. Černohorský, D. Štancl, M. Ropek, V. Tměj – D. Ondráček,
  M. Chmelík, Z. Skala, B. Válek – F. Bureš, M. Kopecký, V. Novák, J. Vít, J.
  Kopecký, J. Kubeš
goly: M. Chmelík
multimedia: https://drive.google.com/drive/folders/1Vyi0TOTKDfNN0PORmBYKoLzVXgVrWflT
---
K poslednímu přátelskému zápasu tohoto krátkého jara jsme doma přivítali sousední Dolní Újezd. Ten tradičně namixoval trochu od "A", trochu od "B", my jsme do sestavy netradičně zamíchali J. Kubeše. Už při rozcvičce obou mužstev bylo znát, že za vysokých teplot se nikdo nechce zbytečně vysilovat a během zápasu to vypadalo podobně. Většinu času se hrálo mezi vápny a šancí bylo poskrovnu. Hosté, kteří více drželi míč, zahrozili dvěma nepřesnými střelami a jednou nastřelenou tyčí, u nás stojí za zmínku nepřesná hlavička J. Kopeckého zapomenutého při rohu na zadní tyči a slabá střela Válka, který po souhře s Novákem a Kopeckým zakončoval z levé strany.
Druhý poločas probíhal ve stejném "nasazení" jen se občas gólově urodilo. Jako první se radovali hosté, když Špinar neudržel „Gangovu“ střelu a obrana pozdě reagovala na následnou dorážku. Vyrovnání zařídil oslavenec Chmelík, který využil celozápasového vysokého postavení hostujícího gólmana (koho by taky bavilo čekat ve výhni jestli se náhodou nepřiblíží nějaká střela )a jeho chybnou rozehrávku potrestal gólem a lá Schick. Poslední gól zaznamenali hosté, když Vít zachytil dlouhý míč v našem vápně, ale místo odkopu volil neúspěšnou kličku a po následné přihrávce na Mokrejše v okolí penalty to bylo 1:2. A bylo to tak až do konce.