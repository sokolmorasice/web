---
title: Poslední mohykáni
date: 2020-10-13T08:48:59.837Z
category: pripravka
image: /public/images/uploads/logo.png
---
## **Mladší přípravka**

Vzhledem k vládním opatřením proti koronaviru šlo o poslední turnaj podzimu. Karanténa se dotkla i našeho týmu, a tak jsme k domácímu turnaji nastoupili pouze v šesti hráčích, takže jsme měli jen jednoho náhradníka, starší přípravka ke svému turnaji neodjela vůbec.

Sestava**:** Jaroslav Chaun, David Klusoň, David Boček, Dominik Prokop, Štěpán Kovář, Štěpánka Gábrlíková

**Výsledky:**

**Horní Újezd/Morašice – Březová 7:2 (4:1)**

branky: David Klusoň 3, David Boček 3, Jaroslav Chaun

**Horní Újezd/Morašice – Borová 10:2 (5:1)**

branky: David Klusoň 3, David Boček 3, Dominik Prokop 2, Jaroslav Chaun, Štěpán Kovář

**Tabulka turnaje:**

1.Horní Újezd/Morašice (6 b.)

2.Březová (3 b.)

3.Borová (0 b.)\
\
I když výsledky vypadají jednoznačně, výhry se nerodily lehce. Kvůli chybějícím hráčům jsme znovu museli přeskládat sestavu, ale kýžený úspěch se dostavil. Teď si dáme chvilku od fotbalu pauzu a uvidíme, jak se situace vyvine.

Autor: Zdeněk Beneš