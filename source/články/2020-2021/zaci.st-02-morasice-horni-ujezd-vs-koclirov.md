---
title: I žáci umí brát dech
date: 2020-09-13
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Morašice/Horní Újezd vs Koclířov
sestava: Martina Famfulíková - Jonáš Krška, Jakub Kvapil, Anežka Břeňová, Šimon
  Kušnír, Vojtěch Andrle, Daniel Hanyk, Vlastimil Hladík, Šimon Lněnička, Lukáš
  Mach, Jan Odehnal
goly: Šimon Kušnír 4, Jan Odehnal 4, Lukáš Mach, Anežka Břeňová, Vlastimil
  Hladík, Vojtěch Andrle, Jonáš Krška
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1A/2020/Morašice/Horní
  Újezd/470de59b-1828-432b-9f3b-9dcc637dd658.html
---
Druhé utkání sezony jsme odehráli v Morašicích, proti družstvu Koclířova. Do zápasu jsme nastoupili v pozměněné sestavě. Nejvýraznější změna byla v bráně, kde nemocného Němce zastoupila Marťa Famfulíková ze starší přípravky. Ke smůle holek, rodiny, fanoušků i historických hnidopichů nemohla nastoupit její setra Zuzka, a tak se první zápas se sesterským duem v sestavě zatím odkládá. Po vlažných 5-ti minutách nás do vedení dostal Andrle, jeho gól tým uklidnil, další branky přibývaly snáz a poločas skončil 8:0. Nejhezčí gól dal hlavou Odehnal, který nechal vzpomenout na Honzu Kollera. 

Druhý poločas jsme začali lépe a hned to skončilo golem Odehnala. Dále jsme už kontrolovali hru a přidávali branky na konečných 13:0. Prosadil se poprvé i Lukáš Mach, který před golem musel vyměnit kopačku o tři čísla větší a vyplatilo se. Chválím všechny zúčastněné a chtěl bych vypíchnout Marťu, která si počínala velmi dobře. Další utkání hrajeme 20.9. v 10:00 v Němčicích.

Autor: Jan Špinar