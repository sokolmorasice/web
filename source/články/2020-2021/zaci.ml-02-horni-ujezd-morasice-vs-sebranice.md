---
title: Zvládnuté derby
date: 2020-09-06
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Sebranice
sestava: Matěj Kopecký – Ondřej Kusý, Ondřej Žďára, Lukáš Mach, Jindřich Mičík,
  Filip Střasák, Jakub Bartoš, Jan Kopecký, Jan Flídr, Martin Flídr, Jan Kvapil
goly: Jakub Bartoš, Filip Střasák, Jan Kvapil
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2020/Horní
  Újezd/Morašice/afa0cec6-dada-4eb3-bdf7-5b5d838a8576.html
---
Druhé utkání sezony jsme odehráli v domácím prostředí proti sousedům ze Sebranic. Do zápasu jsme vstoupili aktivněji než soupeř, díky čemuž jsme se již v 8. minutě dostali do vedení po individuální akci Jakuba Bartoše. I nadále jsme pokračovali v dobrém výkonu, za což jsme byli ve 29. minutě odměněni druhým gólem, kdy Lukáš Mach potáhl míč po pravé straně a jeho střelu dorazil do brány Filip Střasák. Druhý poločas začal velmi podobně jako první, kdy jsme poměrně dobře kombinovali a mírně přehrávali soupeře. V 55. minutě po pěkné kombinační akci zvýšil Jan Kvapil na konečných 3:0. Po tomto gólu se ze hřiště pomalu vytratila fotbalovost a oba týmy začaly nakopávat dlouhé míče. Příští zápas odehrajeme v sobotu 12. září v 11:30 v Březové nad Svitavou.

Tomáš Chadima