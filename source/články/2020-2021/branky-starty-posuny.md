---
title: Branky, starty, posuny
date: 2020-11-01T21:02:04.796Z
category: obecne
image: /public/images/uploads/logo.png
---
Jubilejní 50.rok fotbalu v Morašicích stihl i přes děravý program přinést několik osobních zajímavých milníků. Zajímavost je samozřejmě subjektivní pojem, ale u čtenářů těchto řádků se jistá shoda na míře zajímavosti předpokládá.

Milníkem nejdůležitějším je samozřejmě to, že jsme si v době, kdy už celá Praha jede na Instáči, založili [Fejs](https://www.facebook.com/TJ-Sokol-Mora%C5%A1ice-102120544934787/?view_public_for=102120544934787). A teď k těm dalším.

Zatímco hrající trenér A-týmu **Martin Ropek** si na poslední chvíli stihl připsat 800.start v mužském dresu, **Josef Kopecký**, který v létě pokořil 1200 startů v součtu všech kategorií, si musel chlapskou tisícovku na poslední chvíli odložit. Ale když ho trenér vynechá v přátelácích, aspoň to bude mít na jaře i s diváky. Snad. Posouvali se i kluci Karalovi. **Milan Chmelík** překonal 725 startů za muže v podání Petra Kuty a vyhoupl se tak na 3.místo za dva výše zmíněné matadory. **Milan Klement** se sebezapřením naskočil proti Jaroměřicím a stal se tak 5. nejčastěji nastoupivším morašickým fotbalistou všech dob. Přidat další start k těm 694 už mu zdraví na podzim nedovolilo, ale všichni jemu i áčku přejeme, aby se mu to ještě podařilo zaokrouhlit. Na 800. Na metu 300 zápasů v mužích se dostal **Zdeněk Skala**, premiéru si odbyl **Michal Kopecký**, **Jan Špinar** se posunul na 7. místo v potu zápasů odchytaných v áčku.

Ve střeleckých statistikách na sebe hlavní pozornost poutá **Bohumil Válek**, který se stal rekordně popáté nejlepším střelcem sezóny (4x se to povedlo F. Ropkovi, 3x J. Vosmekovi, Jiřímu Jánovi st. a Karlovi a Petrovi Kutovým). Mezi muži načal druhou stovku gólů a poskočil z 9. místa na 6., v součtu kategorií (z praktických důvodů počítáno bez elévků) už mu chybí pouze 10 gólů na první místo Petra Kuty. Produktivním rokem se **Milanu Chmelíkovi** podařilo posunout do TOP 20 a díky potenci posledních let předběhnout i svého souseda. Své první góly za muže, a ani v jednom případě nezůstalo jen u jednoho, vstřelili **Miroslav Hubáček, Michal Kopecký, Vojtěch Štěpán a Jan Vít**.

Teď už dorostenec **Jiří Jána ml.** zamíchal pořadím historických žákovských statistik a nebýt předčasně ukončené sezóny, mohl atakovat nejvyšší příčky co do odehraných zápasů (nakonec 9.místo) i metu 59 žákovských gólů svého táty (nakonec 43, čili mu při jeho loňském tempu chybělo zhruba 5 zápasů). Do třicítky nejlepších žáků se prostřílel **Šimon Lněnička**, těsně za ní je **Šimon Kušnír**, jehož 8 gólů v podzimním zápase proti Křenovu je tímto vyhlášeno žákovským rekordem do doby, než se podaří projít archivy nebo se někdo přihlásí, že dal za žáky víc.

Žebříčky elévků se mění nejrychleji ze všech, jak nám na fotbalovém dni názorně předvedl moderátor. **Martina Famfulíková** už překročila 250 zápasů, **Daniel Válek** 300 gólů a kromě nich mohou svoje pozice ještě vylepšovat i **Vít Famfulík** a **Vojtěch Huryta**.

Tabulky TOP 30 jednotlivých kategorií, seznam všech nastoupivších hráčů i další statistické perličky naleznete [zde](https://docs.google.com/spreadsheets/d/1leVjJu7cU-j48mBYp9hx7PgFwTmH01byvIbeJqPWgPo/edit#gid=1378160355) (nebo přes záložku "historie"), aktuální čísla mužů i v "soupisce".