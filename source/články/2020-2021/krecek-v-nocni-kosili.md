---
title: Křeček v noční košili
date: 2021-05-09T14:46:06.062Z
category: muzi
image: /public/images/uploads/logo.png
h1: Horní Újezd - Morašice  0:2
---
Když nám FAČR v úterý ukončil [soutěže](https://facr.fotbal.cz/vv-facr-ukoncil-soutezni-rocnik-2020-21/a14103), neprodleně jsme jeho vedení i dvěma kandidátům na předsedu ostrou prosbu o dohrání naší již pokročile rozehrané soutěže výměnou za podporu při červnovém hlasování. Marek Malík nám jménem FAČRu sdělil, že výjimky jsou nepřípustné a vzhledem ke končícímu mandátu mu je prý u Kudele, jestli ho za to budeme hanit. Karel Poborský nám jménem FAČRu napsal, že FAČR chce všem vyjít vstříc, ať soutěž dohrajeme a příští ročník na dohrané pořadí bude brán zřetel. Petr Fousek nám s diplomatickým šarmem sobě vlastním sdělil, že je pro, pokud budou pro všechny zainteresované strany. Vladimíru Šmicerovi jsme psát nemuseli. Ten v pondělí večer potřetí za sebou chyběl v TIKI-TAKA, protože rozesílal hromadný e-mail, ve kterém sděloval, že kdo chce soutěž dohrát, ať neposlouchá ty staré kádry ze Strahova a dohraje ji. Sice to bylo ještě před oficiálním ukončením soutěží, ale to hlavní se povedlo - říct pozitivní zprávu dříve než Petr Fousek. Tak nám z toho vyšlo, že nic nebrání odjet k utkání 17.kola na Horňák.

Po delší době se k nám připojil [Klement](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2018-2019/horni-ujezd-vs-morasice) a bylo to znát ještě víc, než jsme si mysleli, že to vůbec znát být může. Už po pěti minutách se vydal vstříc bráně soupeře, nepadl k zemi, i když si o to kontakt se soupeřem a umístění ve vápně říkaly, ve svém klasickém polovysokém pokleku se placírkou opřel do míče a připravil o domov pár pavouků ve vzdálenějším rohu domácí brány. Škoda, že si tím tak nějak řekl, že má splněno nad plán. Ne že by se během poločasu nehodilo, že se spolehlivě věnoval čištění středu hřiště, vymazání Pražana i Pechance, nebo předbíhání našich stoperů při dlouhých míčích za obranu, ale jeho odvaha z úvodní akce zápasu nám vepředu chyběla, a tak většina kombinací, i povedených, končila neadresným centrem, nepovedeným rohem nebo nepřesnou střelou.

V kabině nám zatrnulo, když Klement v reakci na pochvalu řekl, že vydrží tak maximálně čtvrt hodiny, ale hned na hřišti zase ukázal, že běhání je jeho slabá stránka jen v případě chabé motivace, jako jsou například stopky. Jak má poblíž míč, protihráče s míčem nebo jen nepříjemného protihráče, je jeho fyzický fond bezedný a na krocích nešetří. A obdržená žlutá karta na tom zpravidla nic nemění. I s jeho přispěním měl náš brankář minimum práce a druhý [poločas](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2019-2020/muzi-08-horni-ujezd-vs-morasice) byl hlavně o nepřidané pojistce v domácí síti. Po mnoha neproměněných šancích snad všech našich ofenzivních hráčů tak stvrzení výsledku přišlo až pět minut před koncem, když si neúnavný a neobsazený Klement našel centr od rohového praporku a hlavičkou umístil míč do sítě prudčeji než při první gólové střele. Všichni to sice víme, ale dnešní zápas byl i pro potenciální pochybovače jasným důkazem, že je Křeček platnější při vytváření výsledku než při jeho pouhém sledování večer v posteli.

**Další výsledky 17.kola**

Opatov - Boršov 4:1 (1:1)

Mladějov - Radiměř 0:0)

Jaroměřice - Borová 4:2 (3:1)

Hradec nad Svitavou - Cerekvice 0:3 (0:1)

Dlouhá Loučka - Jevíčko 0:2 (0:1)

Janov - Linhartice 3:1 (2:0)

**Tabulka okresního přeboru po 17.kole:**

|     |                     |     |        |       |     |
| --- | ------------------- | --- | ------ | ----- | --- |
| 1.  | Morašice            | 17  | 13-2-2 | 50:15 | 41  |
| 2.  | Jaroměřice          | 17  | 12-2-3 | 45:20 | 38  |
| 3.  | Janov               | 17  | 12-1-4 | 43:29 | 37  |
| 4.  | Jevíčko             | 17  | 10-5-2 | 26:18 | 35  |
| 5.  | Mladějov            | 17  | 7-4-6  | 47:34 | 25  |
| 6.  | Opatov              | 17  | 8-1-8  | 55:33 | 25  |
| 7.  | Radiměř             | 17  | 6-4-7  | 34:37 | 22  |
| 8.  | Cerekvice           | 17  | 6-6-5  | 32:37 | 24  |
| 9.  | Dlouhá Loučka       | 17  | 5-4-8  | 30:35 | 19  |
| 10. | Borová              | 17  | 5-4-8  | 36:43 | 19  |
| 11. | Horní Újezd         | 17  | 5-2-10 | 26:46 | 17  |
| 12. | Linhartice          | 17  | 3-6-8  | 28:35 | 15  |
| 13. | Boršov              | 17  | 1-6-10 | 20:46 | 9   |
| 14. | Hradec nad Svitavou | 17  | 2-3-12 | 18:60 | 9   |