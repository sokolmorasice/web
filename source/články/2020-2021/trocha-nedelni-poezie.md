---
title: Trocha nedělní poezie
date: 2020-08-03T21:38:08.027Z
category: muzi
image: /public/images/uploads/logo.png
sestava: J. Špinar - V. Tměj, J. Kopecký, V. Štancl, V. Novák, M. Hubáček, M.
  Chmelík, M. Ropek, V. Štěpán, D. Ondráček, F. Bureš, D. Vejrych, Z. Skala, J.
  Vít
---
Sedmé vzájemné utkání během posledních dvou let jsme pojali ukázkově přátelsky. "Nejzodpovědnější" přišli pozdě, opory si dovolenkovali (takovej Válek po bezmála 50-ti zápasech), na postech i v rozestavení se experimentovalo, emoce na bodě nula. První poločas ještě nevyzněl tak špatně, podařilo se několik kombinací, domácí nám benevolentně nechávali prostor, ale z rohů, centrů, odražených balónů, míčové převahy a pravokřídlých průniků jsme mezi tři tyče nezakončili víc než Hubáčkovou střelou z dálky. Rovněž domácí trápila finální přihrávka nebo přesnější zakončení, do jejich nejtutovější pozice prošel středem v půlce půlky Kladiva, ale z vápna těsně přestřelil. V druhé části pětačtyřicetiminutovky byli hodně vidět střídající Vít s Vejrychem, koncovka vycházela na druhého jmenovaného, který poprvé z levé strany minul, podruhé, když zakončoval ve vápně akci Nováka se Štěpánem, ho od tučného zápisného za první gól zachránila tyč.

V druhé půli jsme prvních deset minut nebyli schopní podržet balón. Pak nás zmrazil, nebo spíš dorazil, Marek Kusý, který si na pětadvaceti metrech zpracoval dlouhý nákop a levačkou z takové polootočky udělil balónu větší rychlost, než se kterou padal Špinar, a pak už jsme nebyli schopní a ochotní udělat vůbec nic. Špinar si za rozhozenou obranou natrénoval výběhy k vápnu, Novák s Vejrychem při závěrečných postupech z křídla vzali neúspěšné zakončení radši na sebe, než aby vystavili potenciálnímu trapasu v zakončení nabíhajícího kapitána. Ten si svou chvíli slávy vybral kolem 70. minuty, kdy ukázkově stehny "odvrátil" centr domácích jen k číhajícímu Flídrovi, který zblízka nezaváhal. Vyjma uvedených dvou akcí jsme se k vážnějšímu ohrožení brány nedostali, přesto (klasici by dodali - a nebo právě proto), že se na konci zápasu na hrotu objevilo stoperské duo z úvodu zápasu, a tak jsme poprvé po více než 20-ti letech odjeli z Horňáku s prázdnou. Odrazit se opravdu není od čeho, bohužel ani od toho, že jsme všichni zdraví. A protože měl náš výkon blíž ke čtenářskému kroužku než k fotbalu, zakončíme to básničkou:

Nebylo to posvícení, 

jenom běžná neděle, 

ani to však důvod není, 

hráti takhle z prdele.