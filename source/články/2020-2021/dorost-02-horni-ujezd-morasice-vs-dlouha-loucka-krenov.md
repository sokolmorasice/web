---
title: Lidí málo, bodů dost
date: 2020-09-06
category: dorost
image: /public/images/novinky/2019-2020/dorost.png
h1: Horní Újezd/Morašice vs Dlouhá Loučka/Křenov
sestava: Emanuel Hanyk - Marek Vích, Pavel Beneš, Jakub Nádvorník, Daniel Hanyk,
  Martin Tmej, Šimon Kušnír, Jan Vedral, Jakub Kvapil, Jiří Jána, Jan Pechanec,
  Jan Odehnal
goly: Jan Odehnal 2, Jiří Jána 2, Pavel Beneš
zlute: Šimon Kušnír
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533C1A/2020/Horní
  Újezd/Morašice/573da8cc-6d42-4180-827d-89e205d46ab7.html
---
I v druhém letošním zápase jsme řešili problém s počtem hráčů, proto nás, velmi pravděpodobně ne naposled, zachraňovali žáci. Všichni, kteří naskočili v průběhu zápasu, takřka bezchybně zalepili díry v sestavě a ten jediný v základu, Odehnal, už ve 4. minutě otevřel skóre. Druhou branku přidal z přímého kopu Beneš, když míč doslova procedil za záda brankáře. Soupeř nezahálel, využil chybu v naší obraně a snížil na rozdíl jedné branky, ale o poločasové skóre se slepenými brankami ve 29. a 32. minutě postaral Jána.

V druhém poločase přibylo více osobních soubojů, a hlavně na straně hostů i žlutých karet, což fotbalu na kráse nepřidalo. Asi nás ani nemusí mrzet neodpískaná penalta, když sudí po faulu ve vápně nechal běžet výhodu. Druhý poločas přinesl pouze jednu branku, když Odehnal svým druhým zásahem zápasu upravil na konečných 5:1.