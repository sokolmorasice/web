---
title: Kde jsme to přestali...
date: 2020-08-30
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Morašice/Horní Újezd vs Křenov/Dl. Loučka
sestava: Lukáš Němec, Zuzana Famfulíková, Jonáš Krška, Jakub Kvapil, Anežka
  Břeňová, Jiří Kučera, Daniel Hanyk, Vlastimil Hladík, Šimon Kušnír, Šimon
  Lněnička, Lukáš Mach
goly: Šimon Kušnír 8, Jonáš Krška 4, Jakub Kvapil, Anežka Břeňová, Šimon Lněnička
zlute: null
cervene: null
scortes: "https://scortes.rozpisyzapasu.cz/api/533E1A/2020/Morašice/Horní Újezd/933611a8-1a40-4649-9490-c8f77ae2f9e3.html"
---
První utkání nové sezony jsme odehráli proti sdruženému družstvu Křenova a Dlouhé Loučky. Po celý zápas jsme kontrolovali míč a dostávali jsme se do zakončení. Soupeř nás ohrozil jen dvakrát po našich chybách z lenosti. V utkání zazářili Kušnír s Krškou, kteří zařídili 12 gólů z celkových 15 (a to jich dalších 10 zahodili). Soupeř přijel s velmi mladou sestavou, takže zápas při vší úctě posloužil hlavně jako dobrá příprava na další utkání.

Jan Špinar