---
title: "Legendy vzpomínají - kapitola 5: Jízda na červenou"
date: 2020-12-27T08:30:54.602Z
category: obecne
image: /public/images/uploads/logo.png
---
**Bobši, áčko po postupu v roce 2009 chvíli pendlovalo mez A třídou a B třídou, ty si kromě toho pravidelně objížděl prales s [béčkem](https://www.youtube.com/watch?v=dSeIbf0ETy4). V kabině si byl pojem, legenda, ten co odehrál tisící zápas za Morašice, ten, kdo to měl rozdat ostatním. Ale když se třeba v Kunčině prohrálo 1:5, my se dostali k jedné a soupeř ke čtyřem střelám na bránu, nenapadlo tě, že by se ta odpoledne dala trávit jinak?**

**JK:** Ne, já jsem jezdil kvůli kamarádům a vždycky jsem se nejradši viděl na hřišti. A tu tisícovku jsem nijak neřešil.

**Hned po tom postupu se v roce 2010 A třídu podařilo zachránit až na [konci](https://vysledky.lidovky.cz/soutez2.php?id_soutez=5218), na to si pamatuješ?**

**JK:** Jasně, to nezapomeneš. Hattrick v 35-ti letech proti [Litomyšl](https://svitavsky.denik.cz/fotbal_region/matador-kopecky-porazil-litomysl20100615.html)i. To jsem šel na posledních dvacet minut, dal jsem tři góly, dva hlavou, všechno z Kuťákových rohů. Franta Dvořáku na lavičce Litomyšle byl nepříčetný.

**No tady připojím osobní vzpomínku já, abych trochu vysvětlil ty dva nezodpovědné hráče, kterým to Petr natřel v novinách. Tenkrát se hrálo o záchranu A. třídy, ve čtvrtek na Dolňáku a v sobotu doma s Litomyšlí. Já měl s Cvejnem dávno domluvené rafty na Vltavě, ale ve čtvrtek jsme na Dolňák po Petrově velkém naléhání zůstali. K ničemu to nebylo, protože se prohrálo, a my, k Petrově velkému zklamání, jsme se rozhodli zbytek zájezdu dohnat vlakem. Noční čekání v Praze na spoj do Vyššího Brodu nás samozřejmě těžce poznamenalo na zbytek víkendu, takže když jsme v sobotu večer ve Zlaté Koruně dostali zprávu, co se v Morašicích dělo, nevěřili jsme tomu až do nedělního rána. A s jistotou záchrany jsme jeli týden na to do [Semanína](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2009-2010/seman%C3%ADn-vs-mora%C5%A1ice), kde ses Petře poprvé loučil s kariérou, stejně jako tvůj bratr. Složili jsme Vám [báseň](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2009-2010/rekapitulace-jara-2010-a-penze), vyvěsili [dresy](https://drive.google.com/drive/folders/1EhZ62Qlsd7PgypjuFdNSJcYxnj1qH6oX),  a mohla to být dokonce vítězná rozlučka s tvým vítězným gólem, ale rozhodčí byl proti.**

**PK:** I Litomyšl i Semanín mi samozřejmě utkvěly v paměti, jen bych neřekl, že to bylo tak po sobě. Každopádně s kariérou se člověk loučí dvakrát třikrát za život, a myslím, že rozhodčí mi to taky zkazili víckrát. Ale jako Míra Albert svého času v Dolním Újezdě jsem to nevyřešil, ten poslal rozhodčího do ču\*\*\*ů, do p\*\*i, dal mu facku přes obličej až mu zarazil píšťalku do huby, a ve finále mu vypustil všechny pneumatiky od auta se slovy: „neděli mě ku\*\*\*it nebudeš“. Dostal dva nebo tři roky natvrdo, a tak tu kariéru na rozdíl ode mě opravdu ukončil. Já se tenkrát ovládnul, tak jsem se ještě mohl nechat zlomit k pokračování. Jinak u dvouletého distancu jsem byl i osobně, to byl dopolední zápas s B-týmem [Letohradu](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2011-2012/letohrad-b-vs-mora%C5%A1ice) na hřišti v Mistrovicích. Domácí byli po předchozím večírku ještě ráno pod silným vlivem omamných a psychotropních látek, tak to tam emočně nezvládali. Kléma si odnesl tržnou ránu na čele z MMA hlavičky. Rozhodčí to na začátku druhého poločasu musel ukončit, protihráč dostal tu dvouletou stopku a Josef Jokeš, který přijel až na druhý poločas, nám stihnul pogratulovat k vítězství a my mu stihli jenom proplatit cesťák.

**A byl zápas, kde ses neovládnul?**

**PK:** Při jednom ze zářezů, a taky jich nebylo málo, si pamatuji červenou ze Žamberka, tam jsem byl ještě relativně nevybouřeným mladíkem, tak jsem byl nucen říci rozhodčímu několik vyjmenovaných slov, poslal jsem ho snad na všechna slušná i neslušná místa a on mě za odměnu poslal do šatny s červenou kartou v ruce, Franta Jandáčku mě nebyl schopný uklidnit. Prohráli jsme 0:1 nebo 1:2, domácí rozhodli asi čtvrt hodiny před koncem.

**Já myslel [Králíky](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2014-2015/kr%C3%A1l%C3%ADky-%C4%8Derven%C3%A1-voda-vs-mora%C5%A1ice), kde jsem byl, ale ten popis sedí.**

**PK:** Jojo. No tam byl pan pomezní taky hodně nakloněný domácím no.

**Bobši, co dovedlo k červené přimět tebe?**

**JK:** Nespravedlnost. Protože jsem tříčtvrteční Slovák, tak mám horkou krev, i když teďka už jsem klidnější. Třeba Zemědělský pohár, střela od břevna do brány, jasný gól, rozhodčí nic, pískej ty ču\*\** a bylo.

**A to se stalo kolikrát?**

**JK:** To nevím, tak 4, 5 jsem jich doma určitě dostal.

**Čili exces, ke kterému došlo maximálně dvakrát za deset let. A za nějaký brutální nebo taktický faul jsi někdy skončil předčasně?**

**JK:** Ne, nikdy.

**Mary, byl jsi pravidelně nejžlutějším hráčem z týmu nebo si měl v tomhle ohledu nějakou zdatnou konkurenci? Já si pamatuju jednu tvoji červenou, byla nějaká předtím nebo sis ji schovával až na starý kolena?**

**MR:** Červenou já si vybavuju v Janově, tam to bylo myslím po dvou žlutých kartách, při té druhé jsem stáhl útočníka, který by šel sám na bránu, byla z toho remíza, myslím, že to bylo v roce, kdy jsme nakonec postupovali do B-třídy. Že jsem byl nejžlutějším hráčem, bylo dané asi mojí bojovností a zarputilostí, že, ač jsem s tím byl asi u soupeřů hodně neoblíbený, jsem ho radši pokopal, než aby mě utekl nebo nám dal gól. Mým nejzdatnějším konkurentem v počtu červených karet byl jednoznačně Šamot *(Tomáš Rajman -pozn. redakce)*.

**Tak u tebe jsem pro změnu myslel [Trnávku](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/mestecko-trnavka-vs-mora%C5%A1ice), ale jsem rád, že se pochlubíte.**

***Zítra nás čeká uzavření cesty z okresu tam a zase zpátky, ale kromě okresu dojde i na Německo, Španělsko, Anglii, Borušov,…***