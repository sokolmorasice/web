---
title: Koleda, koleda Štěpáne
date: 2021-03-28T13:07:41.489Z
category: muzi
image: /public/images/uploads/logo.png
h1: Morašice - Linhartice 4:1 (0:1)
---
Příznivé klimatické podmínky nás k dalšímu zápasu už pustili na náš přírodní trávník. Protože rozvolnění nastane až po Velikonocích, museli jsme areál zavřít před diváky, kteří tak byli nuceni obsadit střechu kabin, balkony bytovky, štaflemi osázeli plot antuky a ti nejvěrnější se usadili v korunách těch několika zbylých stromů na jižní tribuně. Už je to dávno, co jsme proti soupeři z Linhartic potvrdili papírové předpoklady favorita, ale letošní jaro je ideální čas pro výhru, na kterou se není třeba moc nadřít. Z naší územní převahy proti soupeři, jemuž nedáváme góly snadno, plynuly pouze rohy, jejich exekucí se do našeho ofenzivního vínku snažil přispět Štěpán, jenž taky nedává góly snadno, ale nesetkával se s pochopením nabíhajících. Hosté na nás léta praktikují styl dlouhého výkopu, těžce bránitelného prodloužení a sprintového náběhu. Po jedné takové aplikaci došlo k dosud nepochopenému úderu do [míče](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2015-2016/mora%C5%A1ice-vs-linhartice), který, ať byl myšlen jakkoliv, měl za následek hostující vedení. Rozhodli jsme se na to výrazněji nereagovat a počkat na přestávkovou poradu.

Za tónů Scooteru jsme vylezli z kabin a plácli si se Seňorem, netuše, co udělá poločasová muzika s králem našich trávníků a parketů. Štěpán odšpuntoval své představení už po pěti minutách, když se i svým 80% sprintem v pohodě prosmýkl po [lajně](https://www.youtube.com/watch?v=41IvXF747hg) (čas 1:02), ze samého rozhlížení po spoluhráčích musel mu trochu utekl [balón](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2019-2020/muzi-07-borsov-vs-morasice), ale úplně volného Nováka stihl přesně oslovit i přihrávkou z dlouhé nohy a bylo vyrovnáno. A po hodině hry už jsme vedli, když se situace opakovala, jen zbyla koncovka na Skalu. Nabyté asistence ve Štěpánovi zvedly i střelecké sebevědomí, ale když ho zchladil úspěšně trefeným [reflektorem](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2019-2020/one-forlan-show), přenechal to opět jiným. Třetí asistenci mu dopřál Ropek proměnivší hlavou alespoň jeden náš roh v zápase a v závěru zápasu ještě jeho centr z levé strany donesl do brány [Chmelík](https://www.youtube.com/watch?v=41IvXF747hg) (čas 2:52). 

**Další výsledky 12.kola okresního přeboru:**

Borová - Opatov 1:4 (1:0)

Cerekvice - Jevíčko 1:1 (0:1)

Hradec nad Svitavou - Radiměř 1:6 (1:3)

Horní Újezd - Boršov 1:1 (0:1)

Jaroměřice - Mladějov 1:0 (0:0)

Janov - Dlouhá Loučka 4:1 (2:1)

**Tabulka okresního přeboru po 12. kole:**



|     |                     |     |       |       |     |
| --- | ------------------- | --- | ----- | ----- | --- |
| 1.  | Morašice            | 12  | 9-1-2 | 37:13 | 28  |
| 2.  | Jaroměřice          | 12  | 9-1-2 | 34:16 | 28  |
| 3.  | Janov               | 12  | 9-0-3 | 31:17 | 27  |
| 4.  | Jevíčko             | 12  | 7-4-1 | 21:15 | 25  |
| 5.  | Opatov              | 12  | 7-1-4 | 46:21 | 22  |
| 6.  | Radiměř             | 12  | 5-3-4 | 29:28 | 18  |
| 7.  | Dlouhá Loučka       | 12  | 4-3-5 | 24:22 | 15  |
| 8.  | Mladějov            | 12  | 4-2-6 | 31:30 | 14  |
| 9.  | Cerekvice           | 12  | 3-5-4 | 22:31 | 14  |
| 10. | Linhartice          | 12  | 3-4-5 | 22:24 | 13  |
| 11. | Borová              | 12  | 3-2-7 | 23:35 | 11  |
| 12. | Horní Újezd         | 12  | 3-2-7 | 20:38 | 11  |
| 13. | Boršov              | 12  | 1-4-7 | 15:31 | 7   |
| 14. | Hradec nad Svitavou | 12  | 1-2-9 | 13:45 | 5   |