---
title: "Legendy vzpomínají - kapitola 6: Rohozná, Svitavy, Camp Nou"
date: 2020-12-27T19:13:45.427Z
category: obecne
image: /public/images/uploads/logo.png
---
**Kuťák nabízel během svých angažmá několikrát funkci k dispozici. A nabízel je možná slabé slovo. Mary, přemýšlel jsi, že to vezmeš už dříve nebo ses k tomu odhodlal až ve chvíli, kdy bylo vidět, že se s Kuťákem nehne?**

**MR:** Nikdy dřív jsem o tom nepřemýšlel, já jsem naopak vždycky Petra přemlouval, že ty jeho abdikace jsou nesmysl.

**Bobši, z tvých spoluhráčů se stávali trenéři, ty jsi ale nabídky nikdy neakceptoval, přestože si odkoučoval úspěšná pohárová finále. Můžeš vypíchnout hlavní důvod, proč ne?**

**JK:** Já jsem vedl lidi v práci a strašně mě to psychicky unavovalo, takže pak vést ještě něco jiného, připravovat si to věci, na to jsem neměl sílu ani pomyšlení.

**Mary, otázka, kterou už dostal Petr, jaké to je, když se ze spoluhráče staneš trenérem?**

**MR:** Já jsem to cítil naprosto přirozeně, a to z toho titulu, že jsem chtěl, aby mě ty tréninky bavili, aby to k něčemu dál v Morašicích vypadalo. Nikdy před tím jsem neměl ambice stát se trenérem.

**Po pádu do okresu se podařilo [dvakrát](<https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2015-2016/morasicexopatov_finale_poharu>) vyhrát [pohár](<https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2016-2017/fin%C3%A1le-poh%C3%A1ru>), třikrát k nám přijeli soupeři z kraje, [Přelouč](<https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/mora%C5%A1ice_p%C5%99elou%C4%8D_poh%C3%A1r>) jsme dokonce porazili, dali jsme doma desítku [Rohozné](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2016-2017/mora%C5%A1ice-vs-rohozn%C3%A1). Jak vzpomínáte na tyhle zápasy? Dokázali byste porovnat např. vítězství v poháru s tím historickým umístěním nebo zmíněným postupem do A třídy?**

**JK:** Já popravdě tu Rohoznou tak neberu, ty výsledky z kraje cením mnohem víc. Stejně jako to, že jsem dal gól [Svitavům](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2017-2018/svitavy---krajsky-pohar), to jsem běžel k Matesovi *(Štanclovi – pozn. redakce)* a říkal jsem mu, že teď už můžu skončit.

**MR:** Ten pohár je určitě super úspěch, byl jsem rád, že jsem ještě něco takového na sklonku kariéry mohl zažít, obrovská škoda, že jsme nedosáhli hattricku, ještě o to víc, že to bylo v souboji s naším největším rivalem - Cerekvicí. Ale víc pro mě byl určitě návrat do A-třídy, protože to je dlouhodobá soutěž.

**K fotbalu patří neodmyslitelně i soustředění, rozlučky, oslavy, apod... Jaké máte mimořádné zážitky mimo hřiště související s fotbalem?**

**PK:** Mimořádných bylo opravdu velmi, velmi mnoho. Například poprvé v bazénu se šampaňským v hotelu Badinka nezapomenutelné výjezdy do sklípků, [oslavy](<https://drive.google.com/drive/folders/1pWGvmNkjPXw8_5L0vvG40uqnIi8pIXFg>) [postupu](<https://drive.google.com/drive/folders/1u1VN8Vdg0X_qa0Gz9mEQdRhS1_QeKjDs>) byly moc fajn. Ale "fotbalové", to jsou teď již nemožné výjezdy za hranice, vyprodaný olympijský stadion v Berlíně, nebo úplně prázdný [Camp Nou](<https://drive.google.com/drive/folders/1oOc-H8XJILUWwaZVF4ZTQQ8b0HJwQpft>) v Barceloně. To je opravdu fotbalová paráda a genius loci. Ale ještě dva výjezdy se mi vryly do paměti. Liverpoolské derby mezi slavným FC a Evertonem a slavná hymna, kterou zpívá na začátku zápasu celý vyprodaný stadion, z toho jsem měl husí kůži. A na výjezd do Wroclavi, s bratrem na ME, se taky nedá zapomenout. Ke stadionu jsme jeli našlapanou tramvají, a on těsně před stadionem zjistil, že ho v ní okradli o peníze i lístky. A protože zápas byl vyprodaný, tak se díval jen na parkoviště plné aut a na tribuny zvenku.

**MR:** Za mě určitě první morašické soustředění v Martinicích za trenéra Portlíka, během čtvrtečního večera jsme vypili téměř všechno pivo, které mělo být na celý víkend a Ozzy *(Jiří Lněnička – pozn. redakce)*  snědl skoro celou sekanou, kterou jsme měli mít v pátek k večeři, drzého Šamota staří pardálové vyhazovali oknem a on se vždy vrátil dveřmi. Samozřejmě všichni vzpomínáme na více než dvanáctikilometrový výběh, který někteří běžci absolvovali za sedačkami stopnuté dodávky. Jinak všechna soustředění se vždy vydařili, těch míst jsme navštívili spousty, a ještě dnes si rád pustím Matesovo nádherné video ze soustředění v [Borušově](<https://www.youtube.com/watch?v=kzL-bWEi8Fs>). Všechny akce, ať už sklípky nebo zájezdy do Německa se vždy vydařili a máme spousty krásných zážitků.

**JK:** Já si snad radši na nic nevzpomínám.

***Zítra zjistíme, kteří spoluhráči utkvěli našim matadorům v paměti, ať už byl důvod jakýkoliv..***