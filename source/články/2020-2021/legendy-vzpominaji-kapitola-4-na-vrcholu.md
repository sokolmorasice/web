---
title: "Legendy vzpomínají - kapitola 4: Na vrcholu"
date: 2020-12-26T08:19:54.157Z
category: obecne
image: /public/images/uploads/logo.png
---
**Mary, dlouhá léta jsi touhle dobou ještě dospával zábavu, teď druhý rok po sobě nic. Chybí ti to?**

**MR:** Vůbec ne. Jsem rád, že to není a že mi to nekazí Vánoce.

**A z pohledu hospodáře, nechybí ti ta zábava ani po ekonomické stránce?**

**MR:** Zisky ze zábav a plesů pro nás byly vždycky důležitým zdrojem příjmů, ale teďka máme hodně šikovné kluky, kteří jsou schopni získat dotace, takže můžu říct, že si nemůžeme na finance stěžovat. Navíc na Štěpánské ubývalo lidí jak na pořadatelské, tak na divácké straně. Místo toho, abychom nad ránem v kumbálu hromadně zapíjeli povedenou akci, která má pro klub zásadní přínos, uklízeli jsme v pár lidech, víceméně pořád těch samých a ten výdělek se dal zalepit třeba z dotací nebo darů.

**Lituješ nějakých utracených peněz?**

**MR:** Nenapadá mě žádný peníz, který by mě mrzel, ale spíše taková perlička, že za Vomajdu jsme zaplatili za přestup na letošní jaro, ale díky Covidu se žádná mistrovská utkání neodehrála, takže to byly zbytečně investované peníze. Ale v žádném případě jich nelituju, protože já jsem byl moc rád, že se Vomajda vrátil do naší party.

**Fotbalově jsme skončili u té nejúspěšnější sezóny 2003/2004, jak na ni vzpomínáte?**

**PK:** Celá ta éra postupu z okresu až do A třídy je pro mě to nejlepší období v Morašicích. Ale ne jen proto, že se výsledkově dařilo, ale byla tam neskutečná parta lidí, kteří spolu drželi i mimo fotbal a užili jsme si spousta legrace a zábavy.

**JK:** Já z té doby nezapomenu na konkrétní zápas, a to když se ještě před tím vrcholem postoupilo do A. třídy a přijel k nám Srch. Kuťák se strašně těšil, že potopíme Titanic, ale poločas se prohrál 1:3 a přestávka byla hodně dusná. V druhý půlce jsme to ale otočili na 5:3, takže Petrovi se se splnilo poslání Titanicu ke dnu. A myslím, že na tom slovním spojení mu hodně záleželo.

**MR:** Pro mě je to nezapomenutelné období. Zmiňoval jsem tu povedenou střeleckou sérii, do toho se mi na podzim narodila dcera, ta sezóna neměla chybu. Kdybych si měl já z té éry vybrat zápas, tak po postupu do kraje pamatuju zápas s Dolním Újezdem, když jsme tady*(sic!)* v Újezdě vyhráli 3:1 nebo 4:1. Já jsem hlavou po rohu trefil tyč a Franta Ropku to dorazil. Mimochodem mě mrzí, že jsem dodneška nedal Újezdu žádný gól. Ale ten zápas byl památný hlavně z toho titulu, že na tenhle zápas se jelo z Morašic autobusem, v kravatách, a odjíždělo se s vítězstvím, takže to bylo něco velkého v té době. Jestli si dobře pamatuju, tak se jednalo o první derby v krajské soutěži, pro nás obrovský svátek, autobus troubil přes celý Újezd a pak v Morašicích se skandovalo z okének. Byli jsme po tom týden za hvězdy.

**Petře napadalo tě delší dobu, že bys trénoval, nebo to v zimě 2005 po odchodu Franty Jandáčka bylo momentální rozhodnutí?**

**PK:** To, že bych měl trénovat, mě nikdy nenapadlo. Bylo to momentální rozhodnutí, protože úplně hned nebyl nikdo k mání a lidi z okolí mě přemlouvali, ať Sokolu pomohu a zkusím to.

**Ty jsi byl tou dobou kapitán?**

**PK:** Kapitánem jsem asi byl, myslím po Pepovi Kopeckém.

**JK:** Tenkrát se kapitán volil myslím co půlrok, tam se to tak nebralo.

**A jaké to bylo, když se ze spoluhráče staneš trenérem? Ty si to v opačném gardu zažil už předtím s Frantou Jandáčkem.**

**PK:** Když se staneš z hráče hned trenérem, je to divné, ale když se staneš hrajícím trenérem, je to špatně. A s Frantou Jandáčkem to bylo úplně v pohodě, nepamatuji si na sebemenší problém.

**Mary, jaké to bylo, když se ze spoluhráče Kuťáka stal trenér Kuťák?**

**MR:** Bylo to naprosto přirozené, Petr měl vždycky obrovskou autoritu v týmu, takže bylo jasné, že on je ten pravý, kdo má trénovat, a myslím si, že vždycky ten mančaft za ním šel a důvěřoval mu.

**Ta sezóna 2003/2004 byla takovým pomyslným vrcholem, umístění se pak postupně zhoršovalo. Mary, mančaftům kolem tebe se do té doby dařilo a ty jsi snad poprvé hrál v týmu, který se musel obávat sestupu, což se v roce 2007/2008 opravdu stalo. Čím myslíš, že to bylo?**

**MR:** Z Morašic odešel trenér, odešli s ním další hráči, ať už Hanyket *(Jan Štěpán - pozn. redakce)* a Jirka Vosmeku do České Třebové, dále skončili někteří starší hráči a prostě ta základna morašických hráčů byla logicka úzká, tak pak už na to herně nebylo, neměli jsme na to dostatečně kvalitní mančaft, abychom mohli hrát důstojně dál A třídu, což vyvrcholilo sestupem 2007/2008.

**JK:** Dva roky po tom Srchu, o kterým jsem mluvil, jsme u nich dostali 9:1, Cvejn *(Jan Němec – pozn. redakce)* říkal, že už tam nikdy chytat nepojede. A pak později ještě 10:1 ve [Valech](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2011-2012/valy-vs-mora%C5%A1ice), to jsou ty zápasy, kdy tam prostě spadne všechno, a na které bys radši nevzpomínal. To si taky myslím vyžral Cvejn.

**To si zalovil Préša. Bobši ty jsi byl v té sestupové sezóně nejlepší střelec, ale zároveň jsi celou první dekádu trénoval mládežnické týmy. Mary zmínil tu slabší základnu. Je někdo, komu jsi tenkrát věřil, že bude pokračovat až do áčka, případně, že v něm vydrží déle, ale nepovedlo se to?**

**JK:** Rosa *(Milan Chadima – pozn. redakce)*. Ten se s tím narodil, ale pak se přestěhoval do Budislavi a ani nevím, kde mu je dneska konec. A třeba Kokyn nebo Pýtí *(Josef Kyncl, resp. Petr Novák – pozn. redkace)*, o těch jsem si myslel, že by na něco v áčku měli, ale ty měli jiné zájmy *(hokej, resp. chlast – pozn. redakce)*.

**Petře, ty si vzpomínáš ze svých trenérských let na nějakého svěřence nebo ročník, ve kterém jsi během trénování viděl budoucnost a nic z toho nezbylo?**

**PK:** Asi ano, těch bylo taky poměrně dost, ale každý si může vybrat tu svoji cestu a já ji určitě nikomu nechtěl brát.

**Mary, když tou dobou nevyšel krajský přebor v Morašicích, tak jsi vyslyšel volání nejvyšší krajské soutěže na půl půlsezóny v Dolním Újezdě. Nemyslím to nijak posměšně, ale ukázalo ti to něco? Např. že na to nemáš? Nebo že na to máš, ale nechceš / nemůžeš být bez morašické kabiny?**

**MR:** Situace byla taková, že jsem byl možná fotbalově v nejlepších letech a chtěl jsem si alespoň jednou v životě krajský přebor zahrát a Újezd ambice postoupit také měl. Před podzimní sezónou jsem oznámil Petrovi, že chci jít do DÚ, absolvoval jsem tam 14 dní tréninků, pak jsem měl sezení s Petrem a Frantou Ropkem, kteří zapůsobili na moje morašické srdíčko a přemluvili mě, že do Újezda půjdu až na posledních 5 zápasů podzimu, s čímž jsem nakonec souhlasil. Takže začátek podzimu jsem odehrál Morašicích, výsledky byly v B třídě takové průměrné, posledních pět kol jsem odehrál v Újezdě. Do té doby DÚ neprohrál ani jedno utkání, v mém prvním jsme hned hráli derby v Litomyšli a dostali nařezáno. V Újezdě jsem si nemohl na nic stěžovat, ale v hlavě jsem si tam srovnal, že patřím do Morašic, a že fotbal mám hrát tam, kde mám fotbalové srdíčko. Na jaro jsem se vrátil a hned jsme se dokázali postupem vrátit do A-třídy, takže si myslím, že i tohle moje krátké dolnoújezdské extempore nakonec lehce pomohlo našemu návratu o soutěž výš. V posledním zápase sezóny jsme potřebovali vyhrát nad Mýtem „B“ a to byly obrovské emoce. [Zápas](https://svitavsky.denik.cz/fotbal_region/vesela-nedele-v-morasicich-sokol-slavi-postup.html) se nevyvíjel úplně optimálně, prohrávali jsme gólem Štancla, toho vysokomýtského, takže ne vlastním, poté zavelel Bojda k obratu a dokázali jsme to urvat a vyhráli jsme 6:1.

***Nejsvátečnější dny jsou za námi, tak se Pánbu určitě nebude zlobit, když si zítra popovídáme mj. o červených kartách.***