---
title: Tak jsme se tu všichni sešli, co myslíš, osobo?
date: 2020-09-07T07:34:39.717Z
category: obecne
image: /public/images/uploads/logo.png
h1: SG Morašice - SG Hradec Králové     3:3, na pen. 5:5
sestava: Michal Prešinský, Jiří Novák – Tomáš Andrle, Václav Beneš, Martin
  Burián, Jiří Famfulík, Jindřich Jána, Josef Jána, Josef Jokeš, Josef Kopecký,
  Miloš Kopecký, Pavel Kuta, Petr Kuta, Petr Nováček, Pavel Nováček, František
  Ropek, Oldřich Ropek, Ladislav Štancl, Jan Štěpán, Josef Štěpán, Petr Štěpán,
  Jan Vedral, Jiří Vosmek, Luboš Vosmek
goly: Jan Štěpán 2, Josef Kopecký
multimedia: https://drive.google.com/drive/folders/1YzI9uK4wf-wmrLifbb7UP9b6cly4gac_
---
Zlatým hřebem oslav bylo utkání starých gard. Zatímco naše sestava, kterou poskládali GM Petr Kuta a Martin Ropek, nepostrádala lesk, co do jmen i záře dresů, Hradec Králové bohužel nejznámější jména nasadit nemohl a jejich dresy už pár zápasů pamatují. Což se samozřejmě v dobrém projevilo, hostující souhra měla nad našimi slavnými a nablýskanými jmény navrch, příliš platné nám nebyly ani sourozenecké dvojice, ani trojice, ani velezkušení trenéři Jan Klofanda se Zdeňkem Krejzou. Že naším nejstarším hráčem bude nějaký Ropek starší šedesáti let, konkrétně Oldřich, se dalo čekat stejně jako to, že o fous mladší František Ropek vydrží celý zápas bez střídání. Brankář Jiří Novák asi během slavnostních ceremonií příliš nestudoval povětrnostní podmínky, protože první hradecký gól jde čistě za tím, že střela z dálky před ním neskutečně zaplavala. Druhý gól už jde za umem hostí, další minimálně trojí zazvonění brankové konstrukce zase vypovídá o gólmanových stavebních zkušenostech. Ne že by naši starogvardějci hráli úplně druhé housle, jen byla brána Hradce na vzdálenější straně, tak se ty šance hůře identifakují. Byly tam, ale mužstvo se muselo srovnat s tím, že hlavní cíl, dostat se ve zdraví do výčepu, se rozpadl poměrně záhy. Zatímco Petr Kuta ze hry odstoupil tiše a bez fanfár, samostatným pádem zakončený náběh Miloše Kopeckého ničí pozornosti neunikl. Ale měl to aspoň i s potleskem, houkačkou a díky zapůjčeným trenkám s námi zůstal ještě po část večera. Nepřehlédnutelná byla i pilka jménem Josef Jána, který na hřišti lítal jako zamlada. Další moment, který byl na vzdálenější straně hřiště bezpečně vidět, obstaral Josef Kopecký, který prošel po straně a předložil míč Janu Štěpánovi, jenž ho zvedl přes padajícího brankáře a stanovil poločasové skóre.

K hospodě se našim útočilo přece jen lépe, což dokládá jak zvýšený počet střel (povětšinou mimo bránu), tak odmávaný ofsajd. Vyrovnat se podařilo až Josefu Kopeckému, který vyplaval sám před gólmanem, udělal mu dlouhou kličku a z úhlu míč uklidil do sítě. V naší bráně se druhý poločas představil benjamínek staré gardy Michal Prešinský, který musel zaskočit za starší absentující gólmany (s výjimkou Jana Němce ze zdravotních důvodů), a ten dlouho držel hradecké střelce na uzdě. Přesto se hradečtí po centru zleva a gólovém zakončení ještě jednou dostali do vedení. To vymazal až těsně před závěrečným hvizdem Jan Štěpán, který si tváří v tvář počkal na gólmanovo posazení a střelou do prázdné brány namlsal publikum na penaltový rozstřel.

S tím hradečtí evidentně nepočítali nebo se jim do toho nechtělo, a tak po čtyřech stoprocentních sériích, kdy za nás proměnili Václav Beneš, Pavel Kuta, Ladislav Štancl i Luboš Vosmek, nastalo obligátní přemlouvání. Hradecký hráč nakonec šel s hlavou odkrytou a šel bosý, a stejně proměnil, Josef Kopecký taky vyslyšel volání spoluhráčů a s hlavou vlasatou a nohou obutou srovnal. A protože se nikoho dalšího ukecat nepodařilo, skončilo to smírně i po rozstřelu.

Sestava HK: Karel Podhajský, Jiří Poděbradský, Rosťa Macháček, Milan Ptáček, Karel Krejčík, Luděk Jelínek, Radek Hrubý, Jaroslav Dvořák, Pavel Dapecí, Jan Brendl, Jaroslav Plocek, David Franc, Michal Lesák, Radek Langr, Josef Frýda, Matěj Frýda.

Trenéři HK: Jan Rolko, Luděk Pečenka