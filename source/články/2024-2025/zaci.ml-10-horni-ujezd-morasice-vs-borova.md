---
title: Bod na závěr
date: 2024-11-03
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Borová
sestava: Lukáš Mach - Marek Renza, Lukáš Tichý, Markéta Lněničková, Magdalena
  Drábková, Patrik Vukmirovič, Viktor Špás, Julie Vodehnalová, David Boček,
  David Klusoň, Tereza Hlušičková, Matyáš Lněnička, Tereza Veselíková, Jaroslav
  Chaun, Šimon Vrbica, Jonáš Rovner, Jan Langr, Dominik Prokop
goly: Tereza Hlušičková, David Klusoň
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2024/Horní
  Újezd/Morašice/5216f684-17b1-416f-aec4-72354a00f0e4.html
---
V posledním utkání podzimu jsme hostili družstvo z Borové. Po dlouhé době se nám sešla skoro kompletní sestava dětí, a tak byl trochu problém dopřát všem dostatečný herní čas a zároveň udržet tempo a úroveň hry po celou dobu. Ve vyrovnaném zápase se soupeř v prvním poločase ujal vedení, ale dvěma rychlými góly po přestávce jsme vývoj otočili. Borové se podařilo vyrovnat pro nás velmi nešťastnou brankou. Přes šance na obou stranách už se vítězného gólu žádný tým nedočkal a remíza působí jako spravedlivý výsledek, který nás po podzimu udrží v lepší polovině soutěže.