---
title: S nulou a grácií
date: 2024-11-02T19:33:14.552Z
category: zaci.st
image: /public/images/uploads/logo.png
sestava: Filip Jadrný - Sára Vomočilová, Matěj Kovář, Barbora Kučerová, Jan
  Kvapil, David Čapek, Michal Kovář, Daniel Křivka, Šimon Flídr, Josef Rosypal,
  Šimon Dřínovský, Nikita Izzy Ondrůšek, David Klusoň
goly: Jan Kvapil 2, Šimon Dřínovský
---
Do zápasu jsme vstoupili dobře a už ve třetí minutě jsme vedli zásluhou slabší nohy Honzy. I přes naši územní převahu jsme v prvním poločase další gól nepřidali. Ve druhém poločase jsme přidali dva góly a v pohodě si pohlídali vítězství.

Opět musím pochválit děti za hezký fotbal, který nám předvádí celý podzim. A vyzdvihnout i Filipa, který to jistí v bráně a i když nemá úplně moc práce, dokáže v případě potřeby vytáhnout skvělé zákroky.

K poslednímu podzimnímu zápasu jedou starší v sobotu do Trnávky autobusem, zřejmě i s velkou podporou mladších a rodičů.

Pa﻿vel Beneš (Horní Újezd)