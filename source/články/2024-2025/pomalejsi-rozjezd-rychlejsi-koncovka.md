---
title: Pomalejší rozjezd, rychlejší koncovka
date: 2024-10-26T18:18:43.992Z
category: zaci.st
image: /public/images/uploads/logo.png
sestava: Filip Jadrný - Sára Vomočilová, Matěj Kovář, Barbora Kučerová, Jan
  Kvapil, David Čapek, Michal Kovář, Daniel Křivka, Šimon Flídr, Josef Rosypal,
  Šimon Dřínovský, David Klusoň
goly: David Klusoň 3, Barbora Kučerová, Jan Kvapil, Šimon Flídr, Sára
  Vomočilová, Daniel Křivka
---
Zápas jsme začali naší herní převahou, ale většinou střel jsme trefovali jenom rukavice soupeřova brankáře. Teprve v polovině prvního poločasu jsme se mohli radovat. Do kabin jsme šli s náskokem dvou gólů. V druhém poločase jsme už začali přidávat branky výrazně rychleji. A i přes spoustu zahozených příležitostí jsme vysoko vyhráli. Děti nám zase předvedly hezký fotbal a jako každý týden si užily vítězné oslavy v kabině.

Pavel Beneš (Horní Újezd)