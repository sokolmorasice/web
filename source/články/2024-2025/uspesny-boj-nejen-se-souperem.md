---
title: Úspěšný boj nejen se soupeřem
date: 2024-10-02T05:44:45.294Z
category: zaci.st
image: /public/images/uploads/logo.png
h1: Vendolí - Horní Újezd / Čistá
sestava: Filip Jadrný, Vojtěch Štěpán, Matěj Kovář, Jaroslav Chaun, Jan Kvapil,
  David Čapek, Michal Kovář, Samuel Vomočil, Daniel Křivka, Šimon Flídr, David
  klusoň, Nikita Ondrůšek, Šimon Dřínovský
goly: David Klusoň 2, Šimon Dřínovský 2, Vojtěch Štěpán, Šimon Flídr
---
Ve Vendolí jsme byli od první chvíle týmem dominujícím ve všech směrech a brzy vedli o dva góly. Na přelomu poločasů jsme polevili, čehož soupeř využil, stejně jako svojí "zvyklosti" na domácí terén, který nám dělal problémy, a dvěma slepenými góly vyrovnal. Na to kluci zareagovali zlepšenou hrou a uklidňujícími čtyřmi fíky v soupeřově síti.

M﻿artin Kovář