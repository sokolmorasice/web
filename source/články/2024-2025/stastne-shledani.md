---
title: Šťastné shledání
date: 2024-10-20T14:57:44.792Z
category: zaci.st
image: /public/images/uploads/logo.png
sestava: Filip Jadrný - Sára Vomočilová, Matěj kovář, Barbora Kučerová, Daniel
  branda, jan Kvapil, David čapek, Michal Kovář, Samuel Vomočil, Daniel křivka,
  Šimon Flídr, Josef Rosypal, Nikita Izzy Ondrůšek, Šimon Dřínovský, David
  Klusoň
goly: Barbora Kučerová 2, Jan Kvapil 2, David Klusoň, Šimon Flídr, Daniel Branda
---
Na posledního soupeře ze Sebranic jsme byli dobře připraveni a takřka v plné sestavě. Všichni hráči si moc dobře uvědomovali, že hrajeme s týmem, který nás moc dobře zná z minulé sezony, kdy jsme hráli společně. To se potvrzovalo i na hřišti, ale jen počátečních 10 minut. Poté se povedla krásná souhra našich děvčat, kdy Sára krásně vyslala do volného prostoru Báru a ta nedala brankáři žádnou šanci. Následně to odšpuntovali i ostatní kluci a převrátili zápas v jednoznačnou záležitost. Do bezchybného zápasu nám chyběla jen nula vzadu, o kterou jsme přišli v posledních minutách po nedorozumění ve vlastních řadách a zápas skončil naší výhrou 7:1.

Martin Kovář