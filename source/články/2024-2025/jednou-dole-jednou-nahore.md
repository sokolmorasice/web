---
title: Jednou dole, jednou nahoře
date: 2024-10-26T18:26:45.130Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Starší přípravka

##### **26.10.2024 Březová nad Svitavou**

Odložený a zároveň poslední podzimní turnaj v Březové nad Svitavou se nám nevydařil ani herně, ani výsledkově. Dokonce to i vypadalo, že si neužijeme ani gólové radosti, ale aspoň této malé ceny útěchy jsme se v posledním poločase dočkali.

**S﻿estava:** Matěj Vrbica, Daniel Hlušička, Lukáš Poslušný, Lukáš Chmelík, Jan Klement, Jiří Famfulík, Matyáš Tóth, Jiří Kubík, Ivo Langr

**Morašice** - Hradec nad Svitavou 0:9 (0:3)

Březová nad Svitavou - **Morašice** 3:0 (2:0)

**Morašice** - Čistá 2:7 (0:6)

**Branky:** Ivo Langr, Matěj Vrbica

**Tabulka turnaje:**

1.Čistá (9 bodů, skóre 17:5)

2.Hradec nad Svitavou (6 bodů, skóre 16:7)

3.Březová nad Svitavou (3 body, skóre 4:8)

4.Morašice (0 bodů, skóre 2:19)

## Mladší přípravka

##### **26﻿.10.2024 Němčice**

Stejně jako minulý týden jsme předvedli vynikající výkon. Na hřišti všichni makali a výsledek se dostavil. Mám radost jednak z toho, že naše opory si udržují stabilní výkonnost, ale také z toho, že na hřišti jsou stále více vidět jejich mladší kamarádky a kamarádi. Máme za sebou poslední turnaj velice vydařené podzimní části. Pevně doufám, že nás fotbal bude bavit stejně i v začínající halové sezoně.

**S﻿estava:** Jiří Famfulík - Vít šimek, Timotej Vomočil, Lukáš Chmelík, Filip Šplíchal, Marie Flídrová, Ondřej Sýkora, Antonín Flach, Nikola Kovářová, Kateřina Kovářová, Jiří Voženílek, Sofie Nováková

**Morašice/Horní Újezd** - Bystré  8:0 (2:0)

Poctivý výkon na úvod turnaje.

**Branky:** Lukáš Chmelík 2, Vít Šimek 2, Ondřej Sýkora, Jiří Voženílek, Filip Šplíchal, vlastní

Němčice - **Morašice/Horní Újezd**  3:6 (3:1)

Po vysoké výhře z prvního zápasu jsme vstoupili do utkání v přílišné pohodě, nadšeně bojující domácí toho využili a dostali se do vedení 3:0, my jsme ale zvedli hlavu a předvedli nevídaný obrat.

**Branky:** Vít Šimek 4, Lukáš Chmelík 2

**Morašice/Horní Újezd** - Čistá  2:0 (1:0)

V napínavém zápase o první místo na turnaji jsme měli větší vůli po vítězství.

**Branky:** Lukáš Chmelík, Vít Šimek

**Tabulka turnaje:**

1.Horní Újezd/Morašice (9 bodů, skóre 16:3)

2.Čistá (6 bodů, skóre 8:8)

3.Němčice (3 body, skóre 10:13)

4.Bystré (0 bodů, skóre 4:14)

Zdeněk Beneš (Horní Újezd)