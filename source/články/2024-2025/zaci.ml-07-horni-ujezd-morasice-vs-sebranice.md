---
title: Prohra se ctí
date: 2024-10-12T22:00:00.000Z
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Sebranice
sestava: "Lukáš Mach - Miriam Vomočilová, Lukáš Tichý, Tereza Veselíková,
  Magdalena Drábková, Julie Vodehnalová, Samuel Vomočil, David Boček, David
  Klusoň, Tereza Hlušičková, Jakub Hlušička, Jaroslav Chaun, Damián Záleský,
  Antonín Krejsa, Patrik Vukmirovič, "
goly: David Klusoň
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2024/Horní
  Újezd/Morašice/bd93cda4-e5fe-4657-89f2-376957adffad.html
---
Den před zápasem to vypadalo na problémy se sestavou, a tak se povolaly děti ze starší přípravky. Nakonec se ale některé děti uzdravily a na zápas jsme odjeli v 15-ti lidech. Nasazení i hra byli o sto procent lepší než minulý týden, ale na soupeře to i tak nestačilo, i proto, že jsme chtěli hráče protočit a nechat zahrát i ty menší. Ale i přes prohru velmi cením výkon, ze kterého se dá pro příště odrazit.

Jiří Lněnička