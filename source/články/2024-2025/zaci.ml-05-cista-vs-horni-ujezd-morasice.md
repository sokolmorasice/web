---
title: Zbytečně těsné
date: 2024-09-29
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Čistá vs Horní Újezd/Morašice
sestava: Lukáš Mach, Marek Renza, Lukáš Tichý, Markéta Lněničková, Magdalena
  Drábková, Patrik Vukmirovič, Viktor Špás, Julie Vodehnalová, Samuel Vomočil,
  David Boček, David Klusoň, Tereza Hlušičková, Jan Langr, Jaroslav Chaun, Šimon
  Vrbica, Damián Záleský, Jonáš Rovner, Antonín Krejsa
goly: David Klusoň 2, David Boček
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2024/Horní
  Újezd/Morašice/3c9cad31-7c9c-4585-a4eb-7fa13b5e8096.html
---
Zatímco domácí Čistá šla na začátku do vedení z ojedinělé akce, naše vyrovnání o chvíli později byl zaslouženým výsledkem dosavadního obrazu hry. Celý poločas se hrál prakticky na jedné polovině, poločasový stav mohl klidně být 4:1, ale k větší efektivitě nám chyběl drajv. Když jsme se brzy po přestávce konečně dostali do vedení, zdálo se, že soupeř nemá na odpověď, ale po chybě v naší obraně se mu podařilo lacině srovnat. Pak to byla stejná písnička jako v prvním poločase, ale 2 minuty před koncem se nám zaslouženě, ale vlastně šťastně podařilo překlopit zápas na naši stranu.

J﻿iří Lněnička