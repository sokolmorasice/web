---
title: Povedený rozjezd
date: 2024-09-12T16:42:40.307Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Starší přípravka

##### **8﻿.9.2024 Němčice**

**S﻿estava:** Jiří Famfulík, Daniel Hlušiča, Lukáš Chmelík, Matouš Jetmar, Jan Klement, Jiří Kubík, Ivo Langr, Lukáš Poslušný, David Soukup, Matěj Vrbica

###### Janov - **Morašice** 4:3

###### **Morašice** - Hradec nad Svitavou 1:7

###### Němčice - **Morašice** 2:7

**B﻿ranky:** Matěj Vrbica 4, Lukáš Chmelík 3, Matouš Jetmar 2, Dan Hlušička, vlastní

**Tabulka turnaje:**

1. Hradec nad Svitavou
2. Janov
3. Morašice
4. Němčice

## Mladší přípravka

##### **11﻿.9.2024 Borová**

**S﻿estava:** Lukáš Chmelík, Jan Klement ml., Jiří Famfulík, Pavel Kubík, Filip Šplíchal, Timotej Vomočil, Marie Flídrová, Jan Kroulík, Nikola Kovářová

###### Borová - **Horní Újezd/Morašice** 0:8

Po celkem vyrovnaném prvním poločase jsme o osudu utkání rozhodli krátce po pauze.

**B﻿ranky:** Lukáš Chmelík 3, Jan Klement 2, Tim Vomočil 2, Filip Šplíchal

###### **Horní Újezd/Morašice** - Sebranice 8:3

Zasloužené vítězství, zápas jsme měli po celou dobu pod kontrolou.

**B﻿ranky:** Lukáš Chmelík 3, Tim Vomočil 2, Jiří Famfulík 2, Jan Klement

###### **Horní Újezd/Morašice** - Čistá 3:5

V zápase o vítězství na turnaji jsme velkou část první půle vedli, ale před pauzou soupeř skóre otočil a pak si brzy vypracoval bezpečný náskok.

**B﻿ranky:** Jan Klement, Jiří Famfulík, Lukáš Chmelík

**Tabulka turnaje:**

1. Čistá
2. Horní Újezd/Morašice
3. Borová
4. Sebranice

Přestože šlo o první společné setkání, vstup do nové sezóny se nakonec vydařil nad očekávání. Zkušenější hráči tým táhli a mladší se jim svým nasazením snažili přiblížit. Takto by to mělo vypadat. Doufejme, že se nám na dnešní výkon podaří navázat v dalších turnajích.