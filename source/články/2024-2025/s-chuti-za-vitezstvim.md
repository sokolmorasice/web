---
title: S chutí za vítězstvím
date: 2024-09-22T14:25:20.786Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Mladší přípravka

##### **22﻿.9.2024 Horní Újezd**

**S﻿estava:** Jiří Famfulík, Lukáš Chmelík, Jan Klement, Pavel Kubík, Filip Šplíchal, Vít Šimek, Timotej Vomočil, Marie Flídrová, Nikola Kovářová, Antonín Flach, Jan Kroulík

###### **Horní Újezd/Morašice** - Opatov 9:0

**B﻿ranky:** Lukáš Chmelík 3, Vít Šimek 2, Jan Klement, Filip Šplíchal, Timotej Vomočil, Antonín Flach

###### **Horní Újezd/Morašice** - Sebranice 15:0

**B﻿ranky:** Vít Šimek 7, Jiří Famfulík 2, Lukáš Chmelík 2, Timotej Vomočil 2, Jan Klement, Filip Šplíchal

###### **Horní Újezd/Morašice** - Březová nad Svitavou 11:1

**B﻿ranky:** Timotej Vomočil 3, Jan Klement 2, Jiří Famfulík, Filip Hájek, Lukáš Chmelík, Filip Šplíchal, Vít Šimek, Antonín Flach

**Tabulka turnaje:**

1. Horní Újezd/Morašice
2. Opatov
3. Sebranice
4. Březová nad Svitavou

I když výsledky nejsou v této kategorii to nejpodstatnější, tohle zahřeje u srdíčka. Domácí turnaj naše ratolesti jednoznačně ovládli, a to navíc se skóre 35:1. Jen houšť takových turnajů.

## S﻿tarší přípravka

##### 1﻿8.9.2024 Morašice

**M﻿orašice - Borová 2:14**

S﻿tarší přípravka si místo víkendového turnaje zahrála středeční zápas na domácím hřišti s Borovou, díky čemuž měli aspoň trenéři k dispozici i kluky z mladší přípravky, a ti naopak nemuseli běhěm víkendu absolvovat dva turnaje. Nicméně rozdíl v kvalitě byl i kvůli našemu doplnění mladšími ročníky příliš velký.

**S﻿estava:** Daniel Hlušička, Matouš Jetmar, Jiří Kubík, Ivo Langr, Lukáš Poslušný, David Soukup, Matěj Vrbica, Lukáš Chmelík, Jan Klement, Jiří Famfulík

**B﻿ranky:** Matouš Jetmar, Matěj Vrbica