---
title: Na vítězné vlně
date: 2024-09-22T14:40:41.865Z
category: zaci.st
image: /public/images/uploads/logo.png
sestava: Filip Jadrný - Sára Vomočilová, Matěj Kovář, Barbora Kučerová, Daniel
  Branda, Jan Kvapil, David Čapek, Michal Kovář, Samuel Vomočil, Daniel Křivka,
  Šimon Flídr, Josef Rosypal, David Klusoň, Nikita Izzy Ondrůšek, Šimon
  Dřínovský
goly: Šimon Flídr 3, Šimon Dřínovský
---
Po volném týdnu jsme měli premiéru v Čisté. Oba poločasy jsme začali naprosto stejně špatně, po našich chybách nás soupeř potrestal gólem. Pro děti to byl jenom signál k zvýšení tempa a rychlému otočení výsledku. Zápas nebyl takové představení jako v Koclířově. Bylo k vidění dost špatných přihrávek, ale také moc hezkých akcí, které baví je i diváky. Poděkovani patří i rodičům. Byli všichni, mimo těch, kteří měli povinnosti u hasičů, nebo na jiném fotbale.

Jiří Lněnička