---
title: Těsně a krutě
date: 2024-09-01
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Bystré vs Horní Újezd/Morašice
sestava: Matěj Vrbica - Marek Renza, Lukáš Tichý, Markéta Lněničková, Dominik
  Prokop, Julie Vodehnalová, Samuel Vomočil, David Boček, David Klusoň, Tereza
  Hlušičková, Jan Langr, Jaroslav Chaun, Šimon Vrbica, Lukáš Mach, Jonáš Rovner,
  Viktor Špás
goly: null
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2024/Horní
  Újezd/Morašice/7acc0746-846d-4868-a174-a44490308497.html
---
Podruhé v tomto týdnu jsme si zajeli zahrát do Bystrého. Bohužel jsme oba zápasy prohráli. Většinu mistrovského utkání jsme byli spíše lepším týmem, měli jsme poměrně hodně střel, ale z dosti nepřipravených pozic. O těsné porážce rozhodl trestný kop po faulu, který by rozhodčí možná ani nepískl, kdyby na sebe narazili stejně velcí hráči.

Pavel Beneš (Horní Újezd)