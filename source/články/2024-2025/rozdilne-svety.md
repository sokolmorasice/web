---
title: Rozdílné světy
date: 2024-10-02T05:50:20.557Z
category: pripravka
image: /public/images/uploads/logo.png
---
## St. přípravka

**2﻿8.9. 2024 - Morašice**

Domácí turnaj jsme zakončili s bilancí jedné výhry a dvou porážek, o kterých rozhodly druhé poločasy.

**Sestava:** Daniel Hlušička, Matouš Jetmar, Jiří Kubík, Ivo Langr, Lukáš Poslušný, Jiří Famfulík, Lukáš Chmelík, Jan Klement

**Morašice** - Němčice 5:3 (3:1)

**Morašice** - Třebařov 3:7 (3:3)

**Morašice** - Dlouhá Loučka 1:4 (0:1)

**Branky:** Jan Klement 5, Lukáš Chmelík, Jiří Kubík, Ivo Langr

## Ml. přípravka

**2﻿9.9.2024 - Sebranice**

**Sestava:** Lukáš Chmelík, Jan Klement, Jiří Famfulík, Pavel Kubík, Jiří Voženílek, Tim Vomočil, Marie Flídrová, Jan Kroulík, Nikola Kovářová, Vít Šimek, Antonín Flach

Místo klasického turnaje jsme zajeli pouze k zápasu do Sebranic. V oficiální části jsme zvítězili **13:2**, stejný výkonnostní rozdíl panoval i v pokračujícím "přátelském" utkání. A to jsme ještě domácí hráče šetřili.

**Branky:** Vít Šimek 4, Tim Vomočil 2, Jiří Voženílek 2, Lukáš Chmelík, Jan Klement, Marie Flídrová