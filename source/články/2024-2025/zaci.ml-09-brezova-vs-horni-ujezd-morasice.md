---
title: Na poslední chvíli
date: 2024-10-27
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Březová vs Horní Újezd/Morašice
sestava: Lukáš Mach - Jan Langr, Matyáš Lněnička, Markéta Lněničková, Dominik
  Prokop, Jonáš Rovner, Šimon Vrbica, David Boček, David Klusoň, Marek Renza,
  Jaroslav Chaun
goly: David Klusoň 2
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2024/Horní
  Újezd/Morašice/05b0e783-7de7-4f8b-b440-20e3920f17a2.html
---
V Březové nás čekal soupeř, který za námi zaostával v tabulce pouze o 3 body a vyrovnanost hry tomu zcela odpovídala. Hrálo se sice za naší územní převahy, ale do velkého množství vyložených šancí nás pozorný domácí blok nepouštěl, naopak jeho brejky do naší otevřené obrany nás musely držet v opatrnosti. Z tohoto obrazu hry vyplynuly dva góly, když domácí dokázali po přestávce odpovědět na gól do šatny v podání Davida Klusoně, který pak v poslední minutě zápasu svou druhou trefou dokázal zlomit výsledek na naši stranu. V posledním zápase sezóny nás v neděli v Morašicích čeká další tabulkový soused z Borové.