---
title: Zimní příprava
date: 2024-12-22T21:29:12.534Z
category: muzi
image: /public/images/uploads/logo.png
---
PO	06.01.2025		UMT Dolňák	fotbálek???

PO	13.01.2025		UMT Dolňák	fotbálek???

ČT	16.01.2025	17:30	ZŠ Morašice	trénink

ÚT	21.01.2025	18:00	ZŠ Morašice	trénink

ČT	23.01.2025	17:30	ZŠ Morašice	trénink

ÚT	28.01.2025	18:00	ZŠ Morašice	trénink

ČT	30.01.2025	17:30	ZŠ Morašice	trénink

ÚT	04.02.2025	18:00	ZŠ Morašice	trénink

ČT	06.02.2025	17:30	ZŠ Morašice	trénink

ÚT	11.02.2025	18:00	ZŠ Morašice	trénink

ČT	13.02.2025	17:30	ZŠ Morašice	trénink

**PÁ	14.02. - NE 16.02.            Daňkovice      soustředění**

ÚT	18.02.2025	18:00	ZŠ Morašice	trénink

**PÁ	21.02.2025	19:00	UMT LITOMYŠL	trénink**

**SO	25.02.2024	12:00	UMT LITOMYŠL	PU - Luže**

ÚT	25.02.2025	18:00	ZŠ Morašice	trénink

**PÁ	28.02.2025	19:00	UMT LITOMYŠL	trénink**

**SO	01.03.2025	11:00	UMT Litomyšl	PU - Sruby**

ÚT	04.03.2025	17:30	hřiště Morašice	trénink

ČT	06.03.2025	17:30	hřiště Morašice	trénink

**SO	08.03.2025	11:00	UMT Litomyšl	PU - Jehnědí**

ÚT	11.03.2025	17:30	hřiště Morašice	trénink

ČT	11.03.2025	17:30	hřiště Morašice	trénink

**NE	16.03.2025	14:00	hřiště Krouna	PU - Krouna**

ÚT	18.03.2025	17:30	hřiště Morašice	trénink

ČT	20.03.2025	17:30	hřiště Morašice	trénink

***NE	22.03.2025	15:00	M. Třebová	       V***

*NE	30.03.2025	15:00		Vendolí	D*

*SO	05.04.2025	16:00		Dlouhá Loučka	V*

*NE	13.04.2025	16:30		Pomezí	V*

*SO	19.04.2025	16:30		Sebranice	V*

*NE	27.04.2025	17:00		Horní Újezd	D*

*NE	04.05.2025	17:00		Dolní Újezd "B"	V*

*ČT	08.05.2025	17:00	semifinále poháru	???	???*

*SO	10.05.2025	17:00		Jevíčko	V*

*NE	18.05.2025	17:00		Mladějov	D*

*SO	24.05.2025	17:00		Opatov	V*

*NE	01.06.2025	17:00		Hradec nad Svitavou	V*

*SO	07.06.2025	17:00		Polička "B"	V*

*SO	14.06.2025	17:00		Boršov	D*