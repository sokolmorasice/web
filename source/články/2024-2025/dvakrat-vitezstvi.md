---
title: Dvakrát vítězství
date: 2024-09-12T16:27:49.502Z
category: zaci.st
image: /public/images/uploads/logo.png
---
##### **31.8.2024 Horní Újezd/Čistá - Sebranice   2:0**

K prvnímu zápasu přijely Sebranice, se kterými část týmu hrála minulou sezonu. Na začátku byla na dětech vidět nervozita, že hrají proti bývalým spoluhráčům. A určitě se projevila i únava z noci v hotelu Průchod. Ovšem většinu zápasu jsme byli lepším týmem. První dvě branky sezóny jsme vstřelili až ve druhém poločase a konec zápasu jsme už v pohodě dohráli.

Pavel Beneš (Horní Újezd)

**S﻿estava:** Filip Jadrný - Sára Vomočilová, Matěj Kovář, Barbora Kučerová, Daniel Branda, Jan Kvapil, David Čapek, Michal Kovář, Samuel Vomočil, Daniel Křivka, Šimon Flídr, Šimon Dřínovský, František Flídr

**B﻿ranky:** Šimon Dřínovský, Barbora Kučerová



##### **7.9.2024 Koclířov - Horní Újezd/Čistá   1:10**

Toto jednoznačné vítězství asi není třeba více rozepisovat.

**S﻿estava:** Filip Jadrný - Sára Vomočilová, Matěj Kovář, Barbora Kučerová, Nikita Izzy Ondrůšek, Jan Kvapil, David Čapek, Michal Kovář, Samuel Vomočil, Daniel Křivka, Šimon Flídr, Josef Rosypal, David Klusoň, Šimon Dřínovský, František Flídr

**B﻿ranky:** Šimon Flídr 3, Šimon Dřínovský 3, Jan Kvapil 3, David Klusoň