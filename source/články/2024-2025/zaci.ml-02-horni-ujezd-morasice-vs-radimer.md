---
title: Zasloužená výhra
date: 2024-09-08
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Radiměř
sestava: Lukáš Mach - Marek, Renza, Markéta Lněničková, Dominik Prokop, Julie
  Vodehnalová, Samuel Vomočil, David Boček, David Klusoň, Tereza Hlušičková, Jan
  Langr, Jaroslav Chaun, Šimon Vrbica, Antonín Krejsa, Jonáš Rovner
goly: Jaroslav Chaun, Marek, Renza, David Boček, Tereza Hlušičková, David Klusoň
zlute: David Klusoň
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2024/Horní
  Újezd/Morašice/300d0c91-2fd5-4d6e-baa4-3eedbec01dca.html
---
Téměř po celý zápas jsme měli jasnou převahu. V prvním poločase jsme se dostali dvakrát do vedení, ale soupeř pokaždé dokázal z ojedinělé šance odpovědět. Ve druhém poločase už to byla hra na jednu bránu a po třech velkých neproměněných šancí jsme se dokázali prostřílet až ke konečnému výsledku 5:2.