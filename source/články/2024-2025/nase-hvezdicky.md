---
title: Naše hvězdičky
date: 2024-10-02T06:06:38.578Z
category: obecne
image: /public/images/uploads/logo.png
---
N﻿euškodí ani připomenout, jak se daří našim mládežnickým vyslancům ve vyšších až nejvyšších patrech mládežnických soutěží.

**V﻿ojtěch Huryta**, který v létě přestoupil do FK Pardubice, naskočil do všech zápasů 1.ligy žáků U14, v týmu dosud stoprocentního lídra nastoupil většinou v základní sestavě, ve zbylých byl mezi prvními náhradníky. Odkaz na soutěž [zde](https://www.fotbal.cz/souteze/turnaje/hlavni/4f94943d-0f53-4702-acad-12cdeb36e7c5)

**M﻿artina Famfulíková** hájila branku FK Pardubice dosud ve všech 4 odehraných zápasech české 1.ligy starších žákyň s bilancí dvou výher, dvou porážek a 7 inkasovaných gólů. Její soutěž najdete [zde](https://www.fotbal.cz/souteze/turnaje/hlavni/5eb0f207-0430-422b-b327-9fb52096f0ec).

**Daniel Válek** letos ve Vysokém Mýtě hraje krajský přebor starších žáků, skupinu A, kam tato kategorie loni spadla. V dosavadních 5 zápasech tam nastřádal 6 gólů, viz statistiky soutěže [zde](https://www.fotbal.cz/souteze/turnaje/hlavni/7f594d29-a244-4f24-9112-76df62c4c6b0).

D﻿ěkujeme jim za reprezentaci našeho oddílu a přejeme i nadále radost ze hry a dosažených výsledků.