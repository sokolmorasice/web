---
title: Takhle by to šlo
date: 2024-10-19T15:19:34.939Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Starší přípravka

##### **19.10.2024 Morašice**

Na domácím hřišti se jednalo o jeden z nejlepších turnajů, které jsme letos odehráli. Měli jsme pouze jednoho hráče na střídání, což nám paradoxně pomohlo. Kluci byli mnohem více vtaženi do hry, každý si zahrál na více postech a byla na nich vidět radost ze hry. Podařilo se nám několik pohledných kombinací včetně gólového zakončení, pochvalu si zaslouží všichni hráči za bojovnost a fyzickou aktivitu v průběhu všech zápasů.

**S﻿estava:** Daniel Hlušička, Matouš Jetmar, Jiří Kubík, Ivo Langr, Matěj Vrbica, Lukáš Chmelík, Jan Klement

**Morašice** - Sebranice 2:3 (0:1)

První zápas s pozdějším vítězem turnaje jsme prohráli pouze o gól, zápas to byl vyrovnaný a rozhodovaly maličkosti. 

**Branky:** Lukáš Chmelík, Matěj Vrbica

**Morašice** – Horní Újezd 1:1 (1:1)

Šance byly na obou stranách, my jsme bohužel neproměnili několik slibných příležitostí v prvním poločase a následně dostali zbytečný gól. V druhém poločase jsme už více bránili a nakonec byli spokojeni s remízou.

**Branky:** Lukáš Chmelík

**Morašice** - Němčice 7:2 (3:1)

Závěrečný zápas jsme si už pohlídali, i když tak jednoznačný jako konečný výsledek rozhodně nebyl. 

**Branky:** Jan Klement 3, Matouš Jetmar 2, Ivo Langr, Matěj Vrbica

**Tabulka turnaje:**

1.Sebranice (9 bodů, skóre 15:8)

2.Horní Újezd (4 body, skóre 12:6)

**3.Morašice (4 body, skóre 10:6)**

4.Němčice (0 bodů, skóre 6:23)

## Mladší přípravka

##### **19﻿.10.2024 Radiměř**

Dnešní turnaj se vydařil jak výsledkově, tak herně. Když pominu nepřesvědčivý první poločas s Bystrým, troufnu si říct, že jsme se přiblížili maximu, co v této sestavě můžeme hrát. Ocenit chci především naše elitní obranné duo Vítek Šimek – Tim Vomočil, které hrálo ve velké intenzitě, kluci stíhali bránit, ale také neúnavně útočit. V bráně se zdárně zabydlel Jířa Famfulík. Pochvalu za úspěšný turnaj ale samozřejmě zaslouží i všichni ostatní.

**S﻿estava:** Jiří Famfulík - Vít šimek, Timotej Vomočil, Filip Šplíchal, Marie Flídrová, Jiří Voženílek, Kateřina kovářová, Sofie nováková, Antonín Flach, Filip Hájek, Filip Hájek

**Morašice/Horní Újezd** - Bystré  8:4 (3:2)

První poločas byl z naší strany ospalý, dvakrát jsme prohrávali, dvakrát jsme dokázali vyrovnat, krátce před pauzou jsme poprvé dostali do vedení, které jsme v druhém dějství postupně navyšovali.

**Branky:** Vít Šimek 4, Marie Flídrová 2, Timotej Vomočil, Sofie Nováková

**Morašíce/Horní Újezd** - Radiměř  9:0 (7:0)

Do zápasu jsme vletěli jako vítr, minulý týden jsme s tímto soupeřem bojovali o vítězství až do konce, tentokrát bylo rozhodnuto brzy, těší nula vzadu.

**Branky:** Vít Šimek 4, Timotej Vomočil 3, Marie Flídrová, Jiří Voženílek

**Tabulka turnaje:**

1.**Morašice/Horní Újezd**

2.Bystré

3.Radiměř