---
title: Otočka sem, otočka tam
date: 2024-09-11
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Jevíčko/Jaroměřice
sestava: Lukáš Mach - Matěj Vrbica, Lukáš Tichý, Markéta Lněničková, Dominik
  Prokop, Patrik Vukmirovič, Julie Vodehnalová, Samuel Vomočil, David Klusoň,
  Tereza Hlušičková, Jan Langr, Jaroslav Chaun, Šimon Vrbica, Antonín Krejsa,
  Jonáš Rovner
goly: Markéta Lněničková, Dominik Prokop, Lukáš Tichý
zlute: David Klusoň
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2024/Horní
  Újezd/Morašice/a9cc0361-603d-4597-8149-bf818e8e2bbf.html
---
Proti společnému družstvu Jevíčka a Jaroměřic jsme na hřišti v Morašicích měli především v prvním poločase mírnou převahu, kterou jsme přetavili ve vedení 1:0. To však nevydrželo dlouho, protože soupeř rychle ,a celkem lacině, vyrovnal a dokonce vývoj zápasu otočil. Do konce poločasu se nám podařilo vyrovnat, a po obrátce si vzít vedení zpět. To jsme bohužel ve druhé půli neudrželi, když soupeř vyrovnal po laciné penaltě, ale náš brankář tuto penaltu odčinil v poslední minutě, kdy vytáhl přímý kop soupeře na břevno a zajistil nám tak alespoň bod.