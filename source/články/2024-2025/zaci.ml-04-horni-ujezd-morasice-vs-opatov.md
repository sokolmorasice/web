---
title: Zápas pod kontrolou
date: 2024-09-21
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Opatov
sestava: Lukáš Mach - Marek Renza, Martin Šplíchal, Markéta Lněničková,
  Magdalena Drábková, Patrik Vukmirovič, Viktor Špás, Samuel Vomočil, David
  Boček, David Klusoň, Tereza Hlušičková, Jan Langr, Jaroslav Chaun, Šimon
  Vrbica, Jonáš Rovner
goly: David Boček 2, David Klusoň 2, Samuel Vomočil, Šimon Vrbica
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2024/Horní
  Újezd/Morašice/27730d2a-132b-475c-82c8-b90d9061ceb4.html
---
V utkání, které se kvůli velké vodě přesunulo z Horního Újezda do Morašic, jsme byli po většinu zápasu lepším týmem. Tomu nasvědčuje i zasloužené poločasové vedení 4:0. Do druhé půle jsme ale trochu polevili a dovolili soupeři snížit na rozdíl dvou branek. Čtyřgólový náskok jsme si ale vzali zpět a dokráčeli tak k vítězství 6:2.