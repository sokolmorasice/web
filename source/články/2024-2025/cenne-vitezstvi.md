---
title: Cenné vítězství
date: 2024-10-06T18:13:35.516Z
category: zaci.st
image: /public/images/uploads/logo.png
sestava: Filip Jadrný - Sára Vomočilová, Matěj Kovář, Barbora Kučerová, Daniel
  Branda, Jan Kvapil, David Čapek, Michal Kovář, Daniel Křivka, David Klusoň,
  Josef Rosypal, Nikita Izzy Ondrůšek, Šimon Dřínovský
goly: Šimon Dřínovský 2
zlute: Daniel Křivka
---
V zápase bylo vidět, že na sebe narazily týmy z popředí tabulky. Oba soupeři chtěli hrát a k vidění byl hezký fotbal. Po remízovém poločase jsme skórovali hned po přestávce a chvíli před koncem pojistili druhým gólem. Výhra je cennější o to, že jsme opět hráli bez dvou nemocných kluků ze základní sestavy.

Pavel Beneš (Horní Újezd)