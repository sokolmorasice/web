---
title: Úspěšná derniéra
date: 2024-11-24T13:49:40.347Z
category: pripravka
image: /public/images/uploads/logo.png
---
Poslední turnaj tohoto kalendářního roku jsme odehráli v hale poličského gymnázia. Turnaj byl rozdělen na dvě části, my jsme se zúčastnili odpoledního dějství, které bylo výrazně kvalitněji obsazeno, mimo jiné jsme se utkali s týmy ze Svitav nebo Poličky. Ostudu jsme určitě neudělali – spíše naopak. Celý tým předvedl poctivý, bojovný výkon, kdy všechna střídání byla od všech stoprocentně odkmakána. Z jednotlivců se mi dnes nejvíc líbil Honza Klement, který v obraně působil jako zeď a navíc byl nebezpečný i v ofenzivě. Na turnaji se nezapisovaly výsledky a nevyhlašovalo se konečné pořadí, ale statistika je statistika:

**Sestava**: Jiří Famfulík – Jan Klement, Filip Šplíchal, Jiří Voženílek, Pavel Kubík, Vít Šimek, Timotej Vomočil, Marie Flídrová, Antonín Flach

**Z﻿ápasy:**

**Horní Újezd/Morašice** - Polička černá 1:3

**branka**: Jan Klement

**Horní Újezd/Morašice** – Svitavy černá 1:1

**branka**: Jan Klement

**Horní Újezd/Morašice** – Svitavy žlutá 5:0

**branky:** Jan Klement, Timotej Vomočil, Vít Šimek, Pavel Kubík, vlastní

**Horní Újezd/Morašice** - Polička bílá  1:0

**branka:** Vít Šimek

**Horní Újezd/Morašice** – Bystré 1:0

**branka:** Filip Šplíchal

**Horní Újezd/Morašice** – Čistá 2:1

**branka:** Vít Šimek, Jan Klement

Zdeněk Beneš