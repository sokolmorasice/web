---
title: Šťastné a veselé
date: 2024-12-22T21:34:55.235Z
category: obecne
image: /public/images/uploads/logo.png
---
TJ Sokol Morašice přeje ještě jednou všem svým členům, trenérům, partnerům, pomocníkům i příznivcům klidné svátky, a stálé zdraví i spoustu osobních radostí a úspěchů v novém roce. Fotbalové rozloučení se současným rokem proběhne tradičně na Silvestra od 10:00 na našem hřišti. Bude nás více nebo méně než loni (33+3)?