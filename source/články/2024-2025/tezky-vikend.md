---
title: Těžký víkend
date: 2024-10-05T21:26:14.294Z
category: pripravka
image: /public/images/uploads/logo.png
---
## M﻿ladší přípravka

**5﻿.10.2024 - Morašice**

N﻿a domácím turnaji nám i kvůli výpomoci starší přípravce chyběli ti nejlepší hráči, takže zápasy byly hlavně o otrkávání dětí, jejichž čas přijde v příštích ročnících.

**S﻿estava:** Ondřej Sýkora – Vít Šimek, Marie Flídrová, Nikola Kovářová, Jan Kroulík, Filip Šplíchal, Jiří Voženílek, Pavel Kubík, Katěřina Kovářová, Sofie Nováková, Jakub Tomaš

**Horní Újezd/Morašice** – Březová 2:11 (1:6)

**Branky**: Jakub Tomaš, Jiří Voženílek

**Horní Újezd/Morašice** – Jevíčko 2:13 (1:5)

**Branky:** Vít Šimek, Filip Šplíchal

**Horní Újezd/Morašice** – Čistá 0:15 (0:7)

**Tabulka:**

1.Čistá (skóre 30:7, 9 bodů)

2.Březová (skóre 20:15, 6 bodů)

3.Jevíčko (skóre 20:13, 3 body)

**4.Horní Újezd/Morašice** (skóre 4:39, 0 bodů)

## Starší přípravka

**5﻿.10.2024 - Třebařov**

Na turnaji v Třebařově jsme sestavu lepili pomocí mladších elévků a jejich snaživý výkon stačil na jednu výhru nad domácím týmem.

**Sestava:** Jan Klement, Lukáš Chmelík, Jiří Famfulík, Matouš Jetmar, Ivo Langr, Matyáš Tóth, Jiří Kubík

**Morašice** - Třebařov 2:1 (1:1)

**Branky:** Ivo Langr, Lukáš Chmelík

**Morašice** - Jevíčko 0:16 (0:7)

**Morašice** - Hradec nad Svitavou 0:8 (0:3)