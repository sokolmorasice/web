---
title: Zbytečně moc
date: 2024-10-05
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Němčice/Sloupnice
sestava: Lukáš Mach - Jan Langr, Markéta Lněničková, Jonáš Rovner, Šimon Vrbica,
  David Boček, David Klusoň, Tereza Hlušičková, Julie Vodehnalová, Marek Renza,
  Jaroslav Chaun
goly: David Boček 3, David Klusoň 2, Markéta Lněničková
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2024/Horní
  Újezd/Morašice/e372692b-29b6-47af-a8f5-985d7594c79a.html
---
Zápas proti spojenému družstvu Němčice a Sloupnice rozhodl první poločas, který jsme prohráli žalostně 1:7,  za což mohla jak soupeřova vysoká efektivita, tak náš trestuhodný (ne)zápal pro hru. Začátek druhého poločasu a tři rychle vstřelené góly představovaly jistou naději na zdramatizování utkání, ale poté přišla rychlá odpověď soupeře, který si nakonec pohlídal vítězství s celkovým skóre 6:12. Příště to bude chtít mnohem víc chuti a bojovnosti.

J﻿iří Lněnička