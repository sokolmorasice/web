---
title: Jen trošku štěstí
date: 2024-10-12T16:05:12.163Z
category: pripravka
image: /public/images/uploads/logo.png
---
## M﻿ladší přípravka

**12﻿.10.2024 - Březová nad Svitavou**

Jako již téměř tradičně v neúplné sestavě jsme se zúčastnili dalšího turnaje v Březové. Odehráli jsme tři vyrovnané zápasy s bilancí dvě porážky a jedno vítězství. K lepšímu výsledku nám chyběla troška štěstíčka, dneska nám to do branky moc nepadalo. Je škoda, že nám po solidním úvodu vzala vítr z plachet vynucená změna v defenzivě.

**S﻿estava:** Jiří Famfulík, Vít Šimek, Timotej Vomočil, Filip Šplíchal, Marie Flídrová, Ondřej Sýkora, Kateřina Kovářová, Jiří Voženílek, Nikola Kovářová, Antonín Flach

Březová nad Svitavou - **Horní Újezd/Morašice** 6:4 (4:1)

**Branky**: Timotej Vomočil 2, Jiří Famfulík, Kateřin Kovářová

**Horní Újezd/Morašice** – Borová 1:4 (1:1)

**Branky:** Timotej Vomočil

**Horní Újezd/Morašice** – Radiměř 4:3 (2:2)

**Branky:** Timotej Vomočil 2, Marie Flídrová, Vít Šimek

**Tabulka:**

1.Radiměř

2.Borová

3.Horní Újezd/Morašice

4.Březová nad Svitavou

Pavel Beneš (Horní Újezd)