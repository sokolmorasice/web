---
title: Stále bez ztráty kytičky
date: 2024-10-13T15:14:51.734Z
category: zaci.st
image: /public/images/uploads/logo.png
sestava: Filip Jadrný - Sára Vomočilová, Matěj Kovář, Barbora Kučerová, Jan
  Kvapil, David Čapek, Michal Kovář, Daniel Křivka, Šimon Flídr, Josef Rosypal,
  David Klusoň, Nikita Izzy Ondrůšek, Šimon Dřínovský
goly: Josef Rosypal 2, Nikita Izzy Ondrůšek, Jan Kvapil, vlastní
---
V dalším zápase o čelo tabulky jsme byli po většinu zápasu lepším týmem. Soupeře jsme přehrávali naší tradiční hrou po zemi. Kluci z Němčic naopak vepředu byli lepší ve vzduchu. Nakonec jsme slavili poměrně vysoké vítězství a děti si opět v kabině s chutí zazpívaly.

Pavel Beneš (Horní Újezd)