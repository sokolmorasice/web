---
title: Podzimní ohlédnutí
date: 2024-11-29T22:38:50.697Z
category: obecne
image: /public/images/uploads/logo.png
multimedia: https://drive.google.com/drive/folders/1CKdGia70WN7rLed-IjIP1qxEVojuGhwu
---
Pro rok 2024 je na fotbalovém hřišti dobojováno. Než si přečtete hodnocení uplynulého půlroku očima trenérů mládeže, rádi bychom jim i touto formou poděkovali, protože ve fotbalovém oddíle prakticky není záslužnější činnosti. Do mistrovských zápasů zasáhlo za podzim 35 dětí ve věku 7-15 let (pro srovnání to loni bylo 31 dětí). Ke starším žákům se se svými kluky posunul Martin Kovář, k mladším nastoupil Jiří Lněnička, o přípravky se starají Milan Chmelík, Milan Klement a Jiří Famfulík, který k tomu ještě rozvíjí pohyb a chuť k němu u těch nejmenších, což je dalších přibližně 12 dětí navíc. Dík patří i Miloši Kopeckému, který se po letech u žáků ujal role řidiče našich dorostenců. Přejeme všem hodně síly a chutě do jejich práce, budiž jim odměnou nebo motivací působení Vojty Huryty a Marti Famfulíkové v ligových soutěžích v dresu FK Pardubice. 

**Mladší přípravka**

Po poměrně úspěšné sezóně 2023/2024 jsme se z důvodu menšího počtu dětí, kdy nám několik hráčů odešlo do starší přípravky, rozhodli spojit síly v souklubí s týmem Horního Újezdu. Začali jsme trénovat po letní prázdninové pauze na začátku září. Tréninky jsou v sezóně pravidelně v pondělí a středu společně se starší přípravkou. První turnaj jsme odehráli netradičně ve středu 11. září v Borové, kde jsme si poprvé otestovali spolupráci s hráči HÚ. Vše klapalo na výbornou a dařilo se, po těsné prohře s Čistou jsme skončili druzí. Za podzim jsme odehráli celkem 7 turnajů, resp. 18 zápasů, z nichž jsme 12 dokázali dovést do vítězného konce. 

3 hráči se pravidelně účastnili zápasů starších přípravek. Místo nich jsme zapojili děti z fotbalové školičky, které sbíraly své první zkušenosti z "mistrovských" zápasů. 

**Starší přípravka**

Starší přípravka začala trénovat spolu s mladší na začátku září a hned 8. jsme si mohli poměřit síly se soupeři na prvním turnaji v Němčicích. Po odchodu několika stěžejních hráčů do kategorie žáků nás zajímalo, jak na tom budeme. Skončili jsme třetí po jednom vítězství a 2 prohrách, což nebylo špatné. Zápasy se hrají v počtu  5+1 a vzhledem k dlouhodobější nepřítomnosti několika dětí jsme byli nuceni povolat 3 hráče z mladší přípravky, nakonec na celý podzim. Kluci se velmi dobře zapojili. 

Celkem jsme se zúčastnili 7 turnajů s následující bilancí: 16 odehraných zápasů z nichž jsme 4 vyhráli, jednou remizovali a 12x prohráli. Vzhledem k věkovému složení týmu to nejsou špatné výsledky. Za nejlepšími týmu okresu jsme zaostávali, ale věříme, že s postupem času budou rozdíly mizet. Přes zimu chceme pilně trénovat a na jaře soupeře řádně potrápit.

Přes zimu budeme trénovat v tělocvičně místní ZŠ. V pondělí a středu společně mladší a starší přípravka, úterý je vyčleněno pro fotbalovou školičku. Začátky tréninků vždy od 17h. Srdečně zveme všechny nové zájemce. Kromě tréninků nás určitě čekají i nějaké halové turnaje, ale jejich rozpis zatím není znám.

*Milan Chmelík*

**Mladší žáci**

Do nové sezóny jsme vstoupili opět spojení s Horním Újezdem a s nezvykle velkým počtem dětí. Z něho musíme mít samozřejmě radost, ale zároveň je potřeba zmínit, že je to často oříšek. Hrajeme soutěž 7+1 (na šířku velkého hřiště), ale byly i zápasy, na kterých se nám sešlo 18 dětí. Při takové rotaci je samozřejmě složité jednak zajistit spokojenost všech, jednak udržet tempo a kvalitu zápasu, i proto u nás s výsledným podzimním 5.místem (17 bodů z 10 zápasů) panuje spokojenost.

*Jiří Lněnička*

**Starší žáci**

Letošní sezónu jsme opět v trojklubí, tentokrát s Čistou místo Sebranicemi. Naše "spoluúčast" se bohužel smrskla na pouhé tři kluky, ale díky "Horňáku" se nemění mimořádně šikovná kostra týmu, který už před dvěma roky ovládly přebor mladších žáků, loni skončil o bod druhý a letos na podzim byl v deseti zápasech stoprocentní. Děti si díky skvělé partě i silným individualitám umí poradit jak v soubojích 1 na 1, tak jako tým a my trenéři se jim do toho snažíme mluvit, jen když je opravdu třeba, protože z předchozích let víme, čeho jsou schopní.

*Martin Kovář*

**Dorost**

Naši dorostenci, pro tento rok Lukáš Němec, Lukáš Mach a Jan Kopecký, jsou stejně jako loni a stejně jako kluci ze Sebranic, součástí dorosteneckého družstva Horního Újezdu, který nakonec skončil v okresním přeboru Chrudimi, kde mají kluci možnost hrát klasický velký fotbal. Sezónu jsme začínali se sedmnácti hráči na soupisce, ale během prázdnin jsme nestihli žádné přátelské utkání, takže jsme do ní vlétli na konci prázdnin naostro. V zápasech nám vypomáhali i starší žáci, především Jan Kvapil. První zápasy jsme se sehrávali, od větších porážek náš často uchránil Lukáš Němec. Třetí zápas jsme poznali, že i soupeř se dá porazit, další zápasy jsem střídali prohry s výhrami. Ze zdravotních i jiných důvodů se pak začaly kupit omluvenky, ale kluky to nijak neodradilo, myslím, že mají dobrou partu, ve které budou dále pokračovat. Po podzimu jsme s 12 body na osmém místě. 

*Miloš Kopecký*

**Muži**

Na první pohled mají za sebou muži úspěšný podzim, když skončili na 2. místě okresního přeboru, postoupili do jarních bojů okresního poháru a vyhráli 10 z celkových 15 zápasů. Na druhý pohled stálo neskutečné komunikační úsilí, abychom trénovali alespoň jednou týdně a abychom se na zápasy vůbec scházeli, čestná výjimka asi třem utkáním. Před sezónou odešel do Dolního Újezdu Michal Kopecký, dlouhodobé zdravotní problémy vyřadily několik hráčů (Ropek, Vít, Novák, Nádvorník, J. Štancl), někteří mají jiné priority a někteří během léta nebo podzimu prostě přestali chodit a komunikovat, což je pro trenéra a koneckonců i kabinu ta nejmíň pochopitelná varianta. Právě děravé sestavy a nulová možnost zásahu do zápasu stojí podle mě za jedinými prohrami s Pomezím a Opatovem, ve spojení s kvalitou soupeřů. Naštěstí z dorostu přišel Tomáš Huryta, v průběhu podzimu se vrátil po dlouhé době Jan Pitra a naštěstí máme dost gardistů, kteří se jednou nebo vícekrát nechali přemluvit na výpomoc (Burián, Chmelík, Vosmekovi). Tak se stalo, že se ve zmíněných 15 zápasech protočilo těžko uvěřitelných 28 fotbalistů, ale pouze 12 jich absolvovalo více než polovinu. Tohle úzké jádro se ale obdivuhodně semklo a z mého pohledu se oproti minulému roku zvýšil počet těch zápasů, které jsme díky bojovnosti zvládli. Za všechny vypíchnu derby s nabitou sestavou "dolňácké" rezervy nebo výhru nad Dlouhou Loučkou. Dalšími důvody k nejlepšímu podzimnímu bodovému zisku od našeho pádu do okresu jsou podle mě pevná defenziva (pouze 13 minut chybějící a výjimečně chybující Černohorský + výborná brankářská dvojice), neúnavný kapitán D. Štancl ve středu pole (3 góly, všechny vítězné, jednoznačný vítěz týmové ankety o nejužitečnějšího hráče), ofenzivní standartní situace (téměř polovina gólů) a gólová produktivita Bureše (téměř třetina gólů). Ten vstřelil v podzimních mistrovských utkáních celkem 11 gólů (10 v přeboru, 1 v poháru), což se za celou zmapovanou historii Sokola povedlo pouze Standu Hanusovi na podzim 1979 ve III. třídě.

Když se tímto přesuneme k číslům, druhým nejlepším střelcem byl T. Huryta s 5 góly, na 3 góly se dostali D. a V. Štanclovi spolu se Z. Skalou. Ten byl s pěti asistencemi nejlepším nahrávačem a s 8 body (3+5) 3. nejproduktivnějším hráčem právě střelci Burešem (11b.-11+0) a Hurytou (9b. - 5+4). Kompletní statistiky najdete [zde](https://docs.google.com/spreadsheets/d/12RDBybdIfwAnptau3mS_vyLCJLjbg6utglt7vGg450E/edit?gid=89346912#gid=89346912) nebo v galerii článku.

Můj dík za podzimní část patří Lubovi Rusnákovi za vedení tréninků, všem těm hráčům, na které byl s účastí spoleh, stejně jako těm, na které spoleh být neměl, ale jednorázovou pomoc neodmítli. V tomto ohledu patří zvláštní dík, a vlastně i obdiv, J. Kopeckému za to, že patří stále do té první skupiny i za pomoc s trenérským vedením zápasů. Děkuji také všem fanouškům, kteří si nás na podzim užili víc než dost (10 domácích zápasů), doufám, že jsme jim povětšinou dělali radost a že tomu tak bude i na jaře, kdy si nás zdaleka tolik neužijí.

*Václav Štancl*

Kromě zmíněných trenérů bychom za podporu a spolupráci poděkovali obci Morašice, díky za příspěvky patří i okolním obcím, díky za spolupráci posíláme i TJ Horní Újezd a jejím trenérům, vděční jsme i za každého rodiče, který nám pomůže s dopravou nebo jiným zajištěním chodu oddílu, stejně tak jsme rádi za dobře fungující občerstvení na hřišti. Jeden zápas nám v tomto roce přece jenom zbývá, a to tradiční silvestrovský fotbálek, na který jsou v poslední den v roce od 10:00 zváni všichni, co si troufnou po svátcích na trochu pohybu. Pro děti, co by si troufly na více pohybu, budou naši trenéři ve všední dny nového roku v tělocvičně, každé pondělí a středu tam sportují děti ročníků 2014-2017, každé úterý ti ještě mladší, vždy od 17:00. Neváhejte a přijďte se s dětmi podívat. Stejně tak máme dveře otevřené pro všechny dospívající a dospělé, kteří by nám chtěli pomoct jinak, práce je pořád dost.

Závěrem TJ Sokol Morašice přeje všem členům, příznivcům i ostatním Morašákům klidné a příjemné prožití vánočních svátků, a hodně štěstí a zdraví v novém roce, ve kterém náš fotbalový oddíl oslaví 55 let.