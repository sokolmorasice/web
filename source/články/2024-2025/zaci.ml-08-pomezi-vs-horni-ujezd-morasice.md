---
title: Nejdřív práce, potom zábava
date: 2024-10-19
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Pomezí vs Horní Újezd/Morašice
sestava: Lukáš Mach - Lukáš Tichý, Markéta Lněničková, Viktor Špás, Julie
  Vodehnalová, Samuel Vomočil, David Boček, David Klusoň, Tereza Hlušičková, Jan
  Langr, Jaroslav Chaun, Šimon Vrbica, Matyáš Lněnička, Dominik Prokop
goly: Jaroslav Chaun 3, David Klusoň 3, Šimon Vrbica (pen.)
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2024/Horní
  Újezd/Morašice/36563f66-5609-4c62-8672-d351ab413d3f.html
---
Soupeř z Pomezí pro nás byl velkou neznámou. Ve vyrovnaném prvním poločase jsme jako první prohrávali, těsně před poločasem dokonali obrat, ale po chybě v naší obraně jsme dovolili domácím bleskově srovnat. Očekávání vyrovnaného druhého poločasu vzalo po dvou našich dvou rychlých gólech za své, soupeř odpadl a zbytek zápasu se hrál prakticky na jednu bránu, což se projevilo i na konečném skóre.

Jiří Lněnička