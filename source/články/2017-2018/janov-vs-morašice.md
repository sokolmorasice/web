---
title: BuBo útok
date: 2018-04-07
scortes: https://scortes.rozpisyzapasu.cz/api/533A1A/2017/Morašice/bda2467b-574f-4eba-8907-e591505d4a64.html
image: /public/images/zapasy/janov-morasice.png

sestava: J.Špinar - D.Štancl, M.Ropek, M.Klement, V.Novák - V.Tměj (M.Janypka), M.Štancl, M.Chmelík, Z. Skala (M.Sokol) - B.Válek, K.Vopařil (T.Holomek)
goly: M.Klement
zlute: V.Novák
---

<p>Po týdnu se vítr uklidnil a nás v sobotu přivítal vedoucí celek přeboru, v čele s velice dobře připraveným trávníkem. Už na podzim jsme se na vlastní kůži přesvědčili, že není radno čekat, co tento soupeř vymyslí, protože mu to v útoku střelecky pálí. Chybama se člověk učí a nemyslím si, že tuhle uděláme potřetí. Po pětadvaceti minutách to bylo skoro na ručník a to mělo ještě jedenáct z nás kliku, že se na morašické "herní pojetí" nemuselo koukat z ochozů. V 7. minutě využil Wejda svých pár kilogramů k přetlačení obranné dvojice a odcentroval před branku, kde z bezprostřední blízkosti otevřel skóre hlavou Bureš. O 10 minut později šel Novák do souboje nohou místo tělem a rozhodčí posoudil jeho počínání jako nedovolené. Boháček přestřelil zeď a plachtící Špinar byl o fous krátký. Ve 24. minutě Martin Štancl přebral špatně míč a brejk z poloviny hřiště dorážel do poloprázdné branky Novotný. Chaos, rozpaky a ztráty míče vedly k tomu, že i samotní hráči se na hřišti museli pýřit. Chvílemi to vypadalo spíš na amatérský trénink krasobruslařů, kterým na led kdosi hodil merunu. Přesto, že jsme podali poločas hrůzy, mohli jsme z minima vytěžit dvě branky. Chmelík a Válek se ocitli ve vyložených šancích, kterým však scházelo lepší řešení v zakončení. Nutno dodat, že i soupeř nadále pokračoval v převaze a Špinar několika zákroky udržel už tak bídné skóre.</p>

<p>Ve druhé půli jsme si zkusili zahrát na Slávku a nebýt toho, že předvádíme lamí zakončení, tak se nám to povedlo. Již v 50. minutě prodloužil Holomek Tmějův aut do vápna a po několika odrazech snížil se štěstím patičkou Klement. To mělo zřejmě za následek mírné "připosrání" domácích a nám se otevřel dostatek možností jak upravit počet branek na naší straně. Najednou se dařila kombinace, vyhrávali jsme osobní souboje a tolik standartních situací jsme zahrávali naposledy před týdnem. Jen Beránek v brance neměl pořád co dělat. Štancl a Novák pálili v největších šancích mimo, prvně jmenovaný dokonce několikrát. Na druhé straně se v ojedinělém úniku ocitl Bureš, ale Špinar jeho zpracovaní včasným výběhem zasedl.</p>

<p>Prohráli jsme zápas opět v jeho úvodu. V sobotu nás čeká derby s Cerekvicí, tak zkusíme znovu nezaspat.</p>
