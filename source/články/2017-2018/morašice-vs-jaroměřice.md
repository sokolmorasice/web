---
title: Šetřme góly na finále
date: 2018-06-10
scortes: https://scortes.rozpisyzapasu.cz/api/533A1A/2017/Morašice/8b4e159c-2c9d-4879-83b1-5010471309bf.html
image: /public/images/zapasy/morasice-jaromerice.png

sestava: J. Špinar – D. Štancl, M. Chmelík, V. Štancl, V. Tměj – B. Válek, M. Klement, M. Štancl (M. Janypka), Z. Skala – K. Vopařil, M. Sokol (J. Kopecký)
goly: B. Válek, M. Štancl
zlute: B. Válek
---

<p>
Posledními návštěvníky našeho areálu v&nbsp;letošní sezóně byly Jaroměřice. Celek s&nbsp;výborným útokem, vedeným až bájným Švecem, ale slabší obranou, 
což je předurčuje přesně k&nbsp;takové taktice, která by nám měla vyhovovat. Chtěli jsme se doma rozloučit výhrou, chtěli jsme konečně překonat loňskou 
bodovou hranici, chtěli jsme se nominovat na středeční zápas roku. V&nbsp;tom jsme to pro dnešek vykartovanému trenérovi moc nezavařili, protože z&nbsp;třinácti 
lidí se vybírá poměrně snadno. Švec dostal stejně jako před měsícem v&nbsp;poháru osobního strážce, ale i když to byl někdo jiný, zase měl kapitánskou pásku, 
to aby znervózněl, že na něho hrají ti nejlepší (za přežitého předpokladu, že kapitánskou pásku má ten nejlepší). Kdo ví, jak by se zápas vyvíjel, 
kdyby hosté proměnili svoji šanci po prvním rohu. Ten rozehráli na velké vápno, odkud přilétl rozfalšovaný oblouček na malé vápno, 
kde se právě Švec stihl natlačit před Špinara, ale jeho rotující hlavička za dozoru D. Štancla pouze olízla tyč zvenčí. Poté ještě nepřesně střílel hostující záložník 
po vysunutího od M. Štancla, ale záhy se ukázala i druhá stránka hostujícího mančaftu, když M. Štancl přenesl balón na levou stranu a Válek si na vápně poměrně jednoduše zasekl, 
navedl balón na střed a placírkou na nikterak zadní tyč otevřel skóre. Bylo příjemné po deseti minutách pro změnu vést a bylo to na nás i znát, protože jsme během zbytku 
první půlhodiny hráli na balónu a udržovali hostující obranu v&nbsp;permanenci. Ale všichni víme, že to ještě neznamená góly. Velké nebezpečí představovaly naše standartky.
Chmelík se dostal k&nbsp;hlavičce před gólmanem, ale mířil nad. Skala rozehrávkou na krátko vyslal do šance Válka, který si narazil s&nbsp;obráncem, ale jeho střelu
z&nbsp;úhlu vyrazil gólman těsně za zadní tyč. Na Skalův centr osamoceně naskakovali tři naši hráči, ale první Sokol hlavičkoval na „dlouhou“ hlavu, protože 
si nikdo za ním nekřikl. Vopařil po několika sklepnutích zakončoval hlavou z&nbsp;malého vápna rovněž vedle. Šance nebo nadějné brejky jsme si tvořili i ze hry, 
ale spoustu dalších brejků zazdily zbytečné ofsajdy, neochota střílet, šlápnutí si na balón, špatná finálka nebo špatný první dotek, takže ze všech těch náběhů 
jsme vytěžili jenom střelu Vopařila do středu brány. Hosté vepředu moc nebezpeční nebyli, ale právě naše zazděné brejky se staly vodou na jejich mlýn. Na krásný
křížný balón včas nezareagoval ani Válek ani Špinar, ale střelu rozběhnutého útočníka stihl na poslední chvíli zakřižovat Chmelík. Vymanit se z&nbsp;následujícího rohu 
nám trvalo skoro do konce poločasu. Hosté na nás byli nalezlí, jejich přímáky na zadní tyč smrděly jako podebraná pata, ale to, že se pastýř Švec dostal k&nbsp;nepřesnému 
voleji z&nbsp;těžké pozice, bylo spolu s&nbsp;lobovanou hlavičkou nejkonkrétnější ohrožení Špinarovy brány. Když se hra vyrovnala, podařilo se nám zasadit soupeři úder do šatny, 
když Vopařil patičkou prodloužil aut, Sokol narýsoval kolmici pro M. Štancla, který chvilku chodil vápnem hledajíc moment, kdy jeho střela projde. A po dvou až třech kličkách 
skutečně procedil balón mezi gólmanovýma nohama do sítě.
</p>

<p>
V&nbsp;druhém poločase jsme obrázek toho prvního dotáhli k&nbsp;dokonalosti, když jsme hostům dovolili ještě méně zakončení a sami spálili ještě více šancí. Špinar si připsal
prakticky jediný zákrok, když se musel pořádně natáhnout pro Švecův lob, v&nbsp;jiném případě měl štěstí, když si hosté ve vápně sklepli míč proti noze, ale napálili ho vysoko nad.
Tou dobou už ale mělo být dávno hotovo, jenže naše brejky 3 na 1 i 5 na 2 končily tolika ofsajdy a zkaženými přihrávkami, že si snad jediné zakončení, byť ve faulem zavánějící tísni,
připsal Sokolem vysunutý Klement při samostatném nájezdu. Vopařil to zkusil podobně jako M. Štancl, vymíchal v&nbsp;soupeřově vápně dva hráče, ale při zakončení promáchl,
což mu škodolibí diváci dali sežrat stejně jako Sokolovi několikáté podklouznutí. Jsme opravdu rádi, že se naší hrou tak baví. Na zmar našich předních zad se nevydržel dívat
D. Štancl, který za 10 vteřin proběhl s&nbsp;míčem přes tři čtvrtě hřiště, ale sám před gólmanem levačkou přestřelil. V&nbsp;posledních minutách už šancí ubylo,
protože jsme v&nbsp;útoku měli spíš dva jednonohé, ale zasloužené vítězství nad třetím celkem tabulky jsme si vzadu v&nbsp;pohodě pohlídali. V&nbsp;lize jsme tedy překonali výsledky
z&nbsp;minulých dvou let, teď ještě navázat na nastavenou laťku i v&nbsp;poháru. Kdo nechce dlouho čekat na článek, ať se zajede ve středu v&nbsp;18 hodin podívat do Pomezí osobně.
</p>
