---
title: Ve stínu MS
h1: Polička B vs Morašice
date: 2018-06-16
scortes: https://scortes.rozpisyzapasu.cz/api/533A1A/2017/Morašice/65d0ebc9-0f02-41a3-ae9a-eaaf92b66ce8.html
image: /public/images/zapasy/policka-morasice.png

sestava: J. Špinar - D. Štancl, M. Ropek, V. Štancl, V. Tměj - B. Válek, M. Klement (Z. Škoda), M. Štancl, Z. Skala - J. Kopecký (M. Burián), M. Chmelík
goly: M. Chmelík2, B. Válek2, V. Štancl
zlute: M. Chmelík
---

Tak trochu ve stínu mistrovství světa se v jednotný čas rozjelo mimořádně napínavé poslední kolo svitavského okresního přeboru, 
ve kterém se hrálo o umístění mezi druhým a dvanáctým místem. Naším prvořadým cílem bylo dát vůbec dohromady jedenáct lidí, 
případně se pobavit fotbalem a v případě neočekávaného zaváhání soupeřů se pokusit vylepšit páté místo. Díky návštěvě Škody
a Buriána jsme se dostali alespoň na klasickou třináctku. Zhlédli jsme úvodní ceremonii a s pohybem podobným místnímu devadesátiletému oslavenci
jsme se dali "do boje". Prali jsme se s neustálými ofsajdy po neustálých zakázaných průnikovkách, s přesností přihrávek i s motivací a nasazením.
Domácí na tom s motivací byli možná ještě hůř, protože ještě ani nevěděli, zda budou příští rok vůbec hrát, ale stačili jim tři běhaví a šikovní jedinci
a bylo v naší lajdácké obraně o zábavu postaráno. První náznaky šancí jsme měli my, konkrétně Kopecký nastoupivší v sehraném útočném duu s Chmelíkem,
ale po Skalově centru ze strany mířil vedle. Domácí se výše popsaným individuálním způsobem dostali před Špinara ze strany, ale náš brankář si připsal
úspěšný zákrok. Na tu bídu pro nás překvapivě nebyl velký problém dostat se do šancí. V. Štancl byl první u vyraženého přímáku, ale v souboji doslova
z očí do očí se mu nepodařilo nasměrovat míč mimo gólmanovo břicho. Chmelíka po dalším centru ze strany vychytal gólman a následná dorážka se mezi
tři tyče nevešla. V polovině prvního dějství si jeden z domácích áčkařů poradil v osobních soubojích postupně s oběma stopery, udělal kličku Špinarovi
a zavěsil do prázdné. Na pokyn rozhodčího jsme se šli osvěžit a hned na to ukončili domácí obléhání našeho vápna průnikovkou středem naší obrany
a chladnokrevným zakončením. &nbsp;V tu chvíli to vypadalo, že sezónu v klidu dochodíme a zkusíme neinkasovat větší příděl, ale domácí nás
po desetiminutové letargii sami vrátili do hry, když nuznou rozehrávku jejich gólmana vystihl Klement, předal Skalovi a ten se napotřetí dočkal asistence,
když jeho centr hlavou zužitkoval Chmelík. Odpověď domácích útočníků po rozehrávce skončila u Špinara, jeho dlouhý výkop pečlivě sledoval Chmelík,
ve vápně udělal kličku lajdácky dostoupivšímu obránci a dal pod sebe na Válka, který chvíli před poločasem srovnal stav.

Dalo nám to práci, ale nakonec se podařilo rozhodčím vysvětlit, že z kabiny vychází náš brankář v hracím dresu, jeho místo v bráně zaujme střídající hráč Burián
a Kopecký už se na hřišti neukáže. O vzrušující momenty byla dlouho nouze, ale herní převaha byla na naší straně i díky přibývající únavě a zdravotním potížím
 v domácím kádru. Pod nakonec vítězný gól se po hodině hry podepsal V. Štancl, i když na to potřeboval tři pokusy v rychlém sledu. Nejprve skončila jeho hlavička
 po rohu na břevně, poté nájezd jím neuhlídaného útočníka na tyči a až jeho další hlavička po centru Skaly zapadla k tyči. Domácí snahu o vyrovnání eliminoval
 dvěma zákroky Burián, který hlavně při dalekonosném oblouku pod břevno riskoval zdraví své odrážecí nohy. Jeho protipól Špinar se zase na hrotu připomněl
 svou dalekonosnou dělovkou, která ale o kousek minula bránu. Domácí snížili stavy až na devět zdravých lidí, poslali do brány jednoho z těch zdravějších,
 ale definitivní uklidnění přinesl až Válek, který po krásném přenesení hry od M. Štancla vnikl ze strany do pokutového území a přízemní střelou si připsal
 svůj druhý zásah. A vzápětí ho napodobil Chmelík, který z hranice vápna překonal domácího nedobrovolníka v bráně. To se pak ještě z úplně jasné pozice nepovedlo
 Špinarovi, který měl míč na svou slabou nohu, ale napsat do zápisu gól k brankáři by se rozhodčímu, který v druhém poločase naprosto přestal ovládat své řemeslo,
 možná ani nepovedlo. Tak skončil poslední ze 36 zápasů sezóny, které chyběla snad jen větší útočná efektivita, obhajoba pohárového vítězství a hlavně více
 spolehlivě docházejících hráčů. Tady se sluší veřejně připomenout, že Martin a Dominik Štanclovi společně s Milanem Chmelíkem ty zápasy absolvovali všechny,
 takže pilná docházka je možná jak u studentů, tak u cestovatelských profesí, tak u otců od rodin.
