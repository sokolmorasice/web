---
title: Zimní příprava 2018
date: 2018-01-05
---

<p>Čt&nbsp; 11.1.&nbsp; &nbsp;od 18:00 hod&nbsp; &nbsp; &nbsp; tělocvična&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; zahajovací trénink</p>

<p>Út&nbsp; 16.1.&nbsp; &nbsp;od 18:30 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; tréninková jednotka</p>

<p>Pá&nbsp; 19.1.&nbsp; od 18:00 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; tréninková jednotka</p>

<p>Ne&nbsp; 21.1.&nbsp; od 10:15 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hřiště&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;herní trénink</p>

<p>Út&nbsp; 23.1.&nbsp; &nbsp;od 18:30 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; tréninková jednotka</p>

<p>Pá&nbsp; 26.1.&nbsp; od 18:00 hod&nbsp; &nbsp; &nbsp; &nbsp;tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; tréninková jednotka</p>

<p>Ne&nbsp; 28.1.&nbsp; od 10:15 hod&nbsp; &nbsp; &nbsp; &nbsp; hřiště&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; herní trénink</p>

<p>Út&nbsp; 30.1.&nbsp; &nbsp;od 18:30 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;tréninková jednotka</p>

<p>Pá&nbsp; 2.2.&nbsp; &nbsp; od 18:00 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;tréninková jednotka</p>

<p>Ne&nbsp; 4.2.&nbsp; &nbsp; od 10:15 hod&nbsp; &nbsp; &nbsp; &nbsp; hřiště&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; herní trénink</p>

<p>Út&nbsp; 6.2.&nbsp; &nbsp; &nbsp;od 18:30 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; tréninková jednotka</p>

<p>Čt&nbsp; 8.2.&nbsp; &nbsp; &nbsp;od 18:00 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; tréninková jednotka</p>

<p><strong>Pá-Ne&nbsp; &nbsp; &nbsp; &nbsp;9. - 11.2.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Daňkovice&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;soustředění</strong></p>

<p>Út&nbsp; 13.2.&nbsp; &nbsp;od 18:30 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;tréninková jednotka</p>

<p>Pá&nbsp; 16.2.&nbsp; od 18:00 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;tréninková jednotka</p>

<p>Ne&nbsp; 18.2.&nbsp; od 10:15 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hřiště&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; herní trénink</p>

<p>Út&nbsp; 20.2.&nbsp; &nbsp;od 18:30 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;tréninková jednotka</p>

<p>Pá&nbsp; 23.2.&nbsp; od 18:00 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;tréninková jednotka</p>

<p><strong>So&nbsp; 24.2.&nbsp; od 18:00 hod&nbsp; &nbsp; &nbsp; &nbsp; Rychta&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Obecní ples</strong></p>

<p>Út&nbsp; 27.2.&nbsp; &nbsp;od 18:00 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;tréninková jednotka</p>

<p>Pá&nbsp; 2.3.&nbsp; &nbsp; od 18:00 hod&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;tréninková jednotka</p>

<p><strong>Ne&nbsp; 4.3.&nbsp; &nbsp; od 11:00 hod&nbsp; &nbsp; &nbsp; &nbsp; UMT Litomyšl&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;přípravné utkání - Horní Újezd</strong></p>

<p>Út&nbsp; 6.3.&nbsp; &nbsp; &nbsp;od 18:00 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;tréninková jednotka</p>

<p>Pá 9.3.&nbsp; &nbsp; &nbsp;od 18:00 hod&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; tréninková jednotka</p>

<p><strong style="text-indent: 2em;">Ne&nbsp;11.3.&nbsp; &nbsp;od 13:00 hod&nbsp; &nbsp; &nbsp; &nbsp; UMT Litomyšl&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;přípravné utkání - Borová</strong></p>

<p>Út&nbsp; 13.3.&nbsp; &nbsp;od 18:00 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;tréninková jednotka</p>

<p>Pá&nbsp; 16.3.&nbsp; od 18:00 hod&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; tělocvična, výběh&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; tréninková jednotka</p>

<p><strong>Ne&nbsp; 18.3.&nbsp; od 15:00 hod&nbsp; &nbsp; &nbsp; &nbsp; hřiště DÚ nebo UMT Lit.&nbsp; &nbsp; &nbsp; &nbsp;přípravné utkání Dolní Újezd B</strong></p>

<p>Út&nbsp; 20.3.&nbsp; &nbsp;od 18:00 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hřiště&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;tréninková jednotka</p>

<p>Pá&nbsp; 23.3.&nbsp; od 18:00 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hřiště&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;tréninková jednotka</p>

<p><strong>So&nbsp; 24.3.&nbsp; od 14:30 hod&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;hřiště Křenov&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;MU Křenov</strong></p>

<p>&nbsp;</p>

<p>Termín, čas a místo se může změnit dle aktuálních podmínek, bude upraveno na stránkách.</p>

<p>Na trénink mít s&nbsp;sebou věci na běh, na hřiště, na umělku i do tělocvičny.</p>

<p>Na www stránkách vyplňovat účast!!!</p>
