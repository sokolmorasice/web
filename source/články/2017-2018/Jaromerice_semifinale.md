---
title: Třikrát a dost?
date: 2018-05-08
scortes: https://scortes.rozpisyzapasu.cz/api/A3A/2017/Morašice/985dc0f8-337b-405d-ab99-3c87ae480e01.html
image: /public/images/uploads/fotbal.jpg

sestava: J. Špinar – D. Štancl, M. Ropek, M.Chmelík, V.Novák (K.Vopařil) – B. Válek, V.Tměj, M. Klement, M. Štancl (J. Kopecký) – Z. Skala, M. Sokol (V.Štancl)
goly: M.Klement, M. Ropek, M. Chmelík
zlute: M.Chmelík
---

<p>Ani druhý svátek nás nenechal v klidu doma s rodinami, na výletě nebo na stavbě u míchačky. Vcelku obstojně jsme zvládli náročný fotbalový turnus a věřili jsme, že i napotřetí dokážeme postoupit do finále okresního poháru bo snad i završit zlatý hattrick. Mohli nám to zhatit dvě vady, komplementárně spojené. Tu první moc nechápeme a upozorňujeme, že to neděláme schválně. Za celé jaro jsme odehráli 9 utkání, ať už mistrovských nebo pohárových a pouze Linhartice nám nedokázali dát první branku do 15. minuty (oni nám ji nedali vůbec, pozn. aut.). Tímto bychom chtěli apelovat na fanoušky soupeře, aby chodili na zápasy včas, protože víme jak je nepříjemné se z gólu radovat před prázdnými ochozy.</p>

<p><span style="text-indent: 2em;">Tou druhou byl soupeř, který sice disponuje větším počtem obdržených branek, ale jeho ofenzivní síla je značná. Mnohdy záleží na tom, jak dobře jejich lodivod, snajpr a kapitán "Baťa" obuje. Dovolíme si použít jeden pravdivý citát z bible: Pokud nechceš, aby se ti rozutekly ovečky, musíš uhlídat pastýře (Daniel 29,2). Pastýře dostal na starost Chmelík a nutno říci, že až na jedno zaváhání, kdy mu musel ve vápně pomoci i vlčák Klement, si vedl velmi dobře.</span></p>

<p><span style="text-indent: 2em;">Za posledních 14 dní bylo toho fotbalu tolik, že se těžko vybavují zárodky šancí a zajímavých momentů. První čtvrthodina byla taková nijaká. Náš pohyb byl však již na první pohled pomalejší, těžkopádnější, takže se hrálo většinou na naší polovině. Za zmínku stojí pouze jedna vydařená střela soupeře do horního rohu branky, kterou Špinar výborně vyrazil na roh. Nevím jak, ale kde se vzal tu se vzal, najednou byl za obranou hostů Skala a štrádoval si to na Továrka. Podle extrenéra Kuty udělal všechno dobře, jen to chtělo silnější zakončení. &nbsp;Poté špatně sklapla ofsajdová past Vendou Novákem a dva volní chlapci ve žlutém postupovali od lajny ke Špinarově brance. Musíme uznat, že Moozovi střela sedla hezky.</span></p>

<p><span style="text-indent: 2em;">Když jsme se konečně dočkali a prohrávalo se, byla řada na nás. Do dvou příležitostí se dostal Válek, ale neurodilo se. Venda Tměj se rozhodl pro novou variaci rohových kopů a taky to nebylo šťastné řešení. Od druhého praporku to Válkovi lítalo přeci jenom výš. Poslal míč do ohně, ten se z ohně odrazil ke Klementovi, který s pomocí tyče vyrovnal.</span></p>

<p><span style="text-indent: 2em;">Do druhé půle jsme přeci jen trochu vytáhli nohy z betonu a u soupeře bylo živěji. Před tím ovšem ze standartky soupeře proplachtil míč celým vápnem a na zadní tyči ho při dorážce výborně vytáhl na roh Špinar. Co nám poslední dobou vyloženě nejde je postupný útok po lajnách. Většinou je po náročném sprintu místo centru míč zadrbán, odebrán a následuje další vysilující sprinterský návrat s úmyslem uhasit soupeřům brejk. Vše nasvědčovalo tomu, že Tmějův úprk bude obdobný, ale místo ztráty míče přišel faul a nám se naskytla standartka z pravé strany. Na centr M.Štancla si do zadní tyče málem naběhl Ropek a bosou hlavou v nás zažehl finálové plamínky naděje. &nbsp;O dvacet minut později nastřelil kdosi ruku soupeře ve vápně a z pokutového kopu Chmelík bezpečně zvýšil na 3:1. Pár minut před koncem si Skala až moc nahrával s Kopeckým nebo Tmějem na vápně, takže z více než slibné pozice nebylo nic. No a protože se soupeři nepodařilo ani snížit, tak budeme mít proti Cerekvici ještě jednu krásnou středu.&nbsp;&nbsp;</span></p>
