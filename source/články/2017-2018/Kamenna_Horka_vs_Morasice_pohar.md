---
title: Druhé první kolo
date: 2017-08-04
scortes: https://scortes.rozpisyzapasu.cz/api/A3A/2017/Morašice/9770c5b3-b343-4ae3-8b12-12f7928e3e98.html
image: /public/images/uploads/logo.png

sestava: V. Štancl - D. Štancl, M. Ropek, M. Chmelík, V. Novák - B. Válek, M. Klement, M. Štancl, Z. Skala (M. Burián) - M. Vodehnal (K. Vopařil), M. Sokol
goly: M. Sokol, M. Vodehnal, M. Klement
zlute: M. Ropek
---

<p>Kvůli nečekanému druhému kolu krajského poháru jsme museli trochu upravit program, a tak jsme i díky vstřícnosti soupeře zahájili cestu okresním pohárem netypicky v pátek večer na půdě Kamenné Horky. Ta se do okresních soutěží vrací po odmlce, a proto je její zařazení do pralesa jen orientační známkou kvality. Vítězná sestava proti Přelouči doznala jediné změny, když Skala nahradil chybějícího Tměje. Nebylo to ve velkém dusnu žádné velké tempo, ale během prvních deseti minut došlo na šanci na každé straně. Zatímco domácí zakončili průnik z boku do boční sítě a až do konce zápasu se tím ofenzivně odmlčeli, Klement podobně jako před týdnem účinným napadáním získal balón a tentokrát připravil pozici pro Sokola, který měl ve vápně dostatek času, aby si srovnal balón na noze a nikterak razantně vstřelil svůj první gól v našem áčku. Nemusel zůstat jen u jednoho, ale v dalším náznaku šance svůj čas ke střele prováhal, v čemž ho později napodobil i útočný kolega Vodehnal. Uprostřed poločasu byla hra rozháraná, oba týmy se snažily držet balón na zemi, což se dařilo jen v zadních řadách a přechod do útoku povážlivě váznul. Oživení přineslo až prohození našich krajních záložníků, ale Skalovi nejdříve ve vápně nebylo dopřáno ani vystřelit a jeho pozdější přímák zkrotil domácí gólman bez větších potíží, a tak se šlo kabin za našeho hubeného vedení.</p>

<p>I druhý příchod na hřiště nám vyšel, po osmi minutách účinně presoval D. Štancl a jeho vyraženou střelu dorazil do brány i s pořádným štěstím Vodehnal. I pro něho to byla gólová premiéra, jestli kluci vědí, co se sluší a patří, mohlo by být cestou ze Semína veselo. Hru jsme i nadále kontrolovali, minimálně jsme domácí nepustili ani k náznaku snížení. My jsme aspoň ty náznaky měli, Ropek, Vopařil nebo Novák ale stříleli vedle, slabě nebo zblokovaně. Až deset minut před koncem se do míče z takových dvaceti metrů opřel Klement a jeho střela s pomocí zásahu gólmana i tyče znamenala nejhezčí gól zápasu. Na V. Štancla v bráně se dostalo až v závěru večera, když si dvojitým zákrokem v poslední minutě uhájil první shootout nové sezóny. Cesta za zlatým pohárovým hattrickem tak bude pokračovat na konci září.</p>
