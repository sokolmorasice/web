---
title: Šťastné a veselé
date: 2022-12-21T20:23:01.512Z
category: obecne
image: /public/images/uploads/51-vanocni-sokol-1-.jpg
---
T﻿J Sokol Morašice naprosto smířeně oznamuje všem svým členům a příznivcům, že nás brzy opustí rok 2022. Přejeme všem, aby si jeho poslední chvíle užili v klidu a zdraví se svými nejbližšími a aby jim začaly co nejdříve a co nejsilněji klapat vztahy s rokem novým.

P﻿oslední rozloučení s rokem 2022 se uskuteční v sobotu 31.12. v 10:00 na hřišti v Morašicích. Po rychlé zdravici proběhne veřejně přístupný fotbálek.