---
title: Tréninky mládeže
date: 2023-01-09T20:39:59.721Z
category: obecne
image: /public/images/uploads/nabor_stranky.jpg
---
T﻿J Sokol Morašice zve všechny chlapce a dívky do školní tělocvičny, aby zde nad rámec tělocviku udělali něco pro své zdraví a třeba si i našli nového koníčka a nové kamarády. Fotbalové tréninky a pohybové hry začínají vždy v 17:00.

P﻿ondělí: ročníky 2014-2016

Ú﻿terý: ročníky 2017 a mladší

S﻿tředa: ročníky 2013 a starší

P﻿ro detailnější info kontaktuje trenéra Jirku Famfulíka na čísle 602 101 813.