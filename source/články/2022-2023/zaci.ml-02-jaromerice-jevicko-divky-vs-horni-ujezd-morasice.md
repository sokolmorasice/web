---
title: Znovu na Malé Hané
date: 2022-09-10
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Jaroměřice/Jevíčko dívky vs Horní Újezd/Morašice
sestava: Štěpán Vojtěch, Kvapil Jan, Bulva Matyáš, Flídr Šimon, Vomočilová Sára,
  Kovář Matěj, Kovář Michal, Rosypal Josef, Chaun Jaroslav, Huryta Vojtěch.
goly: Kvapil Jan 3, Rosypal Josef 2, Bulva Matyáš 2, Chaun Jaroslav 2, Flídr Šimon
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2022/Horní
  Újezd/Morašice/3c9d0258-52d9-469e-bb91-8aeeefba9e2c.html
---
Krásně prosluněné podzimní sobotní dopoledne vybízelo k domácím venkovním pracem a různému šumrdění kolem našich domečků. **My jsme asi jiní.** A tak jsme se po dvou týdenní pauze opět přesunuli mezi moravské kopce do Jaroměřic. Místní hřiště bylo pro některé naše hráče novou neznámou štací v poznávání hřišť a kiosků k nim přilehlých v našem okrese. Zato místní tým dívek jsme si pamatovali dobře již z nižší kategorie přípravek. Někteří si vzpomněli na staré rány, někteří na jména soupeřek.

První minuty byly velmi rozhárané. Přeci jen v hlavách hráčů, zvláště těch starších bylo ,,vždyť jsou to jen holky“. Jenže dívčí něžný styl nemá co do činění s pohlazením od maminky. Je o urputném bránění svého území a nevynechání žádného možného souboje. Až poté naši hráči pochopili, že to dnes nebude tak jednoduché utkání. Začali více kombinovat a využívat prostor celé šířky hřiště. Za což byli odměněni třemi poločasovými góly. Do druhé půle jsme vstoupili tak, jak jsme skončili tu první. Dobýváním. Děvčata bránily svých dvacet metrů a dělaly ze svého území nedobytnou tvrz. Postupem času se nějaké to okénko v obraně otevřelo a my mohli navyšovat skóre.

a﻿utor: Martin Kovář