---
title: Se smůlou na patách
date: 2022-10-02
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Sebranice vs Horní Újezd/Morašice
sestava: Martina Famfulíková, Sára Vomočilová, Jan Kopecký, Jindřich Mičík,
  Zuzana Famfulíková, Jan Kvapil, Ondřej Žďára, Filip Flídr, Lukáš Mach, Šimon
  Flídr
goly: Šimon Flídr, Ondřej Žďára
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1B/2022/Horní
  Újezd/Morašice/98bb9853-654d-4a0f-a1ea-1d84fc7d9dd2.html
---
V neděli jsme odehráli za slunného počasí další mistrovské utkání.  Na zápase jsme se sešli na Sebranickém hřišti v devíti, až během utkání nás doplnil Šimon Flídr a měli jsme tak alespoň jednoho na střídání. Začátek jsme nepodchytli a brzy inkasovali, ale podařilo se nám odpovědět. Když nám soupeř nám znovu odskočil, měli jsme několik dalších možností  k vyrovnání, až se nám jedna střela před přestávkou přece jen ujala.

Druhý poločas se nám začala bortit sestava. Nejprve se zranila Marťa Famfulíková zranila tak, že musela z brány do pole. Mezi třemi tyčemi ji vystřídal Lukáš Mach, ale zranění se nevyhnulo ani jemu a dost možná nám bude chybět do konce podzimu. Soupeř nám odskočil na dvoubrankový rozdíl a v posledních 5 minutách, kdy jsme po zranění Šimona Flídra dohrávali v osmi lidech, pak ještě skóre navýšil. Chtěl bych poděkovat všem hráčům za účast a snahu. Hlavně mladším žákům, kteří nás doplňuji a bez jejichž podpory bychom asi nemohli zápasy jako tento vůbec odehrát. A doufám, že na další zápas se sejdeme ve větším počtu. 

Miloš Kopecký