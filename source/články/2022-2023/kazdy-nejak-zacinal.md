---
title: Každý nějak začínal
date: 2022-09-04T10:45:18.498Z
category: pripravka
image: /public/images/uploads/logo.png
---
Po dlouholetém spojení s Horním Újezdem jsme pro letošní sezónu kategorii mladších přípravek přihlásili každý oddíl zvlášť. Na první turnaj letošního roku zavítal tým pod vedením Milanů Chmelíka a Klementa do Bystrého.

**Sestava:** Matyáš Tóth, Ivo Langr, Jan Klement, Lukáš Chmelík, Kateřina Kovářová, David Soukup, Matouš Jetmar

**Morašice - Borová   0:11 (0:6)**

Zatímco většina našich dětí nastoupila ke svému prvnímu zápasu v životě, soupeř měl tým poskládaný pouze z nejstaršího ročníku 2014. Přes výrazný tlak Borové je výsledek ještě přijatelný, děcka se rozkoukávali, statečně vzdorovali a párkrát se dostali i k zakončení.

**Morašice - Bystré 5:6 (5:4)**

S domácím týmem to byl vyrovnaný zápas. V prvním poločase jsme měli navrch a vedli už 4:1. O tři góly se postaral Ivo Langr, který byl v tomto zápase velmi aktivní a zúročil loňské oťukávací zkušenosti. bohužel se nám v druhém poločase nepodařilo proměnit několik šancí, soupeř vyrovnal a v závěru udělali chybu trenéři, když ve snaze pravidelně střídat všechny děti odvolali hráče ke střídán zrovna v době rozehrávky našeho gólmana. Ten si toho bohužel nevšiml a ukázkově nalil soupeři k rozhodujícímu gólu.

**Branky:** Ivo Langr 3, Matouš Jetmar, Jan Klement

**Morašice - Březová 0:15 (0:7)**

Poslední zápas byl velmi podobný tomu prvnímu. Na dětech již byla vidět únava, nechtělo se jim moc běhat a soupeř měl v týmu velice kvalitní děvčata, která svoje šance nekompromisně proměňovala.

**Ostatní výsledky:**

Bystré - Březová 0:16 (0:8)

Borová - Březová 8:3 (5:2)

Bystré - Borová 3:13 (1:6)

**Tabulka turnaje:**

1. Borová 9b. (32:6)

2. Březová 6b. (34:8)

3. Bystré 3b. (9:34)

**4. Morašice 0b. (5:32)**

Vstup do soutěže je potřeba i přes výsledky hodnotit pozitivně. Už při přihlášení soutěže jsme věděli, že náš tým bude patřit k nejmladším, ale děti se snažily a zkušenosti potřebují nasbírat stejně jako trenéři a rodiče.

autor: Milan Chmelík