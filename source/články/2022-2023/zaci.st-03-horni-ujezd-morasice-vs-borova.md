---
title: Poločas nestačil
date: 2022-09-11
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Horní Újezd/Morašice vs Borová
sestava: Vojtěch Štěpán - Jindřich Mičík, Ondřej Žďára, Jan Kopecký, Lukáš Mach,
  Matěj Rejman, Jan Kvapil, Sára Vomočilová, Vít Famfulík, Fili Flídr, Matěj
  Brettinger, Josef Rosypal, František Flídr, Vojtěch Huryta
goly: Ondřej Žďára
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1B/2022/Horní
  Újezd/Morašice/ffd46af9-b609-4206-9e66-7edfca68f818.html
---
V nedělním utkání jsme na Horním Újezdě hostili klub z Borové. Do branky se místo zraněné Marti Famfulíkové postavil Vojta Štěpán. Zápas jsme nerozehráli špatně, soupeře jsme k ničemu vážnějšímu nepustili a v osmé minutě jsme vstřelili naší první branku sezóny, když Ondra Žďára prostřelil brankáře z velké vzdálenosti. Další naše šance v prvním poločase jsme bohužel nevyužili a v tom druhém už převzal iniciativu soupeř, který brzy otočil skóre dvěma brankami. A protože byla Borová nadále bojovnější, už se nám nepodařilo s výsledkem něco udělat. Snad příště.