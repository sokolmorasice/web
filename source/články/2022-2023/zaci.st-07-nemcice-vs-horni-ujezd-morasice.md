---
title: Jeden gólman za druhým
date: 2022-10-09
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Němčice vs Horní Újezd/Morašice
sestava: Sára Vomočilová, Jan Kopecký, Šimon Flídr, Vojtěch Paťava, Zuzana
  Famfulíková, Ondřej Žďára, Vojtěch Huryta, Jan Kvapil, Filip Flídr, Martina
  Famfulíková, Vít Famfulík
goly: Ondřej Žďára
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1B/2022/Horní
  Újezd/Morašice/83b35004-0677-4ed7-9130-468da692dd7c.html
---
V neděli jsme odjeli k odpolednímu zápasu do Němčic. Sešlo se nás opět akorát 9. Tím, že nás zase doplnil Šimon Flídr, jsme měli alespoň jednoho navíc, který si mohl občas odpočinout. Začali jsme čisté s dívčím útokem Marťa a Sára a novým brankářem Vojtou Paťavou. A nezačali jsme moc dobře. Obdrželi jsme dvě rychlé branky a pak jsme pro zranění ruky museli [opět](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2022-2023/zaci.st-06-sebranice-vs-horni-ujezd-morasice) poslat brankáře do pole. Ve zbytku poločasu už jsme zmátořili a bojovný fotbal nám přinesl i několik nevyužitých šancí.

Do druhého poločasu jsme nastoupili opět s novým brankářem. Tentokrát Honzou Kvapilem, který, ač se mu do brány moc nechtělo, předvedl skvělý výkon. Na třetí zásah soupeře jsme hned odpověděli, a i přes následný čtvrtý inkasovaný gól, byl náš výkon celkem vydařený. Musím všechny pochválit za  bojovnost a opět poděkovat mladším žákům za jejich doplnění, bez kterého bychom nemohli odehrát zápas.

Miloš Kopecký