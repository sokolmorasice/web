---
title: Utkání starých gard
date: 2022-09-24T22:16:09.923Z
category: obecne
image: /public/images/uploads/logo.png
h1: "Staré gardy: Morašice - Dolní Újezd 4:8 (2:2)"
sestava: Vojtěch Andrle, Jan Němec - Josef Jokeš, František Ropek, Oldřich
  Ropek, Martin Ropek, Luboš Vosmek, Jiří Famfulík, Josef Kopecký,  Petr Štěpán,
  Jan Štěpán, Josef Štěpán, Petr Kuta, Martin Burián, Jindřich Jána, Václav
  Beneš
goly: Václav Beneš 2, Josef Kopecký, Martin Burián
multimedia: https://drive.google.com/drive/folders/1R3ET0Ty3IcNzlpv9rdY1HwD3-essYE0-
---
Na zápas starých gard si manažeři Petr Kuta s Martinem Ropkem nachystali sestavu prošpikovanou nejen sourozeneckými a příbuzenskými dvojicemi až trojicemi, ale také místně příslušnými dolnoúježďáky. Po úvodní minutě ticha za Honzu Klofandu a otevíracích šancích otevřel skóre Josef Kopecký. Tomu se sice nepodařilo prosadit během 5 minut [mistrovského zápasu](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2022-2023/muzi-07-dolni-ujezd-b-vs-morasice), ve kterém si připal jubilejní 60. start proti Dolnímu Újezdu, ale v tom 61. pohodlně uklidil do sítě rohový kop, který k němu propadl po souboji gólmana s Buriánem. Zatímco střelu hostujícího Čejky nečekaně mrštným skokem zneškodnil Andrle v naší bráně, pokus našeho Beneše zpoza vápna zapadl přesně za zadní tyč a poslal náš tým do dvoubrankového vedení. Z velké šance ho pak nenavýšil Famfulík, který napálil tyč, na druhé straně naopak snížil povedenou střelou Petr Kopecký. Po dvou nevyužitých šancích L. Vosmeka pak přišel před plánovaným půlhodinovým poločasem trest v podobě vlastního gólu Josefa Štěpána.

Na druhý poločas si prohodili dresy M. Ropek a L. Vosmek, výměnou za ně nás doplnili Kořízek a Pavel Kuta. Do naší brány se postavil po 12 letech a dvou měsících Jan Němec, kterého přesnou střelou přímo z rozehry přivítal Heinisch. Ten byl i na konci pěkně fotbalové akce z křídla, po které pohodlně zvýšil na 4:2. Nedorozumění v hostující obraně pak nabídlo snadný gól Buriánovi a když se do rohu Pavla Kuty vložil Beneš, bylo po jeho přízemním voleji srovnáno. Němec v naší bráně pak ukázal, že v něm ještě něco je a připsal si několik zákroků, tím nejdůležitějším překazil kličku do prázdné brány našemu převlečenému M. Ropkovi. Vedení vrátil Dolnímu Újezdu v 50. minutě po špatné rozehrávce Vajrauch, chvíli na to si na pobídku z křídla naběhl Svoboda a bylo to o dva góly. Naši hráči ve žlutém dresu donutili Vrabce v hostující bráně pouze k jednomu zákroku, náš hráč v modrobílém dresu L. Vosmek se naopak poučil z nezdarů v prvním poločase a dvěma góly v závěru určil konečné skóre na 8:4. Zvlášť ten druhý gól byl ukázkou pro všechny naše áčkařské obloučkující hráče. Narozdíl od dva roky starého [zápasu](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2020-2021/tak-jsme-se-tu-vsichni-sesli-co-myslis-osobo) se podařilo ho všem dokončit ve zdraví, což je základní předpoklad k tomu, aby se podobné setkání mohlo třeba někdy zopakovat.