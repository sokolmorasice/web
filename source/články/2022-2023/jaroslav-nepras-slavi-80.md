---
title: Jaroslav Nepraš slaví 80
date: 2023-05-18T18:47:13.914Z
category: obecne
image: /public/images/uploads/logo.png
---
V﻿ minulých dnech oslavil 80. narozeniny pan Jaroslav Nepraš, který je s naším oddílem spjatý téměř od jeho začátku. TJ mu jménem všech členů i fanoušků děkuje za téměř půl století neocenitelné práce pro celý Sokol, zejména pro oddíly fotbalu a stolního tenisu, i za trvalou přízeň přímo v hledišti, a přeje mu hodně zdraví, spokojenosti i radosti z výsledků našich sportovců. Zároveň všechny zveme na nedělní domácí zápas, před jehož začátkem našemu oslavenci předáme i malou pozornost. A u příležitosti jeho jubilea jsme s ním i trochu zavzpomínali.

**Půjdeme od začátku. Jak jste se k morašickému fotbalu vlastně dostal?**

V roce 1972 za mnou jako za pravidelným divákem přišel tehdejší předseda fotbalu Láďa Tobku, jestli bych jim nechtěl s fotbalem nějak pomoci. Tak jsem se připojil jako takový sekretář, tenkrát se tomu říkalo organizační pracovník. To jsem dělal 18 let. A v 80. letech jsem přebral od pana učitele Malečka i pokladníka. On už měl léta, tak nejdřív jsem mu s tím pomáhal, a pak to dělal sám.

**Jak vypadala tahle organizační práce v době bez mobilů a internetu?**

Hodně se toho řešilo po telefonu, já jsem pak měl chvíli pevnou linku i doma. Ale když třeba bylo špatné počasí a věděli jsme, že se asi nedá hrát, tak se prostě k soupeři muselo jet. Protože zápas se odložil, až když to rozhodčí řekl na místě, takže jsme tam museli být, jinak by nás zkontumovali.

**Já jsem ve vašich zápiscích našel napříč kategoriemi těch zkontumovaných zápasů celkem dost, čím to bylo?**

Nejvíc dopravou, s tou bylo hodně starostí. On byl problém sehnat dost aut, hlavně pro dorostence, tak často by lidí bylo dost, ale neměli se k soupeři jak dostat. Hodně se to řešilo autobusem, v Cerekvici ho jeden čas měli, ze Zámrsku nás taky jezdili, ale nejvíc autobusů jsme sehnali od ČSAD. Často hráli dorostenci a chlapi na jednom místě, tak jeli spolu, a to se chlapům moc nelíbilo, protože to zabralo celé odpoledne.

**Bylo něco, co Vás v té práci dokázalo vytočit nebo Vám ji přímo otrávit?**

To ne, drželo se hodně pohromadě. Láďa Tobek neměl rád malej fotbal, protože ho spousta kluků hrála třeba v sobotu dopoledne, a pak to na nich bylo odpoledne samozřejmě znát. Ještě možná dochvilnost hráčů. Ale to jsme spíš byli nervózní nebo naštvaní všichni, když se třeba jelo tím autobusem a čekalo se na pár lidí, co šli na poslední chvíli. To se kolikrát bylo třeba převlíkat v autobuse, a přímo z něj se šlo na hřiště, abychom tam byli nastoupení dřív, než skončila čekačka.

**A když to klaplo a všichni přišli včas, tak se na zápas jezdilo s jakým předstihem?**

Tak čtvrt hodiny stačilo, abychom jezdili hodinu dopředu jako dneska, to ne.

**To by možná leckterý současný hráč uvítal. Stejně jako domácí zápas v sobotu. V Morašicích se hrálo vždycky v neděli nebo to někdy bylo jinak?**

Vždycky v neděli. Párkrát se zkusila sobota, ale lidi vůbec nechodili. Vím, že Cerekvice často hraje o sobotách, ale tady se v sobotu vždycky pracovalo.

**A hráčům tenkrát nevadilo, že se po zápase neposedí v hospodě, že se po fotbale musí brzy rozejít, protože se jde druhý den do práce?**

Kdepak, vždyť alkohol byl zakázanej. Hráči dostali flašku limonády, ale jinak se nalejvat nesmělo. I na zábavách se mohlo dávat jenom pivo, limo a víno. Tam jsme kořalky měli schovaný, a chodilo se na ně tajně, „do pekla“ se tomu říkalo. Naštěstí nás nikdy žádnej tajnej nenaprášil. A na hřišti se nejdřív převlékalo jenom v malé přepažené buňce, kam jsme se museli vejít i se soupeřem. Pak se v roce 73 sehnal vagón, ale pití jsme si mohli schovávat až v nových kabinách, tam už se na to našlo místo.

**Kabiny jste stavěli svépomocí, což je dneska těžko představitelné. Tomu někdo musel velet ne?**

Jarda Kopecký byl zedník, tak ten se na tom nadřel. A hodně věcí zajišťoval Jarda Krejza. Ten byl zrovna v té době chvilku předsedou fotbalu, ten musel mít všechno pintlich. Ale některým to zas nevonělo, tak to dělal jenom chvíli.

**Takže organizační pracovník, pokladník, zedník, co dalšího jste pro Sokol dělal?**

Cedil krev. Tak jsme říkali tomu, že jsme v rámci nějakého závazku chodili za oddíl darovat krev, tam jsem byl asi dvanáctkrát, pak už se mi po tom nedělalo dobře. A organizovali jsme brigády tenkrát. A pomezního jsem tady dělal, tam jsem naběhal kilometrů po lajně. Dlouho a často to bylo tak, že přijel jenom hlavní rozhodčí, rovnou na dva zápasy, co byly za sebou a já na lajně strávil odpoledne. Přes 2000 zápasů jsem odmával.

**Vaše statistiky, to je další pozoruhodná věc. S tím jste začal hned, když jste nastoupil k fotbalu?**

Ne, to začal psát Láďa, ten to psal prvních asi dvacet let, pěkně přehledně v jednom sešitu. Já jsem si začal svoje poznámky psát od roku 1983. A dělal jsem to až do roku 2019.

**Jak jste si obstarával data?**

Na domácích zápasech jsem byl a zvenku mě to řekli trenéři nebo jsem si to přečetl v novinách. Ale to už je pár let, co žáci v novinách vůbec nejsou.

**No aspoň, že jste ty trenéry jste přesvědčil, dneska to dá práci z nich dostat alespoň střelce. Na kronice, kterou jste nechal podle vašich zápisků udělat, mě zaujalo, že je vedená kalendářně, a ne sezónně. To vzniklo jak?**

Takhle s tím Láďa začal, tak se v tom pak zůstalo.

**To je strašně paradoxní, že i když jste do těch statistik zahrnoval i zápasy starých gard, tak Vaše jméno tam prostě není.**

Není. Já mám odehraných 43 sezón a přes 800 zápasů ve stolním tenise. Ten jsme tady založili v roce 1976. Nejdřív jsme domluvili s Josefem Pakostou přátelák proti Tržku, a protože spadal Tržek pod Morašice, tak jsme pak domluvili, že dáme dohromady tým. Jednu dobu jsme měli i áčko a béčko. I letos jsem si ještě připsal výhru. Sice jenom administrativně, takovým trochu podvodem, ale radost jsem měl, že mi ji Milda připsal.

**Takže když se zeptám, co Vám v současné době v Morašicích chybí, co se činnosti Sokolu týče, tak asi pořádná herna.**

To jo. Před deseti rokama jsem myslel, že s tím něco udělá starostka, že by se zrekonstruovala „dvacítka“ a nahoře vzniknul víceúčelový sál, ale když byla v Pardubicích, tak to vyšumělo a teď už na to peníze nebudou. O to víc mě dodneška mrzí, že neprošel můj návrh, aby se tenkrát kabiny udělali poschoďový, že by nahoře byl sál nejen na ten pink ponk. S Láďou jsme kvůli tomu měli i třenice, protože on to nechtěl, a jeho kamarádi taky ne. A když jsem se pak v lednu 1994 rozhodl ve výboru skončit, tak nakonec nesehnal podporu těch kamarádů ani Láďa.

**A ještě něco, co vás mrzí?**

Myslím, že je škoda, že skončila družba s Kleinbautsenem. To začalo nějak přes JZD, pak se do toho zapojil Sokol, tak jsme každý rok soutěžili ve volejbalu, pink ponku a fotbalu. Já jsem proti nim odehrál od roku 1991 13 zápasů a mám nejvíc bodů z oddílu. A tenkrát mě naštvali fotbalisti, protože jeden rok se to přerušilo, protože se nesehnal autobus. Další rok už autobus byl a já ve čtvrtek přišel na pink ponk a zjistil, že v sobotu se nejede, že mezi fotbalisty není zájem. A byl konec.

**Na která období nebo události naopak vzpomínáte nejraději?**

Na všechna období, kdy se dařilo, kdy se na to dobře koukalo. Ale to nebyla jenom ta A třída. Když se založil fotbal, tak naši chlapi měli takový čtyřletý cykly. Po čtyřech letech se postoupilo do III. třídy, po dalších čtyřech od okresního přeboru, a po další čtyřletce jsme měli tým na postup, ale během roku nám skončilo šest hráčů. Frantu Ropka, Bobše Poslušnýho a Mirka Částka si vzala Litomyšl, Franta Štanclu šel do Újezda, Drahoš šel učit do Nového Města a Franta Kalibánu s tím skončil v 26-ti. Mozek a střelec. No a tým byl najednou na sestup, během roku.

**Co jste tenkrát od Litomyšle za takové hráče dostali?**

To si nevzpomenu. Byla to velká sláva, přijela delegace z Litomyšle, ten Oliva - zubák, ale kolik jsme dostali, nebo jestli šli hráči za to, to nevím. Mám pocit, že když šel v roce 1992 do Újezda Pavel Kuta, plus ještě hostování Martina Ropka s Radkem Burešem, tak že za to bylo 21 tisíc, to bylo tenkrát dost peněz. A Jirka Vosmek do Třebové šel za 3 hráče. Ale tu Litomyšl nevím.

**Taková tradiční otázka na závěr, je něco, co byste rád popřál morašickému fotbalu?**

Aby prosperoval. Je podle mě velká chyba, že není dorost, protože když byla dobrá mládež, tak se to pak vždycky posouvalo dál, až do chlapů, tak aby pak bylo s kým hrát.