---
title: 3 v 1, pokaždé jinak
date: 2022-10-01
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Sebranice
sestava: Martina Famfulíková - Vojtěch Štěpán, Sára Vomočilová, Šimon Flídr,
  Josef Rosypal, Matěj Kovář, Matyáš Bulva, Jan Kvapil, Michal Kovář, František
  Flídr, Samuel Vomočil, Jaroslav Chaun
goly: Jan Kvapil, František Flídr
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2022/Horní
  Újezd/Morašice/716980c4-6963-418e-801b-9fc4fa061c96.html
---
Minulou sobotu jsme díky gólům Jana Kvapila a Matyáše Bulvy porazili v Litomyšli 2:1 místní družstvo spojené s Čistou, ve sváteční předehrávce jsme remizovali v Morašicích s Jevíčkem, když za nás skóroval Vojtěch Vojtěch Huryta, a ve třetím utkání „na fleku“ během jednoho týdne jsme v sobotu na Horňáku hostili Sebranice. Tým, který nám je nejblíže na mapě i v tabulce. 

Do druhé půle jsme vstoupili odhodláni zvrátit výsledek na naši stranu. Dobrým pohybem po celém hřišti jsme soupeře uzavřeli na jeho půli. Jenže takto zahuštěný prostor se nám nedařilo překonat dobrou finální nahrávkou a ani střelou ze střední vzdálenosti. Z přemíry naší snahy, nepozorností a chybami v rozehrávce jsme darovali soupeři další dva góly. A až poté se nám podařilo brankáře hostů překonat. K vyrovnání a zdramatizování tohoto utkání jsme již nenašli síly.

M﻿artin Kovář