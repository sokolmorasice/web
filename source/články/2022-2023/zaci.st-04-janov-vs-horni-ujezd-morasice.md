---
title: Zápas blbec
date: 2022-09-17
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Janov vs Horní Újezd/Morašice
sestava: Vojtěch Štěpán - Jindřich Mičík, Ondřej Žďára, Lukáš Mach, Sára
  Vomočilová, Vojtěch Huryta, Jan Kvapil, Josef Rosypal, Jan Kopecký, Filip
  Flídr
goly: null
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1B/2022/Horní
  Újezd/Morašice/f776dfed-488c-4e41-981d-53fccc454eba.html
---
N﻿a sobotní utkání do Janova jsme odjeli v deseti a bylo to znát.  Soupeř nás od začátku zatlačit a vytvářel si šance. První akce se nám sice podařilo ustát, ale když jsme po dvaceti minutách inkasovali dvě branky, začali někteří hráči působit odevzdaně. Janov nás přehrával i v druhém poločase, což vyústilo v další dvě branky. Teprve poté jsme se trochu herně zmátořili, vytvořili si nějaké šance a zkusili pár střel z dálky, ale nic nám tam nespadlo. Ke konci zápasu se soupeř vrátil k herní dominanci a dalšími třemi zásahy stanovil výsledek. Tímto děkuji všem zúčastněným za jejich snahu a doufám, že příště nás bude více.

M﻿iloš Kopecký