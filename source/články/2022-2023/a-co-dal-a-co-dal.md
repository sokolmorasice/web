---
title: A co dál, a co dál...
date: 2023-06-30T11:24:32.304Z
category: obecne
image: /public/images/uploads/logo.png
---
A co bude příští rok?

**Mužům** se téměř ze čtvrtiny proměnilo složení okresního přeboru, když postoupivší Bystré, sestoupivší Trnávku a odhlášivší se Borovu nahradily loni "pralesní" Vendolí, Třebářov a Němčice. Sezónu začnou okresním pohárem první víkend v srpnu, poté se rozjede "ligová" soutěž.

Na své premiérové letošní góly v áčku nenaváže příští rok Tomáš Huryta, neb je ještě **dorost**enec a ty po domluvě dáme tento rok k dispozici Hornímu Újezdu. Ten ve spojení s našimi a sebranickými hráči přihlásil krajskou soutěž 10+1, což by mělo všechny kluky připravit na přechod do mužského fotbalu mnohem lépe, než dosavadní soutěž 8+1. Soupeřem jim budou tyto týmy: Dolní Újezd, Heřmanův Městec, Jablonné nad Orlicí, Lázně Bohdaneč, Žamberk, Sezemice, Chrast/Zaječice, Proseč, Polička/Bystré, Rudoltice/Žichlínek. 

Stejný postup jsme zvolili i s našimi **staršími žáky**, kteří budou na rok zapůjčení do souklubí Horní Újezd / Sebranice. Účelem tohoto spojení bylo také přihlášení soutěže 10+1. Na okrese se bohužel se nenašlo dost týmů s tak početným kádrem, a tak budou hrát opět "jen" 8+1, a to pouze v 6(!) týmech, kromě "nás" přihlásili starší žáky ještě Březová, Městečko Trnávka, Koclířov, Borová a Němčice/Janov. Škoda. Na přihlášení krajské soutěže 10+1 by ale bylo zapotřebí i širší základny u mladších žáků. Alespoň se počty starších žáků rozšíří natolik, aby nemusela řada mladších žáků hrát 2x za víkend. Na starosti je za nás bude mít Miloš Kopecký. 

To, že jsme v obou kategoriích, tj. dorostu i st. žácích, přišli o oficiální zastoupení v názvu souklubí jde na vrub administrativním požadavkům nadřízených orgánů, nicméně jsme dohodnutí, že nějaký zápas do Morašic dostaneme. 

**Mladší žáci** nakonec zůstanou jedinou kategorií, která bude pokračovat jako dosud, tj. v souklubí s Horním Újezdem v okresním přeboru 7+1, přičemž ji z naši strany zajišťuje stejně jako letos Martin Kovář. Jejich soupeři budou Jevíčko/Jaroměřice, Bystré, Sebranice, Čistá, Opatov a Němčice.

Ž﻿áci začnou své soutěže o víkendu 26.-27.8.

**Starší** i **mladší přípravku** přihlásíme příští rok samostatně, o jejich fungování i cvičení nejmenších dětí se budou stejně jako letos starat Jiří Famfulík ve spolupráci s Milanem Klementem a Milanem Chmelíkem.