---
title: Mladí a úspěšní
date: 2023-06-30T10:51:55.296Z
category: obecne
image: /public/images/uploads/logo.png
---
A﻿ jak si v uplynulé sezóně vedli naše mládežnické týmy?

### Dorost

Na morašickém hřišti to letos nebylo tak úplně vidět, ale tento rok jsme měli souklubí s Horním Újezdem od starší přípravky až po dorost. V nejstarší mládežnické kategorii, tj. dorostu, který hrál meziokresní přebor 8+1, jsme dlouhou dobu jsme byli zastoupeni pouze jedním hráčem, klíčovým brankářem Lukášem Němcem, jinak dorost fungoval plně pod taktovkou "Horňáku". Ke konci sezóny, která nakonec skončila slušným 5. místem, se nám podařilo do dorostu začlenit další hráče.

### Starší žáci

Starší žáci v dlouhodobé části okresního přeboru 8+1 uhráli pouze jednu remízu, ale nadstavbovou skupinu ovládli, takže jejich sezónní bilance z celkových 18 zápasů se zastavila na 4 vítězstvích,1 remíze a 13 porážkách, při skóre 28:54.  Jejich nejlepším střelcem byl Ondřej Ždára z "Horňáku" s 11 góly. Takto hodnotí jejich sezónu trenér **Miloš Kopecký**:

"Starším žákům se letošní ročník bodově moc nepodařil. Po odchodu minulého ročníku se kluci a holky herně hledali, jak na podzim, tak na jaře. Sehrání trvalo delší dobu, ke konci základní části se nám čím dál častěji dařilo hrát i pěkný fotbal, ale bez bodového úspěchu. To se nám podařilo zlomit až v nadstavbě, kde jsme vyhráli všechny 4 zápasy. Kdyby se nám dařilo stejně celou sezónu, mohli jsme pomýšlet na lepší umístění, ale snad se nám to podaří navázat v příští sezóně, na kterou se těšíme. Chtěli bychom všechny hráče pochválit a poděkovat jim za snahu i účast, jak na tréninku, tak na zápasech."

### Mladší žáci

Mladší žáci hráli okresní přebor 7+1 a připsali si prakticky opačnou bilanci než jejich starší spoluhráči, kterým téměř v každém zápase vypomáhali. Ze svých 18 zápasů 14 vyhráli, jednou ﻿emizovali a pouze třikrát prohráli, při skóre 76:26. Z třetího místa po podzimní části se skvělým jarem, během něhož byli kromě jedné prohry stoprocentní, vyšvihli až na první místo, a tak po závěrečném utkání mohli křepčit s pohárem pro **okresní přeborníky**!!! Nejlepšími střelci mladších žáků byli se 13 góly Vojtěch Huryta a hornoújezdský Jan Kvapil. Ten za celou sezónu vynechal jediný zápas starších nebo mladších žáků, takže se zastavil až na porci 35 utkání. T﻿renérem mladších žáků byl **Martin Kovář**.

K působení žáků ještě zmínka několika individuálních úspěchů:

Martina Famfulíková s Vojtěchem Hurytou úspěšně reprezentovali v mládežnických okresních výběrech v kategorii U12. Za zmínku stojí vítězství dívčího týmu OFS Svitavy WU13 v Krouně, kterého se zúčastnilo 6 týmu z dalších okresů Pardubického kraje, kde jsme měli zastoupení Martiny Famfulíkové a Sáry Vomočilové z TJ Horní Újezd. Další ocenění tréninkového úsilí vyústilo pozvánkou Martiny Famfulíkové do krajského výběru dívek.

A úspěšně si v žákovských kategoriích vedou i dva naši odchovanci. Daniel Válek v dresu Vysokého Mýta nastupuje v žákovské lize, což obnáší soutěžní zápasy s Hradcem, Pardubicemi nebo v Praze. Přestože spadá do kategorie U12, pravidelně nastupuje a prosazuje se i mezi o rok staršími dětmi. V dresu Svitav se zase úspěšně seznamuje s divizí žáků Vojtěch Andrle, ani on nezůstal v porovnání s vrstevníky bez střeleckých zápisů. 

### Starší přípravka

Největším "problémem" starší přípravky byl velký počet dětí, právě jejich spravedlivé točení pak stálo za nevyrovnanými výsledky. Na finálovém turnaji nicméně stejně jako loni obsadili smutně skvělé druhé místo!!! 

### Mladší přípravka

Problémem mladší přípravky, kterou jsme měli samostatně, byla hlavně nezkušenost a nízký věk, vždyť půlka dětí měla na tuto kategorii ještě rok čas a hrála soutěžní fotbal prvním rokem. Ale postupem sezóny se dařilo svádět s některými soupeři vyrovnanější zápasy, během jara dosáhli na jednu remízu a na závěrečném turnaji se v posledním zápase sezóny dočkali i první společné výhry. Příští rok by se díky zkušenostem z této sezóny i posunu starších ročníků měli radovat častěji. Na to se určitě těší nejen děti, ale i trenéři **Jiří Famfulík, Milan Chmelík a Milan Klement.**

### F﻿otbalová školička

Fotbalová školička se scházela pravidelně jednou týdně v zimním období v tělocvičně a poté co se umoudřilo počasí se přemístila na hřiště. Na cvičení s **Jirkou Famfulíkem** docházelo v průměru 12 dětí od 3 let do 7 let. V pondělí 26.6. proběhlo ukončení sezóny školičky a mladší přípravky na hřišti. 

Velký dík za celou mládežnickou sezónu patří nejen našim trenérům, ale i TJ Horní Újezd, rodičům a všem, co se k čemukoli jakkoli pozitivně nachomýtli.