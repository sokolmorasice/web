---
title: Ani déšt, ani mráz na hračičky neplatí
date: 2022-09-18
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Bystré
sestava: Vojtěch Štěpán, Sára Vomočilová, Šimon Flídr, Vojtěch Huryta, Josef
  Rosypal, Matěj Kovář, Matyáš Bulva, Jan Kvapil, Michal Kovář, František Flídr
goly: František Flídr 2, Jan Kvapil, Vojtěch Huryta
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2022/Horní
  Újezd/Morašice/dd99a954-31c9-49ae-aa56-8fc617677273.html
---
Poprvé v této sezóně nás poslalo rozlosování zápasů před, snad díky počasí, nepříliš početné domácí publikum na Horním Újezdě. Pohled nad naše hlavy nevěštil nic dobrého. Černé mraky plné vody naznačovaly, že se každou chvílí musí spustit déšť. Jen co naši borci vyběhli na hrací plochu, tak také začalo. Na tribuně schovaní rodičové, ohřívající si ruce o své teplé čaje a grogy, nevěřícně mohli sledovat počínání svých ratolestí. Veselé rozcvičování a pobíhání při báčku plné různých vtípků, jako by se nepršelo, naznačuje sílu této party. Třeba v této sezóně nejlepší nebudeme, ale věřím tomu, že s úsměvem z každého zápasu, ať se zadaří nebo ne, odejdeme.

S příchodem rozhodčího nám déšť ještě zesílil. Ani zhoršující se počasí nám nesmylo z tváří dobrou náladu. V tomto bezstarostném duchu jsme vlétli na soupeře. Krásné akce a herní nadvláda nám jako vždy kazila špatná koncovka. Naši hračičkové v útoku to tak mají a my si na to musíme asi zvyknout. Přesto se nám dvě akce podařilo dotlačit za brankovou čáru.

Do druhé půle nám začal ustávat déšť a my se do soupeře zakousli s ještě větší chutí. Na gólmana soupeře se valila jedna povedenější akce za druhou až na …. . Soupeř převážně hrozil jen brejky. S gólovou tečkou mohl slavit pouze jednou a to v momentě, kdy v hlavách našich hráčů byli ještě oslavy po našem třetím. S vykukujícím sluníčkem se nám zadařilo i počtvrté a konec zápasu si už pohlídali.

M﻿artin Kovář