---
title: Okolnosti horší výsledku
date: 2022-10-23
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Horní Újezd/Morašice vs Pomezí
sestava: Vojtěch Štěpán - Sára Vomočilová, Jan Kopecký, Šimon Flídr, Jindřich
  mičík, Martina Famfulíková, Ondřej Žďára, Vít Famfulík, Jan Kvapil, Filip
  Flídr, Josef Rosypal, Daniel Branda, Jan Simonides, Matěj Kovář Jan Kopecký
goly: Jan Kopecký
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1B/2022/Horní
  Újezd/Morašice/5ebc9bf0-4053-460f-882a-d2f0b304ee7f.html
---
Proti Pomezí, se kterým jsme uhráli dosud jediný bod, to byl od začátku vyostřený zápas. Podařilo se nám otevřít skóre po pěkné kombinaci zakončené přesně k tyči Honzou Kopeckým. Přes stoupající emoce se nám i ve druhém poločase dařilo držet vedení na naší straně až do závěru, kdy jsme soupeře nechali dvakrát vystřelit ze středu hřiště a otočit skóre. Na úplném konci jsme měli ještě šanci na srovnání, ale krásnou střelu Marti Famfulíkové vychytal hostující brankář. Po zápase se bohužel děly věci, které nepatří k fotbalu a k mládeži už vůbec ne, jak uznala i disciplinární [komise](https://svitavsky.denik.cz/fotbal_region/petrus-si-nezahraje-rok-drahos-vyfasoval-trest-na-dvanact-utkani-20221029.html).

M﻿iloš Kopecký