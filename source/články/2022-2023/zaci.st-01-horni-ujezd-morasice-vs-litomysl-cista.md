---
title: Nejsou šance, nejsou góly
date: 2022-08-27
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Horní Újezd/Morašice vs Litomyšl/Čistá
sestava: Martina Famfulíková - Zuzana Famfulíková, Ondřej Žďára, Vojtěch Paťava,
  Jan Kopecký, Lukáš Mach, Vít Famfulík, Vojtěch Štěpán, Matěj Brettinger,
  Matyáš Bulva, Jan Simonides, Jindřich Mičík, Filip Flídr, Sára Vomočilová, Jan
  Kvapil, Vojtěch Huryta, Matěj Rejman
goly: null
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1B/2022/Horní
  Újezd/Morašice/8800d05c-090c-406b-9557-c99e5404ebda.html
---
Novou sezónu jsme zahájili na Horním Újezdě zápasem proti Litomyšli. Nerozehráli jsme ho špatně, hrálo se na obě strany nahoru a dolů, ale jen ve středu pole a prakticky jsme si nevypracovali žádnou větší šanci. Druhý poločas se odehrával ve stejném duchu.  Když ale soupeř využil naše dvě chyby ve středu hřiště a dal dvě branky, neměli jsme sílu odpovědět. Ale chtěl bych všechny pochválit za přístup a bojovnost. Doufám, že se začneme sehrávat a výsledky se dostaví.

autor: Miloš Kopecký