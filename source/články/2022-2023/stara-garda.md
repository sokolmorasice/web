---
title: Stará garda
date: 2023-05-23T04:59:03.019Z
category: obecne
image: /public/images/uploads/logo.png
h1: Dolní Újezd SG - Morašice SG
sestava: J. Němec, V. Andrle - J. Jokeš, O. Ropek, F. Ropek, M. Ropek, Josef
  Štěpán, Josef Jána - M. Chmelík, J. Kopecký, Jan Štěpán, J. Růžek, Petr Kuta -
  Jindřich Jána, V. Beneš
multimedia: https://drive.google.com/drive/folders/1j8IAOeG_oQCmqdHClZv1oV7gyn4I7FM4
---
Za pěkného slunečného počasí se v sobotu konalo [odvetné](https://sokolmorasice.cz/%C4%8Dl%C3%A1nky/2022-2023/utkani-starych-gard) utkání starých gard v Dolním Újezdě, které kvůli zranění bedlivě sledoval pouze v roli nehrajícího zapisovatele Petr Štěpán. Hned v 1. minutě jsme začali velkou šancí Jindřicha Jány, ve 4. minutě v dobré pozici střílel vedle Václav Beneš. Po dobrém úvodu bohužel jsme ve 12. minutě bohužel dostali gól od Kořízka, který dorazil do brány Heinischovu střelu do tyče. V 17. minutě se objevil ve velkém vápně soupeře téměř sám Chmelík, ale neproměnil.

Na začátku druhého poločasu, tedy v 31. minutě, obstřeluje svojí slabší pravou nohou Pavel Kuta z hranice vápna čerstvého brankáře Andrleho, který si tak na míč poprvé sáhne až v bráně. I druhý gól nám tedy paradoxně vstřelil hráč, který měl původně nastoupit v našem týmu. Ve 41. minutě pak uzavřel gólový účet zápasu hlavičkou do protipohybu brankáře J.Lipavský.

V zápase jsme měli větší držení míče než soupeř, dařila se nám i kombinace, ale bohužel jen před vápno domácích (ale co by za to áčko v neděli dalo), koncovka byla v našem podání až žalostná a v některých dobrých situacích dokonce žádná. Spokojenost i opojení ze společně stráveného odpoledne a zároveň mrzutost z výsledku a nepoznané radosti ze vstřeleného gólu pak na pozápasovém důkladném rozboru vyústila v domluvu pravidelných měsíčních fotbálků staré gardy, které by tyto nedostatky měly do budoucích odvet odstranit. První domluvený termín je pátek 3. června.