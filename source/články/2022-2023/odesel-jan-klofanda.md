---
title: Odešel Jan Klofanda
date: 2022-09-18T11:19:54.139Z
category: obecne
image: /public/images/uploads/klofanda.jpg
---
V pondělí 12.9.nás navždy opustil Honza Klofanda. Jako hráč působil v našem Sokolu od jeho založení, za muže odehrál 5. nejvyšší počet zápasů, 539, ve kterých vstřelil 16 gólů. Byl to obětavý člověk a kamarád, který dlouhá léta souběžně s hraním za áčko trénoval v žácích generaci "husákových dětí", která pro morašický fotbal později vykopala I.A třídu. Když skončil s hraním, stal se až do posledních chvil pravidelným divákem domácích zápasů.

"Naše generace mu neřekla jinak než "táta" a on to opravdu táta byl, ten fotbalový. Naučil nás, a myslím, že velmi dobře, nejen fotbalovému řemeslu, ale především lásce k fotbalu. Za všechno co jsi nás naučil, i za spoustu vzpomínek, nejen na jízdy na zápasy tvým plným Moskvičem, ti "Táto" děkujeme." Petr Kuta