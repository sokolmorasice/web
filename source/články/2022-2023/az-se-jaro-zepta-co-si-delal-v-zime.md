---
title: Až se jaro zeptá, co si dělal v zimě
date: 2023-03-19T01:01:52.813Z
category: muzi
image: /public/images/uploads/logo.png
---
Zimní příprava mužů byla hlavně personálně hodně turbulentní. Noví trenéři, stále zdravotně indisponovaní Václav Štancl a Michal Kopecký, přivítali v kabině i navrátilce Josefa Štancla a dorostence Tomáše Hurytu, dočasně (slovy 1 jaro) se naopak museli smířit s absencí Filipa Halsbacha. Vrcholem posilovací a běhací části přípravy bylo povedené únorové soustředění, které počtem připomínalo staré časy. Určitě by se hodilo, aby jelo ještě více lidí, kteří tyto staré časy nepamatují, ale na druhou stranu by se i hodilo, aby mladší z pamětníků ještě v kádru zůstali. Bohužel záhy po soustředění opustili tým dva takoví. Nejlepší střelec Bohumil Válek šel dobrovolně hledat štěstí k rivalům z Cerekvice, Václav Štancl se musel nedobrovolně stáhnout na Horňák (myšleno k rodině, ne k soupeři). Jeho pozici převzal na plný úvazek Michal Kopecký, kterému bude pomáhat z pozice hrajícího asistenta Vojtěch Štěpán. 

**5.3.2023 - UMT Litomyšl**

### **Morašice - Horní Újezd 2:2 (1:0)**

Za těchto okolností byly i přátelské zápasy trochu loterie se sestavou a herním projevem. V tréninkovém zápase s Cerekvicí jsme ještě přišli o zraněného Nádvorníka, a tak se nás na první přátelák proti Horňáku sešlo 12, i díky začlenění dalšího dorostence Václava Jirečka a povolání Jiřího Kubeše. Díky neefektivnímu soupeři, vynikajícímu Špinarovi v bráně a Novákovi vepředu se nám podařilo v děravé sestavě nabýt dvougólové vedení. Nejprve si před přestávkou naskočil na Vítův roh Novák, který zhruba po hodině hry sólem po levé straně připravil centrem relativně snadný gól Kubešovi. Deset minut před koncem byl Horňák konečně úspěšný v jednom z útoků do otevřené obrany a po nedůrazu na vápně a pohotové střele jsme prakticky se závěrečným hvizdem přišli i o nejtěsnější vedení.

**Sestava:** J. Špinar - Josef Kopecký, M. Černohorský, K. Kutzer, D. Štancl, J. Vít, V. Novák, J. Kubeš, M. Frank, M. Sokol, T. Huryta, V. Jireček

**Góly:** V. Novák (J. Vít), J. Kubeš (V. Novák)

**18.3.2023 - UMT Litomyšl**

### **Morašice - Pomezí 3:4 (0:2)**

Plánovaný zápas s Borovou nedopadl, a tak byl druhý přátelák zároveň generálkou, na UMT v Litomyšli jsme utkali s bétřídním Pomezím. Hojnější účast sice umožnila rotaci hráčů na více postech, ale především v prvním poločase nás velmi srážela nepřesná rozehrávka či špatný dotek s míčem. Z našich chyb pramenilo několik šancí soupeře včetně obou gólů, které soupeř vstřelil zaskakujícímu Buriánovi v prvním poločase. V tom druhém se naše hra výrazně zlepšila a dočkali jsme se i gólů, o které se postarali Bureš (jubilejní 30.zásah) a Huryta s Černohorským (premiérové góly). První dva proměnili samostatné úniky, třetí hlavičkoval na zadní tyči, přičemž si všichni prohodili role nahrávačů. Každý gól byl bohužel pouze kontaktní, protože Pomezí mezi ně vtěsnalo zásahy z laciných kopů, pokutového a přímého. Benjamínek našeho týmu Huryta pak ještě orazítkoval tyč, a tak naše příprava skončila výsledkově neslavně.

**Sestava:** M. Burián – J. Kopecký, K. Kutzer, M. Černohorský, D. Štancl, Z. Skala, D. Ondráček, J. Vít, M. Frank, M. Sokol, F. Bureš, J. Štancl, T. Huryta, V. Štěpán

**Góly:** F. Bureš (M. Černohorský), M. Černohorský (T. Huryta), T. Huryta (F. Bureš)

Jarní sezóna bude možná pro leckoho z [hráčů](https://sokolmorasice.cz/o-klubu/hr%C3%A1%C4%8Di) a trenérů, ale i pro tým jako celek, poněkud těžší fotbalovou zkouškou, (moderně řečeno zajímavou zkušeností), ale pořád je to jenom fotbal. Tým má v okresním přeboru dobrou výchozí pozici k tomu, aby zkusil hrát fotbal, který ho bude bavit, bez zbytečného tlaku na výsledky, ale s touhou výsledky dělat, hlavně doma, a tím se udržet v popředí tabulky. První příležitost ke společné radosti, pokud to počasí, terén a následně výkon týmu dovolí, bude již tuto neděli od 15:00 v domácím zápase proti Mladějovu.