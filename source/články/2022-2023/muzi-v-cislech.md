---
title: Muži v číslech
date: 2023-06-28T20:39:12.310Z
category: muzi
image: /public/images/uploads/logo.png
---
Naše áčko má za sebou nakonec úspěšný rok. Muži si výsledkově dobře poradili se zimními trenérskými i taktickými změnami, a v těsném finále soutěže obsadili díky lepší bilanci s "Hradcem" a "Dolňákem" třetí místo. Bylo to hlavní díky nejlepší defenzivě soutěže, a bylo to navzdory spoustě laciných ztrát, zejména v domácím prostředí. Jak v půlce podzimu, tak v půlce jara naše mužstvo shodou okolností po výhrách nad Dolním Újezdem "B" mohlo pomýšlet i výš, ale tyto myšlenky v obou případech zahnala následující série nezdarů.

Celkem muži odehráli 30 zápasů, s bilancí 14 výher, 6 remíz a 10 porážek při skóre 53:41. Nejvíce zápasů, 28, odehrál D. Štancl, který na hřišti strávil i nejvíce minut (2490). Alespoň do jednoho zápasu naskočilo 28(!) hráčů.

Některé z výsledkových nezdarů nich šly i na konto toho, že áčku chybí vyloženě gólový hráč, vždyť 6 podzimních gólů odešlého B. Válka pouze dorovnali F. Bureš a nejlepší střelec jara D. Štancl. Celkově si alespoň jeden gól připsalo 19 hráčů. Někde nemají ani tolik lidí, natož střelců, jinde zase mají střelce, kteří dají tolik, co naši nelepší dohromady.

Nejlepším nahrávačem byl J. Vít s 9 asistencemi, 6 gólů připravil Z. Skala a 5 D. Ondráček.

V produktivitě se pak tři hráči srovnali na 10 bodech, a to F. Bureš (6+4), Z. Skala (4+6) a J. Vít (1+9).

Nejtrestanějším, a vzhledem k minimálnímu počtu karet za nesportovní chování, asi i nejbojovnějším hráčem, byl D. Ondráček, který nasbíral 10 žlutých a jednu červenou. Více karet za nesportovní chování určitě nasbíral druhý v této statistice Vít (7 ŽK), ale dlužno mu přiznat, že poměr zbytečných žlutých býval v jeho případě vyšší a že ho právě kázeňské polepšení v této sezóně stálo titul nejtrestanějšího hráče.

Za nejlepší defenzivou okresního přeboru se vystřídali 4 gólmani, z nichž pouze jeden nedokázal udržet čisté konto. Nejčastěji to byl J. Špinar (17z.), za kterého po většinu jara obětavě zaskakoval M. Burián (10z.). V závěru soutěže se v bráně ještě vystřídali premiérově F. Halsbach (2z.) a V. Štancl (1z.)

Trenérská funkce pro jaro zbyla na Michala Kopeckého, který povede mužstvo i do sezóny nové. Na jaře mu výrazně pomáhal Vojtěch Štěpán, který se ale od nové sezóny bude věnovat trénování svých dětí v Litomyšli a zdraví ho bohužel příliš limituje i na to, aby nám ve větší míře pomáhal jako hráč. Nicméně za jeho divákovi neviditelný přínos při neplánovaném jarním úvazku mu patří velký dík.

S﻿ouhrnné statistiky najdete [zde](https://docs.google.com/spreadsheets/d/12RDBybdIfwAnptau3mS_vyLCJLjbg6utglt7vGg450E/edit?pli=1#gid=556056943).

O﻿hlédnutí za výsledky mládeže i historického okénka se dočkáte v následujících dnech.