---
title: Když se na slunci nedaří
date: 2022-09-25
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Horní Újezd/Morašice vs Bystré
sestava: Vojtěch Štěpán - Sára Vomočilová, Jan Kopecký, Vojtěch Huryta, Jindřich
  Mičík, Lukáš Mach, Zuzana Famfulíková, Jan Kvapil, Filip Flídr, Vojtěch
  Paťava, Ondřej Žďára, Martina Famfulíková, Daniel Branda, Vít Famfulík
goly: Ondřej Žďára 2
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1B/2022/Horní
  Újezd/Morašice/b2cae08a-fccb-4bf6-9ece-f0c8d541dca0.html
---
Do nedělního zápasu jsme vstoupili takřka v plné sestavě.  Přálo nám počasí a vyšel nám i začátek zápasu, ve kterém jsme si šance nejen vytvářeli, ale ve dvou případech Ondrou Žďárou i proměnili. Za stavu 2:0 jsme přenechali iniciativu soupeři, kterému se podařilo do poločasu srovnat. Ve vstupu do druhého poločasu jsme inkasovali branku, na kterou už jsem nedokázali odpovědět. Soupeř nám potom přidal další čtyři góly a bylo rozhodnuto. Tak doufáme, že naše první vítězství přijde příští víkend, aby to kluky trochu namotivovalo do dalších zápasů.

M﻿iloš Kopecký