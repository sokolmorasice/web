---
title: Kdo si počká....
date: 2022-10-16
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Litomyšl/Čistá vs Horní Újezd/Morašice
sestava: Martina Famfulíková - Sára Vomočilová, Jan Kopecký, Šimon Flídr,
  Jindřich Mičík, Zuzana Famfulíková, Ondřej Žďára, Vojtěch Štěpán, Jan Kvapil,
  Filip Flídr, Vít Famfulík, Josef Rosypal, Matěj Brettinger
goly: null
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1B/2022/Horní
  Újezd/Morašice/b2e1b685-ada1-4223-8fec-2b5cf0b091de.html
---
V nedělním utkání jsme nastoupili proti domácí Litomyšli a konečně se nás sešlo dost, konkrétně třináct.  Zápas jsme jako obvykle začali špatně brzo inkasovali. Pak jsme si dokázali vytvořit několik šancí, ale branku jsme nedali. Nad vodou nás držela Marťa, která po delší době nastoupila v brance a likvidovala soupeřovy šance, až na jednu těsně před poločasem. Druhý poločas se odehrával ve stejném duchu, jenom soupeř už neskóroval. Takže na první výhru si stále musíme počkat.

M﻿iloš Kopecký