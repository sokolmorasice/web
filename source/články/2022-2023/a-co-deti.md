---
title: A co děti?...
date: 2022-12-11T19:18:20.925Z
category: obecne
image: /public/images/uploads/logo.png
---
Z mládeže spojené s Horním Újezdem si nejlépe vedli mladší žáci, kteří vstoupili do přípravy i začátku sezóny vlažně, ale když se po prázdninách zlepšila docházka, dostavily se i výsledky a je z toho stejně jako u mužů třetí místo. A kromě svých zápasů jich většina pomáhá doplnit soupisku starších žáků, kteří se sezónou výsledkově protrápili a i s porcí smůly v podobě opakovaných zranění zakončili půlrok s jediným bodem. Oba týmy doplatili několikrát i na slabou efektivitu, nejvíce se dařilo Janu Kvapilovi ve starších a Ondřeji Žďárovi v mladších, kteří zaznamenali 7, resp. 5 gólů.

Starší přípravka je také spojená s Horním Újezdem a dá se říct, že "bojuje" s vytížením dětí, kterých je skoro na dva týmy. Výsledkově to nebylo úplně ono, ale právě množství dětí a jejich chuť je mnohem důležitější.

Větší počet dětí byl už před sezónou v kategorii mladších přípravek, kterou jsme letos přihlásili samostatně. Takto její působení hodnotí jeden z trenérů Milan Chmelík: "Začátek sezóny byl hodně rozpačitý. Pro většinu dětí byl podzim prvním vyběhnutím na fotbalový trávník. Postupně se seznamovaly s oficiálními pravidly a systémem hry. Zkušenější protivníci měli výrazně navrch. To však děti neodradilo, poctivě trénovaly v hojném počtu dvakrát týdně a zápas od zápasu se zlepšovaly. Absolvovali jsme 3 přípravné turnaje, dále 7 oficiálních v rámci mistrovské soutěže, z toho jsme 2 turnaje pořádali na domácím hřišti v Morašicích. Bohužel se dětem nepodařilo v žádném zápase zvítězit, i když by si to několikrát za bojovnost zasloužily. To je však neodradilo, sbíraly cenné zkušenosti a zlepšovaly se každým zápasem, což je dobrým příslibem do budoucna. Ale hlavně se fotbalem bavily, a to je to nejdůležitější."

V tréninkovém spojení s mladší přípravkou funguje pravidelně i školička i pro nejmenší. Tréninky se s pokročilejší roční dobou přesunuli do tělocvičny, ale pro zájem dětí trvaly až do prosince.

Za péči o našich zhruba 30 dětí si zaslouží dík všichni naši trenéři - Jirka Famfulík, Miloš Kopecký, Martin Kovář, Milan Klement a Milan Chmelík - všichni rodiče, kteří pomohou třeba jenom odvozem na zápas a v neposlední řadě i oddíl "Horňáku", bez kterého bychom se do většiny soutěží nemohli přihlásit. Jak jsou se sezónou spokojeni oni, si můžete přečíst na jejich stránkách [zde](http://www.tjhorniujezd.cz/).