---
title: Nedáš, naleješ
date: 2022-08-07
category: muzi
image: /public/images/zapasy/morasice-borova.png
h1: Morašice vs Borová
sestava: J. Špinar - V. Tměj, D. Štancl, V. Štancl (M. Kopecký), V. Novák - J.
  Vít, T. Nádvorník, B. Válek, D. Ondráček (V. Štěpán, M. Černohorský) - F.
  Bureš (J. Kopecký), P. Lněnička (M. Frank)
goly: F. Bureš (P. Lněnička)
zlute: null
cervene: null
multimedia: https://drive.google.com/drive/folders/14CnOB2APHwSArMRqUoY3bJgMZ9Wo3k5N
scortes: https://scortes.rozpisyzapasu.cz/api/533A4A/2022/Morašice/e3a8c3e4-40c7-4f8b-96b5-bf3686f0eace.html
---
Tradiční ostrý začátek fotbalové sezóny obstaralo první kolo poháru, ve kterém jsme přivítali obhájce z Borové. Los těžký ,ale pořád lepší než v současném suchu hrát v Telecím. Do rychlé úvodní šance vyslal hosty na beku hrající Novák, bortelský útočník naštěstí z úhlu a v tísni jen dovedl míč až do malého vápna ke Špinarovi. Naše první zaměstnání soupeřova brankáře si připsal roh do brány kroutící Válek. Na povážlivě velký počet odevzdaných míčů jsme si dokázali vypracovat i povážlivě velký počet nadějných pozic, ale Ondráček i Bureš si prvním dotekem zkazili možnost nájezdu. Druhý jmenovaný to trochu vyžehlil střelou, proti které gólman předvedl další dobrý zákrok. Při dalším zabrzděném nájezdu přenesl odpovědnost na Víta, ale ten si v první řadě nepohlídal ofsajd, v druhé řadě ani mířidla. Hosté hrozili častou, ale nepřesnou střelbou a opravdu rychlými náběhy, náš gólman tak zákrokem jistil pouze jedno zakončení z úhlu, to druhé vytlačil očima na protější tyč.  Jeho letošní neprůstřelnost tak skončila až po 130 minutách, když po našem autu na polovině neudržel útočník míč, skoro kompletní záloha ho následně nezvládla odebrat kličkujícímu hbitonožkovi, skoro kompletní obrana nedokázala zachytit náběh na kolmici středem a hostující hráč poslal z penalty lehkou střelu vedle kompletně stojícího gólmana. Rychlou odpověď si naštěstí nachystal Bureš, který Tmějovu nouzovou pobídku do sóla opět neřešil přímým sprintem, ale předáním balónu Lněničkovi, ten ustál souboj koleno na koleno a tvrdou střelou o břevno přihrál zpět na velké vápno Burešovi, jenž po zpracování napálil míč do sítě, jak jinak než o břevno. 
Na startu druhé půle se střídajícímu Štěpánovi nepodařilo potrestat dvě chyby v hostující rozehrávce, ale před rychlým nuceným střídáním na oplátku naservíroval dva rohy přímo na hlavu V. Štancla. Náš stoper bohužel ani jednu z ložených pozic neproměnil a před nuceným střídáním na oplátku chyboval v rozehrávce on. Hosté si dvakrát vyměnili balón a z vápna zakončili přesně na zadní tyč. O dalších hostujících střelách nebo našich šancích, jako třeba té Nádvorníkově nebo anonymní po rohu, je zbytečné psát, protože všechno přebil M. Kopecký, který dvě minuty před koncem po Novákově průniku odmítl penaltový rozstřel překopnutím prázdné brány z 5 metrů. Takže nám nezbývá, než vrhnout všechnu sílu na ligu. A že to zrovna příští týden možná budou i starodávné síly.