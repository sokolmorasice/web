---
title: Nejsou góly, není výhra
date: 2022-09-04
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Pomezí vs Horní Újezd/Morašice
sestava: Martina Famfulíková - Vojtěch Huryta, Ondřej Žďára, Vojtěch Paťava,
  Lukáš Mach, Šimon Flídr, Jan Kvapil, Sára Vomočilová, Jan Kopecký, Filip
  Flídr, Matěj Rejman, Vojtěch Štěpán, Zuzana Famfulíková
goly: null
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1B/2022/Horní
  Újezd/Morašice/e7b87b04-0250-4baa-a63e-fa087f79a759.html
---
Do druhého zápasu v Pomezí jsme nenastoupili v plné sestavě.  Hráli se stejně jako před týdnem pěkný fotbal na obě strany, jen bez kvality v závěrečné  fázi a tím pádem bez šancí. A protože na tom byl soupeř stejně, byl poločas bez branek. Ve druhém poločase jsme po zbytečném soupeřově zákroku přišli o Marťu, ke konci se šance přelévali z jedné strany na druhou, ale žádná nebyla úspěšná, takže jsme se spravedlivě rozešli s remízou.

autor: Miloš Kopecký