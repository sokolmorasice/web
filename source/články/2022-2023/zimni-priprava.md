---
title: Zimní příprava
date: 2022-12-21T20:07:38.315Z
category: muzi
image: /public/images/uploads/logo.png
---
## P﻿lán zimní přípravy

Č﻿T 12.1. 17:30 - první trénink v tělocvičně, poté každé úterý a čtvrtek od 17:30

P﻿Á 10.2. -NE 12.2. - soustředění v Daňkovicích 

* P﻿Á - ráno odjezd, v 16:30 přátelák na UMT v Domaníně s Rožnou (OP Žďár nad Sázavou)
* S﻿O - 14:30 - 16:30 hala v Jimramově, 19:00 wellness
* N﻿E - 10:00 - 11:30 možnost haly v Jimramově nebo Sněžném, po obědě rozchod

N﻿E 5.3. 15:00 - přátelák s Horním Újezdem na UMT v Litomyšli

N﻿E 12.3. 9:00 - přátelák s Borovu na UMT v Litomyšli

S﻿O 18.3. 9:00 - přátelák s Pomezím na UMT v Litomyšli (v případě příznivého počasí změna času a přesun na naše nebo jejich hřiště)

NE 26.3. 15:00 - mistrák s Mladějovem v Morašicích