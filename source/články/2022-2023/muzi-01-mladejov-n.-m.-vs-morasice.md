---
title: Plni vděku všem všeho věku
date: 2022-08-13
category: muzi
image: /public/images/zapasy/mladejov-morasice.png
h1: Mladějov n. M. vs Morašice
sestava: J. Špinar - F. Bureš, J.Kopecký, M. Ropek, V. Novák - D. Ondráček (M.
  Burián), B. Válek, T. Nádvorník, J. Vít - Z. Skala (M. Zerzán), V. Štěpán (M.
  Frank)
goly: B. Válek 2 (1x pen.)
zlute: Z. Skala, T. Nádvorník, M. Burián
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533A1A/2022/Morašice/ffffc13b-5263-4f6d-8cf0-d99d2562f2e5.html
---
Přehození veškerého soustředění z neúspěšné pohárové pouti na ligu vypadalo v našem podání tak, že jsme po část týdne kalkulovali s dopravou pouze dvěma auty. Naše obvyklá základní sestava byla rozházená po celé republice, od Vltavy po Vranov, jakoby se nedalo chlastat doma jako každou, třeba minulou, sobotu. Trenér z toho hrůzou nespal, ale nakonec jsme to díky zaskočivším Zerzánovi a Buriánovi dotáhli na 14 lidí. Stabilně docházející 95-ti letou stoperskou dvojici překvapivě doplnil na beku Bureš, ale původně se schylovalo i k většímu překvapením. Návrat po 15-ti letech nakonec nedopadl, a tak naše obrana spolu s vysokorychlostním (TG)V. Novákem alespoň zůstala pod 150 lety.

Taktika pro zápas byla jasná, výjimečně nenapadat, šetřit síly, a neztrácet míče. Útočníci Štěpán a Skala si tak velmi často chodili do hloubi pole pro balón a nahrávkami rozpumpovávali ofenzivní choutky krajních záložníků i obránců. Domácí šli alespoň podle dění na hřišti do zápasu s podobnou strategií, a zbytečně nelétali. I tak ale první poločas přinesl řadu šancí na obou stranách, domácí využívali volného prostoru ve středu hřiště a kombinací přihrávek a štěstí se dostali k několika nebezpečným šancím. Při jedné střele si Špinar zaskákal, podruhé se před ním po zaváhání v obraně zjevil útočící hráč, jehož střelu ve vteřině zalepil do rukavic. Mladějovští měli i další příležitosti a náznaky gólovek, ale k jejich promění chybělo přesnější zakončení. Domácího brankáře prověřil tvrdou střelou Nádvorník, štěstí zkusil i Bureš, který si po celý zápas s přehledem plnil defenzivní povinnosti. Ondráčkovi nabídl možnost ke skórování přesným pasem ještě ledově klidný Vít. První jmenovaný sice mířil lépe než při odbočce na Mladějov, ale ani to nestačilo, aby spolehlivě chytajícího Cajthamla překonal. Vstřelení branky odmítl i Novák, a tak první poločas skončil bez branek. Za zmínku kromě šancí stojí i sprinterský souboj, který s Kopeckým prohrál o dvě generace mladší soupeř. 

Na začátku druhé půle se znovu připomněl Novák, ale při samostatném úniku trefil pouze gólmana. Následovala hlušší pasáž, tedy co se týče šancí.  Přes mírnou převahu domácích totiž nejvíc zaujaly stupňující se slovní hrátky, mezi kterými vyčníval další generační souboj (domácí zkušený Kudyn a hostující mladí a neklidní Vít s Ondráčkem). Půl hodiny před koncem náš kapitán Válek precizní střelou přes zeď zařídil vedení a mírný klid na duši. Dalších 15 minut rozkouskovalo 7 střídání, ale čerstvé síly ale pomohly mnohem více nám. Zvlášť po 4 letech povolaný Burián byl hodně vidět. Pár minut mu stačilo na žlutou kartu a o něco delší chvíle na vybojování prvního a posledního pokutového kopu v zápase. Ten s přehledem proměnil Válek, a vítězství už nám nesebral ani rychlý mladějovský útok po rozehrávce ze středového kruhu, ani rohový kop domácích. Takže ani nemrzela neodpískaná penalta po zákroku mladějovského gólmana, opět na Buriána. 

Bojovný a zodpovědný výkon, a na okresní poměry i místy pohledný fotbal vedly ke třem bodům z venku. Oproti předpokladům tak největší fiasko přišlo až při vítězném pokřiku, kde se výrazně ukázala absence kapitána V. Štancla. „Cigi caga“ zvládal totiž lépe i boss Matějka, alespoň že svědkem výstupu nebyla morašická fanouškovská základna. Ta se může nechat překvapit, jak hodně bude trenér sahat do vítězné sestavy.