---
title: Jen počkejte...
date: 2022-10-03T17:54:59.340Z
category: pripravka
image: /public/images/uploads/logo.png
multimedia: https://drive.google.com/drive/folders/13uqbzym0S7VlpWFpyEvxQhVYSNKPQzHH
---
## T﻿urnaj mladší přípravky 25.9. v Borové:

**S﻿estava:** Matyáš Tóth, Max Čechal, Jiří Famfulík, Matouš Jetmar, Jiří Kubík, Jan Klement, Lukáš Chmelík, David Soukup

**M﻿orašice - Jevíčko 1:8 (0:4)**

b﻿ranka: Maytáš Tóth

**M﻿orašice - Borová 0:17 (0:7)**

**M﻿orašice - Litomyšl 1:7 (1:4)**

## **T﻿urnaj mladší přípravky 2.10. v Morašicích:**

**S﻿estava:** Matyáš Tóth, Max Čechal, Jiří Famfulík, Matouš Jetmar, Jiří Kubík, Jan Klement, Lukáš Chmelík, David Soukup, Kateřina Kovářová, Filip Šplíchal

**M﻿orašice - Dlouhá Loučka 2:4 (1:1)**

b﻿ranky: Matyáš Tóth, Ivo Langr

**M﻿orašice - Borová 0:12 (0:6)**