---
title: Když dojdou slova
date: 2022-11-06
category: muzi
image: /public/images/zapasy/morasice-policka.png
h1: Morašice vs Polička B
sestava: J. Špinar - V. Tměj, M. Ropek, M. Černohorský, V. Novák - J. Vít (F.
  Bureš), T. Nádvorník, B. Válek, Z. Skala - P. Lněnička (V. Štěpán), M. Sokol
  (J. Kopecký)
goly: null
zlute: B. Válek, J. Vít
cervene: null
multimedia: https://drive.google.com/drive/folders/1kQ0xNUJ8VD6mcHKFZgOlVa3oDV8ONYGZ
scortes: https://scortes.rozpisyzapasu.cz/api/533A1A/2022/Morašice/de8aadb1-43dc-4c14-a4ef-6611bb1d870b.html
---
Tak už to v životě bývá, že něco začíná a něco končí. 13.5.1996 začal náš hrající trenér Martin Ropek dávat góly za áčko a před zápasem s Poličkou na konec podzimu jsme mu připomněli, že minulý týden jejich počet jako 8. hráč v naší historii zaokrouhlil na rovnou stovku. A to je možná všechno, co se dá o tomto zápase napsat hezkého. A protože kvůli svatebnímu úklidu poprvé od 18.8.2013 chyběl v domácím zápase Morašic nějaký Štancl, je to asi tak všechno, co se o prohře 0:2 dá napsat celkově. Aspoň, že fotky máme.