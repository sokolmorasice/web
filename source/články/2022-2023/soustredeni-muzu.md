---
title: Soustředění mužů
date: 2023-02-16T07:26:58.358Z
category: muzi
image: /public/images/uploads/logo.png
multimedia: https://drive.google.com/drive/folders/1mb8Eds3PM-vVD9KNxowcWBmIuTQQpNUG
---
N﻿aši muži absolvovali o víkendu 10.-12.2. tradiční soustředění v již opraveném penzionu Selský dvůr v Daňkovicích. Abychom umožnili náhled publikovatelných fotek i lidem bez facebooku, a abychom splnili přání Dolního Újezdu, který s námi sdílel penzion, zveřejňujeme o něm tento kratičký článek.