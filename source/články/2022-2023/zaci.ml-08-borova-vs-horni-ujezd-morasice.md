---
title: Závěrečnou výhrou na bednu
date: 2022-10-29
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Borová vs Horní Újezd/Morašice
sestava: Martina Famfulíková, Vojtěch Štěpán, Sára Vomočilová, Šimon Flídr,
  Vojtěch Huryta, Josef Rosypal, Matěj Kovář, Matyáš Bulva, Jan Kvapil, Michal
  Kovář, František Flídr, David Klusoň, Jaroslav Chaun
goly: Jan Kvapil, Matyáš Bulva
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2022/Horní
  Újezd/Morašice/0b222203-515a-48b3-b2f0-e045a061a78b.html
---
V sobotu jsme vyrazili na poslední střetnutí podzimní části sezóny do Telecího, kde se hrají zápasy s týmem Borové. Dojet ke hřišti byl ale oříšek, mlha s viditelností do deseti metrů by se dala krájet, naštěstí se s příjezdem ke hřišti situace začala lepšit, sluníčko se postupně začalo prokousávat skrz tuto bílou tmu a zápas mohl regulérně začít.

Začátek nám vyšel parádně. Povedené akce po lajnách i středem s výborným pohybem všech hráčů naznačoval pohodový zápas, ale postupem času jsme se přizpůsobili soupeři, který využíval znalost místního velmi malého a úzkého hřiště. Z čehož také pramenila domácí první branky zápasu v jejich podání. Z této sprchy se naši hráči oklepali celkem brzy, začali makat jako na začátku a odměnou jim byl míč v soupeřově síti a poločasové skóre 1:1.

Do druhé půle nebylo co měnit, jen drobnost, přidat na důrazu v soupeřově vápně. Jestli jsme soupeře v první půli pustili do šance tak čtyřikrát, ve druhé to bylo už jen jednou. Dnes pro nás byla šťastná minuta osmnáctá, ve které jsme vstřelili gól v prvním i druhém poločase. 

Martin Kovář