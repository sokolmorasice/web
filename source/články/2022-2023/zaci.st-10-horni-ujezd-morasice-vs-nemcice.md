---
title: Do jara bez výhry
date: 2022-10-30
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Horní Újezd/Morašice vs Němčice
sestava: Vojtěch Štěpán - Sára Vomočilová, Jan Kopecký, Šimon Flídr, Jindřich
  Mičík, Zuzana Famfulíková, Ondřej Žďára, Martina Famfulíková, Jan Kvapil,
  Filip Flídr, Vít Famfulík, Matyáš Bulva, Matěj Kovář, Daniel Branda, Jan
  Simonides
goly: Jindřich Mičík 2 (1x pen.)
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533E1B/2022/Horní
  Újezd/Morašice/191cf7cd-f470-47e5-9526-eb9dcbe4979c.html
---
V závěrečném utkání podzimu proti Němčicím jsme od začátku doháněli, ale na to, že jsme hráli s prvním, nebyl rozdíl na hřišti tak znát. Vytvářeli jsme si pěkné šance, všichni se snažili bojovat a nakonec jsme našli i dalšího střelce, přesto jsme prohráli i poslední zápas podzimu. Ale chtěl bych všem poděkovat za výkony, těším se na jaro a doufám, že po kvalitním "zimním spánku" začneme vozit nějaké body.

M﻿iloš Kopecký