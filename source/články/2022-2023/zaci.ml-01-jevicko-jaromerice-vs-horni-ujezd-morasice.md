---
title: Lepení na začátek
date: 2022-08-28
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Jevíčko/Jaroměřice vs Horní Újezd/Morašice
sestava: Famfulíková Martina, Kvapil Jan, Bulva Matyáš, Flídr František,
  Vomočilová Sára, Kovář Matěj, Kovář Michal, Tomšíček Kryštof, Boček David,
  Krejsa Antonín
goly: null
zlute: null
cervene: null
scortes: https://scortes.rozpisyzapasu.cz/api/533F1A/2022/Horní
  Újezd/Morašice/30f32f4f-b9b8-4315-ab1e-fbc80b49e1c0.html
---
Dobrý den a ahoj všem rodičům a fanouškům našich malých sportovců. K prvnímu utkání nové sezóny jsme se sjeli na hřiště v Jevíčku. Po sobotní průtrži mračen slibovalo velmi podmáčené hřiště tuhý boj obou týmů o každý metr hřiště. Náš nově složený tým plus několik omluvenek kluků ze základní sestavy přineslo hned na začátek několik otazníků se skládáním sestavy.

V úvodních deseti minutách zápasu jsme soupeře překvapili několika slibnými akcemi s nedůrazným zakončením, na což soupeř zareagoval přeskupením řad a lepším pokrýváním našich hráčů. To nám začalo dělat velké problémy v rozehrávce a různá nedorozumění mezi hráči přineslo poločasové skóre 3:0 ve prospěch domácích. 
Ve druhém poločase jsme se začali herně zvedat, kombinace začaly být koukatelnější a osmělování do útoku častější. Jenže každé naše zlepšení bylo sraženo špatnou a nedůraznou koncovkou. Zato soupeř nám dal dalšími dvěma góly jasně najevo, kdo je dnes domácím pánem.

a﻿utor: Martin Kovář