---
title: Podzimní ohlédnutí
date: 2019-11-22T05:21:03.987Z
category: zaci.ml
image: /public/images/uploads/logo.png
---
Mladší žáci v souklubí HÚ/Morašice na podzim sehráli 10 soutěžních zápasů. Všech deset bylo vítězných s celkovým skóre 55 : 6. Mohlo by se zdát,že to byla jasná záležitost,ale opak je pravdou. Začátek sezóny jsme pálili ostrými a v pěti odehraných zápasech jsme vstřelili 38 branek a pouhé 3 obdrželi. Šestý zápas jsme sehráli s naším největším rivalem ze Skutče a byla to opravdu bitva. Po osmi minutách jsme prohrávali už 0:2,ale ani to naše kluky a holky nepoložilo, v deštivém počasí předvedli nádherný obrat, a tak jsme si ze Skutče odvezli těsnou výhru 3:2 a osamostatnili se na čele tabulky právě před Skutčí.

Zbylé 4 zápasy nás zastihli tak trochu v "krizi" ( vyjma dvou povedených poločasů). Nedařila se nám kombinace, navíc "došel střelný prach", a tak se víc bojovalo než hrálo. A možná právě proto si těch výher moc cením. Když to nešlo fotbalově, tak to tam vždy ti naši kabrňáci tak nějak "dotlačili" :) Je to prostě dobrá parta a musím říct, že nás, nebo tedy mě určitě, asi nikdy nepřestanou překvapovat :) 

Velké dík patří starším hráčům za to, jak "přijali" ty mladší, nově příchozí. A těm mladším za velký přínos pro celý tým... 

Moc děkuji také rodičům za praní dresů, za pomoc s dopravováním hráčů na zápasy a na tréninky...Děkuji, bez Vás by to nešlo.

A s Vámi, kteří zavítáte na jarní zápasy, se těším na viděnou a zároveň děkuji za podporu....

Jirka Kučera
