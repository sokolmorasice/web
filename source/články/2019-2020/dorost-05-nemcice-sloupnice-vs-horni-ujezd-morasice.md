---
title: Zřejmě jsme zůstali doma
date: 2019-09-22T00:00:00.000Z
category: dorost
image: /public/images/novinky/2019-2020/dorost.png
h1: Němčice/Sloupnice vs Horní Újezd/Morašice
sestava: >-
  Jaromír Loskot - Martin Tmej, Pavel Beneš, Jan Kazda, Petr Kazda - Radovan
  Flídr, Tomáš Briol, Jakub Nádvorník, Jiří Šturc (Jan Vedral) - Jan Pechanec
  (Filip Kopecký), Luděk Bureš
zlute: 'Radovan Flídr, Jan Pechanec'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533C2B/2019/Horní
  Újezd/Morašice/23a8938f-b6f0-4be2-aa13-bd474c295f4d.html
---
Jedno přísloví praví: Do třetice všeho dobrého. Ne vždycky. To, co se dělo v neděli ve Sloupnici, založíme hluboko do archívu, snad. Přibývající omluvenky nakonec posadili na lavičku pouze žáky Kopeckého a Vedrala. Patří jim velký dík, za to, že nám přijeli pomoct.

Nemá cenu dlouze rozepisovat chyby, nebo se vymlouvat na to, že nám hrubě nevyšly oba začátky poločasů. Výsledek 6:0 hovoří za vše, a to domácí ještě dost šancí zahodili. My jsme brankáře neprověřili ani jednou. Za celý zápas si sáhl na míč asi dvakrát, když stahoval centry Šturce. Jinak to bylo po většinu zápasu trápení, ze kterého bolely oči a s fotbalem to z naší strany nemělo nic společnýho. Sešlo se najednou víc špatných faktorů, které zkusíme do příštího týdne minimalizovat. Doufáme, že na nás bude v neděli vidět, že nás fotbal pořád baví. Stejně jako to bylo až do tohohle zápasu. Taky se říká, že jedna vlaštovka jaro nedělá. Obráceně to platí stejně.
