---
title: Jaroměřické nadělení
date: 2019-11-03T00:00:00.000Z
category: dorost
image: /public/images/novinky/2019-2020/dorost.png
h1: Jaroměřice vs Horní Újezd/Morašice
sestava: >-
  Jaromír Loskot - Pavel Beneš, Martin Tmej, Jakub Nádvorník, František Lněnička
  (Emanuel Hanyk) - Jiří Šturc, Vojtěch Opletal, Radovan Flídr, Jan Pechanec -
  František Bartoš, Luděk Bureš
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533C2B/2019/Horní
  Újezd/Morašice/f82c2510-4f55-4d13-bd37-237acff947dd.html
---
Na závěr podzimní část sezóny jsme byli vysláni na samý okraj svitavského okresu, abychom s lídrem tabulky poměřili své síly. Příjezd autobusu 70 minut před zápasem budil určitě respekt. Bohužel se nám do něj podařilo sehnat jen 12 hráčů (včetně dvou brankářů), dva trenéry, dva otcofanoušky a jednoho řidiče. Zbylá část týmu hrála za žáky, stonala nebo se trapně vymluvila.

S určením sestavy nebyly tentokrát žádné potíže a těžko říct, jestli nám alespoň trochu pomohlo hokejové střídání. Prvních deset minut se hrál vyrovnaný fotbal, alespoň co se nasazení a bojovnosti týče. Za dalších deset minut bylo prakticky po zápase. Stačilo pár chyb, nafasovali jsme 4 branky a pak už jen bolely oči. Za celý zápas jsme si vypracovali jen jednu střelu (šanci) na branku. Za stavu 1:0 pro domácí se po přihrávce Bartoše dostal do samostatného úniku Flídr, ale tváří v tvář Nečasovi neuspěl. Naštěstí nebyl takový nečas jako v sobotu, takže psychicky odolný jednec neměl výraznější problémy zápas dokoukat.

Měsíc před Mikulášem jsme dostali hezky zaokrouhlenou nadílku. Nechceme nikterak snižovat výkon domácích, ale nutno dodat, že na tyhle body se příliš nenadřeli. Podzimní zápasy venku nám, slušně řečeno, nevyšly. Jen je škoda trávit půl neděle tak daleko, aby to po chvíli vypadalo, že jsme na hřišti letos poprvé.
