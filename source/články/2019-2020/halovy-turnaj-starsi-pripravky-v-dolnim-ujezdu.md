---
title: Halový turnaj starší přípravky v Dolním Újezdu
date: 2020-02-23T09:40:44.441Z
category: pripravka
image: /public/images/uploads/logo.png
---
**Výsledky:**\
Dolní Újezd žlutá – Horní Újezd/Morašice 3:1

Horní Újezd/Morašice – Litomyšl 3:5

Dolní Újezd modrá – Horní Újezd/Morašice 3:0

Horní Újezd/Morašice – Sebranice 1:5

Horní Újezd/Morašice – Svitavy 0:5

**Tabulka turnaje:**

1.Svitavy

2.Sebranice

3.Litomyšl

4.Dolní Újezd žlutá

5.Dolní Újezd modrá

**6.Horní Újezd/Morašice**
