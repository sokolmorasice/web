---
title: Znovu v akci
date: 2020-05-24T08:54:17.831Z
category: muzi
image: /public/images/uploads/logo.png
---
Všechny rozmrzelé a nedočkavé fanoušky zveme na pátek 29.5. do Dolního Újezdu, kde se v 17:30 utkáme se směsicí místního áčka a béčka. 

První červnové neděle se pak představíme i na našem hřišti, kde postupně přivítáme následující soupeře:

Ne 7.6. 16:30  - Horní Újezd

Ne 14.6. 16:30 - Březová nad Svitavou

Ne 21.6. 16:30 - Semanín

Změny jsou samozřejmě vyhrazeny, sledujte naše stránky.