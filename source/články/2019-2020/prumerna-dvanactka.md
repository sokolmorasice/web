---
title: Průměrná dvanáctka
date: 2019-09-14T15:31:34.411Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Mladší přípravka

Druhé dějství okresní soutěže mladších přípravek nás poslalo do Němčic. Obsazení turnaje bylo velice podobné jako minulý týden, jenom jsme se poprvé v nové sezóně utkali s Jevíčkem. Jestliže panovaly před začátkem soutěže mírné obavy o to, jak se hráči, kteří sbírají první turnajové zkušenosti, začlení do sestavy, po do dvou kolech jsou všechny obavy pryč. Až na úvodní poločas se Sebranicemi, kdy chvilku trvalo, než jsme se rozkoukali, jsme všechny zápasy zvládli na jedničku. Zkušenější hráči jsou oporami pro mladší kluky, kteří rostou zápas od zápasu a představují dobrý potenciál do dalších let. 

**Sestava:** Martina Famfulíková, Vojtěch Huryta, Šimon Flídr, František Flídr, Daniel Válek, Jaroslav Chaun, Michal Kovář, Matěj Kovář, Matyáš Lněnička, David Klusoň

Naše výsledky:

**Horní Újezd/Morašice – Sebranice 11:4 (4:3)**

branky: Daniel Válek 5, František Flídr 3, Šimon Flídr 2, Matyáš Lněnička

**Němčice – Horní Újezd/Morašice 0:13 (0:6)**

branky: Vojtěch Huryta 4, Jaroslav Chaun 3, Michal Kovář 2, Daniel Válek, Martina Famfulíková, Šimon Flídr, vlastní

**Horní Újezd/Morašice – Jevíčko 12:0 (8:0)**

branky: František Flídr 3, Daniel Válek 3, Jaroslav Chaun 3, Martina Famfulíková, Vojtěch Huryta, vlastní

**Tabulka turnaje:**

1.Horní Újezd/Morašice

2.Sebranice

3.Jevíčko

4.Němčice

Další turnaj se uskuteční v sobotu 21.9. od půl desáté na Sebranicích.

## Starší přípravka

Ani druhý turnaj sezóny, tentokrát v Březové, se neodehrál kompletní, protože opět nedorazilo družstvo z Cerekvice. 

**Sestava:** Matyáš Bulva, Filip Flídr, Ondřej Kusý, Jan Kvapil, Kryštof Tomšíček, Matouš Zach, Martin Burián, Martina Famfulíková, Vít Famfulík, Lukáš Mach, Matěj Rejman, Vojtěch Štěpán, Jan Tichý

**HÚ/Morašice – Březová 4: 2**, v zápase jsme byli po celou dobu lepším týmem, hned v úvodu jsme neproměnili několik vyložených šancí a soupeř z první vážnější situace skóroval, průběh zápasu se nezměnil, my marně dobývali branku domácích a soupeř byl z druhé střely na branku opět úspěšný. Naštěstí se nám ještě do poločasu podařilo vyrovnat a po přestávce jsme vývoj zápasu otočili a dovedli do zaslouženého vítězství. 

**Branky**: Lukáš Mach 2, Filip Flídr, Matyáš Bulva

**HÚ/Morašice – Litomyšl B/Čistá 3 : 0**, povedené, odbojované utkání, s vítězným koncem pro nás. V utkání jsme byli lepším týmem, hru kontrolovali a když se už soupeř dostal do šancí, tak vše pochytala Marťa Famfulíková. 

**Branky**: Lukáš Mach 2, Jan Kvapil 

Celý tým zaslouží pochvalu za nasazení a odběhané zápasy, příslibem je celkový výkon a odměnou první místo v turnaji, které by tak mohlo být motivací do dalších zápasů.
