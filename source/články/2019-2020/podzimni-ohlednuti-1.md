---
title: Podzimní ohlédnutí
date: 2019-11-20T05:03:05.288Z
category: zaci.st
image: /public/images/uploads/logo.png
---
Do letošní podzimní části jsme nastoupili se soupiskou čítající 11 hráčů. Počet slibný, ale téměř každý zápas byl doplňován hráči z mladších žáků, největší problém jsme měli na pozici brankáře, kterou ve většině případů zastal brankář mladších, Lukáš Němec. Tréninky probíhali odděleně dvakrát týdně, povětšinou s hojnou či plnou účastí.

V soutěži jsme odehráli 9 utkání, ve kterých jsme neztratili ani bod a díky tomu nám právem náleží první místo tabulky se skórem 49:6, jednoduše řečeno to znamená 5,4 vstřelených a 0,6 obdržených branek na zápas.  Na střeleckou listinu se zapsalo 10 hráčů. Nejvíce branek vsítil Jána (25), což ho řadí na dělené první místo střelecké tabulky soutěže.  V brance se celkem vystřídalo 5 hráčů (Jána, Kučera, Němec, Odehnal,  Žďára), ze kterých nejvíce odchytáno má Němec, který v 6 utkáních za svá záda pustil pouze dva góly.

Poděkování patří všem hráčům, rodičům a ostatním pomocníkům. Těšíme se na Vás opět na jaře.

David Severa
