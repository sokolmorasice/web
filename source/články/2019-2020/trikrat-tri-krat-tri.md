---
title: Třikrát tři krát tři
date: 2019-09-22T12:57:26.607Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Mladší přípravka

**Sestava:** Martina Famfulíková, Vojtěch Huryta, Šimon Flídr, František Flídr, Daniel Válek, Jaroslav Chaun, Matěj Kovář, David Klusoň, Sára Vomočilová, David Boček, Jan Nováček, Samuel Vomočil

**Naše výsledky:**

**Sebranice – Horní Újezd/Morašice 1:3**

branky: Daniel Válek 2, Vojtěch Huryta

**Horní Újezd/Morašice – Borová 11:0**

branky: Vojtěch Huryta 3, František Flídr 3, Martina Famfulíková 2, Šimon Flídr, Samuel Vomočil, Daniel Válek

**Horní Újezd/Morašice – Čistá 7:1**

branky: Daniel Válek 3, David Klusoň 2, Šimon Flídr, Jaroslav Chaun

**Tabulka turnaje:**

1.Horní Újezd/Morašice

2.Sebranice

3.Borová

4.Čistá

## Starší přípravka

**Sestava:** Matyáš Bulva, Filip Flídr, Ondřej Kusý, Jan Kvapil, Štěpán Řejha, Kryštof Tomšíček, Martina Famfulíková, Vít Famfulík, Lukáš Mach, Jan Kopecký, Matěj Rejman, Vojtěch Štěpán, Jan Tichý

**HÚ/Morašice – Radiměř/ Hradec nad Svitavou 2 : 4**, kluci na domácím hřišti opět „nezklamali“ a předvedli doposud nejhorší podzimní výkon bez bojovnosti, nasazení, pohybu a chuti do hry. V průběhu zápasu jsme prohrávali již 0 : 3, povedlo se nám však vstřelit dvě branky a utkání alespoň zdramatizovat, místo vyrovnání jsme však inkasovali a o výsledku tak bylo rozhodnuto. 

**Branky:** Filip Flídr, Kryštof Tomšíček

**HÚ/ Morašice – Koclířov 3 : 1**, v tomto utkání jsme pokračovali ve velmi matném výkonu z minulého zápasu, který nám však tentokrát stačil a my „utrpěli“ výhru. 

**Branky**: Matyáš Bulva, Filip Flídr, Jan Kvapil

**HÚ/Morašice – Sebranice 4 : 4**, před tímto zápasem s favoritem turnaje jsme se snažili kluky v kabině namotivovat a výsledek se dostavil, kluci se vrátili k tomu co dosud v turnajích předváděli a k vidění byl tak zajímavý, dramatický a vyrovnaný zápas, který skončil spravedlivou remízou. Po obdržené brance jsme dokázali otočit a poločas vyhrát 2 : 1, soupeř poměrně rychle po přestávce vyrovnal a záhy šel do vedení 3 : 2, kluci, v brance s Marťou Famfulíkovou, však nesložili zbraně, poctivě makali a odměnou jim bylo vedení 4 : 3, o které jsme však v závěru přišli. Všichni odvedli poctivý, bojovný výkon, nicméně zvláštní pochvalu zaslouží autor všech našich 4 branek Jan Kvapil. Tento výsledek nám tak stačil na konečné 3. místo.

**Branky**: Jan Kvapil 4
