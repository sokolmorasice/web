---
title: 'Přistřihnutý trávník, přistřihnutá křídla'
date: 2019-09-21T00:00:00.000Z
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Březová vs Horní Újezd/Morašice
sestava: >-
  Lukáš Němec, Matěj Kopecký, Zuzana Famfulíková, Jiří Jána, Marek Vích, Tomáš
  Briol, Jan Vedral, Jakub Kvapil, Šimon Kušnír, Jiří Kučera, Jan Odehnal,
  Vojtěch Famfulík, Filip Kopecký, Tereza Lněničková
goly: 'Jiří Jána 3, Matěj Kopecký'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533E2A/2019/Horní
  Újezd/Morašice/86a71e35-19c7-4c72-a08a-29372e52aa4e.html
---
V sobotu jsme s družstvem starších žáků zamířili do Březové. Kromě ligově střiženého trávníku nás okouzlila i rozcvička domácích, kteří nekompromisně umisťovali míče do branky. Na hřiště jsme nastoupili s jednou změnou - na pozici pravého záložníka si debut v základní sestavě odbyl Kvapil - a s ,,Brücknerovskou‘‘ taktikou, která se skládala ze zajištěné obrany a jednoduchého přechodu do brejků. Utkání začali lépe domací, kteří nás zatlačili a hrozily hlavně z rohů a našeho nedůrazu. V poločase plném šancí jsme první udeřili my, když po chybě domácích v 27. minutě proletěl obranou soupeře náš kanonýr Jána a tváří v tvář gólmanovi nezaváhal. Chvíli na to ztráta Famfulíkové, křižování Famfulíka, pád domácího hráče a nátlak pěti domácích trenérů rezultoval v pokutový kop, kteroývýborným zákrokem chytil Němec. Pravidlo "nedáš-dostaneš" se naplnilo o pět minut později, kde po podobné akci Jána zkrotil míč, který proskákal obranou domácích a v 35. minutě zajistil pro nás nečekaný stav 2:0. 

Druhý poločas se nesl v duchu toho prvního, domácí nemohli překonat našeho hrdinu Němce, který chytal šanci za šancí. Ve 45. minutě se znovu prohnal obranou Jána a zkompletoval hattrick. Domácí postupně odpadali fyzicky, což nám usnadňovalo hru - víc jsme drželi balón a dostavilo se i více šancí. V 60. minutě po páíř vteřinách na hřišti doklepl míč po skrumáži Matěj Kopecký a svým premiérovým gólem pečetil první porážku domácích v této sezóně. Bojovný výkon podpořený jistým gólmanem měl nakonec jedinou kaňku -  Jána neudržel svůj gólový průměr na hodnotě 4,6 na zápas. Přístí týden v souboji se stejně stoprocentní Trnávkou to ale bude chtít přidat.
