---
title: Tečka
date: 2020-07-02T04:30:38.972Z
category: zaci.st
image: /public/images/uploads/logo.png
h1: Březová - Horní Újezd / Morašice    1:10
sestava: Lukáš Němec, Zuzana Famfulíková, Vojtěch Famfulík, Marek Vích, Tomáš
  Briol, Šimon Kušnír, Tereza Lněničková, Jan Odehnal, Jiří Jána, Vojtěch
  Andrle, Filip Kopecký, Šimon Lněnička, Jan Vedral, Filip Střasák
goly: Jiří Jána 4, Tomáš Briol 2, Jan odehnal 2, Vojtěch Famfulík, Marek Vích
---
Na první a zároveň i poslední utkání jara jsme vyrazili do Březové, kde nás na výborně připraveném hřišti čekal velmi těžký soupeř. S sebou jsme si vzali i dva nové hráče, kteří se k nám připojí ze starších přípravek. Soupeř měl takových kluků více a bylo v utkání to bylo znát. Celé utkání jsme dominovali, soupeře přehrávali hlavně fyzicky, kluci předvedli téměř bezchybný výkon. Vytknout se dají jen neproměnné šance a nedorazy v obraně, ze kterých měl i soupeř dobré šance, které ale nedokázal proměnit. Výsledek kazí pouze zranění nové kapitánky Famfulíkové, kterou z metru hráč Březové tzv. nahulil na hlavu, kterou si kryla rukou a později se ukázalo, že se jedná o zlomeninu. Za celou sezónu děkujeme všem hráčům a rodičům, kteří chodili a pomáhali, a doufáme, že příští sezóna přinese stejně fotbalové radosti jako ta letošní část, kdy se hrálo. Chceme poděkovat i Tomáši Briolovi, který byl kapitánem a tahounem týmu, a popřát mu úspěšné působení a radost z fotbalu na nové štaci ve Vysokém Mýtě. Přejeme všem hezké prázdniny a hodně zdaru v dalších dnech.

Autor: Jan Špinar