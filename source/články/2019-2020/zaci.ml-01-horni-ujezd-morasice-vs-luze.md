---
title: Už nebudem psát rok 2006
date: 2019-08-29T00:00:00.000Z
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Luže
sestava: >-
  Lukáš Němec - Daniel Hanyk, Jakub Kvapil, Ondřej Žďára, Anežka Břeňová, Zuzana
  Famfulíková, Jakub Bartoš, Vojtěch Paťava, Petr Řejha, Jiří Kučera, Martin
  Flídr, Vlastimil Hladík, Jindřich Mičík, Jan Flídr, Matěj Kopecký
goly: >-
  Jiří Kučera 4, Anežka Břeňová, Martin Flídr, Daniel Hanyk, Jindřich Mičík,
  Ondřej Žďára
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/531F1B/2019/Horní
  Újezd/Morašice/7dea0741-399a-4718-83c1-1b57cc94891f.html
---
Do nové sezony jsme vstupovali už bez hráčů ročníku 2006, a tak jsme byli zvědaví, jak se s tím mladší hráči popasují. Po loňské sezóně, kdy jsme obsadili druhé místo, bylo jasné, že budeme chtít atakovat opět příčky nejvyšší. První zápas na hřišti Luže ukázal, že ze špatných výsledků v dalších zápasech strach mít nemusíme.
