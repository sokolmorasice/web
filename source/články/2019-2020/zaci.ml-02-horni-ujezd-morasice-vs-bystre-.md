---
title: Dáme bůra
date: 2019-09-01T00:00:00.000Z
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: 'Horní Újezd/Morašice vs Bystré '
sestava: >-
  Lukáš Němec - Daniel Hanyk, Jakub Kvapil, Jindřich Mičík, Jonáš Krška, Zuzana
  Famfulíková, Anežka Břeňová, Ondřej Žďára, Vojtěch Paťava, Jiří Kučera, Martin
  Flídr, Vlastimil Hladík, Jan Flídr, Petr Řejha
goly: 'Anežka Břeňová 2, Ondřej Žďára 2, Jiří Kučera'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/531F1B/2019/Horní
  Újezd/Morašice/ed8e093b-c62b-47ed-b599-7e46619cc343.html
---
Po kanonádě v Luži by byla škoda neukázat nějaký ten gól i domácím fanouškům, kteří se nakonec dočkali bůra.
