---
title: Přípravné utkání
date: 2020-03-08T12:39:01.416Z
category: muzi
image: /public/images/uploads/logo.png
sestava: >-
  J. Špinar – V. Novák, M. Ropek, M. Chmelík, D. Štancl, D. Ondráček, M.
  Klement, V. Štěpán, B. Válek, J. Kopecký, Z. Skala, J. Vít, D. Vejrych, P.
  Vomáčka, M. Hubáček
goly: M. Chmelík (J. Kopecký)
---
V přátelském utkání mužů s Horním Újezdem mělo prý naše mužstvo jakousi optickou, ale impotentní převahu. Zatímco soupeř hrozil sporadicky a ke gólu mu pomohla penalta, naše snažení naráželo na občasné nesnažení nebo špatnou finální fázi. Gól vstřelil Chmelík poté, co mu Kopecký hlavou sklepl Skalův centr.

O víkendu hrálo přátelské utkání i mužstvo dorostu, které remizovalo s Němčicí / Sloupnicí 5:5.