---
title: Perfect weekend
date: 2019-09-09T19:40:13.566Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Mladší přípravka

Úvodní dějství nového ročníku okresní soutěže mladší přípravky se uskutečnilo na hřišti v Horním Újezdě. Turnaj byl nalosován na tři týmy, a tak jsme vzhledem k dostatečnému množství dětí postavili dvě družstva. Zkušenější A tým byl sestaven z hráčů, kteří hráli už v loňské sezóně, naopak v našem béčku byli hráči, kteří s turnaji teprve začínají. Ani jeden z týmů nezklamal, áčko potvrdilo svoje kvality a turnaj vyhrálo, béčko předvedlo opravdu bojovný výkon a vstřelilo sedm gólů.

**Horní Újezd/Morašice A**: Martina Famfulíková, Vojtěch Huryta, Šimon Flídr, František Flídr, Daniel Válek, Sára Vomočilová

**Horní Újezd/Morašice B**: Michal Kovář, Matěj Kovář, Nováček, Jaroslav Chaun, David Boček, David Klusoň, Matyáš Lněnička, Samuel Vomočil

**Výsledky:**

**Horní Újezd/Morašice A – Sebranice 15:1 (5:0)**

Branky: Daniel Válek 8, Šimon Flídr 3, František Flídr 2, Vojtěch Huryta 2

**Horní Újezd/Morašice B – Němčice 0:4 (0:2)**

**Horní Újezd/Morašice A – Němčice 13:0 (5:0)**

Branky: Daniel Válek 5, František Flídr 4, Vojtěch Huryta 2, Šimon Flídr, Sára Vomočilová

**Horní Újezd/Morašice B – Sebranice 2:9 (1:6)**

Branky: Michal Kovář, Jaroslav Chaun

**Horní Újezd/Morašice A – Horní Újezd/Morašice B 8:5 (5:3)**

Branky: Daniel Válek 3, Sára Vomočilová 2, Šimon Flídr 2, František Flídr - Jaroslav Chaun 2, Michal Kovář, Matěj Kovář, David Klusoň

Němčice – Sebranice 2:9 (1:3)

**Tabulka turnaje:**

1.Horní Újezd/Morašice A (skóre 36:6, 9 b.)

2.Sebranice (skóre 19:19, 6 b.)

3.Němčice (skóre 6:22, 3 b.)

4.Horní Újezd/Morašice B (skóre 7:21, 0 b.)

## Starší přípravka

Na turnaj do Sebranic nedorazil tým Cerekvice, takže náš tým nakonec odehrál jen dva zápasy, z nichž ten druhý byl, vyjma výše uvedeného "béčka", jedinou porážkou za celý víkend napříč kategoriemi.

**Sestava**: Matyáš Bulva, Filip Flídr, Ondřej Kusý, Jan Kvapil, Kryštof Tomšíček, Matouš Zach, Vít Famfulík, Jan Kopecký, Lukáš Mach, Matěj Rejman, Vojtěch Štěpán, Jan Tichý

**HÚ/Morašice – Němčice 3 : 2**. Jednalo se o utkání dvou rozdílných poločasů, kdy v tom prvním dominoval náš tým a bylo z toho zasloužené vedení 3 : 0, podpořené komentářem našeho brankáře Lukáše Macha, že se v brance nudí. Toto však neplatilo v poločase druhém, kdy se obraz utkání otočil, náš tým polevil, zejména v nasazení a bojovnosti, ale i přesto jsme se nakonec mohli radovat z prvního vítězství v podzimních turnajích. 

Branky: Filip Flídr 2, Kryštof Tomšíček

**HÚ/Morašice – Sebranice 1 : 3**. Věděli jsme, že nás bude čekat těžší utkání a kluky jsme na toto i upozorňovali. Úvod zápasu jsme však nezvládli a doslova darovali soupeři tříbrankové vedení. S postupem času se náš výkon začal zlepšovat a nám se podařilo do poločasu snížit. I po přestávce kluci makali a škoda neproměněných šancí, které však měl i soupeř, ale Lukáš Mach nás několikrát podržel. Konečná porážka nás až tak mrzet nemusí, protože kluci se snažili, bojovali a zápas rozhodně neodchodili, což je dobrým potenciálem do dalších turnajů. 

Branka: Vít Famfulík
