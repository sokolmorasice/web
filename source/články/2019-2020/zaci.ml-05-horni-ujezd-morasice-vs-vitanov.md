---
title: Zahřívací kolo
date: 2019-09-28T00:00:00.000Z
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Horní Újezd/Morašice vs Vítanov
sestava: >-
  Lukáš Němec - Daniel Hanyk, Jakub Kvapil, Jonáš Krška, Anežka Břeňová, Zuzana
  Famfulíková, Jakub Bartoš, Ondřej Žďára, Jan Flídr, Jiří Kučera, Jindřich
  Mičík, Vojtěch Paťava, Matěj Kopecký, Petr Řejha
goly: 'Jiří Kučera 5, Anežka Břeňová 2,Matěj Kopecký 2,Jakub Kvapil, Jakub Bartoš'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/531F1B/2019/Horní
  Újezd/Morašice/e69f9ead-d97d-4483-a1e1-f7a16d5590de.html
---
V sobotu jsme přivítali na našem hřišti hráče z Vítanova. Pohled na tabulku už dopředu napovídal o tom, jak se asi bude hra vyvíjet. První poločas jsme po pohledných akcích,kdy nám soupeř nechával hodně prostoru,vyhráli 5:0.Nebýt výborné brankářky soupeře,mohl být výsledek mnohem vyšší.

Ve druhé půli jsme hráli ještě pohlednější fotbal a okořenili ji ještě šesti góly. Byla to dobrá průprava na příští sobotní zápas ve Skutči,kde se porveme o první místo,tak budeme věřit,že předvedeme opět pohledný fotbal s dobrým výsledkem.

Pokud by měl někdo zájem jet se s námi podívat, tak odjíždíme autobusem v sobotu v 9.00.hod od kabin.
