---
title: 'Bez aut, bez hráčů, bez kapitána, bez ztráty'
date: 2019-10-05T00:00:00.000Z
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Křenov/Dlouhá Loučka vs Horní Újezd/Morašice
sestava: >-
  Jan Odehnal - Marek Vích, Jiří Jána, Jan Vedral, Šimon Kušnír, Šimon Lněnička,
  Tomáš Huryta, Vojtěch Famfulík, Filip Kopecký, Tereza Lněničková
goly: 'Filip Kopecký 2, Šimon Kušnír, Jan Odehnal, Tomáš Huryta'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533E2A/2019/Horní
  Újezd/Morašice/1a4c7f47-777d-4118-9a27-e6e33851e8bf.html
---
Po týdenní pauze nás očekával soupeř v Křenově, po menším problému s dopravou jsme na utkání dorazili. Bohužel ve stejnou chvíli odehrávali své utkání mladší žáci a nemohli jsme se spolehnout na jejich výpomoc. Čili jsme opět měli volný brankářský post. Černého Petra si vytáhl Odehnal, který vlastně v brance neměl příliš práce až na pár odkopů. Další komplikací byla absence kapitána Briola, jeho roli převzal Famfulík. Silný déšť nepřidal na kvalitě utkání a naše útočníky dlouho trápila koncovka, když většina střel končila daleko za hřištěm. Celý první poločas byl ve znamení spoustu nepřesností, zbytečných chyb a pokřikování po spoluhráčích. Přesto dvě střely skončily v brance zásluhou Kušníra a Kopeckého.

O poločasové pauze se do brankářského převlékl Jána. Po změně stran se náš herní projev mírně zlepšil, z čeho těžil opět Kopecký a přidal další branku. Odehnal, který byl po prvním poločase v brance plný sil, dostatečně větral soupeřovu obranu a jednu ze svých mnoha šancí dokázal proměnit. Poslední kdo se zapsal jako střelec byl Huryta, který letos hrál své první utkání po delším zranění. Do zápasu jsme šli s jasným cílem získat 3 body, což se povedlo, ale herní projev nebyl vůbec ideální, proto doufáme, že příští týden se už opět budeme fotbalem bavit tak, jako tomu bylo zatím po všechny zápasy.
