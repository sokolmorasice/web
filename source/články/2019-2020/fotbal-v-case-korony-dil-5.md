---
title: Fotbal v čase korony - díl 5.
date: 2020-04-23T20:32:58.633Z
category: obecne
image: /public/images/uploads/logo.png
---
V uplynulých dílech se nám podařilo zmapovat skoro celé dění kolem morašického fotbalu, ale co přerušení sezóny říkají samotní hráči? Obepsali jsme je téměř všechny. Někteří byli sdílní, jiní stručnější, někdo trval na anonymitě, někdo neodpověděl.

Jedním z nejpostiženějších hráčů je bezesporu nejlepší střelec uplynulé sezóny Bohumil Válek. „Moje hospodářství v kombinaci s fotbalem je tak vyčerpávající, že jsem chtěl na jaře překonat Kuťáka, na podzim prodat kravku a na jaře už nezasejt.“ Všechno ale vzalo za své, hospodaření musí pokračovat, protože jim vypadly příjmy z praní dresů. „Založili jsme si na to živnost, odešel jsem z vícesměnného provozu, abych měl více času pomoct manželce s tou špínou, ale teď nevíme, co bude. Loni v březnu jsme ještě neprali, tím pádem se nás „pětadvacítka“ netýká. Třeba nám ji pošle Vláďa Coufal, jako slávista slávistovi, nebo by mohl o nějakou dotaci pro nás poprosit Skalda Andreje, jako zemědělec zemědělce pro zemědělce. Rýsoval se nám i výhodný prodej Dana, ale tomu předčasný konec sezóny zkazil dvoustovkové gólové plány. A nám je jasný, že jen po stogólovým klukoj agenti zrovna neskočej.“

Ligový navrátilec Vojtěch Štěpán nás překvapil, protože domácí premiéra pro něj nebylo hlavní lákadlo jara: „Já se mnohem víc než na domácí fotbal, těšil na hudbu před domácími zápasy, i když to někomu může znít jako paradox.“

Dva vysokoškoláci, kteří by měli na jaře finišovat bakalářské práce, mají na věc zcela odlišný náhled. Zatímco jeden doufal, že bude mít díky bakalářské práci výmluvu na fotbale, druhý je rád, že v případě neúspěchu nebude moci argumentovat fotbalem jeho máma.

Do zapeklité situace se dostal hráč, který se nechal slyšet, že pokud na jaře neodehraje každý víkend aspoň 60 minut, tak končí. Dnes už se nedozvíme, zda by mu trenér vyhověl, ale jeho morální dilema mu rozhodně nezávidíme.

Jediný otevřený příznivec klubu se slavnou a železnou minulostí neskrývá své nadšení nad tím, že Sparta už více než měsíc neprohrála.

Zatímco jednoho z několika letošních noviců na soustředění těší, že si může od ostatních dát pauzu, aby si v klidu rozmyslel, jestli mu onen víkend přišel dobrý na devadesát šest nebo na čtyři procenta, dalšího netěší, že od něj sekretář vybral o dvě stě korun víc a drze mu to připomíná. Aby se vyhnul splacení, prý už několik týdnů ani nechodí do společného zaměstnání.

Václav Novák nijak nehodlá skrývat svou identitu za reakcí: „Škoda, zrovna jsem doufal, že stihnu víc tréninků.“ A přiznat barvu nemá problém ani Filip Bureš, protože mu to většina spoluhráčů stěží věří: „Co se nehraje fotbal a nemůžu do posilovny, začal jsem i běhat a dostal do fotbalu novou chuť." (rozhovor proběhl před oznámením o otevření fitek od 27.4. - pozn. redakce)

Jediná zimní posila je ráda, že splnila až předčila očekávání: „Hele těšil jsem se na víc zápasů, ale zase jsem si udržel gól na zápas, to je dobrý ne?"

Michal Sokol nám, na protest, že nebyl tázán, neodpověděl.

A matador Josef Kopecký si vystačil s jednou větou: „Já už ani chodit nechtěl, vždyť je jich dost.“

Více jsme z hráčů nedostali, respektive ani dostat nechtěli. Děkujeme všem členům oddílu za spolupráci a možnost nahlédnutí pod pokličku dušeného života a uvolněte se, prosím.

***Snad většina ze čtenářů poznala, že většina vyřčených věcí je fake news. Čistou pravdou je, že sezóna je u konce už nyní. Že mladší přípravka a obě žákovské kategorie nemohly dotáhnout do konce svá podzimní stoprocentní tažení. Že Dan Válek měl nakročeno ke dvě stě gólům za sezónu a Jirka Jána k žákovskému rekordu. Že vůbec nevíme, jak se nám podaří udržet při životě letos vzkříšený dorost. Že se odkládají oslavy 50 let fotbalu, hodně předběžně na 5.9. Že Milan Chmelík vyplňoval dotace valnou část podzimu, že Petr Kuta měl kabiny přesně propočítané, že sekat se musí a že nám nefunguje facebook. Že se Berbr ani Damková s žádnými penězi nestavili, že jsou v Osíku semafory a Ropkovi by se měli pohybovat v popředí historických statistik v rodinné kategorii. Že Bojda nedosáhne na pětadvacítku. Že se konala akce, o které mluvil Martin Štancl tak, jak ji popsal Martin Štancl. Že Sparta už měsíc neprohrála. To jsou asi tak všechny podstatné a pár zbytečných pravd o morašickém fotbale v časech koronaviru.***