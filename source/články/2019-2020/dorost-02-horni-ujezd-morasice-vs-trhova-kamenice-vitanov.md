---
title: V pařáku a po zemi
date: 2019-09-01T00:00:00.000Z
category: dorost
image: /public/images/novinky/2019-2020/dorost.png
h1: Horní Újezd/Morašice vs Trhová Kamenice/Vítanov
sestava: >-
  Jaromír Loskot - Pavel Beneš, Martin Tmej, Jakub Nádvorník - Jiří Pechanec
  (Luděk Bureš), Vojtěch Opletal, Tomáš Briol, Radovan Flídr (František
  Lněnička), Jiří Šturc - Jan Pechanec (Emanuel Hanyk), František Bartoš
goly: 'Vojtěch Opletal, Jan Pechanec'
multimedia: http://www.tjhorniujezd.cz/?p=12882
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533C2B/2019/Horní
  Újezd/Morašice/aab59b72-61c2-455b-a771-67fe8ee4847d.html
---
První zářijová neděle by se, vzhledem k počasí, dala strávit radši někde u vody s vychlazenou jedenáctkou, ale nedalo se svítit. Lépe řečeno svítilo až moc a dvě určené jedenáctky určitě vychlazené nebyly. Ta naše si, až na pár úvodních kopanců, počínala lépe a po většinu zápasu udávala tempo. Hosté se dostávali do závarů a náznaků šancí hlavně po našich chybách v rozehrávce, kdy chyběla buď přesnost, nebo míč cestou ke spoluhráči zamrzl. Naštěstí jsme úvod přežili bez úhony a začaly se objevovat šance na druhé straně. V Králíkách to vzduchem nešlo a je dobře, že jsme to teď zkusili opačně. Díky dobře vystupující obraně a fungujícímu středu zálohy jsme hrozili hlavně po krajích, kdy především Jirka Šturc zásoboval útočníky centry a hostující obrana měla co dělat, aby ho občas aspoň chytla. V 15. minutě vyslal Tomáš Briol krásným pasem do brejku Šturce, který pláchl všem a jeho střelu do tyče dorazil do prázdné brány Vojta Opletal. O pět minut později poslal Kuba Nádvorník z autu (z první) centr do vápna, kde se před brankářem zjevila hlava Honzy Pechance - 2:0. A nemuselo zůstat jen u toho, kdyby dobře zahrávané rohy nepropadaly vápnem, Fanda Bartoš z penalty neodmítl nabídku Jirky Šturce a posledně jmenovanému se po samostatném úniku povedlo lépe dokončit obejití brankáře.

Druhý poločas probíhal podle podobného scénáře jako ten první. Hosté se sice dostávali více do šancí, ale na tom měli největší podíl naši, kteří sem tam zadrbali balón, který se rychle ocitl na kopačkách líně se vracejících hostujících útočníků. Branku "jistoty" se nám vstřelit nedařilo a když ano, tak vysunutí Honzy Pechance Briolem zastavil praporek pomezního. V 60. minutě se Radovan Flídr opřel před vápnem do míče, který prosvištěl kolem šibenice. Zhruba dvacet minut před koncem mohli hosté zápas zdramatizovat, když povedenou střelu do tyče našel překvapený Loskot v náručí. Klíčového žolíka nasadili trenéři na posledních 10 minut a zhruba po 18 vteřinách pobytu na hřišti dostal Hanyk pocukrovanou přihrávku od Šturce, ale ve skluzu nedokázal procpat balón skrz brankáře do sítě. Zbytek zápasu se dohrál v poklidu a tak jsme si za zodpovědný a dobře odmakaný výkon mohli připsat zasloužené 3 body. 
