---
title: Halový turnaj v Dolním Újezdu
date: 2020-02-29T05:25:17.373Z
category: zaci.ml
image: /public/images/uploads/received_833385127178889.jpeg
---
V sobotu 29.2. jsme sehráli povedený halový turnaj v Dolním Újezdu. Ještě před samotným turnajem jsme v kabině museli řešit post brankáře,protože se Lukáš nedostavil. Naštěstí nedošlo k dohadům kdo bude chytat (tak jako jindy,když tento problém nastane) a tak tento úkol na sebe vzal Jirka K.

Nebudu se rozepisovat o jednotlivých zápasech,jen ve stručnosti zhodnotím celý turnaj. Hrálo se systémem 4+1 a tak s počtem 12-ti hráčů nebylo jednoduché všechny vždy spravedlivě prostřídat. Turnaj nám ukázal na čem je potřeba ještě zapracovat a kde nás tlačí bota. Musím však říct,že těch pozitivních věcí bylo víc,než těch méně povedených,čemuž ostatně odpovídá i konečné umístění v tabulce. Za zmínku jistě stojí i to,že si naši hráči odvezli obě individuální ocenění - nejužitečnějším hráčem turnaje byl vyhlášen Jakub Bartoš a cenu za nejlepšího brankáře si odnesl Jirka Kučera.

**Výsledky:** 

**HÚ/MOR : Tatenice 2 :1**

**HÚ/MOR : Bystré 5 : 0**

**HÚ/MOR : DÚ (bílí) 1:1**

**HÚ/MOR : Jehnědí 2 : 0**

**HÚ/MOR : Březová 1 : 1**

**HÚ/MOR : DÚ (modří) 0 : 0**

**HÚ/MOR : Semanín 11 :1**

**Góly:** Jakub Bartoš 5, Jirka Kučera 5, Anežka Břeňová 4, Jonáš Krška 2, Matěj Brettinger 2, Zuzka Famfulíková, Vlasta Hladík, Ondra Žďára

**Tabulka**

1. DÚ (modří)

**2. HÚ/MOR**

3. Březová

4. DÚ (bílí)

5. Tatenice

6. Jehnědí

7. Bystré

8. Semanín

Před začátkem sezóny nás ještě čeká na konci března přípravný zápas s Dolním Újezdem a na začátku dubna soustředění v Daňkovicích.

Chtěl bych poděkovat Ivě Kvapilové za občerstvení,které klukům připravila na oba halové turnaje...

autor: Jiří Kučera
