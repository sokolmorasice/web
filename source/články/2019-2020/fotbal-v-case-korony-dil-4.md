---
title: Fotbal v čase korony - díl 4.
date: 2020-04-19T20:16:21.526Z
category: obecne
image: /public/images/uploads/logo.png
---
Zatímco hrající trenér A-týmu Martin Ropek začíná pomalu trpět [abstinenčními příznaky](/články/2019-2020/fotbal-v-case-korony-dil-3), nehrající trenér dorostu Martin Štancl nemá takový problém najít na vynucené pauze i pozitiva. „Poté, co jsme kvůli vládnímu nařízení zrušili tréninky, klesl nám počet neomluvených absencí z 95% na nulu,“ libuje si. Navíc má pocit, že je to vysvobození i pro jeho plánovaného nástupce, kterému by z dnešních mlaďáků, dle jeho slov, „muselo jebnout“. „Franta Ropku je klasa, profík, legenda. Jasně, my jsme se pod ním taky navyváděli blbostí, jednou jsme mu přijeli na zápas přímo z chlastačky, i s asistentem (Josef Kopecký – pozn. redakce), střízlivej byl jenom jeho syn a ještě jeden, možná Préša. Ale aspoň jsme mu to dali vědět, dokonce jsme ho na tu akci zvali taky, věděli jsme, co se sluší a patří. Dneska se vám ty mladý místo odpovědi smějou přímo do mobilu.“ Otázka dalšího působení dorostu, obnoveného před touto sezónou, je tedy ve hvězdách.

Fotbal samozřejmě nechybí jen hráčům, ale i fanouškům. Nestor morašických tribun a jeden z pamětníků úplných začátků fotbalu Jaroslav Nepraš starší přišel během krátké chvíle o aktuální i minulé zápasy. „Stavoval se u mě náš sekretář, vzal si všechny moje zápisky a od té doby jsem o něm neslyšel, Bůh ví, co s tím chystá. A měsíc na to se fotbal zakázal, takže se nemůžu koukat ani na nové zápasy, ani si prohlédnout zápisky z těch starých. Našel jsem si ale novou hru, přiřazuju si po paměti estrádní pořady z ČT3 ke konkrétním sobotám před zápasy áčka v 70. letech.“

Nevalný dojem z podzimní části si morašičtí fotbalisté nespraví ani u fotografa Jana Hendrycha. „Na podzim se na to v Morašicích nedalo koukat. Na hřišti nebyl vůbec žádný pohyb, neviděl jsem jedinou pořádnou kombinaci, neslyšel jsem, že by někdo hecoval ostatní, žádný hráč svolný k focení, místo momentek mám panoramata. A myslíte, že to bude na jaře lepší?“ uzavírá řečnickou otázkou.

„Já jsem během zimy bral speciální hlasová cvičení, abych vydržel křičet každý týden, zvýšil jsem denní dávku cigaret, abych vypiloval chraplák a sledoval přední české speakleadery, abych přišel s novými pokřiky. Ne že by se to nedalo uplatnit u esportu, ale klepou se nám z toho okenní tabulky,“ nabízí další pohled Tomáš Sopoušek.

„Bude to v Seňor areně místo na hřišti, bude to s adrenalinem ze zakázaného setkání místo emocí ze zápasu, ale jinak se pro nás nic nemění,“ prozradil nám mimo záznam člen „udírenského“ sektoru, kterému se tímto omlouváme za zveřejnění jeho plánů, ale správný novinář dnes musí bdít nad dodržováním šéfových nařízení.

Jak půlroční pauza ovlivní životy a plány hráčů A-týmu? Který z nich se cítí nejpoškozenější a proč? Na to vám v pátek odpoví závěrečný díl seriálu.