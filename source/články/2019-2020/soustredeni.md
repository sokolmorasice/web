---
title: Soustředění
date: 2020-03-02T06:58:52.255Z
category: muzi
image: /public/images/uploads/img_20200222_092233.jpg
---
O víkendu 21.-23.2. absolvovalo naše áčko tradiční soustředění v [Daňkovicích](https://www.selskydvur.net/). Po hubených letech byla účast zase jednou vyšší, hned trojice kusů se dostavila po rovných deseti letech. Silniční dril zpestřil místní wellness, procházka se znectěním legendy, zpívánky i trenérova bodovací soutěž, do níž se promítala poctivá docházka, fyzická vyspělost, historické vědomosti, zručnost v jiných sportech (pink ponk, exování piva) i oblíbenost u Martina Štancla. Se shodným skóre ji vyhrály dvojice **Ropek-Chmelík** a **V. Štancl-Novák**. Původně měl zpestřující účel plnit i domluvený přátelák s Dolním Újezdem, ale v něm už se bohužel počet zdravých kusů limitně blížil jedenácti, takže většina z nich si odpočinula jen kolem deseti minut. Přesto to nebyl tak hrozný zážitek, jak bylo před zápasem očekáváno, i šance byly. 

**Morašice – Dolní Újezd      0:2 (0:2)**

Sestava: J. Špinar – M. Ropek, V. Novák, V. Štancl, D. Štancl, B. Válek, M. Klement, M. Chmelík, D. Ondráček, V. Štěpán, K. Vopařil, J. Kopecký, P. Štěpán

Publikovatelné fotky jsou k nahlédnutí v naší [galerii](https://drive.google.com/folderview?id=1jLYptFdOLl9987iXJVsGm1RuBB1prAHw).
