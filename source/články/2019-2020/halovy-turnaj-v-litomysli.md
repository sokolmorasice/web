---
title: Halový turnaj v Litomyšli
date: 2020-01-18T19:29:18.109Z
category: zaci.ml
image: /public/images/uploads/logo.png
---
Na sobotní turnaj jsme odjížděli bez několika opor, a tak jsme byli zvědaví, jak v silné konkurenci obstojíme.

**HÚ/MOR – FK PARDUBICE červení 0 : 3**

Hned první zápas proti FK Pardubice – červení (pozdější vítěz turnaje) nám ukázal, v jakém tempu se turnaj bude odehrávat. Ve stručnosti řečeno, než jsme se stačili rozkoukat, byl konec zápasu. Soupeř byl všude dřív, pohyb bez balónu měl na jedničku a jejich kombinace….jedním slovem nádhera. Ale zase vím, že nemohu srovnávat nesrovnatelné.

**HÚ/MOR – FC ŽAMBERK 1 : 0**

V tomto utkání jsme měli asi jako v jediném trošku navrch a nebýt neproměněných šancí, mohlo být skóre vyšší. Naši hráči už nepůsobili tak zakřikle jak tomu bylo v prvním utkání a občas byla k vidění i hezká akce.

Gól: Jirka Kučera

**HÚ/MOR – FK PARDUBICE bílí 0 : 3**

Proti druhému týmu z Pardubic byla naše hra už znatelně lepší, ale kvalita soupeře byla jasná.

**HÚ/MOR – LANŠKROUN 0 : 1**

Vyrovnané a bojovné utkání se šťastnějším koncem pro soupeře.

**HÚ/MOR – TJ SVITAVY 1 : 1**

Opět vyrovnané utkání, kdy jsme se ujali vedení, ale po rohovém kopu soupeř před koncem zápasu zaslouženě vyrovnal.

Gól: Matěj Brettinger

**HÚ/MOR – SK POLIČKA 0 : 5**

V zápase s TJ Svitavy se nám zranil Kuba Bartoš. To bylo citelné oslabení a na výsledku se to podepsalo. Polička hrála výborně a zaslouženě si z turnaje odváží druhé místo.

Turnaj nám ukázal, že pokud budeme hrát to co umíme a budeme se fotbalem umět bavit, hrát ho pro radost, (občas by neškodilo i víc poslouchat trenéry 🙂 ) tak můžeme směle pomýšlet i na vyšší cíle, než jakých jsme dosáhli na turnaji i v podzimní části sezóny.

Musím vyzvednout Zuzku Famfulíkovou, jako jediná holka na turnaji se určitě neztratila a nejednomu útočníkovi pěkně zatápěla 🙂

Iva Kvapilová opět nezklamala a kromě pomoci s dopravou připravila klukům výborné občerstvení a vitamíny, za což ji moc děkujeme 🙂

**Konečná tabulka**

1.FK Pardubice (červení)

2.SK Polička

3.FK Pardubice (bílí)

4.TJ Svitavy

5.FK Lanškroun

**6.TJ HÚ/MOR**

7.FC Žamberk

autor: Jiří Kučera
