---
title: Trpělivost góly přináší
date: 2019-09-08T00:00:00.000Z
category: dorost
image: /public/images/novinky/2019-2020/dorost.png
h1: Vraclav/Zámrsk vs Horní Újezd/Morašice
sestava: >-
  Emanuel Hanyk - Pavel Beneš, Jakub Nádvorník, Martin Tmej - Jiří Pechanec,
  Vojtěch Opletal, Tomáš Briol, Radovan Flídr, Jiří Šturc (Luděk Bureš) -
  František Bartoš, Jan Pechanec (František Lněnička) 
goly: Jan Pechanec 2
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533C2B/2019/Horní
  Újezd/Morašice/6d093da3-abff-486e-bcdf-3eca3d8e6431.html
---
V pořadí třetí mistrovský zápas nás zavál do nedaleké Vraclavi. Díky pětatřicítce jsme však byli nuceni zvolit trasu, po které mnozí z nás už asi v životě nepojedou. Po vcelku vydatně deštivém víkendu nás čekal překvapivě suchý, skákavý terén a v domácí kabině asi 20 hráčů.

Napotřetí jsme neprochrápali začátek a již v první desetiminutovce jsme si vytvořili několik zajímavých šancí. Bohužel všechny zůstali liché, protože jsme vlastně pořádně nedokázali vystřelit, nebo nadějný brejk zastavoval praporek pomezního. Nejprve se Šturc dostal příliš blízko k domácímu brankáři, Bartoš si netroufl vystřelit levou a v 10. minutě Flídr sice gólmana obešel, ale ten ho zastavil pouze za cenu žluté karty. Zřejmě se rozhodl správně, protože jemně nevyspalý Bartoš zahrál penaltu doprostřed a nabídl tak Syrovému další důležitý zákrok. Domácí hrozili hlavně z nákopů za obranu, a jelikož se naši beci zapomínali vracet, musel dvěma výbornými zákroky hasit vážnou situaci taky Hanyk. Průběh zápasu připomínal spíše kopanou, než fotbal, ale přesto se nám dařilo držet více držet balón a vázlo to jen ve finální fázi. Špatný odkop domácího brankáře vracel do odkryté branky Šturc, ale o kousek minul. A když pár minut před pauzou krásnou střelou neobstřelil Syrového Bureš, zůstali jsme na střídačce za bezbrankového stavu.

Poločasové nabádání k trpělivosti přineslo ovoce již po pár vteřinách. Nádvorník s klidem sobě vlastním postupoval ladně skrz pole a ze středového kruhu vyslal průnikovkou do brejku Honzu Pechance. Ten se rozhodl využít hned svoji první šanci a po zemi na protější tyč nás poslal do vedení. To nemuselo zůstat tak hubené, kdybychom nezahazovali šance jak na běžícím páse. Postupně se v nájezdech na brankáře vystřídal Jirka Pechanec, Radovan Flídr a Jirka Šturc. Všichni tito mají jedno společné, nedali. I když posledně jmenovaný se před tím dostal asi přes 5 protihráčů, kteří se ho snažili dostat do horizontální polohy všemi dostupnými prostředky a Jirkovi prostě na pořádné zakončení nezbyly síly. Pár rohových kopů a standartek poblíž vápna jsme přežili a tak mohl z posledního nájezdu v zápase zpečetit skóre opět Honza Pechanec střelou do šibenice.

Po zpackaném reprezentačním zápase v Kosovu tvrdil Bořil novinářům, že se nemají za co stydět. My to o sobě můžeme říci taky, ale za to s čistým svědomím.
