---
title: Válec i bez Válce
date: 2019-09-30T05:52:33.192Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Mladší přípravka

Další turnaj jsme odehráli v Telecím v rámci oslav kulatého výročí tamního Sokola. Vzhledem k tomu, že vedle připravených hřišť na turnaj probíhal sportovně-kulturní program, měli jsme obavu, jak na tom naši hráči budou s koncentrací. Naštěstí hráči pochopili, že přijeli hrát fotbal. Turnaj poznamenala krátká, ale silná dešťová přeháňka, snad to nikdo neodmarodí.

**Sestava:** Martina Famfulíková - Vojtěch Huryta, Šimon Flídr, František Flídr, David Klusoň,, Jaroslav Chaun, Matěj Kovář,  Michal Kovář, Sára Vomočilová, David Boček, Jan Nováček, Matyáš Lněnička, Samuel Vomočil

**Výsledky:**

**Borová – Horní Újezd/Morašice 0:8 (0:3)**

branky: Šimon Flídr 2, František Flídr 2, Vojtěch Huryta 2, Jaroslav Chaun, Matěj Kovář

**Horní Újezd/Morašice – Němčice 5:3 (3:2)**

branky: Šimon Flídr 3, Vojtěch Huryta, Martina Famfulíková

**Horní Újezd/Morašice – Bystré 5:0 (1:0)**

branky: Vojtěch Huryta 3, Šimon Flídr, Matěj Kovář

**Tabulka turnaje:**

1.Horní Újezd/Morašice

2.Bystré

3.Borová

4.Němčice

Fakta hovoří jasně – čtvrtý turnaj, čtvrté vítězství na turnaji. Mnohem více však těší, že vedle základních pilířů sestavy (Marťa Famfulíková, Vojta Huryta, Šimon a Fanda Flídrovi) rostou mladší hráči, kteří získávají cenné zkušenosti. Góly chybějícího Dády Válka jsme dokázali nahradit pozornou defenzivou, dva zápasy s nulou jsou toho důkazem. 

K dalšímu programu – my na Horňáku budeme trénovat klasicky pondělí, čtvrtek od půl páté. V úterý 1. října budou hrát mladší, kteří nedostávají tolik prostoru na turnajích, na Horním Újezdě proti Němčicím, sraz o půl páté, nominace dle pokynů trenérů. Další turnaj se uskuteční na Horním Újezdě v neděli 6. října od půl desáté.

## Starší přípravka

**Sestava:** Matyáš Bulva, Filip Flídr, Ondřej Kusý, Jan Kvapil, Štěpán Řejha, Matouš Zach, Martina Famfulíková, Vít Famfulík, Lukáš Mach, Jan Kopecký, Matěj Rejman, Vojtěch Štěpán, Jan Tichý

**HÚ/Morašice – Litomyšl „B“ / Čistá 6 : 0**, jednoznačné utkání, ve kterém nelze naším klukům vůbec nic vytknout, k vidění byly přihrávky, akce i zakončení. 

**Branky:** Matouš Zach 2, Filip Flídr 2, Jan Kvapil, Matyáš Bulva

**HÚ/Morašice – Koclířov 1 : 1**, v tomto zápase jsme ubrali na přesnosti a nasazení a bohužel se přizpůsobili soupeřově stylu hry, soupeř šel do vedení, ale my jsme hned vzápětí vyrovnali, ale další vyložené šance jsme již neproměnili.

**Branka:** Filip Flídr

**HÚ/Morašice – Jevíčko 2 : 2**, v tomto zápase jsme pokračovali ve festivalu neproměňování šancí a to i těch nejvyloženějších, přesto jsme šli do dvoubrankového vedení a nic nenasvědčovalo tomu, že bychom utkání nedotáhli do vítězného konce. Ale opak byl pravdou a my jsme v samém závěru po hrubých chybách inkasovali a utkání tak skončilo nerozhodně, což pro nás znamenalo konečné druhé místo v turnaji. 

**Branky:** Filip Flídr 2
