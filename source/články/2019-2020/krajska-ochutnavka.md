---
title: Krajská ochutnávka
date: 2019-11-17T05:37:19.952Z
category: pripravka
image: /public/images/uploads/logo.png
---
První halový turnaj jsme odehráli v Dolním Újezdě. Obsazení bylo velice kvalitní, všichni soupeři hrají krajskou soutěž. I přesto jsme ve všech zápasech byli minimálně vyrovnanými soupeři.

**Sestava:** Martina Famfulíková, Vojtěch Huryta, Šimon Flídr, Daniel Válek, Jaroslav Chaun, Sára Vomočilová, David Boček, Jan Nováček, Samuel Vomočil, David Klusoň

**Naše výsledky:**

**Horní Újezd/Morašice – Dolní  Újezd „A“   0:2**

**Horní Újezd/Morašice – Dolní Újezd „B“ 4:1**

branky: Vojtěch Huryta, Šimon Flídr, Martina Famfulíková, vlastní

**Horní Újezd/Morašice  - Polička   0:0**

**Horní Újezd/Morašice - Litomyšl 2:3**

branky: Daniel Válek, Vojtěch Huryta

Konečné pořadí se tentokrát nevyhlašovalo, pro nás je důležité, že jsme si ověřili, že dokážeme držet krok i se soupeři zvučnějších jmen. Celý tým zaslouží pochvalu za bojovnost a nasazení. Poslední turnaj roku 2019 odehrajeme v sobotu 7.12. v Libchavách.
