---
title: Stoprocentní úspěšnost
date: 2019-10-05T05:52:33.192Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Mladší přípravka

**Sestava:** Martina Famfulíková, Vojtěch Huryta, Šimon Flídr, František Flídr, David Klusoň,, Jaroslav Chaun, Matěj Kovář,  Michal Kovář, David Boček, Jan Nováček, Samuel Vomočil, Daniel Válek

**Naše výsledky:**

**Horní Újezd/Morašice – Bystré 5:3 (1:1)** 

Napínavé utkání, po většinu času jsme vedli, čtyři minuty před koncem ale soupeř vyrovnal na 3:3 a už to vypadalo na první letošní ztrátu, během posledního střídání jsme ale dvěma góly strhli výsledek na naši stranu

branky: František Flídr 2, Matěj Kovář, Daniel Válek, vlastní

**Horní Újezd/Morašice – Čistá 8:0 (4:0)**

Výborný výkon, podařilo se zrealizovat řadu opravdu pěkných akcí, několikrát jsme se prokombinovali až před prázdnou kasu

branky: Daniel Válek 3, Vojtěch Huryta 2, Jaroslav Chaun, František Flídr, Martina Famfulíková

**Horní Újezd/Morašice – Němčice 10:0 (7:0)** 

Brzy jsme se dostali do výrazného vedení, a tak jsme ve snaze dát více prostoru mladším hráčům hodně točili sestavou, což bylo na kvalitě hry znát.

branky: Martina Famfulíková 3, František Flídr 2, Daniel Válek 2, Michal Kovář, Vojtěch Huryta, Jaroslav Chaun

**Ostatní výsledky:**

Čistá – Němčice 6:0 (2:0)

Němčice – Bystré 0:11 (0:3)

Čistá – Bystré 4:2 (2:2)

**Tabulka turnaje:**

1.Horní Újezd/Morašice (skóre 23:3, 9 bodů)

2.Čistá (skóre 10:10, 6 bodů)

3.Bystré (skóre 16:9, 3 body)

4.Němčice (skóre 0:27, 0 bodů)

Další dějství okresní soutěže mladších přípravek pořádá v neděli 13.10. od 9:30 Bystré. Jsme domluveni, že všichni účastníci (my, domácí a Čistá) vezmeme dva týmy. Proběhnou tak dva paralelní turnaje, v jednom budou hrát nejzkušenější hráči a v tom druhém začínající.

## Starší přípravka

Již s předstihem jsme věděli, že budeme oslabeni o dva hráče základní sestavy a to Lukáše Macha a Matyáše Bulvu a současně jsem věděli, že budeme řešit i obsazení brankářského postu, protože Martina Famfulíková hrála za přípravku mladší. Naštěstí se na turnaji poprvé v podzimní části objevil Vojta Andrle, který se zhostil pozice gólmana na jedničku a patří mu poděkování za celý tým.

**Sestava:** Filip Flídr, Ondřej Kusý, Jan Kvapil, Štěpán Řejha, Matouš Zach, Vojtěch Andrle, Vít Famfulík, Jan Kopecký, Matěj Rejman, Vojtěch Štěpán, Jan Tichý, Josef Rosypal

**Horní Újezd/Morašice – Cerekvice 4 : 0**

Utkání bylo vyrovnanější než napovídá výsledek, my jsme tentokrát dokázali svoje šance proměňovat, což se ukázalo jako rozhodující. Kluci se opravdu snažili a nepřipustili žádné komplikace ve vývoji skóre. 
branky: Ondřej Kusý 2, Filip Flídr 2

**Horní Újezd/Morašice – Litomyšl „B“/Čistá 3 : 1**

Tento zápas byl opakem předešlého a my měli zvítězit větším rozdílem, namísto toho jsme po hrubé chybě inkasovali a museli tak zápas otáčet, což se nám dlouho nedařilo, zejména díky neproměňování jasných šancí. Kluci si však šli za svým a zápas dovedli zaslouženě do vítězného konce. 

branky: Jan Kvapil 2, Josef Rosypal

**Horní Újezd/Morašice – Opatov 3 : 1**

Teoreticky se mělo jednat o jednoduchou záležitost pro náš tým, protože jsme měli výškovou i fyzickou převahu. Soupeř však předvedl velmi dobrý výkon a nám dalo relativně hodně práce překonat jejich brankáře, kterého několikrát zachránily dobře postavené tyče jeho branky. I přesto jsme vedli 2 : 0 a zápas se chýlil ke konci, když se soupeři povedlo minutu před koncem snížit a zápas tak zdramatizovat. Jakékoli komplikace však razantně odmítl Ondra Kusý, který společně s Filipem Flídrem měli velký podíl na celkovém prvenství v turnaji, které všechny potěšilo a stane se tak další motivací do dalších tréninků a zápasů, protože zlepšovat toho máme ještě relativně hodně, každopádně si však myslím, že tým jde dobrým směrem. 

branky: Ondřej Kusý 2, Filip Flídr
