---
title: Co bude
date: 2019-07-01T07:22:37.503Z
image: /public/images/uploads/logo.png
category: obecne
---
Po letech snažení se nám snad podaří získat dotace na přestavbu našich starých kabin. Pokud se neobjeví nějaký zádrhel, realizace by začala už v létě a z toho důvodu by muži pravděpodobně odehráli celý podzim na soupeřových hřištích a mládežnické kategorie budou mít domácí prostředí v Horním Újezdě, za což patří tamnímu vedení velký dík. Když vše dopadne podle plánu, bude na jaře v Morašicích fotbal každý víkend a nové kabiny nám zároveň poskytnou důstojné kulisy k připomenutí 50. sezóny v historii morašického fotbalu a 120. výročí založení tělovýchovné jednoty.

Ještě lepší zprávou je, že se nám podařilo zkompletovat mládežnické kategorie a ve spojení s Horním Újezdem obnovit dorostenecké družstvo, které bude hrát skupinu B 1. třídy pardubického kraje. Trénovat ho bude Martin Štancl, kterému podzimní zranění ukončilo aktivní kariéru, a Pavel Beneš z "Horňáku". Sami kluci chtěli pro příští rok rovnou zkusit jedenáctkový fotbal, tak doufáme, že je bude bavit a že se nám je podaří u fotbalu udržet. 

Staré kabiny ještě určitě využije tradiční **turnaj morašických čtvrtí**, který začíná v pátek **5. července ve 13 hodin**.
