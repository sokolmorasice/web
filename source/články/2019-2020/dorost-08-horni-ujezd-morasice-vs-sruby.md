---
title: 'Sruby, chyby'
date: 2019-10-13T00:00:00.000Z
category: dorost
image: /public/images/novinky/2019-2020/dorost.png
h1: Horní Újezd/Morašice vs Sruby
sestava: >-
  Jaromír Loskot - Pavel Beneš, Jakub Nádvorník (Jiří Svatoš), Martin Tmej, Petr
  Kazda - Matěj Roško (František Lněnička), (Jan Kazda), Tomáš Briol, Vojtěch
  Opletal, Radovan FLídr - František Bartoš, Jan Pechanec
goly: 'Vojtěch Opletal, František  Bartoš'
zlute: 'Tomáš Briol, František Lněnička'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533C2B/2019/Horní
  Újezd/Morašice/a0beaee2-6d12-48c7-8e9a-4b5953ee77fd.html
---
Předposlední domácí utkání nás zastihlo ve výtečné formě, alespoň co se kusů týče. Lavička praskala ve švech, že ani vedení si nemělo kam sednout. Dle tabulky se mělo jednat o jasnou záležitost, jenže chyba lávky. V podstatě se po celou dobu hrálo vyrovnané utkání, ve kterém bylo k vidění spousta nepřesností a zkažených přihrávek. Jakousi optickou převahu jsme sice měli, ale přechod přes hostující obranu nám kazily ofsajdy. Po stranách jsme se do hry dostávali pramálo a středem to prostě nešlo. Kombinace na straně hostí vázla o poznání více než u nás a většina balónu končila nákopem směrem dopředu. K jednomu takovému odkopu se v 18. minutě odhodlal Žaba a zhruba ze 60-i metrů otevřel skóre zápasu. Asistenci si připsalo sluníčko a podklouznutí Loskota v bráně. O tři minuty později se nám podařilo vyrovnat Opletalem. Jeho ne příliš kvalitní střela zapadla k tyči a začínalo se znovu.

Druhý poločas se nesl v podobném duchu jako ten první. Hra na tři až čtyři přihrávky a míč měl soupeř. Oba brankáři si připsali po dvou náročnějších zákrocích, ale jinak nuda. V 72. minutě se naše obrana proměnila v koukající kužele. Hosté zahrávali aut, na který jsme se totálně vykašlali a zcela volný Marciš nadvakrát protlačil míč za Loskota. Deset minut před koncem se podařilo vyrovnat Bartošovi, který na několikrát procpal míč do sítě. V samém závěru mohl 3 body vystřelit Opletal, kdyby nestálo v cestě břevno. Vzhledem k průběhu utkání je plichta asi spravedlivá. A jak se říká, penalty jsou loterie. My nedali o jednu víc než soupeř, který si tím mohl připsat první body do tabulky.
