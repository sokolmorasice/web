---
title: Halový turnaj mladší přípravky v Dolním Újezdu
date: 2020-03-10T05:51:36.286Z
category: pripravka
image: /public/images/uploads/logo.png
---
Poslední letošní halový turnaj se nám opětovně vydařil. Kromě výborných výsledků těší především fotbalový projev, troufnu si říct, že ze všech mužstev jsme nejvíce kombinovali a některé naše akce snesly přísné měřítko. Získali jsme stejně bodů jako domácí zelený tým, ale zásluhou lepšího skóre jsme obsadili první místo my.

**Sestava:** Martina Famfulíková – Sára Vomočilová, Vojtěch Huryta, Daniel Válek, David Boček, Šimon Flídr, David Klusoň, Samuel Vomočil, František Flídr, Jaroslav Chaun

Výsledky:

**Dolní Újezd zelení – Horní Újezd/Morašice 1:1**

branky: Daniel Válek

**Horní Újezd/Morašice – Čistá 3:0**

branky: Daniel Válek 3

**Dolní Újezd modří – Horní Újezd/Morašice 1:3**

branky: David Boček, Daniel Válek, Jaroslav Chaun

**Horní Újezd/Morašice – Sebranice 7:1**

branky: Daniel Válek 3, David Boček, Jaroslav Chaun, Šimon Flídr, Martina Famfulíková

**Horní Újezd/Morašice – Polička 2:0**

branky: Daniel Válek, David Boček

\
**Tabulka turnaje:**

**1.Horní Újezd/Morašice**

2.Dolní Újezd zelení

3.Polička

4.Sebranice

5.Čistá

6.Dolní Újezd modří

Nejlepším brankařem (brankařkou) turnaje byla vyhlášena Marťa Famfulíková, naším nejužitečnějším hráčem David Boček. Pevně doufám, že nám forma vydrží i na venkovní turnaje. První jarní mistrovský turnaj by se měl uskutečnit v sobotu 18. 4. na Sebranicích, kompletní rozpis turnajů snad bude v nejbližších dnech.

autor: Zdeněk Beneš