---
title: Obecní ples
date: 2020-02-11T09:43:25.036Z
category: obecne
image: /public/images/uploads/plakaty-silueta-par-tancici.jpg
---
TJ Sokol Morašice, obec Morašice a MS Morašice zve všechny příznivce TJ Sokol Morašice, obce Morašice, MS Morašice a dobrého jídla a hudby na **Obecní ples**, který se koná v Morašicích na Rychtě **v sobotu 29.2**. Již od **17:00** bude dole otevřená myslivecká kuchyně, od **20:00** bude hrát k tanci a poslechu kapela **Vepřo, knedlo, zelo.**  Když se budete dobře [chovat](<https://www.youtube.com/watch?v=-oJ7rv_nsXM>), můžete se těšit na napínavou [](https://www.youtube.com/watch?v=TYfrVDH2new)[tombolu](https://www.youtube.com/watch?v=TYfrVDH2new) a bohaté občerstvení. Nebo obráceně?
