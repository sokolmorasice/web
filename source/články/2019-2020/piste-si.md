---
title: Pište si
date: 2019-11-20T06:40:13.789Z
category: obecne
image: /public/images/uploads/logo.png
---
Je za námi nejen fotbalově úspěšný podzim. Všechny mládežnické kategorie fungují v osvědčeném a úspěšném souklubí s Horním Újezdem. Jen obnovené dorostenecké družstvo nás občas nemile překvapilo, ale obě žákovské kategorie byly na podzim stoprocentní. Stejně jako mladší elévci, u kterých nás těší i to, že chvílemi vydají na dva týmy, 3 turnaje ze 7 vyhráli i starší elévci, muži venku překvapili celou lidovkovou veřejnost. Za podpory obce jsme rozjeli rekonstrukci kabin, podařilo se získat dotaci na ně i na provoz klubu, rozšířili jsme branky, body, vteřiny o další kategorie. Bylo by snadné vypíchnout pár jmen, kterým se sluší poděkovat, ale bylo by také snadné na někoho zapomenout, takže obecně děkujeme všem trenérům a ochotným rodičům, sekáčům, stavbyvedoucím, webovým přispěvatelům, dotačním zařizovatelům a všem organizačně činným lidem. Pokud se někdo v tomto anonymním seznamu nenašel, nechť se pochlubí svým příspěvkem nebo si pro nějaký přijde, práce je dost. Doplnění trenérů u mládeže, zajištění občerstvení na jaro nebo sekání hřiště jsou ty, co nás pálí nejvíc.

Za vytrvalou podporu děkujeme obci Morašice, vážíme si i příspěvků dalších okolních obcí a sponzorů.

Děkujeme také TJ Horní Újezd za půlroční poskytnutí domácího zázemí naší mládeži vyžadující nemalé personální nasazení i za jejich trenéry u jednotlivých kategorií. 

A teď k tomu, co nás (ne)čeká:

**25.12.2019** – ŠTĚPÁNSKÁ UŽ NENÍ. Aspoň v Morašicích. Léta jsme se snažili, abyste si mohli už na Boží Hod večer v klidu říct, co jste dostali pod stromeček. Protože o tuto poklidnou konverzaci jevilo zájem čím dál méně lidí, rozhodli jsme se zůstat s rodinami nebo se nechat hostit někde jinde. Pokud Vám je z toho moc smutno, můžete se cestou z půlnoční mše stavit před dveře rychty zapálit svíčku.

**31.12.2019** – Od 10:00 zveme všechny aktivní, pasivní i příležitostné hráče i fanoušky na tradiční silvestrovský fotbálek.

**21.2.-23.2.2020** – Soustředění A týmu v Daňkovicích, nikterak omezené pouze na hráče, rezervace u Václava Štancla.

**29.2.2020** – Ve spolupráci s obcí a myslivci pořádáme tradiční ples, přijďte pomoct s pořádáním, v mezidobí vyžebrejte nějakou tombolu od svých zaměstnavatelů a zámožných známých.

**23.5.2020** - Tuto sobotu se chystáme oslavit 50 let našeho oddílu na fotbalové mapě a 120 let naší TJ. Termín si zapište, podrobnosti dodáme.
