---
title: Halový turnaj mladší přípravky
date: 2020-01-26T19:59:16.615Z
category: pripravka
image: /public/images/uploads/logo.png
---
Vypadá to, že naši nejmenší se rozhodli rozdávat radost i v novém roce. Na našem úvodním turnaji roku 2020 jsme se znovu předvedli ve velice dobrém světle. Sice jsme začali porážkou po herně vyrovnaném zápase s domácím týmem, který turnaj nakonec vyhrál, ale poté jsme třikrát vyhráli (za zmínku rozhodně stojí výsledek s Litomyšlí) a remizovali po velice dobrém výkonu s Poličkou. Naším nejlepším hráčem byl vyhlášen Šimon Flídr, stejně jako na minulém turnaji v Libchavách se nejlepším střelcem turnaje stal Dáda Válek.

**Sestava:** Martina Famfulíková – Vojtěch Huryta, Daniel Válek, Šimon Flídr, František Flídr, Jan Nováček, David Boček, Sára Vomočilová, Samuel Vomočil, David Klusoň

Výsledky:

**Dolní Újezd – Horní Újezd/Morašice 4:2** 

branky: Daniel Válek 2

**Horní Újezd/Morašice – Litomyšl 9:0**

branky: Daniel Válek 8, Sára Vomočilová

**Horní Újezd/Morašice – Bystré 4:1**

branky: Daniel Válek 2, Sára Vomočilová 2

**Horní Újezd/Morašice – Sebranice 6:1**

branky: Daniel Válek 2, Martina Famfulíková 2, Vojtěch Huryta, František Flídr

**Horní Újezd/Morašice – Polička 1:1** 

branky: Daniel Válek

**Konečné pořadí:**

1.Dolní Újezd

**2.Horní Újezd/Morašice**

3.Polička

4.Bystré

5.Sebranice

6.Litomyšl

autor: Zdeněk Beneš
