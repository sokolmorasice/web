---
title: Krásná neznámá
date: 2019-08-31T00:00:00.000Z
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Koclířov vs Horní Újezd/Morašice
sestava: >-
  Jiří Kučera - Marek Vích, Zuzana Famfulíková, Jiří Jána, Tomáš Briol, Jan
  Vedral, Šimon Kušnír, Šimon Lněnička, Vlastimil Hladík,Jan Odehnal, Vojtěch
  Famfulík, Filip Kopecký, Tereza Lněničková
goly: 'Jiří Jána 3, Tomáš Briol 2, Filip Kopecký, Jan Odehnal, Šimon Kušnír'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533E2A/2019/Horní
  Újezd/Morašice/7a8127b4-8789-493b-bc1a-2db81893c941.html
---
Do nové sezóny jsme vstoupili zápasem v Koclířově. Pro nás neznámý soupeř, pro soupeře neznámá soutěž. Začátek zápasu vyšel na jedničku, když ve 2. minutě otevřel skóre Jána. Na tuto branku soupeř dokázal pohotově odpovědět a v 7. minutě byl stav utkání 1:1. Po srovnání jsme se vší silou snažili změnit skóre, ale spoustu šancí bylo přerušeno soupeřem, nebo naší chybnou komunikací na hřišti. Po dobývání branky soupeře smrští střel se ve 30. minutě opět trefil Jána. Branka jakoby opařila soupeře a o dvě minuty déle zvyšoval na 3:1 Briol. 

Po změně stran se utkání vyvíjelo stejným směrem a v 38. minutě dalekonosnou střelou zvýšil na 4:1 Kopecký a o dvě minuty déle upravil na 5:1 Briol.  Veškeré šance soupeře likvidovalo ofsajdové postavení útočníků, nebo včasné vyběhnutí gólmana Kučery. Ještě v 50. minutě se hlavou trefil Odehnal, v 60. Kušnír zakroutil roh za záda brankáře a v 65. zkompletoval Jána hattrick. Podtrženo sečteno, 1:8 z pohledu domácích. Další utkání odehrajeme na Horňáku v sobotu 7.9. 2019 v 10:00, kde přivítáme družstvo z Hradce nad Svitavou.
