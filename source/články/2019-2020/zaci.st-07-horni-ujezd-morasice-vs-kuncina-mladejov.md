---
title: 'Jde to, ale dře to'
date: 2019-10-13T00:00:00.000Z
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Horní Újezd/Morašice vs Kunčina/Mladějov
sestava: >-
  Lukáš Němec - Marek Vích, Jiří Jána, Zuzana Famfulíková, Jan Vedral, Šimon
  Kušnír, Šimon Lněnička, Tomáš Huryta, Vojtěch Famfulík, Filip Kopecký, Tereza
  Lněničková, Jan Odehnal, Jakub Kvapil, Tomáš Briol, Matěj Kopecký
goly: 'Jiří Jána 4, Jan Odehnal, Tomáš Briol (PK)'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533E2A/2019/Horní
  Újezd/Morašice/dbc6af88-5981-4a6e-9f02-27d5fedd88c3.html
---
Po nehezkém výkonu z minulého týdne jsme si chtěl napravit reputaci na domácím hřišti, kabina čítala nevídaných 15 kusů a to nám dovolilo posadit hned 4 členy základní sestavy z minulého utkání. Hned z úvodním hvizdem sudího jsme se dočkali branky v naší síti, a to i bez toho, aby se soupeř dotkl míče. Briol vracel míč brankáři, kterému smolně balon přeskočil nohu a k nevíře všech na tabuli svítilo skóre 0:1. Herní projev se velice podobal minulému zápasu, proto došlo k brzkému střídání našich řad. Střídající Jána po dvou minutovém pobytu na hřišti vyrovnal na 1:1. Jak už je u něj zvykem do poločasu přidal další dvě branky a do kabin jsme šli za stavu 3:1.

Druhá půlka přinesla další řadu střídání, abychom využili šíři našeho kádru. Obraz hry byl stejný jako první poločas, soupeřovo rychlé brejky likvidovala naše obrana, ve většině případů bez pomoci zálohy. Na druhé straně hřiště naše ofenzíva likvidovala šanci za šancí, až do 43. minuty kdy se prosadil Odehnal. V 50. minutě Briol kličkoval ve vápně až byl zastaven faulem a z nařízené penalty se nemýlil. Za poslední proměněnou šancí stál Jána, který tak vstřelil 4. branku v utkání. V závěru utkání si dobré šance vypracoval Kopecký, ale žádna už neskončila v síti.

Projev našich hráčů se o něco zlepšil, přesto se nám nedaří navázat na výkony z kraje sezóny. Za zmínku ještě stojí výkon Němce, který v brance předvedl nejeden učebnicový zákrok a i díky němu se můžeme radovat ze tří bodů
