---
title: Motýl roztáhl křídla
date: 2019-10-27T00:00:00.000Z
category: dorost
image: /public/images/novinky/2019-2020/dorost.png
h1: Horní Újezd/Morašice vs Kameničky
sestava: >-
  Emanuel Hanyk - Pavel Beneš, MArtin Tmej (František Lněnička), Matěj Roško,
  Jiří Pechanec - Jiří Šturc, Tomáš Briol, Vojtěch Opletal, Radovan Flídr -
  Jakub Nádvorník, Jan Pechanec
zlute: Jan Pechanec
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533C2B/2019/Horní
  Újezd/Morašice/1a231a49-5840-492b-97b1-82c6fd9fd7ca.html
---
Po minulém debaklu v Nasavrkách jsme si jako hlavní cíl dali důstojné rozloučení s domácím podzimem. I když to nevyšlo výsledkově, tak projev nebyl nikterak marnej. Los nám určil na poslední tři zápasy nejvýše postavené soupeře v tabulce. V sobotu se to trochu smeklo, a tak se šlo k reparátu prostě jen makat. Eman nechal podběrák naštěstí v Nasavrkách a místo toho v neděli roztáhl křídla tak, že by se divil i jeho jmenovec s makovou panenkou dohromady.

Prvních dvacet minut jsme, možná až překvapivě, určovali tempo hry my. Za uzdraveným Jirkou Šturcem znovu lítala tráva, vápno i marně stíhající protihráči. Bohužel většina centrů a zpětných přihrávek šla do míst, kde si míč snadno stahoval hostující Vodička. Dalekonosnou ránou, která jen těsně minula kameničskou tyč, se připomněl Opletal. Od 25. minuty jsme ovšem začali více kazit rozehrávku a tím se hosté dostávali do šancí a brejků. Nejprve trefili břevno a pak šli během 15-ti minut asi 4x sami na Emana. Dodnes nevíme, co večer před tím pil, ale svědčí mu to. Díky němu se šlo do kabin za bezbrankového stavu.

Druhá půle byla odpracovaná jako ta první a o branku jsme se připravili jen špatnou finální nahrávkou, nebo nahrávkou místo střely. Naši největší šanci spálil Opletal. Ten měl na penaltě dostatek času, protože sluncem oslepený Vodička na brankové čáře zmrzl a jen poslouchal, co se bude dít. Míč o fous minul levou tyč (z pohledu brankáře). Hned z následného výkopu se po levé straně prodrali hosté k našemu vápnu a Vamberský nabídnutou přihrávkou nepohrdl. Nutno dodat, že před tím skončily v Emanových rukavicích další tři sólové úniky, i míč odrážející se od tyče. Když 5 minut před koncem Vodička reflexivně vykopl přízemní střelu Opletala, Pechance nebo Šturce, bylo hotovo.

Poslední podzimní utkání hrajeme v neděli v Jaroměřicích, tak uvidíme, jaké překvapení nás čeká tam.
