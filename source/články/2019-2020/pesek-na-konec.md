---
title: Pešek na konec
date: 2019-12-07T22:39:51.634Z
category: pripravka
image: /public/images/uploads/logo.png
---
Na závěr roku jsme se vydali na halový turnaj do Libchav, abychom trochu poznali soupeře i z jiných končin. Po ranním procvičení řidičských dovedností na kluzkých silnicích jsme měli trochu zpoždění, ale v pro nás neznámé tělocvičně jsme se brzy adaptovali a oba zápasy ve skupině vyhráli bez obdrženého gólu. V semifinále na nás vyšel celek ze Žamberka, který jsme zdolali vysoko 4:0. Ve finálové bitvě proti Ústí nad Orlicí jsme byli po většinu času lepší, ale štěstí se k nám tentokráte otočilo zády a jediný inkasovaný gól za celý turnaj rozhodl o tom, že na diplomu jsme domů přivezli číslo dvě.

**Sestava:** Martina Famfulíková – Vojtěch Huryta, Šimon Flídr, Daniel Válek, František Flídr, David Klusoň, David Boček

Naše výsledky:

**Horní Újezd/Morašice – Sruby 2:0**

branky: Daniel Válek, Šimon Flídr

**Horní Újezd/Morašice – Solnice 3:0**

branky: Daniel Válek 2, Vojtěch Huryta

Semifinále:

**Horní Újezd/Morašice – Žamberk 4:0**

branky: Daniel Válek 3, David Boček

Finále:

**Horní Újezd/Morašice – Ústí n. O. 0:1**

Konečné pořadí:

1.Ústí n. O.

2.Horní Újezd/Morašice

3.Libchavy

4.Žamberk

5.Sruby

6.Solnice

I když po prohraném finále bylo k vidění na tvářích některých našich hráčů zklamání, nás trenéry dnešní turnaj opětovně potěšil. Zase jsme si dokázali, že tenhle tým má na to hrát i se soupeři z krajských soutěží. Navíc si Dáda Válek odvezl cenu pro nejlepšího střelce turnaje. Snad se nám bude podobně dařit i v roce 2020.
