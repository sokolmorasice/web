---
title: Hradec k obědu
date: 2019-09-08T00:00:00.000Z
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Horní Újezd/Morašice vs Hradec n. S.
sestava: >-
  Ondřej Žďára, Marek Vích, Zuzana Famfulíková, Jiří Jána, Matěj Kopecký, Tomáš
  Briol, Jan Vedral, Šimon Kušnír, Šimon Lněnička, Jonáš Krška, Jan Odehnal,
  Daniel hanyk, Filip Kopecký, Tereza Lněničková
goly: 'Jiří Jána 5, Tomáš Briol, Marek Vích'
multimedia: http://www.tjhorniujezd.cz/?p=12922
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533E2A/2019/Horní
  Újezd/Morašice/c713e174-1b7a-4f8a-8322-a7da6340f385.html
---
Po našem prvním utkání, které skončilo vysoko 8:1 to vypadalo, že jsme soupeře z Hradce nad Svitavou natolik zastrašili, že k utkání druhého kola ani nedorazí. Dočkali jsme se až dvě minuty před oficiálním začátkem zápasu. Jelikož tuto sezónu se na soupisce starších žáků nevyskytuje ani jeden bránkář, tak musíme spoléhat na brankáře mladších žáků. Bohužel tento zápas marně. Černého Petra si vytáhl Žďára a odbyl si svoji golmanskou premiéru. První branky v naší síti jsme se dočkali hned v první minutě, soupeř obranou prošel bez nějakých obtíží, až to vypadalo tak, že se naši hráči zapomněli v kabině. Po pěkné kombinaci jsme se dočkali vyrovnání v 10. minutě, kdy Jána dostal nabídku, která se neodmítá a s klidem skóroval. Takto to pokračovalo dále a Jána ve 14. minutě zkompletoval hattrick (opět). Utkání působilo vyrovnaně, šance se ukazovali na obou stranách, až do 31. minuty, kdy kapitán hostí Frolo přidal svou druhou branku v utkání. Na což dokázal opět odpovědět Jána, ještě před poločasovým hvizdem přidal už 5. branku v utkání a do kabin jsme šli za stavu 5:2. 

Do druhé půli jsme do branky poslali Jánu, aby si nevystřílel všechen střelný prach. Po zdařilém rohu Kušníra ve 38. minutě upravil na 6:2 Vích, když  tváří v tvář odkryté brance s klidem uklidil. Na další branku jsme čekali do 45. minuty, kdy krásnou střelou zvýšil Briol. Jána v brance předvedl pár hezkých zákroků, ale minutu před koncem utkání nedosáhl na střelu Frola a dovolil mu tak, aby také zkompletoval hattrick. Začátek seźony nám vyšel na jedničku a po dvou odehraných kolech máme na kontě 6 bodů a skóre 15:4.
