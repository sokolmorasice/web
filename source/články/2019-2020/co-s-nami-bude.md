---
title: Co s námi bude?
date: 2020-03-13T09:37:48.856Z
category: obecne
image: /public/images/uploads/logo.png
---
Co by, byli jsme před koronavirem, budeme i po něm. Nicméně v návaznosti na vyhlášení nouzového stavu vládou ČR, TJ Sokol Morašice do odvolání ruší veškeré tréninky všech kategorií, protože není ani na co trénovat, začátky soutěží všech kategorií jsou odloženy na "až bude líp". Sice zatím není zakázáno sdružovat se pod širým nebem v počtu do 30 lidí, ale uvažte sami, jak hodně se v případě podezření jediného známého jediného z hráčů rozroste okruh izolovaných, v horším případě rovnou nakažených. Až se situace změní, dozvíte se to od Andreje a posléze i od našich trenérů. Děkujeme za pochopení, přejeme pevné zdraví i zdravý rozum.