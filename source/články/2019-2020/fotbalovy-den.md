---
title: Fotbalový den
date: 2020-06-21T20:01:20.753Z
category: obecne
image: /public/images/uploads/plakat-a3.jpg
---
V sobotu 5.9., pokud to okolnosti dovolí, pořádáme v Morašicích velký fotbalový den u příležitosti 50-ti let fotbalu a 120-ti let Sokola v Morašicích. 

Program: 

**09:30 -** turnaj starších elévků

**12:30 -** mistrovský zápas mužů

**15:00 -**  slavnostní zahájení

**15:30 -** utkání starých gard TJ Sokol Morašice vs. FC Hradec Králové

Chystáme pro Vás výstavu archivních fotografií a historických statistik, prohlídku nových kabin, pro děti bude k dispozici skákací hrad a pěna od hasičů, bude zajištěno bohaté občerstvení a setkání se spoustou starých známých.

Srdečně zveme všechny příznivce, upozorňujeme, že prakticky všechny okolní akce, které budou termínově kolidovat, se budou s velkou pravděpodobností příští rok opakovat, naše výročí nikoliv. Kdo by měl jakékoliv historické materiály, ať se s námi o ně podělí. Kdo by chtěl s čímkoliv pomoc, má dveře otevřené.