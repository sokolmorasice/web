---
title: Podzimní ohlédnutí
date: 2019-11-16T10:00:00.000Z
category: dorost
image: /public/images/uploads/logo.png
---

Po několikaletém půstu se družstvo dorostu dalo znovu dohromady a během podzimního
tažení se ho zúčastnilo 22 hráčů, včetně čtyř žákovských posil, které nám několikrát vytrhly
trn z paty. Tréninky probíhaly společně každý pátek v Horním Újezdě a účast na nich se s
horšícím počasím postupně snižovala.

V hlavní soutěži jsme odehráli 11 zápasů, ve kterých nám osud přidělil 10 bodů a posadil nás
celkově na 9. místo. Na domácím hřišti jsme neodehráli vyloženě špatný zápas. I za
nepříznivého stavu mohl těžko někdo něco namítat proti bojovnosti a snaze s výsledkem něco
udělat. Body nás stálo hlavně neproměňování šancí. Ale vzhledem k tomu, že několik hráčů
doposud nehrálo velký fotbal, lze nad drobnými individuálními a charakterními vadami
přimhouřit oko. Co se nám dařilo doma, tak jsme cestou ven jaksi poztráceli. Z ekonomického
hlediska byly naše venkovní zájezdy dost nevýhodné. Až na výborně zvládnutý zápas ve
Vraclavi a úvodní nezdar v Králíkách jsme si odevšad přivezli dardu. Přitom, až na pár
výjimek, se s většinou ze soupeřů dal hrát vyrovnaný fotbal. Můžeme však hovořit o velikém
štěstí, že nám los tyto zápasy přiřkl venku. Během zimy je jen na nás, aby naši diváci byli
během jara svědkem lepšího fotbalu, než podzimní kopané venku.

Z individuálních statistik lze uvést několik jmen, i když je fotbal kolektivní sport.

* **100% účast na zápasech (včetně dvou přáteláků):** Pavel Beneš, Radovan Flídr, Jan
Pechanec, Martin Tmej
* **Střelci (včetně přáteláků):** 8x Jan Pechanec, 7x Vojtěch Opletal, 6x František Bartoš,
4x Jiří Šturc, 2x Radovan Flídr, Tomáš Briol, 1x Emanuel Hanyk, František Lněnička,
Jakub Nádvorník,
* **Zlí muži (žuté karty):** 2x František Bartoš, 1x Tomáš Briol, Radovan Flídr, Emanuel
Hanyk, František Lněnička, Jakub Nádvorník, Vojtěch Opletal, Jan Pechanec, Pavel
Beneš (trenér) a Luděk Bureš, který v ten den vůbec nebyl na zápase.
* Emanuel Hanyk a Jaromír Loskot si svoji porci gólů rozdělili spravedlivě, každý tahal míč ze
sítě 28x.

Všem hráčům, rodičům, fotbalovým nadšencům a ostatním pomocníkům děkujeme za
podzimní část sezóny. Užijte si Vánoce, zbytek roku a v tom novém zase na fotbale.

Pavel Beneš a Martin Štancl
