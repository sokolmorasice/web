---
title: Hradec k večeři
date: 2019-09-07T00:00:00.000Z
category: muzi
image: /public/images/zapasy/hradec-nad-svit-morasice.png
h1: TJ Hradec n. S. vs Morašice
sestava: >-
  J. Špinar - D. Štancl, M. Ropek (K. Vopařil), V. Štancl, V. Tměj (D. Vejrych)
  - B. Válek, M. Klement, T. Nádvorník, V. Štěpán (J. Kopecký) - M. Chmelík, M.
  Hubáček (J. Vít)
goly: 'T. Nádvorník 2, B. Válek (pen.), M. Ropek, K. Vopařil'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533A1A/2019/Morašice/7dbf2a61-e8ac-4258-8597-d8269115c092.html
---
Po dopoledním žákovském předkrmu pokračovalo v deštivém podvečeru naše oddílové zápolení s Hradcem nad Svitavou v kategorii mužů. Nováček soutěže se minulé kolo dočkal první výhry, když si připsal skalp Jevíčka, které pro změnu skalpovalo nás. Tento fakt děsil hlavně trenéra, který se bál, že to s několika absencemi bude boj o každý centimetr. Pravda, když je hřiště takhle malé a jednadvacet lidí je natěsnáno na jedné polovině, toho místa moc není. My jsme sice hned po rozehrávce dokázali dostat k centru na Klementa, který zakončoval na dlouhou hlavu, jenže pak nám dobrou čtvrt hodinu trvalo, než jsme si zvykli na stísněný prostor a začali kazit méně přihrávek. První velkou šanci měl novic v útoku Hubáček, na kterého domácí obrana zapomněla, ale hlavou z penalty nezamířil přesně. Ještě větší tutovku měl později Válek, který se ve skluzu krásně položil do centru napříč vápnem, ale _trefil se přesně do míst kudy se přesouval domácí gólman ve sparťanském dresu_ / _domácí gólman ve sparťanském dresu předvedl výborný zákrok břichem_*. Jinak jsme z bezradnosti často zkoušeli střílet z dálky, zejména Tměj s Nádvorníkem, a rezultovalo z toho aspoň pár rohů, když ne přímé ohrožení. Po jednom z nich se jal uklízet spadlý míč do bezpečí jeden z domácích hráčů, ale nevšiml si Hubáčkovy nohy v cestě. Celého incidentu si všiml hlavní rozhodčí, který nechal domácí zanadávat, ale penaltu jim stejně neodpustil, což se v tak nesoudném prostředí cení. Válek se nemýlil a po půl hodině jsme se konečně dostali do vedení. Pět minut před přestávkou si Nádvorník našel za vápnem odvrácený centr a po klidné příprave se trefil do černého. Chvíli na to byli domácí tak zaměstnaní divadlem svého beka po kontaktu s Válkem, že absolutně zapomněli reagovat na Štěpánův roh, který na zadní tyči pohodlně uklidil Ropek.

Ten se o přestávce asi jako jediný bál, že jsme schopní tříbrankové vedení proti soupeři bez jediné střely ztratit, a proto volal po pojistce (sic!), aby si mohla zahrát i naše lavička. Nástup jsme měli výjimečně dobrý, respektive se pokračovalo stejně jako v prvním poločase, ale těžko vypíchnout něco, co by v prvních dvaceti minutách opravdu smrdělo gólem. Skóre navýšil až Nádvorník, který se na vápně ladně opřel do Klementova špatného zpracování a jeho oblouček se vešel pod břevno. Vzápětí tentýž hráč úspěšně presoval a předložil míč do šance střídajícímu Vopařilovi, který tváří v tvář gólmanovi napálil míč pod břevno. Možná hattrickovou Nádvorníkovu střelu vyhlavičkoval na penaltě obránce, Štěpán při nájezdu ze strany minul zadní tyč a Vopařil už nezvládl gólmana z bezprostřední blízkosti obstřelit. 300. zápas v morašickém dresu mohl ozdobit asistencí D. Štancl, ale Kopeckému chyběl k zasunutí jeho centru do prázdné brány metr. Špinar si sáhl na balón snad jen při dvou centrech, ale to mu asi dojem z premiérového nuly sezóny nezkazí, stejně jako nám pohodlnost této výhry. 

_\* nehodící se škrtněte_
