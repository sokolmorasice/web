---
title: Chmelík stoupá tabulkami
date: 2020-07-27T04:24:18.794Z
category: muzi
image: /public/images/uploads/logo.png
h1: Sruby - Morašice, 2:2, na penalty 3:3
sestava: J. Špinar - V. Novák, M. Ropek, V. Štancl, V. Tměj, D. Štancl, M.
  Chmelík, T. Nádvorník, V. Štěpán, B. Válek, F. Bureš, M. Hubáček, D. Ondráček,
  D. Vejrych, J. Kopecký
goly: M. Chmelík, M. Hubáček
multimedia: https://drive.google.com/drive/folders/1egqlwislHxxzaOZjqlewcuQuP_RMlNNr
---
Dnes, se podíváme zpátky do minulosti na to, jak naposledy hrály Morašice ve Srubech. Byly to dva zápasy v I.B třídě v roce 1999, jeden z nich dopadl takto:

**Sruby – Morašice 2:4**

Sestava: I. Černík – Petr Lněnička, F. Ropek, M. Ropek, Josef Jána, J. Famfulík, Petr Kuta, J. Kopecký, D. Kubišta, V. Drebit, L. Bárta

Góly: Petr Kuta, J. Famfulík, L. Bárta, Josef Kopecký

Historické zápisy jsme kvůli věrohodnosti konfrontovali s posledními pamětníky se stejnou otázkou: „Víš, kdys tady hrál naposled?“ Odpovědi nejsou zrovna dobrým vysvědčením.

Josef Kopecký: „To bude tak 15 roků.“

Martin Ropek: „Tady jsem nikdy nehrál.“

Ale vraťme se do covidové současnosti. Už při rozcvičení bylo znát, komu na zápase záleží víc, jak dokládá kapitánská páska na ruce a trenérský „pojeb“ během rozcvičky v podání domácích. V prvním poločase to odpovídalo, soupeř měl navrch, ale jednu vyloženou šanci chytil Špinar, druhou přihasil a dokončil to za něj Ropek a rychlostně disponovaná jedenáctka naštěstí poslední dotyky kazila. Za nás si propagační střelu připsal Tměj, Nádvorník se rozhodl neprotahovat nájezd přes čtvrtku hřiště a zakončil, dá se říct že zbaběle, zbytečně z dálky, Štěpán radši nevystřelil vůbec, Válek neobhodil rychle vyběhnuvšího gólmana a jeho předchozí trefu neuznal rozhodčí, protože domácí pomezní z dálky viděl balón v autu při Chmelíkově nahrávce. Dobře pro něj, takovej zrak neměl nikdo na hřišti. Pauzu jsme tak trávili jen s jednogólovým mankem, když zaspal střed, zaspali beci, zaspaly všechny děti, a proti křížnému zakončení z bezprostřední blízkosti nemohl nikdo nic namítnout. Leda pomezní Kopecký mohl přilít olej do ohně zvednutým praporkem, ale neudělal to.

V nástupu do druhé půle napálil Ondráček balón na Chmelíka, který se vzdor instrukcím zjevil na hrotu, vyčkal si na obráncův špatný zákrok a zpoza vápna napálil míč pod břevno nad dosah vystřídavšího gólmana. Pokladník by si měl poznamenat, že se tím gólově trhl od Klementa a posunul se na sdílenou pozici 19. nejlepšího střelce, a to zrovna v 725. zápase, kterým se osamostatnil na 3. příčce co do mužských zápasů, viz. [zde](https://docs.google.com/spreadsheets/d/1leVjJu7cU-j48mBYp9hx7PgFwTmH01byvIbeJqPWgPo/edit#gid=606575394). Náš desetiminutový elán bez velké šance zmrazil domácí aut, po kterém Kopecký hlavou nevyhrál souboj s hostující nohou a důsledkem toho samostatně postupující útočník se trefil mezi Špinarovy nohy. A pak se dá říct, že už jsme šance počítali jen my. Bureš nejprve levačkou procedil míč skrze gólmanovy rukavice, ale jeho výškrab pravděpodobně zpoza čáry nebyl stoprocentně schopen potvrdit nikdo, včetně našeho pomezního Tměje, který jako by nevěděl, že vlastním se připískává vždycky. Další šanci měl Bureš po průnikovce D. Štancla, bohužel se mu pod nohama rychle objevil domácí gólman, bohudík vrátil odražený míč do prázdné brány Hubáček. Dvakrát střílel Chmelík, jednou pod břevno zasáhl gólman, podruhé po rohu mířil vedle, Štěpán nemířil přesně z dálky do odkryté brány, Ondráček neztrestal kiks náhradního gólmana, Válek, kromě jiného, v poslední minutě netrefil ideálně Štěpánův banánový centr a domácí poněkolikáté vykopávali z brankové čáry. Suma sumárum, kdyby domácí „Tonda“ zakroutil svoji střelu v posledních pěti minutách o fous přesněji, bylo by to hodně nespra.

Narychlo domluvené penalty proběhly následovně:

Válek NE / domácí ANO.

Chmelík ANO / domácí NE.

D. Štancl NE / domácí ANO.

V. Štancl ANO / domácí ANO.

Špinar ANO / domácí NE.

Co z toho vyplývá: Remízu neberem, ale remíza po penaltách za to stojí. Válek se vystřílel na čtvrtích a po šesti úspěšných pokusech poprvé od svatého Václava L.P. 2017 selhal. A Špinar si snad nevybral všechnu penaltovou kliku zrovna v tomhle nicneřešícím zápase.