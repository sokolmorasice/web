---
title: Do deseti venku
date: 2019-09-29T00:00:00.000Z
category: dorost
image: /public/images/novinky/2019-2020/dorost.png
h1: Horní Újezd/Morašice vs Luže/Rosice u Chrasti
sestava: >-
  Emanuel Hanyk (Jaromír Loskot) - Martin Tmej, Pavel Beneš, František Lněnička
  (Filip Kopecký), František Bartoš -  Radovan Flídr, Tomáš Briol, Matěj Roško
  (Luděk Bureš), Jiří Šturc - Jan Pechanec (Jiří Jána), Vojtěch Opletal
goly: 'Vojtěch Opletal 2, Tomáš Briol, Jan Pechanec (pen.), František Bartoš'
zlute: František Bartoš
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533C2B/2019/Horní
  Újezd/Morašice/3764f871-2757-4f0e-809f-6a7418f56b7b.html
---
Z minulého týdne bylo potřeba učinit nápravu. Naštěstí se nám podařilo po týdenní odmlce opět zaplnit střídačku. To ovšem nemůže říct soupeř, který zřejmě šetřil na cesťáku, nebo samotným hráčům nevyhovoval poobědový začátek po sobotním ponocování. Tak či onak přijeli pouze v deseti. Chvíli jsme si pohrávali s myšlenkou, že bychom mohli vypomoct a síly na hřišti vyrovnat. Hlavní sudí byl proti a upřímně jsme za to byli po prvních deseti minutách rádi, protože hosté vsadili všechno na bojovnost a začátek zápasu jim vycházel lépe. Sice žádnou ze střel nedokázali vtěsnat mezi tři tyče, ale na míči byli častěji a nám se nedařil přechod skoro ani přes půlící čáru. V 11. minutě vypadl hostujícímu brankáři stažený centr Šturce a z hranice vápna míč v poklidu uklidil do prázdné brány Opletal. Od té chvíle se začala karta obracet, nicméně pouze do finální přihrávky, která pořád jaksi kulhala. Zhruba v polovině poločasu se to rozhodl vzít na sebe Opletal a místo hledání spoluhráče prchl obraně a na vzdálenější tyč navýšil skóre. Čistý hattrick mohl dokonat z penalty, kterou ovšem zahrál podobně jako před pár týdny náš kapitán. Vše napravil o dvě minuty později, kdy zakončil kombinaci přesnou nahrávkou na Briola, který z prostoru malého vápna zakončil zkušeně po zemi. Vedení mohli navýšit Bartoš s Pechancem, kteří si vyměňovali míč ve vápně tak dlouho, až z toho byla ušmrdlaná střela, která skončila jen na tyči. Do teď ani jeden z nich neví, kdo na zmařené šanci nese větší vinu.

Do druhého poločasu jsme trochu prostřídali, ale jinak se obraz hry moc nezměnil. I když Loskot musel za druhou půli předvést dva velice dobré zákroky. Před tím však zvýšil na 4:0 Pechanec z přísně nařízené penalty a Bartoš dostal žlutou kartu za rétorickou vložku o vaginálních opisech, která směřovala k hostujícímu Burešovi za nevybíravý skluz na unikajícího Opletala. V ochozech to zahučelo po ráně Briola do břevna. Čtvrt hodiny před koncem si Bartoš krásně narazil s Jánou a přesnou střelou na přední tyč uzavřel gólový účet utkání. Hosté i přes oslabení bojovali celý zápas, nicméně našeho brankáře nejvíc prohnal letící roj vos kolem jeho lebky.

Za týden jedeme do Chrasti, kde se hraje v neděli už v 10:00 dopoledne. Doufejme, že nebudeme mít obdobný problém, jako měli dnes hosté :-)
