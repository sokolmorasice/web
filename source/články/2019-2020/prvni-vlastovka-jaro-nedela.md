---
title: První vlaštovka jaro nedělá
date: 2020-02-17T05:07:11.866Z
category: muzi
image: /public/images/uploads/logo.png
h1: 'Morašice vs. Čistá   7:1 (2:1)'
sestava: >-
  J. Špinar - M. Ropek, J. Kopecký, D. Štancl, V. Štancl, M. Chmelík, M.
  Klement, D. Ondráček, B. Válek, Z. Skala, V. Štěpán, D. Vejrych, P. Vomáčka,
  K. Vopařil
goly: >-
  B. Válek (Z. Skala), J. Kopecký, V. Štancl (Z. Skala), V. Štěpán (D.
  Ondráček), P. Vomáčka (B. Válek), M. Chmelík (D. Ondráček), D. Ondráček (P.
  Vomáčka)
---
Ono dohromady není o čem vyprávět. V kabině jsme před prvním přátelákem s Čistou dostali tři úkoly: 1)převést do praxe věci z tréninku, tj. běhání, 2) hrát po zemi a do nohy, 3) zapamatovat si, kam se v sestavě přesouváme po půlce půlky. Byť se o inteligenci fotbalistů kolikrát oprávněně pochybuje, ukázali jsme, že patříme k výjimkám a přesun proběhl bez problémů. S tím zbytkem už to bylo horší. Protože přihrávky a pohyb nestály ani za zlámanou grešli, náš první gól padl stejně jako ten zatím poslední, Válek si naběhl na Skalův aut a z první mu to sedlo docela dost hezky. Šeď obyčejného přáteláku mu trochu kazila jubilejní 100. gól v áčku, ale určitě se najde příležitost označit za stovkový nějaký jiný - hezčí a důležitější. Čistá vyrovnala po rohovém kopu, když míč ze zadní tyče vrátil do ohně nejmenší muž na trávníku Nepraš a Kostlán zkazil Špinarovi celou neděli, protože ten po prvním, a dobrém, zákroku ze začátku zápasu už už doufal v nulu. Po prvních škatulatech se na hrotu zjevil mj. Kopecký, který si ve vápně našel nepříliš odvrácený centr Vopařila, klidným obstřelem na zadní tyč zaznamenal svůj gól č. 170 a vrátil nám vedení, které vydrželo až do poločasu. 

Hned první akce druhé půlky znamenala roh a Skalovu pobídku uklidil hlavou naprosto nerušen a takřka ze stoje V. Štancl. Špinar si kromě časté rozehrávky stihl i zachytat při ojedinělých šancích soupeře, který v posledních dvaceti minutách jaksi rezignoval a po našem zastřelování se spustila lavina, kterou si je těžké zapamatovat ve správném pořadí a obsazení. Ale zhruba takhle: Ondráček dostal míč do lajny a jeho centr na zadní tyč hlavou proměnil v áčkový premiérový gól Štěpán (po skončení kariéry bude dotyčná vypraná čepice dána k dispozici našich archivářům a vystavena v dosud neexistujícím muzeu). Podivnou strkanicí středem hřiště se za nerušeného přihlížení soupeřů dostal míč na vápně až k Vomáčkovi. Ten se v našem dresu objevil po více než čtyřech letech, posílen zkušenostmi exligových spoluhráčů procestoval celou sestavu, každý post obohatil cennou radou a na hrotu, kde ho známe nejvíce, se dočkal gólu už ze své druhé šance. Chmelík si pak po Ondráčkově autu tak nevěděl rady s nastalou volností, že do míče na nějakých 25 metrech jen tak kopnul, ten si ještě skočil o zem a o tyč zapadl do brány. V úplném závěru pak po akci z levé strany (možná Štěpán, možná Vomáčka - dotyčný nechť se přihlásí v redakci) zívala na Ondráčka prázdná brána, a tak jsme po necelých třech letech konečně vyhráli vyšším než pětigólovým rozdílem, to to trvalo.
