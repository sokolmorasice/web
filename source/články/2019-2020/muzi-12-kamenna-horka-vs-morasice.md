---
title: 'Z peřiny na beton, z 12 na 0'
date: 2019-10-27T00:00:00.000Z
category: muzi
image: /public/images/zapasy/kamenna-horka-morasice.png
h1: Kamenná Horka vs Morašice
sestava: >-
  J. Špinar - D. Štancl, M. Chmelík, V. Štancl, V. Tměj - B. Válek, M. Klement,
  T. Nádvorník, V. Novák (J. Vít, K. Vopařil) - Z. Skala (M. Hubáček), V. Štěpán
  (D. Ondráček)
zlute: 'M. Klement, D. Ondráček, T. Nádvorník'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533A1A/2019/Morašice/e887e6e6-1511-4fc2-bd4f-c95c6799bcad.html
---
Hřiště v Kamenné Horce se od naší poslední návštěvy, na kterou se opravdu snadno a hezky vzpomíná, opět změnilo, tentokrát rapidně k horšímu, do tvrda a hrbolata. Po dobrém, ale impotentním výkonu v Bystrém jsme vyměnili útok, Víta navzdory čerstvému doklad o správné životosprávě nahradil Skala, místo zataženého Chmelíka se vrátil Štěpán, jen na lavičce a navíc jen na půlku zůstal Ropek. Hned první domácí standartka našla neobsazeného domácího beka, který z bezprostřední blízkosti naštěstí hlavičkoval přímo na Špinara, což nijak nesnižuje jeho výborný reflex, který nás uchránil od brzkého prohrávání. Domácí ze standartek i rohů hrozili při každé příležitosti, kromě nich mohli myslet na gól leda po nepříliš prudké střele ze strany, která ale na nevyzpytatelném povrchu našeho gólmana pořádně protáhla. My jsme měli opravdu pomalý rozjezd, na špatném terénu a při špatném pohybu se nám nedařila kombinace, první centr do vápna přišel až kolem 10. minuty, ale po čtvrt hodině Štěpán vysunul Nádvorníka, který mohl ze samostatného nájezdu domácí dokonale zmrazit, kdyby jeho střelu z první nechytil domácí gólman. O chvíli později Štěpán na brankové čáře natáhl krok i sval, což mu na schopnosti kombinovat rozhodně nepřidalo. Kromě několika nedotažených nadějných pozic, získaných spíš bojem a štěstím než nějakou kvalitou, tak už stojí za zmínku leda dalekonosná střela Tměje, která se bohužel stáčela opačně než bychom potřebovali. 

V druhé půli jsme se zase chvíli rozkoukávali, ale domácí nás vystrašili jen překvapivě padající standartkou, kterou pod břevnem zastavil Špinar a dlouhým autem prodlouženým naštěstí do země nikoho fialového na zadní tyči. Když po jednom z centrů spadl míč na Chmelíkovu ruku, domácím se vařila krev, zatímco v nás by se jí nedořezal. Ke konci utkání to zas vypadalo, že jsme měli víc sil my, ale v naší největší šanci byla Chmelíkova dorážka propadlého míče zblokovaná. Střídající Vít za hlasité podpory osádky jednoho auta našich / jeho příznivců protáhl domácího gólmana stříleným centrem z úhlu, ale to je tak vše, co se dá říct o naší ofenzivě. Vcelku zaslouženě jsme tedy potřetí za sebou neskórovali, aspoň, že dopadla ta šestá nula vzadu. Těch pár diváků těšících se na gólové hody z loňska tedy pěkně ostrouhalo, dobře jim tak, trávit poslední pěknou neděli v roce sledováním evidetně vyhořelých okreskářů.
