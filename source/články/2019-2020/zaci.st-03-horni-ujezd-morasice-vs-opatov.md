---
title: Zase zrána střílel Jána
date: 2019-09-15T00:00:00.000Z
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Horní Újezd/Morašice vs Opatov
sestava: >-
  Lukáš Němec, Matěj Kopecký, Zuzana Famfulíková, Jiří Jána, Marek Vích, Tomáš
  Briol, Jan Vedral, Jakub Kvapil, Šimon Kušnír, Šimon Lněnička, Jan Odehnal,
  Vojtěch Famfulík, Filip Kopecký, Tereza Lněničková
goly: 'Jiří Jána 6, Tomáš Briol 2, Jakub Kvapil, Šimon Lněnička, Jan Odehnal, vlastní'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533E2A/2019/Horní
  Újezd/Morašice/bc566d40-6c66-4ad6-afc0-a50dd0a5a717.html
---
Prosluněné sobotní ráno bylo o to příjemnější, když kabina čítala 14 kůsu, včetně jednoho brankáře. Byla zde cítít lehká nervozita s jakým kádrem přijede soupeř, protože v posledním společném klání jsme byli my ti, co se mohli radovat z toho, že jsme v Opatově urvali aspoň bod. Co nás zdobí tuto sezónu jsou rychlé branky ze začátku utkání a ani v tomto zápase tomu nebylo jinak - ve 2. a 5. minutě se trefil Jána. Postupem času jsme si vypracovávali čím dál víc šancí. Další gólová přišla v 11. minutě, kdy Kopecký z přímého kopu rozezvonil břevno a osamocený Briol dorazil do odkryté branky. Asi nikoho nepřekvapí, že o další dvě branky v síti Opatova se postaral Jána. Poté se dokázal prosadit debutant Kvapil, ihned po něm Vedral trefil hostujícího obránce a od něj balón putoval za záda brankáře. O poslední změnu před poločasem se postaral Briol,  který je ze střední vzdálenosti prakticky neomylný. Do kabin jsme šli za stavu 8:0. Jediné ohrožení naší branky byly občas zbrklé a nepřesné přihrávky od naší obrany. 

Druhý poločas byl v našem podání daleko více uspěchaný a zbrklý, přesto Jána přidal další dvě branky. To znamená, že jeho gólový účet čítá 14 branek. Ne, že by se soupeř dostal více do hry, to spíše my jsme zahodili několik dalších gólovek. Celý druhý poločas hostující brankář Bureš předváděl naprosto ukázkové zákroky, a proto se už našli jen dva další přemožitelé jeho svatyně a to v 45. minutě Odehnal a v 50. Lněnička. Doufáme, že  vítěžnou vlnu prodloužíme i v Březové.
