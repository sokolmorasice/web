---
title: Podzim patří nám
date: 2019-11-03T01:00:00.000Z
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Horní Újezd/Morašice vs M. Trnávka
sestava: >-
  Lukáš Němec – Zuzana Famfulíková, Jiří Jána, Jakub Kvapil, Tomáš Briol, Anežka
  Břeňová, Jan Vedral, Šimon Kušnír, Šimon Lněnička, Tomáš Huryta, Jan Odehnal,
  Tereza Lněničková, Filip Kopecký, Vojtěch Famfulík
goly: Tomáš Briol 2 (1x pen.)
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533E2A/2019/Horní
  Újezd/Morašice/6ae401e5-90b6-4373-9676-b4086cd97691.html
---
Dlouho očekávané utkání dvou stoprocentních týmu se projevovalo hned několika způsoby, zejména velkou nervozitou v kabině a také zvýšenou návštěvností diváků. Ze začátku utkání nám soupěř jasně ukázal svou sílu v ofenzívě a brankář Němec se musel hodně otáčet, aby udržel bezbrankový stav. Na naší straně byla znát veliká nervozita a většinu situací jsme řešili odkopem na soupeřovu polovinu. Soupeř často zkoušel přihrávky za obranu, které často nácházel Kurinec. Nutno říct, že několikrát je našel při ofsajdovém postavení, které ne vždy bylo signalizováno pomezním. Nervozita z nás opadla a ukazovali jsme, že s Trnávkou dokážeme hrát vyrovnaný zápas, nejprve jsme hrozili ze střední vzdálenosti, ale s žádnou střelou neměl Palášek problém. První výraznou šanci měl Odehnal, když se řítil sám na brankáře, který včasným výběhem šanci zlikvidoval.

Po poločasové hecovačce v kabině jsme to byli my, kteří hráli více s míčem na kopačkách a z toho plynuly i šance. Do největší se dostal Huryta, ale opět jeho úmysl přečetl Palášek a včasným výběhem znemožnil vstřelení branky. Po sérii rohových kopů z levé strany si na balon naskočil nikým nehlídaný Briol a my se mohli radovat z vedoucí branky. Soupeř se snažil o rychlé vyrovnání , což ale nedopustila naše skvěle fungující defenzíva. V 52. minutě po faulu na Kušníra ve vápně rozhodčí nařídil penaltu, proti jeho rozhodnutí silně protestoval trenérský štáb hostí, což vyústilo k vyhození trenéra z lavičky. Na desítku se postavil Briol a s ledovým klidem penaltu proměnil. Soupěř již neměl dost sil na vytváření vyložených šancí, zatímco my jsme byli stále při chuti. Za zmínku stojí šance Lněničkové, která se po centru z právé strany dostala nikým nehlídaná před branku, přestlo jsme se třetí branky nedočkali.

Výhra v tomto utkání zajistila přezimování na prvním místě tabulky bez ztráty jediného bodu. Budeme se snažit, abychom stejně spokojení mohli být i po jarní části.
