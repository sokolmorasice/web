---
title: Tipy pro "volný" čas
date: 2020-03-24T05:38:33.281Z
category: obecne
image: /public/images/uploads/logo.png
---
Pro ty, kterým už hrabe z domácího vězení, mají hotové všechny úkoly, přetříděné fotky, změněná hesla, učesanou zahradu, sjeté seriály, přečtené knihy, uspané děti, ušité roušky a hotové nákupy pro všechny blízké a neví, kde dál pomáhat: Jděte [darovat krev](http://litomysl.nempk.cz/darci-krve), každé úterý a pátek.

Nebo si kupte míč, samozřejmě přes eshop, udělejte si v obýváku trochu místa a můžete cvičit jako [tenhle](https://www.youtube.com/watch?v=IeMYh7roBjk) chlápek nebo jako [tenhle](https://www.youtube.com/watch?v=PuF_gM2S7rk) chlápek. Kdo má míň místa, měl by se vejít [sem](https://www.youtube.com/watch?v=XWeypwK6mbk) nebo [sem](https://www.youtube.com/watch?v=8ce48HgB1Eo).