---
title: "1. přikázání: V deseti lidech trénovat budeš"
date: 2020-05-04T07:10:45.505Z
category: obecne
image: /public/images/uploads/logo.png
---
Předseda vlády se dovolává u Boha síly ducha a sleduje velikonoční mše, a moloch jménem FAČR, sestavuje desatero, viz. níže. Zdá se, že česká víra se vrací od zplošťování křivky zpět ke kořenům.

*Vážení,*

*toto desatero nemá charakter dodatečného nebo upřesňujícího výkladu, je tvořeno východisky z dostupných zdrojů.*

*Jsme si vědomi, že tento výklad neodpoví na všechny Vaše otázky ihned, bohužel ani my v této chvíli neznáme všechny odpovědi. Cítíme však potřebu naše členy informovat alespoň v těch oblastech, ve kterých jsme si jisti. Pravidla budeme v dalších týdnech dle okolností nadále aktualizovat.*

*V souvislosti s desaterem je nutné vždy vycházet z daných okolností a možností klubu. Pravidla se týkají výhradně venkovního fotbalového hřiště.*

1. *Trénovat lze ve skupině 10 osob (tento počet se týká i dětí a mládeže); v současné chvíli řešíme, zda a po jaké době se mohou osoby ve skupinách měnit; aktuálně platí, že se osoby ve skupině nemění*
2. *Do skupiny 10 osob se počítají i trenéři; v současné chvíli řešíme, zda a po jaké době se mohou trenéři ve skupinách měnit; aktuálně platí, že se trenéři ve skupině nemění*
3. *Skupina 10 osob nemusí při tréninku používat roušku*
4. *Trénovat ve skupině 10 osob lze bez distanční vzdálenosti = bez udržování vzdálenosti 2 metrů*
5. *Neevidujeme žádné omezení pro kontakt hráčů ve skupině 10 osob*
6. *Na jedné hrací ploše může být více skupin po 10 osobách, společně tyto skupiny do kontaktu přijít nesmí. Skupiny musí dodržovat odstup vždy alespoň 2 metry*
7. *Neevidujeme žádná omezení používání pomůcek – pomůcek je možné dotýkat se hlavou i končetinami*
8. *Dezinfikovat pomůcky je nutné pravidelně, dezinfikovat pomůcky by měl vždy klub tak, aby byl o dezinfekci přehled; na bližší informace prozatím čekáme*
9. *Není povoleno používat související vnitřní prostory sportoviště, zejména společné šatny, umývárny, sprchy a podobná zařízení; v případě sportovní činnosti lze na venkovních sportovištích zpřístupnit WC, nicméně je nezbytné zajistit*

* *zvýšená hygienická opatření, tj. v těchto případech je však třeba zabezpečit provádění zvýšených hygienických opatření zejména dezinfekce rukou, ale také míst, kterých se běžně dotýkají ruce*
* *zamezit současné přítomnosti vícero osob ve stejný čas v prostorách WC a dodržet povinnost užití ochranných prostředků dýchacích cest*

10. *Přípravná přátelská utkání je možné aktuálně plánovat od 11. května, k bližší specifikaci*přistoupíme později

Jan Pauly

generální sekretář FAČR