---
title: Podzimní ohlédnutí
date: 2019-10-29T06:43:49.882Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Mladší přípravka

Mladší přípravka odehrála na 7 turnajích 18 zápasů proti 6 různým soupeřům se stoprocentní bilancí, všechny turnaje ovládla, a to při souhrnném skóre 179:16. Jediné prohry zaznamenal tým složený z nejmladších z mladších, čistě z označovacích důvodů mu říkejme tým „B“, který při nerovných soubojích s „áčkovými“ výběry obstál a na čistě „béčkovém“ turnaji v Bystrém vyhrál oba zápasy se skóre 21:1. Nejlepším střelcem byl Daniel Válek s 64 góly, což je krásné číslo, ale v součtu za celý kalendářní rok si oproti loňsku pohoršil ze 112 na 104 góly😊

**Kompletní tabulka střelců:**

Válek Daniel (MOR)		        64

Huryta Vojtěch (MOR)		32

Flídr František (HÚ)		        28

Flídr Šimon (HÚ)		        23

Chaun Jaroslav (HÚ)		22

Famfulíková Martina (MOR)	17

Kovář Matěj (MOR)		         7

Kovář Michal (MOR)		 6

Vomočil Samuel (HÚ)		 6

Boček David (HÚ)		         5

Klusoň David (HÚ)		         3

Nováček Jan (MOR)		 3

Lněnička Matyáš (MOR)	         1

Vomočilová Sára (HÚ)		 1

## Starší přípravka

Starší přípravka také odehrála 7 turnajů, a to s bilancí 3x 1. místo, 3x 2. místo a 1x 3. místo. Z 19 soutěžních zápasů vyhrála 12, 4x remizovala 3x odešla poražená, při celkovém skóre 57:32. Nejlepším střelcem byl Filip Flídr s 15 góly. 

**Kompletní tabulka střelců:**

Flídr Filip (HÚ)		        15

Kvapil Jan (HÚ)		14

Kusý Ondřej (HÚ)	          7

Mach Lukáš (MOR)	          7

Bulva Matyáš (HÚ)	          5

Zach Matouš (HÚ)	          3

Famfulík Vít (MOR)	          2

Tomšíček Kryštof (HÚ)	  2

Rosypal Josef (HÚ)	          1

Výsledky jsou pěkné a samozřejmě těší děti i trenéry, ale radostnější je velký počet dětí, které baví trávit volný čas aktivně a v kolektivu svých vrstevníků. Obrovský dík za to patří Jiřímu Famfulíkovi a jeho trenérským kolegům z Horního Újezdu, mj. i Zdeňku Benešovi ml. a Františku Bartošovi, kteří si najdou čas i na [zprávy](/výsledky/pripravka) z turnajů. Jsme vděční všem rodičům, kteří pomáhají s dopravou dětí, praním dresů nebo trénováním, pokud by se někdo z nich chtěl zapojit více, budeme jenom rádi, protože by byla škoda, kdyby nám děti skončily jen kvůli tomu, že se jim nemá kdo věnovat. Poděkování patří i celému oddílu z Horního Újezdu za poskytnuté zázemí a převzetí pořadatelských starostí při našem improvizovaném podzimu.
