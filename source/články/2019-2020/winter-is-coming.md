---
title: Zima se blíží
date: 2019-10-20T07:47:28.491Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Mladší přípravka

Poslední podzimní kolo okresní soutěže mladší přípravky se uskutečnilo v Němčicích. Turnaj byl nalosován pro tři účastníky, proto jsme využili možnosti a vzali opět dva týmy, i když se nakonec ukázalo, že tentokrát byl počet našich hráčů hraniční a starší tým musel odehrát turnaj bez střídání. Na druhou stranu si nikdo nemohl stěžovat, že si dostatečně nezahrál. 🙂

**Sestava:** Martina Famfulíková, Vojtěch Huryta, Šimon Flídr, Daniel Válek, Jaroslav Chaun, Matěj Kovář, Sára Vomočilová, David Boček, Jan Nováček, Samuel Vomočil, Matyáš Lněnička

**Naše výsledky:**

**Horní Újezd/Morašice A – Borová 9:2 (4:1)**

branky: Daniel Válek 4, Martina Famfulíková 3, Sára Vomočilová, Vojtěch Huryta

**Němčice - Horní Újezd/Morašice B 6:0 (2:0)**

**Horní Újezd/Morašice A – Němčice  20:0 (11:0)**

branky: Daniel Válek 9, Vojtěch Huryta 5, Šimon Flídr 3, Jan Nováček 2, Martina Famfulíková

**Horní Újezd/Morašice B - Borová 0:7 (0:3)**

**Horní Újezd/Morašice červení - Horní Újezd/Morašice žlutí 7:2 (6:0)**

branky: Daniel Válek 2, Šimon Flídr 2, Martina Famfulíková, Jan Nováček, Jaroslav Chaun - David Boček, Vojtěch Huryta

**Tabulka turnaje:**

1.Horní Újezd/Morašice A

2.Borová

3.Němčice

4.Horní Újezd/Morašice B

Je za námi historicky nejúspěšnější podzim v kategorii přípravek, podařilo se nám vyhrát všech sedm turnajů soutěže. Poděkování za to patří všem hráčům, rodičům, příznivcům a trenérům. V pondělí 21.10. se uskuteční na Horním Újezdě ukončení podzimní části, sejdeme se o půl páté, zahrajeme si fotbal, řekneme si, co bude dál, a dáme si párek a limonádu.

## Starší přípravka

Trochu s obavami jsme vzhlíželi k tomuto turnaji, protože domácí prostředí nás vždy trochu svazuje, což se potom podepíše na konečném výkonu a výsledku. Tentokráte však byly naše obavy zbytečné.

**Sestava:** Filip Flídr, Ondřej Kusý, Martina Famfulíková, Jan Kvapil, Štěpán Řejha, Matouš Zach, Vojtěch Andrle, Vít Famfulík, Jan Kopecký, Lukáš Mach, Josef Rosypal, Matyáš Bulva, Václav Háp, Kryštof Tomšíček

**Naše výsledky:** 

**HÚ/ Morašice – Březová 4 : 2**

Zpočátku to byl relativně vyrovnaný zápas se šancemi na obou stranách. Nám se však podařilo vstřelit důležitý první gól a naše vedení jsme postupně navyšovali až na 4 : 0, v závěru jsme však polevili, udělali pár chyb a soupeř tak snížil, kluci se však zkoncentrovali a zápas dovedli do vítězného konce. 

branky: Ondřej Kusý 2, Filip Flídr, Lukáš Mach

**HÚ / Morašice – Městečko Trnávka 6 : 0**

V tomto zápase jsme od začátku udávali tempo hry a postupně jsme začali i proměňovat šance, které jsme si vypracovali, takže o výsledku zápasu bylo rozhodnuto. K vidění byly už i nahrávky, nabíhání do volného prostoru a pěkné branky 

branky: Jan Kvapil 3, Ondřej Kusý, Vít Famfulík, Matyáš Bulva

**HÚ / Morašice – Němčice 3 : 1**

Již před začátkem zápasu bylo jasné, že vítěz vyhraje tento poslední podzimní turnaj a kluci se chtěli před domácím publikem vytáhnout, což, si myslím, se jim povedlo. V utkání jsme šli do vedení, ale naše radost netrvala dlouho a soupeř vyrovnal. Potom se hra přelévala z jedné strany na druhou a bylo to o tom komu se povede dát další gól. Šťastnější nakonec byli naši kluci, když si roh zahrávaný Lukášem Machem srazil soupeř do vlastní sítě. Zápas byl i nadále vyrovnaný, s patřičným nasazením a bojovností na obou stranách. O to, abychom zápas dohrávali trochu v klidu se v závěru postaral chytrou střelou Filip Flídr, který pojistil naše vítězství a nás posunul na 1. místo v turnaji. 

branky: Jan Kvapil, vlastní, Filip Flídr

Tímto byla ukončena podzimní část soutěže, za kterou patří velké poděkování klukům. Docházka na tréninky i zápasy byla výborná, stejně tak atmosféra na hřišti i v kabině. Tým se postupně sehrával, což patří vždy do tohoto období. Odehráli jsme 7 turnajů s bilancí 3x 1. místo, 3x 2. místo a 1x 3. místo, 19 soutěžních zápasů, z toho 12 vítězství, 4 remízy a 3x jsme odešli poraženi. Poděkování patří současně rodičům za dovoz hráčů, praní dresů, podporu a pečení bábovek, které se nakonec ukázaly jako rozhodující faktor při vítězných zápasech. Poděkovat chci také vedení TJ Horní Újezd za perfektní podmínky při tréninku i zápasech a zapomenout nemohu na trenéry Václava, Jirku a Honzu.
