---
title: Když nás přejde HÚ/MOR
date: 2019-09-29T00:00:00.000Z
category: muzi
image: /public/images/zapasy/horni-ujezd-morasice.png
h1: Horní Újezd vs Morašice
sestava: >-
  J. Špinar – D. Štancl, M. Ropek, V. Štancl, V. Tměj – B. Válek (D. Ondráček),
  M. Klement, V. Štěpán (J. Kopecký), V. Novák – M. Chmelík, Z. Skala (K.
  Vopařil)
goly: 'B. Válek (pen.), M. Klement'
zlute: Z. Skala
multimedia: 'https://drive.google.com/folderview?id=15FtwozdAM0LuO9r1KnjBgMCHbvSUHRoz'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533A1A/2019/Morašice/e439d0c9-7970-4f6e-9cf6-4ba36236dfb3.html
---
Ještě o půl třetí spojené dorostenecké družstvo Horňáku a Morašic s přehledem válcovalo Luži, o hodinu později už se A mužstva Horňáku a Morašic postavila proti sobě. Absenci zraněného Nádvorníka odnesl stažením do středu Štěpán, chudák Chmelík zase po návratu V. Štancla musel do útoku. V opatrném začátku si oba týmy připsali po nepřesné střele z dálky, za nás ji měl na svědomí Válek, před kterým se po kombinaci od autové čáry otevřel na středu zajímavý prostor. Přestože jsme po lajnách zlobili víc, z jedné povedené kombinační akce katapultující Válka si dokonce Štěpán málem cvrknul, nepřesné zpětné přihrávky (když vůbec bylo koho hledat) od brankové čáry toto úsilí spolehlivě hatily. Z rezultujících rohů se nám podařilo jen jednou zakončit Ropkem, a to taky dost nepřesně. Jedinkrát za celý zápas se D. Štanclovi vymklo z rukou honění „Klacka“ a po jeho průniku po lajně a zpětné přihrávce naštěstí Lněnička z vápna vysoko přestřelil. Kousek chyběl k zakončení hlavou kapitánům na obou stranách, naši první opravdu velkou šanci nachystal po půl hodině Skalův pas na Chmelíka, ale ten svým lobem minul bránu. Na druhé straně jsme bez úhony přežili v okrese málo vídané nůžky v podání Kladivy. Těsně před přestávkou se po rohovém kopu odrazil balón do ruky domácího hráče a nařízenou penaltu proměnil v ideálního „šatnáře“ Válek. Hned po rozehrávce jsme ale zaspali a nechali domácí rozjet akci, rozhodčí asi jako kompenzaci za penaltu odpustil Motyčkovi faul na V. Štancla, zpětná přihrávka našla Kusého, jehož zakončení z malého vápna Špinar reflexivně vyrazil tolik zkoušeným kolenem a ušetřil nás ještě ideálnějšího „šatnáře“.

Brzy po přestávce se do šance řítil Chmelík, po další nepřesné zpětné nahrávce dostal po pár odrazech šanci na reparát, našel centrem na zadní tyči Válka, ale ten zblízka vysoko přestřelil. Domácí Motyčka pak v rychlém sledu hlavičkoval do středu brány, žlutě zboural Ropka a vystřídal. Při následné pasáži skoupé na šance zaujalo nejvíc Skalovo žlutě protažené střídání. Když si domácí bek šlápl na balón, Chmelíka v sólovém nájezdu efektně vychytal Havlík, průniky Štěpána z levé a střídajícího Ondráčka z pravé strany vyzněly do ztracena (centr za čáru, neodpískaná penalta, špatná přihrávka). Střídající Vopařil poboural, co mohl a žluté kartě po Tettehovském faulu na Kusého unikl ani neví jak. Po neúspěšných pokusech si Štěpán pět minut před koncem narazil roh nakrátko a následným centrem našel neobsazeného Klementa, který hlavou zatloukl pod břevno jak svůj 50. gól mezi muži, tak definitivní hřebík do domácí rakve. Kompaktní, defenzivně spolehlivý a, pokud nám jen někdo nemaže med kolem huby, prý i koukatelný výkon nás tak spolu s bysterským zaváháním posunul ve vyrovnané tabulce bodově na první místo, fakticky nás od něj dělí jen deset vstřelených gólů.
