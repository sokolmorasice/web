---
title: Na všechny se dostalo
date: 2019-10-13T13:30:12.905Z
category: pripravka
image: /public/images/uploads/logo.png
---
## Mladší přípravka

Další pokračování okresní soutěže mladší přípravky se uskutečnilo v Bystrém. Turnaj byl nalosován o třech družstvech, my, Čistá i domácí mají dost malých fotbalistů, a proto se uskutečnily turnaje dva, jeden pro zkušenější (turnaj A) a jeden pro začínající hráče (turnaj B). Velké poděkování patří domácím za organizaci, nevyzkoušený formát se osvědčil a doufám, že se třeba na jaře tímto způsobem ještě utkáme.

**Turnaj A**

**Sestava:** Martina Famfulíková, Vojtěch Huryta, Šimon Flídr, František Flídr, Daniel Válek, Sára Vomočilová, David Klusoň

**Naše výsledky:**

**Bystré – Horní Újezd/Morašice 1:10**

branky:  František Flídr 4, Daniel Válek 3, Martina Famfulíková, Vojtěch Huryta, Šimon Flídr

**Horní Újezd/Morašice – Čistá 14:0**

branky: Daniel Válek 9, Vojtěch Huryta 2, Martina Famfulíková 2, František Flídr

**Tabulka turnaje A:**

1.Horní Újezd/Morašice

2.Bystré

3.Čistá

**Turnaj B**

**Sestava:** Jaroslav Chaun, Michal Kovář, Matěj Kovář, David Boček, Samuel Vomočil, Matyáš Lněnička, Jan Nováček

**Naše výsledky:**

**Bystré – Horní Újezd/Morašice 1:9**

branky: Jaroslav Chaun 3, Samuel Vomočil 2, David Boček 2, Matěj Kovář, Michal Kovář

**Horní Újezd/Morašice – Čistá 12:0**

branky: Jaroslav Chaun 5, Samuel Vomočil 3, David Boček 2, Matěj Kovář 2

**Tabulka turnaje B:**

1.Horní Újezd/Morašice

2.Bystré

3.Čistá

## Starší přípravka

**Sestava:** Filip Flídr, Ondřej Kusý, Jan Kvapil, Štěpán Řejha, Matouš Zach, Vojtěch Andrle, Vít Famfulík, Jan Kopecký, Matěj Rejman, Vojtěch Štěpán, Josef Rosypal, Matyáš Bulva

**HÚ/Morašice – Jaroměřice 3 : 2**

Zápas ve kterém měl soupeř výškovou převahu byl vyrovnaný s šancemi na obou stranách, nám se však povedlo odskočit do dvoubrankového vedení a soupeři jsme již vyrovnat nedovolili. Kluky je třeba pochválit za kolektivní výkon, kdy se všichni snažili a utkání dovedli do vítězného konce. 

branky: Matouš Zach, Lukáš Mach, Matyáš Bulva

**HÚ/Morašice – Jevíčko 1 : 1**

Po prvním zápase jsme očekávali, že kluci budou pokračovat v dobrém výkonu, ale úplně pravda to nebyla. Místo toho jsme se přizpůsobili soupeřově stylu hry a po hrubé chybě mu darovali vedoucí branku. Šancí ke vstřelení branky bylo relativně dost, ale proměnili jsme pouze jednu.

Branka: Jan Kvapil

**HÚ/Morašice – Hradec nad Svitavou/Radiměř 1 : 5**

Věděli jsme, že nás čeká těžký soupeř, ale my jsme mu úvod zápasu velmi usnadnili svým přístupem, takže jsme  brzy prohrávali 0 : 2. Potom se kluci zvedli, začali běhat, bojovat a výsledkem bylo snížení na 1 : 2. Po poločase jsme si dokázali vytvořit několik velkých šancí a vyrovnání viselo ve vzduchu, místo toho však přišly další hrubé chyby, které soupeř potrestal a z naší strany bylo po zápase. 

branka: Lukáš Mach

 V turnaji jsme tak nakonec obsadili druhé místo, což není úplně špatný výsledek, ale fotbalový projev by mohl být lepší.
