---
title: Sláva už je sníh
date: 2019-12-18T05:56:50.591Z
category: obecne
image: /public/images/uploads/images.jpg
---
Sokol Morašice přeje všem svým členům a příznivcům, jakož i všem lidem dobré vůle, kterým jsme ukradení, aby prožili v kruhu [nejbližších](https://www.youtube.com/watch?v=EjEVIH46WnU) klidné a příjemné svátky , při nichž se [dává a přijímá](https://www.youtube.com/watch?v=nYaKhwR5orM) a dostává a sdílí, a radostný vstup do nového roku, v němž můžeme všichni nejen [dostávat](https://www.youtube.com/watch?v=RXZM-bD4yxM) a [přijímat](https://www.youtube.com/watch?v=wdsRXGTUY14), ale i [dávat](https://www.youtube.com/watch?v=-gDaxjP2mkY) a [sdílet](https://www.youtube.com/watch?v=s_YBSWipk9Y).
