---
title: Průzkum veřejného mínění
date: 2020-02-04T09:03:48.126Z
category: obecne
image: /public/images/uploads/logo.png
---
Přes snahu o zkvalitnění našich služeb není monitorován Váš seznam tužeb. Proto prosím vyplňte tento [dotazník](https://docs.google.com/forms/d/e/1FAIpQLSct1xYNcCmvcv-bfVKydqAyaWs84ixbPQsWgQQWKBaNpTIZdA/viewform?usp=sf_link), abychom věděli, kde přidat, kde ubrat, co vylepšit, koho volit, koho vykázat a tak dále a tak dále.
