---
title: Jak jsem se stal divákem
date: 2020-07-15T05:56:53.828Z
category: muzi
image: /public/images/uploads/logo.png
---
Býval jsem hráčem Premier League. Dostával jsem týdenní plat 100 000 liber a trénoval třikrát denně. Jednou mi řekli, abych před zápasem poklekl, protože na černém životě záleží. Řekl jsem, že u nás se zaklekává i na bílé a nikomu to nevadí. Od té doby jsem hrál Fortuna:Ligu. Dostával jsem měsíční plat 100 000 Kč a trénoval dvakrát denně. Jednou se mě novináři zeptali, jestli je pravda, že jsem se na Strahově sešel se Starkou. Zeptal jsem se jich, kterou strahovskou straku myslí a od té doby jsem hrál krajský přebor. Dostával jsem 1000Kč za zápas a trénoval třikrát týdně po práci. Jak jsme se tak plácali na desátém místě, přišli za mnou, jestli nechci hrát v přeboru o přední příčky. Řekl jsem, že ano a od té doby jsem hrál přebor okresní. Platil jsem příspěvky 700Kč a z tréninků se stačilo včas omluvit. Jednou se mě trenér zeptal, proč jsem se neomluvil z posledního tréninku. Řekl jsem mu, že kdybych věděl, že je opravdu poslední, určitě bych došel aspoň na pivo.

A tak jsem se stal divákem.

Originálem se můžete pokochat [zde](https://www.youtube.com/watch?v=i3eAjI_FLLI), svou jedinou povinnost ohledně docházky splnit [zde](https://docs.google.com/spreadsheets/d/19JiZ2z0OcaOwEy1E_U_44N_X9VA9zgg_Qm_OB8J1dgU/edit).