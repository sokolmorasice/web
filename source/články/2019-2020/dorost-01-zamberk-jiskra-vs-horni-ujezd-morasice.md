---
title: Králíky se počítají až po honu
date: 2019-08-26T10:17:07.632Z
category: dorost
image: /public/images/novinky/2019-2020/dorost.png
h1: 'Žamberk/ Jiskra 2008 - Horní Újezd/Morašice 4:1 (1:0)'
sestava: >-
  Emanuel Hanyk - Luděk Bureš (Jaromír Loskot), Pavel Beneš, Petr Kazda,
  František Lněnička (Martin Tmej) - Tomáš Briol, Vojtěch Opletal, Radovan
  Flídr, Jiří Pechanec (Jan Kazda) - František Bartoš, Jan Pechanec
goly: František Bartoš
zlute: 'Emanuel Hanyk, František Bartoš, Luděk Bureš'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533C2B/2019/Horní
  Újezd/Morašice/c1f0a59c-40e2-4f57-b077-4d258907e027.html
---
K prvnímu mistráku jsme se rozjeli až do dalekých Králík, kde nás čekal první ostrý start. Soupiska doznala pár změn, ale začátek zápasu nijak nenasvědčoval  tomu, že by se mělo jet domů s brekem. Už po pár minutách se do samostatného úniku dostal Vojta Opletal, ale jeho střelu zastavila tyč. Bohužel to bylo za první poločas z naší strany vše. Né že by toho soupeř ukázal víc, ale stačilo to. Po celý zbytek prvního poločasu nám absolutně chyběl pohyb, nabídka pro přihrávku, obsazování soupeře a z fotbalu se stala spíš taková holomajzna, kde získal míč ten, co měl větší sílu v noze. Přesto se soupeř dostal 5 minut před pauzou do vedení. Pavel Beneš na stoperu v tísni přišel o míč a brejk dva na jednoho se podařilo domácím zúročit. 

První polovina druhého poločasu byla z naší strany možná ještě horší než první poločas. Soupeř nás prakticky nepustil k míči a když, tak jsme ho jen odkopli dopředu a valil se zase další útok. Jen souhra štěstí a výborné zákroky Hanyka zajistily pouhé dvoubrankové manko. Když se po nenápadném autu povedlo Bartošovi krásnou střelou z dálky snížit, hráči zjistili, že po zemi se hraje líp a navíc to i docela dobře funguje. Další střelu Bartoše z 25 metrů domácí brankář jen tak tak vytěsnil na roh (i když dodnes neví jak). Po rohu Honzy Pechance mohl hlavou vyrovnat Flídr, ale chyběla větší razance. Dalším závarům ve vápně a únikům po lajně chyběla přesná finální nahrávka. 
A právě z těchto ztrát jsme v závěru zápasu ještě dvakrát inkasovali, kdy domácí využili otevřené obrany a pojistili si vítězství. Za týden máme doma Trhovou Kamenici s Vítanovem, což bude další z mnoha překvapení. Věříme, že již tříbodové. 

- - -
