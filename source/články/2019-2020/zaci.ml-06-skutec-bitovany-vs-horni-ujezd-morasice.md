---
title: A co děti? Musí v dešti stát.
date: 2019-10-05T00:00:00.000Z
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Skuteč/Bítovany vs Horní Újezd/Morašice
sestava: >-
  Lukáš Němec - Daniel Hanyk, Jakub Kvapil, Jonáš Krška, Anežka Břeňová, Zuzana
  Famfulíková, Jakub Bartoš, Ondřej Žďára, Jan Flídr, Jiří Kučera, Jindřich
  Mičík, Matěj Kopecký
goly: 'Jakub Kvapil, Anežka Břeňová, Ondřej Žďára'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/531F1B/2019/Horní
  Újezd/Morašice/a15c93dc-fe52-4b95-a6c5-3513c43691d1.html
---
V deštivém sobotním dopoledni jsme zavítali do Skutče, kde jsme se chtěli porvat o první místo. Začátek zápasu nám hrubě nevyšel, hned z první akce v první minutě jsme inkasovali a o osm minut později už to bylo o dva góly. Od té chvíle jsme přebrali iniciativu a ještě v první půli zásluhou Kuby Kvapila snížili na 1:2. Výbornou kombinací jsme dostávali domácí pod tlak, ale více branek už v prvním poločase nepadlo.

S velkým odhodláním otočit výsledek v náš prospěch jsme se od začátku druhé půle pustili do soupeře a hned v první minutě dala vyrovnávající gól Anežka. Jak ubíhaly minuty, tak se z trávníku vytrácela fotbalovost na obou stranách. Naštěstí jsme to nahradili bojovností a tak deset minut před koncem vstřelil vítěznou branku Ondra Žďára. Velká pochvala patří všem hráčkám i hráčům za veliké nasazení a chuť něco s výsledkem udělat ve chvíli, kdy jsme prohrávali. 

Nakonec si neodpustím  poznámku směrem k domácím "fanouškům", kteří fandili jenom dokud vedl domácí tým. Poté už následovaly pokřiky a nevkusné poznámky na adresu našich hráčů. Myslím, že aspoň v mládežnickém fotbale by si to mohli odpustit. A to nemluvím o "pořadateli", který vyhodil z trávníku dvě naše maminky, které držely deštníky nad střídajícími dětmi a jejich bundy, aby je neměli na zemi, jelikož lavičku jsme nedostali. Když jsem se ptal proč, tak mi bylo řečeno, že tam ničí trávník, tím jak tam stojí. **Slibuji (i za naše maminky),že příště už to neuděláme :-)**

Příští zápas hrajeme v sobotu 12.10. v Luži.
