---
title: Fotbal v čase korony - díl 1.
date: 2020-04-12T12:55:59.104Z
category: obecne
image: /public/images/uploads/logo.png
---
Jednou z komunit, na kterou dopadlo řádění nemoci COVID-19, je i fotbalový oddíl TJ Sokol Morašice. Jak virus ovlivňuje chod klubu? Co dělají jeho činitelé a hráči, když nemůžou hrát? Jaký vliv bude mít nucená pauza na budoucnost klubu? To se dozvíte v našem reportážním seriálu. Všechny jeho postavy jsou skutečné, nicméně charaktery podléhají autorské licenci a nemusí se nikterak zakládat na pravdě.

V prvním díle jsme osobně navštívili areál, který měl mít na jaře díky podzimnímu venkovnímu losu mimořádně napilno. Právě dokončené kabiny září novotou již od návsi. „Jsme rádi, že jsme je (kabiny – pozn. redakce) stihli dokončit ještě před uzavřením státních hranic. Byť stavba nebyla závislá na pracovnících z východu, byl jich nedostatek jinde a hrozil přesun našich kapacit na jiná pracoviště. Škarohlídi tvrdili, že budou nové kabiny moc malé, podle mě je v nich teď místa až až. Když se moje pravda ukázala, doufám, že výbor schválí i mé další investiční návrhy, stejně nemá nic jiného na práci," řekl nám k největšímu projektu poslední doby stavbyvedoucí a šedá eminence oddílu Petr Kuta.

Bohužel vedle kabin nezáří výsledková tabule, všude je prázdno a zavřeno, na hřišti jezdí pouze traktůrek. Když jeho řidič sesedl a přišel za námi, bylo vidět, že před kamerou není poprvé.

„V pondělí večer nás měli dávat na ČT sport, ale teď tam je finále z roku 2003 jak Pardubky prohrály se Slávkou,“ říká, aniž bychom se ho ptali nebo věděli, o co jde. Na náš dotaz, co se teď děje na hřišti odpovídá lakonicky:

„Nic, ale sekat se musí.“ – „Takže Vás se ta stopka nijak netýká?“ – „Tak s žákama jsme chtěli vyhrát přebor, Jirka Jánu mohl mít rekord v gólech i zápasech za žáky, a o to teď kluci přijdou.“

Kromě něho už jsme u hřiště zastihli jen pejskaře. „Je to vážný. Chodím kolem každý den už deset let a tohle je poprvé, co jsem tu dva dny za sebou neviděl Fůču.“ Naráží tak na trenéra starší přípravky, mladší přípravky a školičky, pravidelného diváka mladších žáků a mužů, člena výboru oddílu i zastupitelstva obce v jedné osobě Jiřího Famfulíka. Ten nám pověděl, jak mu koronavirus změnil život:

"Když vyhlásili zákaz vycházení, tak mě to samozřejmě zastihlo na hřišti. Máme tam internet, všechno, tak jsem si rovnou z kabiny poslal e-žádost o změnu trvalého bydliště ze hřiště zpátky k nám do baráku, kde jsem dřív býval jen jednou týdně. Během tréninku mi do mailu přišla nová e-občanka a já šel s klidným svědomím domů. Jako fakt domů," K organizačním věcem se už nechtěl vyjadřovat s tím, že on je v oddílu od trénování, ale když jsme se zeptali na přípravku, ještě se nad námi slitoval: „Mladší děcka to mohli letos vyhrát, navíc doma, starší měli v květnu hrát na oslavách před spoustou lidí, škoda no.“

Na zmíněné oslavy jsme se chtěli zeptat sekretáře oddílu, ale ten nás odkázal na facebookový profil. Teprve když jsme ho konfrontovali s nulovou aktivitou tohoto sociálního média, přiznal, že se nás chtěl zbavit, že nemá čas na nic jiného než na stavbu. Mnohem sdílnější byl místopředseda Milan Chmelík.

Co nám prozradil? A na koho dalšího jsme v Makově narazili? Dozvíte se po Velikonocích.

<!--EndFragment-->