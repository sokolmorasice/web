---
title: Zaokrouhleno na jedno desetinné místo
date: 2019-10-06T00:00:00.000Z
category: dorost
image: /public/images/novinky/2019-2020/dorost.png
h1: Chrast/Zaječice vs Horní Újezd/Morašice
sestava: >-
  Emanuel Hanyk (Jaromír Loskot) - Martin Tmej (Jiří Svatoš), Pavel Beneš, Jakub
  Nádvorník (František Lněnička), František Bartoš - Radovan Flídr, Tomáš Briol
  (Jan Vedral), Matěj Roško, Vojtěch Opletal - Jan Pechanec (Jiří Jána), Jiří
  Pechanec
goly: 'Jan Pechanec. František Bartoš, Vojtěch Opletal'
zlute: Vojtěch Opletal
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533C2B/2019/Horní
  Újezd/Morašice/199c6cbf-b875-4d91-9272-83a6a2dc42a6.html
---
Další venkovní zájezd nás nasměroval do Chrasti u Chrudimi a po menších logistických komplikacích jsme tam skutečně dorazili. Po sobotním lijáku se nad námi počasí smilovalo, a tak to vypadalo na pěkný začátek neděle. Soupeř takové slitování neměl, a tak se předzápasová promluva o vyrovnanosti v tabulce ukázala jako lichá.

Úvod patřil domácím, kteří se celkem snadno dostali do dvoubrankového vedení. On jim teda nepatřil jenom úvod, i když jsme si 3 branky dali prakticky sami. Tím samozřejmě nechceme snižovat kvalitu soupeře, která byla enormní. Jako že hodně velká. Dle našeho pohledu to byl zatím nejlepší soupeř, proti kterému jsme nastoupili. Navíc se kromě fotbalovosti prezentoval i férovým přístupem (až na světlou výjimku) a dokázal, že ani dospívající jedinci z města nemusí mít místo mozku porci míchaných vajec. K deseti inkasovaným brankám zůstalo dost příležitostí nevyužitých, ale nenudili se ani oba naši brankáři, kteří si na prázdné rukavice rozhodně nemohli stěžovat.

A teď k našim. Rozdíl mezi tímto zápasem a zápasem ve Sloupnici byl na první pohled zřejmý. Sice ne výsledkem, ale bojovností a nasazením určitě. Zvláště ve druhém poločase jsme zpřesnili přihrávku, dokázali podržet balón a měli několik šancí, které měly skončit gólem, aby konečný výsledek nebyl tak propastný. Já myslím, že si musíme přiznat, že ten výsledek není takový, jak jsme si slibovali, ale já bych to neviděl tak černě. Ten celkový obraz, jak nám soupeř nastínil, není samozřejmě nijak růžový. Ovšem, a to nesmíme přehlížet, jsou tu zárodky toho pozitivního. Mám na mysli ty pěkný tři vstřelený branky na hřišti soupeře, nebo že jsme i po zápase zůstali kamarádi, že? To jsou dvě první vlašťovky. A navíc, nebýt těch blbejch deseti gólů, tak jsme s přehledem vyhráli 3:0. 

Závěrem se rovněž sluší říct, že i výkon hlavního rozhodčího byl nadstandardní. Bylo vidět, že ten frajer ví, co chce a nám přišel i sympatickej. Viděl i to, co zachráp AR2 a dokonce se snažil na našeho levého beka výchovně zapůsobit. S jakým výsledkem zjistíme už příští týden proti Srubům.
