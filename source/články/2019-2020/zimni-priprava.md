---
title: Zimní příprava
date: 2019-12-09T11:55:04.572Z
category: muzi
image: /public/images/uploads/logo.png
---
_Út 31.12. 10:00_ - Silvestrovský fotbálek pro všechny zájemce

Tréninky od **14.1.** každé úterý a čtvrtek v **17:30** v tělocvičně v Morašicích, s sebou mírnou [nadváhu](https://www.youtube.com/watch?v=S86joDA8r1E) pro snadnější plnění cílů, tombolu pro Milany a výbavu na posilování vevnitř i běhání venku, tj. oddílové oblečení úměrné počasí a věku, dvoje boty a reflexní prvky,

_Ne 16.2. 10:00_ - přátelák s Čistou na UMT v Litomyšli

_Pá - Ne 21.2.-23.2._ – [Soustředění](https://www.youtube.com/watch?v=flrg7w0w96g) v Daňkovicích zpestřené neoficiálním přátelákem s DÚ

_So 29.2. 18:00 - **KONEC**_ – Obecní ples

_St 4.3. 19:00 - 20:30_ - trénink na UMT v Litomyšli

_Ne 8.3. 9:00_ - přátelák s Horním Újezdem na UMT v Litomyšli

_Čt 12.3. 17:30-19:30_ - trénink na UMT v Litomyšli

_Ne 15.3. 9:00_ - přátelák s Borovou na UMT v Litomyšli

_Čt 19.3. 17:30-19:30_ - trénink na UMT v Litomyšli

_Ne 22.3._ - první mistrák s Jevíčkem
