---
title: Zatím to jde
date: 2019-09-08T00:00:00.000Z
category: zaci.ml
image: /public/images/novinky/2019-2020/zaci-mladsi.png
h1: Němčice vs Horní Újezd/Morašice
sestava: >-
  Lukáš Němec - Daniel Hanyk, Jakub Kvapil, Ondřej Žďára, Anežka Břeňová, Zuzana
  Famfulíková, Jakub Bartoš, Vojtěch Paťava, Jan Flídr, Jiří Kučera, Martin
  Flídr, Vlastimil Hladík, Matěj Kopecký, Jindřich Mičík
goly: >-
  Jiří Kučera 3, Jakub Bartoš, Jakub Kvapil, Anežka Břeňová, Ondřej Žďára, Jan
  Flídr
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/531F1B/2019/Horní
  Újezd/Morašice/1cca0000-b93e-45ab-a124-6b3616e11277.html
---
V třetím soutěžním utkání v Němčicích bylo k vidění spoustu krásných akcí a gólů. Soupeře jsme přehrávali kombinačně, ale i důrazem, což nebylo v předchozích zápasech pravidlem. Obzvlášť to těší u mladšího ročníku 2008, který se chytil už v prvním utkání. Skóre 22:2 ze tří zápasů je tou nejlepší pozvánkou na příští zápas, který se koná na hřišti v Horním Újezdě v neděli 15.9. v 10.00 hod proti týmu z Borové. Přijďte povzbudit mladé naděje našeho fotbalu.
