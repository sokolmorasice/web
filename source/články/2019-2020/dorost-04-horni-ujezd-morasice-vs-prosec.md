---
title: Dali nám bídu
date: 2019-09-15T00:00:00.000Z
category: dorost
image: /public/images/novinky/2019-2020/dorost.png
h1: Horní Újezd/Morašice vs Proseč
sestava: >-
  Emanuel Hanyk - Pavel Beneš, Jakub Nádvorník, Luděk Bureš (Petr Kazda),
  František Lněnička (Martin Tmej) - Vojtěch Opletal, Tomáš Briol, Radovan
  Flídr, Jiří Šturc - František Bartoš (Jiří Jána), Jan Pechanec (Matěj Roško)
goly: 'Jakub Nádvorník, František Bartoš, Vojtěch Opletal'
zlute: Jakub Nádvorník
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533C2B/2019/Horní
  Újezd/Morašice/d3121cba-ac98-4482-8930-86d398de0fc9.html
---
Do babí neděle k nám zavítalo dorostenstvo, které se jako jedno z mála uživí samo sebou a nedisponuje spoluprací s dalším klubem. Naše střídačka praskala ve švech i díky několika starším žákům, kteří zřejmě nechtějí svůj volný čas v příštích letech trávit v knihovnách, nebo u počítače. 

Oproti minulému zápasu doznala sestava drobných změn. Bohužel došlo ke změně i v pohybu našich hráčů na hřišti. První poločas lehce připomínal utkání v Králíkách. Chyběl nám pohyb hlavně ve středu hřiště a nabídka pro přihrávku nebyla enormní, jako hodně velká. Zároveň jsme měli problém přebírat rychlý přechod Proseče do útočné fáze. Zvláště jejich pravá strana vedená Hledíkem dělala naši obraně velké problémy. Dle vyjádření hostujícího trenéra má prý Hledík problémy s hlavou, škoda že ne taky s nohama a fyzičkou. Výsledkem bylo pět inkasovaných branek v naší síti během půl hodiny. A to ještě tak 2 - 3 velké šance Hanyk zlikvidoval. Naštěstí nám hodil malou udičku hostující Janecký, kterému první půle jaksi nevyšla. Škoda že jen ta první. Nejprve nasměroval zpackaný přímák Bartoše na nohy Nádvorníka, který snížil na 2:1 a 10 minut před přestávkou nestačil na střelu Bartoše, který upravoval na 2:5 z našeho pohledu. Bohužel to byly poslední šedé chvíle hostujícího brankáře. Těsně před pauzou ještě stihl vytáhnout Bartošovou dalekonosnou standartku na rohový kop a šlo se do kabin.

Přestávková promluva pomohla a druhý poločas už více řídily naše kopačky. Když se po pěkné akci prosadil uvnitř vápna Opletal, zdálo se, že by se s výsledkem dalo ještě něco udělat. Jenže stejně tak, jako jsme se probrali my, tak se probral i Janecký. Proti křížné střele Šturce, která minula tyč o pár centimetrů, zasahovat nemusel. Vytáhl se až po pokusu Nádvorníka, kdy vložil reflexivně nohu tečovaného přímého kopu Briola. No a když rukama zalepil i střelu Honzy Pechance, bylo jasné, že body odjedou do chrudimského okresu. Hosté si vypracovali ve druhém poločase 2 šance. Samostatný únik skvěle zastavil Hanyk, který musel po pár minutách včasným výběhem zmenšit střelecký úhel a doufat, že polostřela skončí alespoň na tyči.

Chyby v prvním poločase nás stály body, ale stanou se horší věci. Nejsme žádná kapela, abychom pořád vyhrávali. Nakonec z toho byl odbojovaný a správně heclý zápas, na který snad nebude početná divácká kulisa moc nadávat. Příští týden nás čeká utkání ve Sloupnici, se kterou máme po výsledcích 1:3 a 10:2 v přípravě tuhle bilanci. Uvidíme, jak to dopadne potřetí.
