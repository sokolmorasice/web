---
title: Hlava XXII.
date: 2020-05-14T06:59:25.157Z
category: obecne
image: /public/images/uploads/logo.png
---
Co musí hráči a fanoušci dodržovat na morašickém hřišti od 11. května:

\- důsledně počítat přítomné a vykázat 101. člověka, který by se pokusil do areálu vměstnat

\-dodržovat 2m rozestup mezi hráči a ostatními (tj. hráč např. neoslavuje vstřelený gól známým vběhnutím mezi fanoušky mačkající se těsně za autovou čárou, nýbrž gayským poplácáním v chumlu svých spoluhráčů; fanoušci toužící po exhibicionistickém slalomu by neměli zatáčky mezi hráči příliš "řezat")

\-převléknout se až doma (doporučeno i pro zmíněné exhibicionisty)

\-toalety navštěvovat pouze individuálně, nikoli ve skupinách

Pořadatel musí zajistit nádobu s dezinfekcí dostupnou všem bez rozdílu pohlaví a klubové příslušnosti a dezinfakovat všechny viry, které v areálu zůstanou po ukončení akce.

Pokud se chce někdo řídit zdravým rozumem, nebude mu v tom bráněno, prokáže-li se úředním povolením o jeho používání.