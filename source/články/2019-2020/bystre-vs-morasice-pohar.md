---
title: Všechno na ligu!
date: 2019-08-04T00:00:00.000Z
category: muzi
image: /public/images/zapasy/bystre-morasice.png
h1: Bystré vs Morašice
sestava: >-
  J. Špinar - V. Tměj, M. Chmelík, V. Štancl, V. Novák - B. Válek, M. Klement,
  V. Štěpán, M. Hubáček (J. Vít) - D. Ondráček (J. Kopecký), F. Bureš (M. Ropek)
zlute: 'V. Štancl, V. Novák, V. Štěpán, D. Ondráček'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533A1B/2019/Morašice/2762dcc9-5d4f-4c02-8a4d-f128327cba2e.html
---
Toužebně očekávanou sérii venkovních zápasů jsme začali v Bystrém, kam nás poslal los prvního kola okresního poháru. Vítězství v něm sice zahřeje, i když rozhodně ne klubovou kasu, ale takhle v létě škoda termínu pro přátelák, ve kterém by se mohlo střídat hokejově. Ne že by se nás sešlo bůhvíkolik, ale co kdyby. Do prvních střeleckých pozic se na místním úzkém hřišti dostali po očekávaných zaváháních v domácí rozehrávce Válek a debutant Štěpán, ale jejich obstřely z levé strany mířily příliš mimo. Těžko říct, zda to domácí čekali jako my, nicméně i oni se dostali k šancím lacino přes naši presink naší obrany. Po čtvrt hodině se to dalo dohromady s jednou z mnoha našich nepřesných přihrávek, míč se rychle dostal na vápno a, jak se později ukázalo, nejlepší domácí hráč střelou pod břevno překonal Špinara. Zvýšit úspěšnost zákroků na 50% se našemu gólmanovi povedlo o chvíli později, kdy proti střele z podobné pozice dobře zasáhl nohou. Závěrečná čtvrthodina prvního dějství patřila nám, ale procento zmetků v našich přihrávkách bylo tak velké, že tím zhasla nejedna zajímavá pozice, takže víc než střely Bureše a "tampónka" V. Štancla, které těsně minuly stejný vingl brány,  jsme si nepřipsali.

Možná kdybychom věděli, že za celý druhý poločas (stejně jako za první) nevystřelíme na bránu, ani bychom se nenamáhali ho odehrát. Hned na začátku zakombinoval Vít s Ondráčkem, ale střela prvně jmenovaného k brankáři neprošla, a pak jsme i přes občerstvení sestavy více či méně zralými čtyřicátníky, vytěžili jen velký počet rohů a hlavičku vedle v podání Kopeckého. Na hrot posunutý Chmelík konečně nachytal domácího stopera v příliš sebevědomých výšinách, ale sudí jejich souboj bohužel otočil, nastřelenou ruku ve vápně (podle nových pravidel prostě ruku) asi přehlédl, ale taky nevyloučil Nováka za záchrannou brzdu a na popud pomezního neuznal domácím druhý gól pro domnělý ofsajd. Prostě zápas hodný okresního přeboru, jehož pohárová odnož pro nás končí netypicky brzy. Ale aspoň nebudeme ve zbytku sezóny tříštit síly.
