---
title: Do roka a do dne (skoro)
date: 2020-06-12T21:56:33.025Z
category: muzi
image: /public/images/uploads/logo.png
h1: Morašice - Horní Újezd 3:4 (1:2)
sestava: J. Špinar – J. Kopecký, V. Novák, V. Štancl, D. Vejrych, B. Válek, D.
  Štancl, M. Chmelík, V. Tměj, F. Bureš, M. Hubáček, Z. Skala, M. Ropek, P.
  Vomáčka
goly: B. Válek (D. Štancl), M. Chmelík (Z. Skala), V. Štancl (V. Tměj)
---
15.6.2019 – 26. kolo okresního přeboru

**Morašice – Radiměř 4:1 (2:1)**

Sestava: J. Špinar - V. Novák, M. Ropek (M. Zerzán), V. Štancl, M. Klement - B. Válek, Z. Skala, T. Nádvorník (D. Vejrych), D. Ondráček (Z. Škoda) - M. Chmelík, F. Bureš (M. Janypka)

Góly: V. Štancl, F. Bureš, M. Zerzán, B. Válek

Vzpomíná si někdo?

Po 362 dnech se fanoušci i hráči dočkali premiéry nových kabin, jakož i domácího fotbalového zápasu vůbec. To vyšlo náhodou takhle na den přesně. Po výhře na Dolňákem nam byl soupeřem ten druhý z Újezdů. A byť od začátku za našeho rozvážného dozoru více držel míč, kapituloval jako první, když cca po čtvrt hodině pronikl po lajně D. Štancl a jeho precizní křížnou nabídku na zadní tyč uklidil pod břevno Válek. V půlce půlky přišlo občerstvení, protočení sestavy (aspoň teda u nás) a záhy i vyrovnání. To se V. Štancl přecenil a vyslyšel volání Skaly po křížném balónu, čímž nastartoval smrtící horňácký protiútok zakončený čítankově k protější tyči. O chvíli později zůstal stát Špinar na čáře, Novák hasil na roh, po něm zůstal stát Válek na vápně, čímž nám všem umožnil nevšední podívanou, jakou ten vingl napálený zpoza vápna dozajista byl. A že je to přátelák, a na konci toho bylo hodně, není třeba plkat o zbytku poločasu.

Úvod toho druhého nás zastihl jaksi ztuhlé, z čehož se viditelně vymykal jen čerstvý Vomáčka. Špinar si při míčové, ale jalové převaze hostů zase šáhnul na balón až při výlovu, když zjistil, že autor druhého gólu umí zpoza vápna i pravou. Za nás Válek nedokázal podruhé usměrnit do sítě přihrávku D. Štancla, pak se dvakrát trefil, ale pokaždé si na něho někdo zasedl. Poprvé viděl jeho ruku jenom hlavní rozhodčí, podruhé stál v ofsajdu pouze podle Bureše. Ten si předtím mimochodem dvakrát trefil tyč, a tak zbyl regulérní gól až na Chmelíka, který dostal za vápnem prostor a trefil se přesně k tyči. V. Štancl se trefil hlavou až na čtvrtý pokus z té nejméně jasné pozice, ale to už bohužel pouze snižoval, protože si mezitím připsal úspěšný a mimořádně neprospěšný penaltový zákrok, což o sobě bohužel Špinar po nařízené desítce říct nemohl. Suma sumárum jsme na gól potřebovali moc šancí (ale pozor na termín „nadřeli se“) a zároveň jsme jednoduše dostávali. A protože nově zní rozkaz trenéra dát o gól víc než soupeř, nikoli o gól míň dostat, dají se čekat gólové hody i v neděli od 16:30 proti Pomezí. Přijďte se podívat, zda do obou branek.