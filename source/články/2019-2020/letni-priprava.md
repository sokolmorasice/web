---
title: Letní příprava
date: 2019-08-26T09:47:14.459Z
category: dorost
image: /public/images/uploads/logo.png
---
Po několikaleté odmlce jsme u nás dali opět dohromady družstvo dorostenců. Navíc nebyl důvod trhat fungující spolupráci s Horním Újezdem, a tak začátkem srpna začala letní příprava s dvaceti hráči postpubertálního věku. Co se týče přístupu hráčů, tak zatím převládá spokojenost, protože tréninková účast je více než hojná. Na nedostatek hráčů si stěžovat nemůžeme, přesto se několik kluků z Morašic na fotbal vykašlalo a vůbec nechodí, i když před sezónou slíbili, že se s nimi může počítat. Všem ostatním patří poděkování, včetně oddílu Horního Újezdu za podzimní azyl. 

K tréninkovým jednotkám jsme odehráli dva přátelské zápasy se souklubím Němčice / Sloupnice. V neděli 11.8. jsme zajížděli do Němčic a v očekávání, jak to bude vypadat, jsme nebyli jenom my, ale i soupeř, který byl v úplně stejné situaci. V celkovém pohledu na utkání byli asi lepším týmem domácí, kteří na šance bohaté utkání vyhráli v poměru 3:1. Naším jediným střelcem byl Radovan Flídr, který se trefil z penalty. První přípravný zápas ukázal, že by to nemusela být prvním rokem bída a že bychom v soutěži nemuseli hrát zanďoura nebo druhý housle. Za předvedený výkon není třeba strkat hlavu do písku, nebo se stydět, vzhledem k tomu, že několik kluků se vrátilo po x leté pauze, další jsou v dorostu prvním rokem a Tomáš Briol letos teprve začíná ve starších žácích. 

**Výsledek**: Nemčice/SLoupnice - Horní Újezd/Morašice 3:1 (0:0) 

**Sestava**: Jaromír Loskot, Emanuel Hanyk - Luděk Bureš, Martin Tmej, Pavel Beneš, František Bartoš, Vojtěch Opletal, Jiří Pechanec, Jan Pechanec, Radovan Flídr, František Lnenička, Tomáš Briol, Jiří Šturc 

**Branky**: Radovan Flídr (P) 

O týden později jsme odehráli odvetu v Horním Újezdě a nutno říct, že kdyby se dvojutkání hrálo v rámci ligy mistrů nebo EL, tak bychom hravě postoupili dál. Nevím, kolik měl soupeř změn v sestavě oproti minulému týdnu, ale po celé utkání naši dorostenci jasně dominovali, přehrávali soupeře kombinační hrou a na rozdíl od minulé neděle byla většina šancí zakončena nebo gólově využita. Za hluché pasáže zápasu, kdy se Němčice dostali trochu do hry, můžou trenéři přeskupováním sestavy, aby zjistili kdo kde hrát opravdu nemůže :-) Výsledek 10:2 mluví za vše a to zůstalo ještě několik tutových šancí hluchých. 

**Výsledek**: Horní Újezd/Morašice - Němčice/Sloupnice 10:2 (4:1) 

**Sestava**: Jaromír Loskot, Emanuel Hanyk - Martin Tmej, Petr Kazda, Jan Kazda, Pavel Beneš, František Bartoš, Vojtěch Opletal, Jiří Pechanec, Jan Pechanec, Radovan Flídr, František Lnenička, Tomáš Briol, Jiří Šturc 

**Branky**: Jiří Šturc 4, Jan Pechanec 2, Tomáš Briol, Radovan Flídr, František Bartoš, Emanuel Hanyk 

- - -
