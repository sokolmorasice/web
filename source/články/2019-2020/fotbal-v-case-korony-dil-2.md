---
title: Fotbal v čase korony - díl 2.
date: 2020-04-14T04:49:08.026Z
category: obecne
image: /public/images/uploads/logo.png
---
V [úvodním dílu](/články/2019-2020/fotbal-v-case-korony-dil-1) našeho seriálu jsme navštívili osiřelý areál s novými kabinami, dozvěděli se od trenérů mládeže, jak se (ne)změnil jejich život, že koronavirus připravil děcka o možné, až pravděpodobné, triumfy a že se v oddíle chystaly nějaké oslavy, o kterých nám ale sekretář odmítl cokoliv sdělit. Proto jsme se v druhém díle vypravili do nedalekého Makova za místopředsedou Milanem Chmelíkem.

„My jsme už loni odložili oslavy výročí 120 let od založení našeho Sokola s tím, že to spojíme letos s 50 lety fotbalu. No když to nebude možné teď, jakože nebude, zkusíme to na podzim. Když ani to ne, tak k tomu v příštím roce ještě připojíme 50 let Pepy Kopeckého, to mu udělá radost, klidně se ho zeptejte“ říká a ukazuje nám matadora týmu na společné fotce, kterou vytáhl z peněženky. Na dotaz, jak snáší vynucenou pauzu z pozice hráče už reaguje s většími obavami: „Samozřejmě je to komplikace. Vůbec s manželkou nevíme, jak budeme trávit neděle, protože za dobu našeho manželství jsem na jaře doma zažil maximálně tři.“

„Mně to nevadí, já mám hodně práce a aspoň si nemusím zas natahovat tříslo,“ vmísil se nám do rozhovoru zpoza plotu jeden ze služebně nejstarších hráčů, který si ale kvůli trenérovi nepřál být jmenován. Vzápětí začal freneticky křížit ruce nad hlavou, aby dal znamení „STOP“ blížícímu se zelenému vraku.

Z „auta“ vystoupil kšiltovkovaný muž v brýlích, který pod rouškou viditelně posmutněl, když byl upozorněn, že schůze místního SDH se nekoná. „Celý podzim jsem se těšil na vybírání vstupného, při kterém potkávám spoustu zajímavých lidí, " sdělil nám ke zrušeným zápasům. "Ale víc mi vadí to vycházení jen ve dvou. Já jsem byl zvyklý chodit se svou osudovou láskou zásadně v davu, ale kluci mě varovali, že teď ve dvou by to vypadalo spíš jako sledování,“ bezelstně nám přiznává, ani by si uvědomoval si nástrahy svého stalkingu i jeho veřejného přiznání.

Jak se nucená pauza projeví na finanční situaci oddílu? A jak se daří v karanténě trénovat sám sebe, natož celý tým? Na to nám již ve čtvrtek odpoví hospodář a hrající trenér Martin Ropek.