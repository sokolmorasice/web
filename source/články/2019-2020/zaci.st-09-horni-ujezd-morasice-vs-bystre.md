---
title: Klid před bouří
date: 2019-10-27T00:00:00.000Z
category: zaci.st
image: /public/images/novinky/2019-2020/zaci-starsi.png
h1: Horní Újezd/Morašice vs Bystré
sestava: >-
  Lukáš Němec – Zuzana Famfulíková, Jiří Jána, Jakub Kvapil, Tomáš Briol, Jan
  Vedral, Šimon Kušnír, Šimon Lněnička, Tomáš Huryta, Jan Odehnal, Tereza
  Lněničková, Filip Kopecký, Vojtěch Famfulík
goly: 'Jiří Jána 3, Tomáš Briol'
scortes: >-
  https://scortes.rozpisyzapasu.cz/api/533E2A/2019/Horní
  Újezd/Morašice/34603fea-a156-450f-9da5-bb0d164ab4ed.html
---
V 9. kole jsme na hřišti přivítali soupeře z Bystrého. Hned na úvod zápasu Jána zkompletoval čistý hattick, na který se ani nemusel nijak nadřít. Stačil mu pouze Lafatovský výběr místa ve vápně. Náš cíl byl jednoznačný, přidat více branek a být po zbytek zápasu týmem s míčem na kopačkách. To se ne vždy dařilo a ve 20. minutě se Vedral dopustil nedovoleného zákraku ve vápně a hostující Sabin i přes veškerou snahu Němce pokutový kop proměnil. Obraz hry se razantně změnil, začalo přibývat více nepřesností na obou stranách. Zejména nám pokulhávala rozehrávka od brankáře. Ještě do poločasu proběhlo několik změn v sestavě, což vedlo k mírnému zlepšení našeho herního projevu.

Do druhého poločasu nastoupila opět základní sestava a začala takřka podle představ. Fotbal byl pohledný, soupeře jsme nepustili k výrazné šanci, za to my si jich vypracovali více. Další gólová přišla ve 47. minutě, kdy Odehnal po krásném průniku po lajně našel Briola a ten upravil na 4:1. Ke konci zápasu ubývalo sil na obou stranách a utkání se již dohrávalo bez vyložených šancí. Nemůžeme říct, že výkon byl nějak optimální, ale 3 body máme, čož je důvod ke spokojenosti. Poslední utkání podzimu odehrajeme 3.11. od 10:00 na hřišti v Horním Újezdě, kde budeme dohrávat odložený zápas s taky stoprocentní Trnávkou.  Doufejme, že vidina podzimu bez porážky a přezimování na první příčce je dostatečná motivace pro žáky k nejlepším výkonům.
