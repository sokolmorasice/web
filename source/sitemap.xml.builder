# encoding: utf-8
xml.instruct!
xml.urlset "xmlns" => "http://www.sitemaps.org/schemas/sitemap/0.9" do
  sitemap.resources.each do |page|
    catch :next_page do

      # If all you want the sitemap to cover is normal html pages, use this:
      # throw :next_page unless page.url.end_with?('.html')
      #
      # If you need more detailed control, this is a starting point:
      throw :next_page if page.url.include?('/public/')
      throw :next_page if page.url.start_with?('/layouts/')
      throw :next_page if page.url.start_with?('/admin')
      throw :next_page if page.url.start_with?('/apple-touch-icon')
      throw :next_page if page.url.start_with?('/40')
      throw :next_page if page.url.start_with?('/.')   # .DS_Store, .git etc.
      throw :next_page if page.url.start_with?('apple-touch')
      throw :next_page if page.url.end_with?('.txt')
      throw :next_page if page.url == '/favicon.ico'
      throw :next_page if page.url == '/sitemap.xml'

      frontmatter_date = nil
      frontmatter_date = article_date(page) if defined?(page.data) && page.data['date']
      throw :next_page if frontmatter_date && !frontmatter_date.is_visible

      xml.url do
        xml.loc 'https://sokolmorasice.cz' + page.url.gsub('.html', '')
        date = nil
        date = frontmatter_date.date if frontmatter_date
        date = DateTime.now if page.url == '/' || page.url == '/články/'
        if date
            xml.lastmod date.strftime("%Y-%m-%d")
        end
      end
    end
  end
end
