---
title: Upozornění
type: text
---
Tréninky mládeže od pondělí do středy od 17:00. Tréninky mužů v úterý a ve čtvrtek již na hřišti. 

Třetí přípravné utkání mužů na UMT v Litomyšli v sobotu 8.3. od 11:00 proti Jehnědí. Vyplnění docházky [zde](https://docs.google.com/spreadsheets/d/19JiZ2z0OcaOwEy1E_U_44N_X9VA9zgg_Qm_OB8J1dgU/edit#gid=0).