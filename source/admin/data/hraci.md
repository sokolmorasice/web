---
title: Hráči
type: people
groups:
  - group: Hráči (statistiky k 31.12.2024)
    people:
      - description: 127 zápasů / 44 gólů (celkem 137 / 44)
        name: Bureš Filip
        image: /public/images/uploads/bures.jpg
      - description: 116 zápasů / 1 gól (celkem 292 / 15)
        name: Černohorský Martin
        image: /public/images/uploads/fa479ab9-4ec4-4438-a943-55a7dd9c1940_crop.png
      - description: 65 zápasů / 7 gólů
        name: Frank Michal
        image: /public/images/uploads/aa326f87-dbf6-4035-a91b-5f854722d828_crop.jpg
      - description: 29 zápasů / 9 gólů (celkem 95 / 18)
        name: Huryta Tomáš
        image: /public/images/uploads/huryta.jpeg
      - description: 773 zápasů / 65 gólů (celkem 853 / 85)
        image: /public/images/uploads/chmelik.jpg
        name: Chmelík Milan
      - description: 1080 zápasů / 163 gólů (celkem 1285 / 175)
        image: /public/images/lide/22.jpg
        name: Kopecký Josef
      - description: 154 zápasů / 13 gólů (celkem 355 / 39)
        name: Kubeš Jiří
        image: /public/images/uploads/3bb8996e-f233-4ae5-ac0f-ff4cbd55cf97_crop.png
      - description: 65 zápasů / 13 gólů (celkem 139 / 51)
        name: Lněnička Patrik
        image: /public/images/uploads/ec7146a4-2b7f-4573-98f9-d3348fe9ed93_crop.png
      - description: 95 zápasů / 22 gólů
        image: /public/images/uploads/b85ec7c7-e1df-4a09-ab98-586e7b2e1866_crop.png
        name: Nádvorník Tadeáš
      - description: 289 zápasů / 39 gólů (celkem 352 / 60)
        image: /public/images/lide/35.jpg
        name: Novák Václav
      - description: 122 zápasů / 11 gólů (celkem 130 / 17)
        image: /public/images/uploads/ce512725-f7f1-417e-98cf-f5581df0b5be_crop.png
        name: Ondráček Daniel
      - description: 27 zápasů / 10 gólů
        name: Pirkl Tomáš
        image: /public/images/uploads/pirkl.jpg
      - description: 4 zápasy / 0 gólů
        name: Pishta Yurii
        image: /public/images/uploads/7bbe2c5e-aad2-4f90-80f5-f422de77a6a5_crop.png
      - description: 57 zápasů / 2 góly (celkem 165 / 39)
        name: Pitra Jan
        image: /public/images/uploads/5ef0a576-f2cd-4256-aa6e-ba836d04769f_crop.png
      - description: 880 zápasů / 102 gólů (celkem 1067 / 144)
        image: /public/images/uploads/4af10ceb-003b-4522-8725-f91d26f02395_crop.png
        name: Ropek Martin
      - description: 37 zápasů / 5 gólů
        name: Rusnák Luboš
        image: /public/images/uploads/959567f2-ea12-4005-ab70-33b971f539be_crop.png
      - description: 389 zápasů / 42 gólů
        image: /public/images/lide/51.jpg
        name: Skala Zdeněk
      - description: 95 zápasů / 6 gólů (celkem 128 / 16)
        name: Sokol Michal
        image: /public/images/uploads/sokol.jpg
      - description: 157 zápasů / 0 gólů (celkem 161 / 0)
        image: /public/images/uploads/061a1527-3c8f-4172-92b3-37bfc7edde1d_crop.png
        name: Špinar Jan
      - description: 261 zápasů / 19 gólů (celkem 400 / 38)
        image: /public/images/uploads/5bf03497-5681-4fda-9ace-8e9154950aa2_crop.png
        name: Štancl Dominik
      - description: 359 zápasů / 45 gólů (celkem 538 / 72)
        name: Štancl Václav
        image: /public/images/uploads/stancl.jpg
      - description: 139 zápasů / 5 gólů (celkem 237 / 9)
        image: /public/images/uploads/7bc8ad5a-f4da-41e0-b9e8-27a4c9f9a927_crop.png
        name: Vít Jan
  - group: Vedení mužstva
    people:
      - description: Hrající trenér
        image: /public/images/uploads/stancl.jpg
        name: Štancl Václav
      - description: Vedoucí mužstva
        image: /public/images/uploads/39dbc8a5-14ef-4ac5-8d27-290273dc50ac_crop.png
        name: Severa David
---
