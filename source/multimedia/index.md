---
title: Multimédia
---
## Fotky a videa

* 2002 - [Bourání vagónu](https://drive.google.com/drive/folders/1-ih8g-SM8ogCK_CA_alaeBFe9qn1-NOn)
* 2008 - [Soustředění v Borušově - Penzion Badinka](https://www.youtube.com/watch?v=kzL-bWEi8Fs)  (video)
* 2009 - [Postup do 1.A třídy](https://drive.google.com/drive/folders/1pWGvmNkjPXw8_5L0vvG40uqnIi8pIXFg), [Křéča střílení](https://drive.google.com/drive/folders/16s-4PEhsoQelb6H-jrqsiQDu6cGkFD6L), [Křéča svatba](https://www.youtube.com/watch?v=LbC6E0qkUZA) (video), [Oslavy s fanklubem](https://drive.google.com/drive/folders/1u1VN8Vdg0X_qa0Gz9mEQdRhS1_QeKjDs), [Rozhovor s Petrem Kutou](https://www.youtube.com/watch?v=J68j40tBKAM) (audio), [Kutovic stáž v Espaňolu](https://drive.google.com/drive/folders/1oOc-H8XJILUWwaZVF4ZTQQ8b0HJwQpft), [Ukončení PODZIM a Ozzy 30](https://drive.google.com/drive/folders/1yTxysYfv2ttvtQ4AC6nNvhw5kAY9Rv7Z)
* 2010 - [Soustředění v Daňkovicích](https://drive.google.com/drive/folders/12-EvfnBuN_x_AHYtqIT6k2jZ16LVjMKg) (fotky), [Soustředění v Daňkovicích](https://www.youtube.com/watch?v=flrg7w0w96g) (video), [Ukončení JARO a vyvěšení Kuťáků](https://drive.google.com/drive/folders/1EhZ62Qlsd7PgypjuFdNSJcYxnj1qH6oX), [Křéča+Holub 30](https://drive.google.com/drive/folders/1jUqkAghNa3OaBlHWW-o2Dz-6U0jdU0UX)
* 2011 - [Ukončení JARO](https://drive.google.com/drive/folders/1DqJYB8l6mTejS_RHKPjiw413GN3Tlzc7), [Ukončení PODZIM v pivovaru Černá Hora](https://drive.google.com/drive/folders/17lTbrybaF8ywAGvMN-Lmr6Asdgcs_mwu)
* 2013 - [Turnaj morašických čtvrtí](https://drive.google.com/drive/folders/1HfSUWD96xMjNXoBoBb8fo4QB4IlEQq6N), [Přestavba antukového hřiště](https://drive.google.com/drive/folders/1N1FRZTXdTDRaZDYdvjmAuv5aGGpYItn9)
* 2014 - [Soustředění v Daňkovicích](https://drive.google.com/drive/folders/1cqNMHJqwa1NNJcPXfmZm0WkhDh0hwoPT), [Vybudování tréninkového hřiště](https://drive.google.com/drive/folders/17cVuv6zZENr2Y0dR5_HyrOq-1mCwsWAp), [Turnaj morašických čtvrtí](https://drive.google.com/drive/folders/1tiYMO0mt63AZSiShP6u3I9ktWCJhgGhb), , [Ukončení PODZIM](https://drive.google.com/drive/folders/1VDpr7XAt49bV0omnnvNUIXPmpGwWf9zP), [Silvestrovský fotbálek](https://drive.google.com/drive/folders/1q7fZGHXhW7xBiGddeKv-MuH5mnK9g443)
* 2015 - [Soustředění v Daňkovicích](https://drive.google.com/drive/folders/18FFNekb5bg4EFUsC6bnKS1tswkGuCzSU)
* 2016 - [Vítězství v okresním poháru proti Opatovu](https://drive.google.com/drive/folders/1E39XiTPsCNrZZYmVz_oEgfEh4UoIePVR), [Vítězství dorostu v I. třídě](https://drive.google.com/drive/folders/1JiDfKf2CfUuzEfjuOutXZEsOwE6vM6Pd), [Turnaj morašických čtvrtí](https://drive.google.com/drive/folders/1uVDi_v_iCXYWUgASb8dOdrrCDm19aQe6), [Ukončení PODZIM](https://drive.google.com/drive/folders/1BOBTj4PtX5XocQc1Jhl8jNk8uJrSPZiE), [Silvestrovský fotbálek](https://drive.google.com/drive/folders/1d_mUYyfSKi1Te6d_yMDwI6qWDRnDqso_)
* 2017 - [Soustředění v Daňkovicích](https://drive.google.com/drive/folders/1ExnzjpsBv-7DHnZj_6P2-FrLW4MuJKHq), [Vítězství v okresním poháru proti Janovu](https://drive.google.com/drive/folders/17agJ6VfwsbpK-q2HcflX9HjCLKARrq01), [Turnaj morašických čtvrtí](https://drive.google.com/drive/folders/11dpPLzqiWXdMuLLjOT6V8wie31g8Y453), [Ukončení PODZIM](https://drive.google.com/drive/folders/1xD2pGOCXhnXJYmbJ8lAsfgtwcX9-MrRL)
* 2018 - [Soustředění v Daňkovicích](https://drive.google.com/drive/folders/1lTFZDfWA8kzA-xozYYF0RaCve1djOvFV), [Finále okresního poháru proti Cerekvici](https://drive.google.com/drive/folders/154vVj6WozL4e2g9hxsl370ehqHG1o5CV), [Ukončení JARO na minigolfu Za Sokolovnou](https://drive.google.com/drive/folders/1dVUxTTPJddyxEzur48eXz3alVJpvz-4c), [Ukončení PODZIM](https://drive.google.com/drive/folders/1fY3p68SrtPWh48cKRv3Hlbzp6K5Se315), [Silvestrovský fotbálek](https://drive.google.com/drive/folders/1GNHZ11rcnkIQOWnXJ9i3tSomX0d8NEvN)
* 2019 - [Turnaj morašických čtvrtí](https://drive.google.com/drive/folders/1o342gebnmoJ8tmuPLlfTxiynL9h-WP0L), [Ukončení PODZIM (bowling + Veselka)](https://drive.google.com/folderview?id=1-EqErzCnZmRL9mhRz_0zrqZcVQQmo8j5), [Silvestrovský fotbálek](https://drive.google.com/drive/folders/1-aGolUWvyUdmwuyxkDAOM_oRtqvnMFNs)
* 2020 - [Soustředění v Daňkovicích](https://drive.google.com/folderview?id=1jLYptFdOLl9987iXJVsGm1RuBB1prAHw)
* 2﻿022 - [Soustředění v Daňkovicích](https://drive.google.com/drive/folders/1mb8Eds3PM-vVD9KNxowcWBmIuTQQpNUG)
* [Od začátku po současnost](https://goo.gl/photos/AXUxCMrnC42YnYeD6) (archiv Jana Hendrycha)

## Zápasy

* Unikátní záznamy zápasů Morašice - Živanice (14.8.2011), Morašice - Dolní Újezd (25.9.2011) a Morašice B - Kunčina B (16.10.2011) na našem [YouTube kanálu](https://www.youtube.com/results?search_query=sokol+mora%C5%A1ice)
* [Fotky ze zápasů](https://drive.google.com/drive/folders/1mJJ41AeGEl7aYpuZccgiu1Bx4UEmu-F9)

Za historické fotky patří poděkování především Jaroslavu Neprašovi, za ty novodobější Janu Hendrychovi a Víťovi Touškovi, videa pocházejí z dílny Martina Štancla. Pokud máte nějaké nezveřejněné materiály, kontaktuje [Václava Štancla](/o-klubu/kontakt).