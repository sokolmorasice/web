---
title: Areál
---
Podmínky z dob začátku morašického fotbalu jsou dnes asi obtížně představitelné. Hřiště vzniklo
na orné půdě a bylo často dost podmáčené. V polovině 70. let byla jedna jeho třetina odvodněna
a vedle hřiště bylo vybudováno antukové hřiště s dřevěnými mantinely, které sloužilo pro volejbal
a tenis, v zimě pro bruslení. Původní šatna pro fotbalisty, domácí i hosty, byla přepažená místnost
o velikosti asi 16m2, na provizorní umytí hráčům sloužil venku umístěný delší žlab. Větší pohodlí
poskytlo zakoupení vyřazeného železničního vagónu v roce 1973. Po založení dorosteneckého a později
žákovského družstva byla morašickým JZD zapůjčena obytná buňka, nicméně hygienické zázemí se zlepšilo
až v roce 1984, kdy byly s finanční pomocí obce svépomocně vybudovány nové kabiny. Vagón přesto zůstal
oblíbenou šatnou dorostenců až do roku 2002, kdy byl demontován a na jeho místě byla postavena nová
hospoda, zprvu spadající pod Sokol, nyní již v majetku obce.

![](/public/images/o-klubu/areal-2002.jpg)

Díky „době dotační“ se v posledních letech daří areál modernizovat. V roce 2014 bylo antukové hřiště
předěláno na umělý povrch s širokým využitím, na druhé straně hřiště vznikla místo „větráků“ nová
tréninková plocha. V reakci na zhoršující se stav hřiště, které v konkurenci okresních plácků patřilo
k nejtvrdším a nejhorším, se v roce 2016 podařilo za finančního přispění obce a Pardubického kraje
zhotovit vrt k závlaze fotbalového hřiště. V roce 2017 začala o průběhu zápasu informovat světelná
tabule, v roce 2018 byl zavlažovací systém rozveden pod po povrchem hřiště a automatizován. V roce 2020, kdy si náš fotbal připomíná 50 let od svého založení, se konečně podařilo dotáhnout do konce i rekonstrukci kabin.