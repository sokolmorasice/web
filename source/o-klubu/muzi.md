---
title: Muži
---
Oddíl kopané byl založen v roce 1970, kdy do okresní soutěže vstoupilo družstvo mužů. Mezi zakládající
členy patřili **Zdeněk Krejza** z Višňárů (první kapitán), **Jiří „Plachťák“ Doležal** z Litomyšle (první brankář)
a bratři **Tobkové – Miroslav** (první střelec), **František** (první trenér) a **Ladislav** (první předseda). V roce
1972 vstoupil do oddílu jako organizační pracovník také **Jaroslav Nepraš st.**, díky jehož dlouholeté práci
máme o začátcích klubu i dnes dobrý přehled. 

![](/public/images/o-klubu/muzi-1973.jpg)

Během 70. let se mužstvo propracovalo přes IV. a III. třídu až do okresního přeboru, kde se pravidelně
umisťovalo na předních příčkách. To vyvolalo logický zájem o naše nejlepší hráče a po odchodu řady opor,
včetně do té doby nejlepšího střelce **Františka Ropka**, nastal výsledkový útlum a pád zpět do III. třídy
 Odtud se podařilo postoupit v roce 1991, kdy se začali v „áčku“ naplno prosazovat odchovanci z úspěšných
mládežnických týmů. V okresním přeboru to žádná paráda nebyla, až nově angažovaný trenér **Eduard Portlík**
během jednoho roku posunul tým z desátého místa k prvenství zaručující premiérový postup do I. B třídy.
Když v roce 2002 nově vznikly kraje, díky umístění na předních příčkách čekal i naše muže posun do nově
vytvořené I.A třídy, kde o dva roky později dosáhli pod vedením **Františka Jandáčka** na historické 3. místo. Během dalších átřídních sezón
se trenérské taktovky ujal nejlepší střelec morašické historie **Petr Kuta**, kterou s abdikačními přestávkami
držel přes deset let. Během nich zažil dva pády z A. třídy následované okamžitým návratem, mezi nimiž oslavil
svůj tisící start v morašickém dresu **Josef Kopecký**. Druhý návrat ze 7.místa B. třídy předznamenal dvě
utrápené sezóny během kterých muži po 18 letech spadli až do okresního přeboru. Svou roli v tom sehrála
i tehdejší absence dorosteneckého družstva a omezené možnosti při omlazování kádru. První roky v okrese
tento fakt nijak nezlepšily, nicméně k umístěním v první pětce soutěže se podařilo zaplnit klubovou
vitrínu i fanouškovskou duši dvěma vítězstvími v okresním fotbalovém poháru. 

V roce 1995 odehrálo první zápasy i B mužstvo, které ale střídavě přihlašovalo a odhlašovalo soutěže
a místo toho, aby sloužilo jako zásobárna A-týmu, potřebovalo spíše pomoc „áčkařů“ a dorostenců, aby
spoustu utkání vůbec odehrálo.  Nejdelším souvislým obdobím byly 4 odehrané sezóny v kuse na startu
nového tisíciletí, během nichž nejlepší střelec řídké historie „béčka“ **Tomáš Holomek** pomohl svými góly
vyhrát pralesní soutěž. I zatím poslední 3letý pokus **Kamila Vokase** narazil na problém současné doby -vesnickému fotbalu postupně holduje čím dál méně lidí i týmů.