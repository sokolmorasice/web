---
title: Kontakty
---
## Výkonný výbor

* Dana Krejsová, předsedkyně
* Milan Chmelík, místopředseda - tel. **731 602 097**, e-mail: **chmelikovi@lit.cz**
* Václav Štancl, sekretář - tel. **737 563 051**, e-mail: **stancl.v@seznam.cz**
* Vojtěch Andrle, člen
* Jiří Famfulík, člen
* Milan Klement, člen
* Miloš Kopecký, člen
* Michal Kopecký st., člen
* František Ropek, člen
* Dominik Štancl, člen
* Martin Štancl, člen

## Revizní komise

* Zuzana Famfulíková, předsedkyně
* Lenka Kuřová, členka
* Václav Novák, člen

## Obec

* Pavlína Jarešová, starostka - email: **starostka@morasice.cz**
* **[www.morasice.cz](https://www.morasice.cz/)**