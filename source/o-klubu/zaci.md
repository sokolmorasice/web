---
title: Źáci
---
Družstvo žáků bylo založeno v roce 1981 a jeho trenérem byl tehdejší hráč A-mužstva
Jan Klofanda, který v této dvojroli vydržel dlouhé roky. Po prvních oťukávacích sezónách,
v nichž nastřádal dosud nepřekonané množství gólů v této kategorii **Josef Štancl st.**,
přišlo úspěšné desetileté období, během něhož se umisťovali pravidelně na nejvyšších
příčkách okresní soutěže, z toho čtyřikrát na stupni nejvyšším. Výsledky se paradoxně
změnily k horšímu v době, když začal góly střílet **Vojtěch Štěpán**, náš nejúspěšnější odchovanec,
který to přes Litomyšl dotáhl až do nejvyšší soutěže, kde nastřádal za Olomouc, Jablonec,
Hradec Králové, Slavii a Ostravu na 200 ligových startů a mj. i hattrick do sítě Slovácka v roce 2012.
Počátkem nového tisíciletí se žáků ujal **Zbyněk Sopoušek** s **Josefem Kopeckým**, kteří s nimi během více než deseti let zažili jak historický postup do krajské soutěže v roce 2004, tak okamžitý pád zpět.
Na další přední umístění v okresní soutěži si žáci museli počkat až doku 2012, kdy obsadili druhé místo. Hned následující sezónu ale kvůli „děravým“ ročníkům odehráli pouze formou přáteláků a následující roky byly spíše hrou o přežití, o čemž svědčí 3 kalendářní roky bez jediné výhry. Díky poctivé práci s elévky se naštěstí podařilo žáky doplnit a přečkat nejhorší a po spojení s Horním Újezdem jsme absolvujeme okresní soutěže v kategorii starších i mladších žáků. Ve dvou nedokončených covidových ročnících patřily k nejlepším na okrese, starší dokonce dva roky po sobě neokusily nic než výhru, ale titul přeborníků jim formálně nebyl dopřán.