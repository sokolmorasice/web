---
title: Přípravka
---

Nejmladší kategorie u nás funguje od roku 2004, a to především díky **Jiřímu Famfulíkovi**,
který ji trénuje od samého počátku. To, že jsou mezi dnešními hráči A-týmu i tací, co
prošli jeho rukama, svědčí o významu jeho dobře odváděné práci lépe než výsledky, které
jsou v této kategorii hodně závislé na vyčnívajících hráčích. V roce 2014 jsme elévky
spojili s Dolním Újezdem, od následující sezóny funguje souklubí s Horním Újezdem, které
umožnilo rozdělit naše nejmladší fotbalisty do dvou věkových kategorií. V té mladší vytvořil
v roce 2018 **Daniel Válek** nový rekord našeho oddílu, když za jeden rok nastřílel 112 gólů.
