---
title: Dorost
---

Dorostenecké družstvo bylo založeno v roce 1975, a přestože k prvnímu mistrovskému zápasu nastoupilo pouze
s 8 hráči a nafasovalo v něm 8 gólů, hned v první sezóně postoupilo ze III. třídy do okresního přeboru.
Z něho po deseti letech střídavě úspěšných výsledků spadlo, aby během následujících dvou sezón vyhrálo
jak III. třídu, tak okresní přebor a jako první družstvo našeho oddílu postoupilo na dva roky do krajské
soutěže. Delší kapitolu v nich napsalo až od v druhé polovině 90.let, kam spadá do té doby nejlepší
umístění – 5. místo v A třídě. To se po dalším lavírování mezi okresem a krajem podařilo vyrovnat ještě
v roce 2007 týmu pod vedením **Františka Ropka** a **Josefa Kopeckého** spoléhajícímu na historicky nejlepšího
střelce dorostu – **Petra Vomáčku**. V roce 2010 dorostenci místo sestupu přerušili činnost na tři roky.
Nedostatek lidí se poté podařilo vyřešit spojením této kategorie s Cerekvicí nad Loučnou a posléze s Horním Újezdem.
Toto souklubí přineslo v roce 2016 historické prvenství v I. třídě pardubického kraje, tým **Stanislava Němečka**
ztratil body pouze za 2 remízy, ale pro nedostatek hráčů i trenérů nebyl příští ročník přihlášen a hráči se
rozprchli po okolních větších klubech. Obnovit tuto spolupráci se podařilo až před letošní sezónou.
