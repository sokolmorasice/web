require 'middleman-core'
require 'yaml'

class BlogExtension < Middleman::Extension
    option :latest, '20XX-20XY', 'Latest season'
    option :previous, '20XX-20XY', 'Previous season (used when latest season has not too many articles)'
    option :minSize, 10, 'Min text length of visible article (empty article is hidden)'

    def initialize(app, options_hash={}, &block)
        super
    end

    def manipulate_resource_list(resources)
        resources.each do |resource|
            if resource.source_file.end_with?(".md") && !resource.source_file.end_with?(".html.md")
                resource.destination_path << '.html'
            end
        end
        resources.select {
            |article| 
                File.dirname(article.path) != "články/#{options.latest}" ||
                File.basename(article.path) == 'index.html' ||
                (article.render({layout: false}).length >= options.minSize && BlogExtension.article_date(article).is_visible)
        }
    end

    def self.article_date(article)
      stringDate = "#{article.data.date}"
      if stringDate.include? ":"
          date = DateTime.strptime(stringDate, '%Y-%m-%d %H:%M')
          OpenStruct.new({
              date: date,
              is_visible: date.to_time <= Time.now,
          })
      else
          date = Date.strptime(stringDate)
          OpenStruct.new({
              date: date,
              is_visible: date <= Date.today,
          })
      end
    end

    helpers do

      def latest_articles(count)
        opts = extensions[:blog].options
        articles_in(opts.latest).concat(articles_in(opts.previous)).slice(0, count)
      end

      def articles_in(season)
        opts = extensions[:blog].options
        files_in("články/#{season}")
            .select { |article| File.basename(article.path) != 'index.html' }
            .map { |article| build_article(article) }
            .sort_by { |article| article.date.date.to_time.to_i }.reverse
            .select { |article| article.content.length >= opts.minSize && article.date.is_visible }
      end

      def cms_articles()
        opts = extensions[:blog].options
        # can't use articles_in, because it contains only published articles
        Dir.glob("source/články/#{opts.latest}/*.md")
            .map {
                |path|
                    yaml = OpenStruct.new(YAML.load_file(path))
                    date = article_date(OpenStruct.new({ data: yaml }))
                    OpenStruct.new({
                        path: path,
                        filename: File.basename(path, '.md'),
                        content: File.read(path),
                        data: yaml,
                        date: date,
                        is_published: date.is_visible && !(yaml.title.include? "PREJMENOVAT"),
                    })
            }
            .sort_by { |article| article.date.date.to_time.to_i }
      end

      def build_article(article)
        OpenStruct.new({
            data: article.data,
            path: article.path,
            content: article.render({layout: false}),
            date: article_date(article),
            url: web_link(article.url),
        })
      end

      def article_date(article)
        BlogExtension.article_date(article)
      end

      def article_heading file
        if file.data.h1 && !file.data.h1.empty?
            file.data.h1
        else
            File.basename(file.path, '.html')
                .gsub('-', ' ')
                .titleize
                .gsub('Vs', 'vs')
                .gsub('Nad', 'nad')
                .gsub('Pod', 'pod')
                .gsub('Zimni Priprava', '<em>Zimní příprava:</em>')
                .gsub('Zimní Priprava', '<em>Zimní příprava:</em>')
        end
      end

      def web_link page
        html_link = if page.end_with? ".html" then page else "#{page}.html" end
        if page.end_with? "/"
            html_link = page
        end
        if !html_link.start_with? "/"
            html_link = "/#{html_link}"
        end
        if build?
            html_link.gsub('.html', '')
        else
            html_link
        end
      end

      def internal_editable_page page
        files_in("admin/data")
            .select { |resource| File.basename(resource.path).start_with? page }
            .map { |resource| resource.render({layout: false}) }
            .first
      end

      def internal_editable_people page
        files_in("admin/data")
            .select { |resource| File.basename(resource.path).start_with? page }
            .first
            .data.groups
      end

      def internal_editable_pripravka page
        files_in("admin/data/pripravka")
            .select { |resource| File.basename(resource.path).start_with? page }
            .first
            .data
      end

      def files_in dir
        sitemap.resources.select do |resource|
          File.dirname(resource.path) == dir
        end
      end

      def nav_active paths
        result = paths.select { |path| current_page.path.start_with?(path) }
        if result.size > 0
            "class='selected'"
        end
      end

      def nav_breadcrumbs
        link = ""
        current_page.path.split("/")
            .select {
                |path|
                    path != 'index.html' &&
                    !(current_page.path.start_with?("články") && path.end_with?(".html"))
            }
            .map {
                |path| 
                    link = "#{link}/#{path}"
                    OpenStruct.new({
                        link: if link.end_with?(".html") then web_link(link) else link end,
                        title: path.gsub('-', ' ').gsub('.html', '')
                    })
            }
      end

      def nav_neighbours article
        season = articles_in article.path.split('/')[1]
        index = season.index { |a| a.path == article.path }
        neighbours = OpenStruct.new({
            prev: if index && index > 0 then season[index - 1] else false end,
            next: if index then season[index + 1] || false else false end
        })
      end
    end

end

::Middleman::Extensions.register(:blog, BlogExtension)
