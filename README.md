# Sokol Morašice

## Editace

**!! https://sokolmorasice.cz/admin/ !!**

Případně ručně v Gitlabu přes tlačítko `Edit` nebo `Web IDE`.

* Nastavení
    * [Služby](/source/admin/data/sluzby.html.md)
    * [Horní zpráva](/source/admin/data/upozorneni.html.md) -  prázdný obsah, pokud se zpráva nemá zobrazovat _(neodstraňovat soubor!)_
* [Články](/source/články/2017-2018/polička-b-vs-morašice.md)
    * mezi `---` se editují základní údaje o článku (titulek, obrázek...)
        * `title` - titulek článku
        * `h1` - název zápasu, když není vyplněn, tak se použije `title`
        * `date` - datum ve formátu `rok-měsíc-den`, např. 1.června 2018 je `2018-06-01`
        * `image` - odkaz na obrázek u článku _(případně může být vložen přímo do textu pomocí `<img>`)_
        * `scortes` - předgenerovaný odkaz pro zobrazení výsledku z IS FAČR
        * `sestava`, `goly`, `zlute`, `cervene` - statistiky zápasu, které se zobrazí pod článkem _(spojí se s údaji ze Scortes)_

_Nejistota? Podívat se do [historie, jak se jindy editoval článek](https://gitlab.com/sokolmorasice/web/commits/master)._

---

## Development

* [netlify](https://coderwall.com/p/e8ms-q)

```bash
git clone git@gitlab.com:sokolmorasice/web.git
cd web

# local development if correct ruby version is installed
bundle install
bundle exec middleman

# docker development
docker-compose up --build
docker exec -it sokolmorasice-middleman bash
    /app/sokolmorasice# bundle exec middleman build
```
